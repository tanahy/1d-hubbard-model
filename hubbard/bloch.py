import numpy as np
from numpy import pi
from scipy.linalg import expm
from matplotlib import cm
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
from matplotlib.ticker import StrMethodFormatter
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage,
                                  AnnotationBbox)
from matplotlib.patches import ConnectionPatch, Ellipse, Circle
from matplotlib.lines import Line2D
import matplotlib.animation as animation
from matplotlib.patches import FancyArrowPatch

from mpl_toolkits.mplot3d import proj3d


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        super().__init__((0, 0), (0, 0),
                mutation_scale = 5,
                #lw = 1.5,
                arrowstyle = '-|>',
                *args, **kwargs)

        self._verts3d = xs, ys, zs

    def do_3d_projection(self, renderer=None):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, self.axes.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))

        return np.min(zs)

'''
    def draw(self):#, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, self.axes.M)

        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
        #FancyArrowPatch.draw(self, renderer)
        FancyArrowPatch.draw(self)
'''

class Bloch_Sphere:
    def __init__(self, r = 1, alpha = .5, cmap = None, ax = None, draw_axes = True, 
                **kwargs):
        if ax is None:
            self.fig = plt.figure()
            self.ax  = self.fig.add_subplot(projection='3d', azim = 45)
        else:
            self.fig = None
            self.ax = ax
        self.ax.set_box_aspect((1,1,1))
        self.ax.set_axis_off()   
        self.r = r
        
        if cmap is None:
            #cmap = cm.Reds
            cmap = cm.viridis
        else:
            self.cmap = cmap

        self.cmap = cmap(np.arange(cmap.N))
        self.cmap[:,-1] = np.linspace(0, 1, cmap.N)
        #self.cmap[:,-1] = np.logspace(-2, 0, cmap.N)
        self.cmap = ListedColormap(self.cmap)
    
        N = 50#100
        phi = np.linspace(0, np.pi, N)
        theta = np.linspace(0, 2*np.pi, N + 1)

        x = r * np.outer(np.cos(theta), np.sin(phi))
        y = r * np.outer(np.sin(theta), np.sin(phi))
        z = r * np.outer(np.ones(np.size(theta)), np.cos(phi))

        self.sphere = self.ax.plot_surface(x, y, z, alpha = alpha, color = 'grey')

        self.sphere_lines = []
        for i in range(0, N, N//10):
            ln, = ax.plot(x[i,:], y[i,:], z[i,:], alpha = alpha, linewidth = 0.3, color = 'grey')
            self.sphere_lines.append(ln)
        for i in range(0, N, N//10):
            ln, = ax.plot(x[:,i], y[:,i], z[:,i], alpha = alpha, linewidth = 0.3, color = 'grey')
            self.sphere_lines.append(ln)
        '''
        self.ax.plot_wireframe(x, y, z, 
                rcount = 30, 
                ccount = 10,
                alpha = alpha,
                linewidth = 0.3,
                color = 'grey')
        '''

        if draw_axes:
            if 'axes_style' in kwargs:
                axes_style = kwargs['axes_style']
            else:
                axes_style = dict()
            self.draw_axes(**axes_style)
        
        self.im = None

    def draw_axes(self, margin = .2, span = .05, color = ('red', 'blue', 'green'), lw=1.5, **kwargs):
        d = self.r + span 
        p = self.r + span + margin
        self.ax.text(p,0,0, '$ S_x $', va='center', ha='center', zorder = 5, **kwargs)
        self.ax.text(0,p,0, '$ S_y $', va='center', ha='center', zorder = 5, **kwargs)
        self.ax.text(0,0,p, '$ S_z $', va='center', ha='center', zorder = 5, **kwargs)
        #self.ax.text(0,0,p,  r'$\| \uparrow   \rangle$', ha = 'center')
        #self.ax.text(0,0,-p, r'$\| \downarrow \rangle$', ha = 'center')
        if type(color) is str:
            color = [color] * 3

        if 'fontsize' in kwargs.keys():
            kwargs.pop('fontsize')
        x_arrow = Arrow3D((0,d),(0,0),(0,0), color=color[0], lw = lw, **kwargs)
        y_arrow = Arrow3D((0,0),(0,d),(0,0), color=color[1], lw = lw, **kwargs)
        z_arrow = Arrow3D((0,0),(0,0),(0,d), color=color[2], lw = lw, **kwargs)

        self.ax.add_artist(x_arrow)
        self.ax.add_artist(y_arrow)
        self.ax.add_artist(z_arrow)

    def plot_equator(self, axis = 'z'):
        theta = np.linspace(0, 2*np.pi, 100)
        self.ax.plot(self.r * np.cos(theta), self.r * np.sin(theta),
                    zs = 0, zdir = axis, lw=1, color='black')

    def plot_linear_move(self, phi=(0,np.pi/2), theta=(0,0)):
        theta = np.linspace(theta[0], theta[1], 100)
        phi = np.linspace(phi[0], phi[1], 100)
        x = self.r * np.sin(phi) * np.cos(theta)
        y = self.r * np.sin(phi) * np.sin(theta)
        z = self.r * np.cos(phi)
        self.ax.plot(x,y,z, lw=1, ls='dashed', color='black')

    def plot_surface(self, data, clear = False, *args, **kwargs):
        if clear and self.im is not None:
            self.im.remove()
        size = data.shape 
        theta = np.linspace(0, np.pi, size[0])
        phi = np.linspace(-np.pi, np.pi, size[1])
        x = self.r * np.outer(np.cos(phi), np.sin(theta))
        y = self.r * np.outer(np.sin(phi), np.sin(theta))
        z = self.r * np.outer(np.ones(np.size(phi)), np.cos(theta))
        self.im = self.ax.plot_surface(x, y, z, 
                rcount =size[0], ccount = size[1],
                #cstride = 1, rstride = 1,
                #alpha = .5,
                facecolors=self.cmap(data.T),
                linewidth=0,
                #antialiased=False, 
                shade=False,
                *args,
                **kwargs
                )

        return self.im
