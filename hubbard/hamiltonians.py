import numpy as np
from scipy.special import zeta
from scipy import sparse

class HBoseMixin:

    def H_0_periodic(self,J=1.):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for i in range(self.M):
                j_range = [i-1,i+1 if i < self.M-1 else 0]
                if j_range[0] % self.M == j_range[1]:
                    j_range = [j_range[0]]
                for j in j_range: 
                    if v[j] >= 1:
                        u = list(v)
                        u[j] -= 1
                        u[i] += 1
                        H_ij = - J * np.sqrt(u[i] * v[j])
                        r = self.T.get(tuple(u), None)
                        if r is not None:
                            data.append(H_ij)
                            row.append(r)
                            col.append(k)

        return sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)

    def H_0_open(self,J=1.):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for i in range(self.M):
                j_range = []
                if i <= self.M - 2:
                    j_range.append(i+1)
                if i >= 1:
                    j_range.append(i-1)
                for j in j_range: 
                    if v[j] >= 1:
                        u = list(v)
                        u[j] -= 1
                        u[i] += 1
                        H_ij = - J * np.sqrt(u[i] * v[j])
                        r = self.T.get(tuple(u), None)
                        if r is not None:
                            data.append(H_ij)
                            row.append(r)
                            col.append(k)

        return sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)

    def H_0_2D_periodic(self,J=1.):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for i in range(self.M):
                j_range = [self.M_d[0] * (i // self.M_d[0]) + (i+1) % self.M_d[0], 
                           (i+self.M_d[0]) % self.M]
                for j in j_range: 
                    if v[j] >= 1:
                        u = list(v)
                        u[j] -= 1
                        u[i] += 1
                        H_ij = - J * np.sqrt(u[i] * v[j])
                        r = self.T.get(tuple(u), None)
                        if r is not None:
                            data.append(H_ij)
                            row.append(r)
                            col.append(k)

        H_0 = sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)
        
        return H_0 + H_0.T.conj()


    def H_0_2D_open(self,J=1.):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for i in range(self.M):
                j_range = []
                if (i+1) % self.M_d[0] != 0:
                    j_range.append(i+1)
                if i + self.M_d[0] < self.M:
                    j_range.append(i+self.M_d[0])
                for j in j_range: 
                    if v[j] >= 1:
                        u = list(v)
                        u[j] -= 1
                        u[i] += 1
                        H_ij = - J * np.sqrt(u[i] * v[j])
                        r = self.T.get(tuple(u), None)
                        if r is not None:
                            data.append(H_ij)
                            row.append(r)
                            col.append(k)

        H_0 = sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)
        
        return H_0 + H_0.T.conj()

    def H_int(self, U=1.):
        data = np.empty(self.D)
        for i in range(self.D):
            data[i] = U/2*sum([ni*(ni-1) for ni in self.A[i]])
        
        return sparse.dia_matrix((data, 0), shape=(self.D, self.D), dtype=np.complex128).tocsc()

    def H_dimers(self,J=1.):
        L = self.M-4 if self.bc == 'open' else self.M
        assert L >= 4
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for i in range(L):
                i = [(i + j) % self.M for j in range(4)]
                for _ in range(2): 
                    if v[i[1]] >= 1:
                        u = list(v)
                        u[i[1]] -= 1
                        u[i[2]] += 1
                        H_ij = - J * np.sqrt(u[i[2]] * v[i[1]]) 
                        H_ij *= (1 - u[i[0]]) * (1 - u[i[3]])
                        r = self.T.get(tuple(u), None)
                        if r is not None:
                            data.append(H_ij)
                            row.append(r)
                            col.append(k)
                    i = i[::-1]

        return sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)
  
    def H_dip(self, V=1, cutoff = None):
        if not hasattr(self, 'N_j'):
            self.N_j_calc()
        if cutoff is None:
            cutoff = self.M
        H = 0
        if self.bc == 'open':
            for j in range(self.M):
                for k in range(j+1, self.M):
                    d = abs(j-k)
                    if d <= cutoff:
                        H += V/d**3 * self.N_j[j] * self.N_j[k]
        else:
            for j in range(self.M):
                for k in range(self.M):
                    d = min(abs(j-k), self.M-abs(j-k))
                    if d != 0:
                        C = V/d**3
                        C *= .5 #?
                        H += C * self.N_j[j] * self.N_j[k]
          
        return H                 

    def H_dip_2D_open(self, V=1, s = [0,0,1], cutoff = None):
        s_j = s_k = np.array(s)
        if not hasattr(self, 'N_j'):
            self.N_j_calc()
        H = 0
        for j in range(self.M):
            x_j = j  % self.M_d[0]
            y_j = j // self.M_d[0]
            for k in range(j+1, self.M):
                x_k = k  % self.M_d[0]
                y_k = k // self.M_d[0]
                r = (x_k-x_j, y_k-y_j, 0) 
                if cutoff is None or np.linalg.norm(r) <= cutoff:
                    coeff = (-V/np.linalg.norm(r)**3 
                             * (3 * np.dot(s_j, r) * np.dot(s_k, r)/np.linalg.norm(r)**2 - np.dot(s_j, s_k))
                             )
                    H += coeff * self.N_j[j] * self.N_j[k]
         
        return H                 




class HFermiMixin:
    def H_0_periodic(self,J=1.):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for i in range(self.M):
                j_range = [(i-1) % self.M, (i+1) % self.M]
                for next_site,j in enumerate(j_range): 
                    for s in (0,self.M):
                        if v[s+j] == 1 :
                            u = list(v)
                            op_permutation = (-1)**sum(v[s:s+j])
                            u[s+j] -= 1
                            if u[s+i] == 0:
                                op_permutation *= (-1)**sum(u[s:s+i])
                                u[s+i] += 1
                                H_ij = - J * np.sqrt(u[s+i] * v[s+j]) * op_permutation
                                
                                w = tuple(u)
                                if w in self.T:
                                    r = self.T[w]
                                    data.append(H_ij)
                                    row.append(r)
                                    col.append(k)

        return sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)
    
    def H_0_antiperiodic(self,J=1.):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for i in range(self.M):
                j_range = [(i-1) % self.M, (i+1) % self.M]
                for next_site,j in enumerate(j_range): 
                    for s in (0,self.M):
                        if v[s+j] == 1 :
                            u = list(v)
                            op_permutation = (-1)**sum(v[s:s+j])
                            u[s+j] -= 1
                            if u[s+i] == 0:
                                op_permutation *= (-1)**sum(u[s:s+i])
                                u[s+i] += 1
                                H_ij = - J * np.sqrt(u[s+i] * v[s+j]) * op_permutation
                                
                                if (i == self.M-1 and next_site) or (i == 0 and not next_site):
                                    H_ij *= -1

                                w = tuple(u)
                                if w in self.T:
                                    r = self.T[w]
                                    data.append(H_ij)
                                    row.append(r)
                                    col.append(k)

        return sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)
    
    def H_0_open(self,J=1.):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for s in (0,self.M):
                for j in range(self.M-1):
                        if v[s+j+1] == 1 :
                            u = list(v)
                            op_permutation = (-1)**sum(v[s:s+j+1])
                            u[s+j+1] -= 1
                            if u[s+j] == 0:
                                op_permutation *= (-1)**sum(u[s:s+j])
                                u[s+j] += 1
                                H_j_jplus1 = - J * np.sqrt(u[s+j] * v[s+j+1]) * op_permutation
                                w = tuple(u)
                                if w in self.T:
                                    r = self.T[tuple(u)]
                                    data.append(H_j_jplus1)
                                    row.append(r)
                                    col.append(k)
                for j in range(1,self.M):
                        if v[s+j-1] == 1 :
                            u = list(v)
                            op_permutation = (-1)**sum(v[s:s+j-1])
                            u[s+j-1] -= 1
                            if u[s+j] == 0:
                                op_permutation *= (-1)**sum(u[s:s+j])
                                u[s+j] += 1
                                H_j_jminus1 = - J * np.sqrt(u[s+j] * v[s+j-1]) * op_permutation
                                
                                w = tuple(u)
                                if w in self.T:
                                    r = self.T[tuple(u)]
                                    data.append(H_j_jminus1)
                                    row.append(r)
                                    col.append(k)

        return sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)

    def H_int(self, U=1.):
        data = np.empty(self.D)
        for i in range(self.D):
            data[i] = U*sum([self.A[i][j]*self.A[i][j+self.M] for j in range(self.M)])
        
        return sparse.dia_matrix((data, 0), shape=(self.D, self.D), dtype=np.complex128).tocsc()
   
    def H_0_rot(self,J=1.):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for i in range(self.M):
                #j_range = [i-1 if i > 0 else self.M-1, i+1 if i < self.M-1 else 0]
                #j_range = [(i-1) % self.M, (i+1) % self.M]
                for si in (self.M, 0):
                    for sj in (self.M, 0):
                        if si == sj:
                            phase_factor = 1 + np.exp(-1j*self.phi)
                        else:
                            phase_factor = 1 - np.exp(-1j*self.phi)
                        
                        for next_site in (True, False):
                            if next_site:
                                j = (i+1) % self.M
                                if self.bc == 'open' and i == self.M-1:
                                    continue
                            else:
                                j = (i-1) % self.M
                                phase_factor = np.conj(phase_factor)
                                if self.bc == 'open' and i == 0:
                                    continue

                            u = list(v)
                            if u[sj+j] == 1:
                                op_permutation = (-1)**sum(v[sj:sj+j])
                                u[sj+j] -= 1
                                if u[si+i] == 0:
                                    op_permutation *= (-1)**sum(u[si:si+i])
                                    u[si+i] += 1
                                    H_ij = - J/2 * phase_factor * np.sqrt(u[si+i] * v[sj+j]) * op_permutation
                                    r = self.T[tuple(u)]
                                    data.append(H_ij)
                                    row.append(r)
                                    col.append(k)

        return sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)
    
    def H_0_rot_open(self,J=1.):
        row, col, data = [], [], []
        phase_factors   = np.exp(1j*self.phi*self.alpha), np.exp(-1j*self.phi*(1-self.alpha))
        for k in range(self.D):
            v = self.A[k]
            for j in range(self.M-1):
                for s in (self.M, 0):
                    if v[s+j+1] == 1:
                        u = list(v)
                        op_permutation = (-1)**sum(v[s:s+j+1])
                        u[s+j+1] -= 1
                        if u[s+j] == 0:
                            op_permutation *= (-1)**sum(u[s:s+j])
                            u[s+j] += 1
                            if s == 0:
                                phase_factor = phase_factors[0]
                            else:
                                phase_factor = phase_factors[1]
                            H_j_jplus1 = - J * phase_factor * np.sqrt(u[s+j] * v[s+j+1]) * op_permutation
                            r = self.T[tuple(u)]
                            data.append(H_j_jplus1)
                            row.append(r)
                            col.append(k)
            for j in range(1,self.M):
                for s in (self.M, 0):
                    if v[s+j-1] == 1:
                        u = list(v)
                        op_permutation = (-1)**sum(v[s:s+j-1])
                        u[s+j-1] -= 1
                        if u[s+j] == 0:
                            op_permutation *= (-1)**sum(u[s:s+j])
                            u[s+j] += 1
                            if s == 0:
                                phase_factor = np.conj(phase_factors[0])
                            else:
                                phase_factor = np.conj(phase_factors[1])
                            H_j_jminus1 = - J * phase_factor * np.sqrt(u[s+j] * v[s+j-1]) * op_permutation
                            r = self.T[tuple(u)]
                            data.append(H_j_jminus1)
                            row.append(r)
                            col.append(k)

        return sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)

    def H_L(self, Omega = 1, phi = 0):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for j in range(self.M):
                for s in (self.M, 0):
                    if s == self.M:
                        phase_factor = np.exp(1j*phi*(j+1))
                    else:
                        phase_factor = np.exp(-1j*phi*(j+1))
                        
                    if v[j+s] == 1:
                        u = list(v)
                        op_permutation = (-1)**sum(v[s:s+j])
                        u[j+s] -= 1
                        if u[j+s-self.M] == 0:
                            op_permutation *= (-1)**sum(v[s-self.M:j+s-self.M])
                            u[j+s-self.M] += 1
                            H_Lj = Omega / 2 * phase_factor * op_permutation
                            r = self.T[tuple(u)]
                            data.append(H_Lj)
                            row.append(r)
                            col.append(k)
                            
        return sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)
 

class HBose2cMixin:
    
    def H_0(self, J = 1):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for i in range(self.M):
                j_range = [(i-1), (i+1)]
                for s in (0,self.M):
                    for j in j_range: 
                        if (j >= self.M or j < 0) and self.bc == 'open':
                            continue
                        j = j % self.M 
                        if v[s+j] >= 1 :
                            u = list(v)
                            u[s+j] -= 1
                            u[s+i] += 1
                            H_ij = - J * np.sqrt(u[s+i] * v[s+j])
                                
                            r = self.T.get(tuple(u), None)
                            if r is not None:
                                data.append(H_ij)
                                row.append(r)
                                col.append(k)

        return sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)

    def H_int(self, U = (1,1,1)):
        data = []
        for k in range(self.D):
            v = self.A[k]
            U_ii = 0
            for i in range(self.M):
                U_ii += U[0] / 2 * v[i] * (v[i] - 1)
                U_ii += U[1] / 2 * v[i+self.M] * (v[i+self.M] - 1)
                U_ii += U[2] * v[i] * v[i+self.M]
            data.append(U_ii)
        
        return sparse.dia_matrix((data, 0), shape=(self.D, self.D), dtype=np.complex128).tocsc()

class HFockSpinMixin:
    def V_harmonic(self, m=1., w=1.):
        data = np.empty(self.D)
        for i in range(self.D):
            data[i] = .5*m*w**2*sum([(j-(self.M-1)/2)**2*(self.A[i][j]+self.A[i][j+self.M]) for j in range(self.M)])
        
        return sparse.dia_matrix((data, 0), shape=(self.D, self.D), dtype=np.complex128).tocsc()

    def H_SOC(self, Omega = 1, phi = 0):
        if not hasattr(self, 'Sx_j'):
            self.S_j_calc()
        Sx, Sy, Sz = self.Sx_j, self.Sy_j, self.Sz_j
        H = 0
        for j in range(self.M):
            H += Omega*(np.cos((j+1)*phi)*Sx[j] - np.sin((j+1)*phi)*Sy[j])
        
        return H
 
    def H_spin_ap(self, J=1, U=1, Omega=1, phi = 0):
        if not hasattr(self, 'Sx_j') or not hasattr(self, 'N_j'):
            self.S_j_calc()
            self.N_j_calc()
        Nj = self.N_j
        Sx, Sy, Sz = self.Sx_j, self.Sy_j, self.Sz_j
        Sp = [Sx[j] + 1j*Sy[j] for j in range(len(Sx))]
        Sm = [Sx[j] - 1j*Sy[j] for j in range(len(Sx))]
        H = 0
        
        cos_phi = np.cos(phi) 
        sin_phi = np.sin(phi) 
        sin_phi2 = np.sin(phi/2)
        cos_phi2 = np.cos(phi/2)

        C_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*sin_phi2**2)
        delta_Omega = Omega - C_SOC
        C_dy   = J**2*Omega/(U**2-Omega**2)*sin_phi
        dOmega = J**2*Omega/(U**2-Omega**2)*sin_phi2
        C_SE  = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*cos_phi2**2)
        C_DM  = 4*J**2/(U*(U**2-Omega**2))*Omega**2*sin_phi2*cos_phi2
        c = 4*J**2/U
        
        for j in range(self.M-1):
            #H += C_SOC * (-np.cos(phi*j)*Sx[j] + np.sin(phi*j)*Sy[j])
            H += C_SOC * (np.cos(phi*(j+1))*Sx[j] - np.sin(phi*(j+1))*Sy[j])
            #H += (Omega-c*U*Omega/(U**2-Omega**2))/2 * (-np.cos(phi*j)*Sx[j] + np.sin(phi*j)*Sy[j])
            #H += (Omega-c*U*Omega/(U**2-Omega**2))/2 * (-np.cos(phi*(j+1))*Sx[j+1] + np.sin(phi*(j+1))*Sy[j+1])
            #H += C_DM * (Sy[j]*Sx[(j+1) % self.M])
            #H -= C_DM * (Sx[j]*Sy[(j+1) % self.M])
            
            H += C_SE * (Sx[j]*Sx[(j+1) % self.M])
            H += C_SE * (Sy[j]*Sy[(j+1) % self.M])
            H += C_SE * (Sz[j]*Sz[(j+1) % self.M])
            H -= C_SE/4*(Nj[j]*Nj[(j+1) % self.M])
           
        #Boundary term when phi = pi
        if phi != np.pi:
            raise ValueError('non-pi phases not implemented yet.')
        '''
        H += 2*c*Sz[-1]*Sz[0]
        H -= 2*c*Sx[-1]*Sx[0]
        H -= 2*c*Sy[-1]*Sy[0]
        H -= 2*c*(1/4*Nj[-1]*Nj[0])
        H += Omega*(1/4*Nj[-1]*Nj[0] - Sx[-1]*Sx[0])
        '''
        j = self.M-1
        H += C_SOC * (-np.cos(phi*j)*Sx[j] + np.sin(phi*j)*Sy[j])
        #H += (Omega-c*U*Omega/(U**2-Omega**2))/2 * (-np.cos(phi*j)*Sx[j] + np.sin(phi*j)*Sy[j])
        #H += (Omega-c*U*Omega/(U**2-Omega**2))/2 * (-np.cos(phi*0)*Sx[0] + np.sin(phi*0)*Sy[0])
        #H += (Omega-c*U*Omega/(U**2-Omega**2))/2 * (-np.cos(phi*(j+1))*Sx[j+1] + np.sin(phi*(j+1))*Sy[j+1])
        H -= C_SE * (Sx[j]*Sx[(j+1) % self.M])
        H -= C_SE * (Sy[j]*Sy[(j+1) % self.M])
        H += C_SE * (Sz[j]*Sz[(j+1) % self.M])
        H -= C_SE/4*(Nj[j]*Nj[(j+1) % self.M])
        
        return H
   
    def H_dip_bose_spin(self, gamma=1):
        if not hasattr(self, 'Sx_j'):
            self.S_j_calc()
        Sx = self.Sx_j
        Sy = self.Sy_j
        Sz = self.Sz_j
        H = 0
        if self.bc == 'open':
            for j in range(self.M):
                for k in range(self.M):
                    d = abs(j-k)
                    if d != 0:
                        C = .5*gamma**2/d**3
                        H += C*(Sz[j]*Sz[k] - 2*Sx[j]*Sx[k] + Sy[j]*Sy[k])
        elif self.bc == 'ring':
            Sp = [Sx[j] + 1j*Sy[j] for j in range(self.M)] 
            Sm = [Sx[j] - 1j*Sy[j] for j in range(self.M)]
            for j in range(self.M):
                for d in range(1, self.M):
                    k = (j+d) % self.M
                    r = abs(np.sin(np.pi/self.M)/np.sin(np.pi/self.M*d))
                    C = .5*gamma**2*r**3
                    phi1 = d*2*np.pi/self.M
                    phi2 = 3*j*2*np.pi/self.M
                    H += C*(Sz[j]*Sz[k] - .5*(Sx[j]*Sx[k] + Sy[j]*Sy[k]))
                    H += C*3/4*(np.exp(-1j*(phi1+phi2))*Sp[j]*Sp[k] + np.exp(1j*(phi1+phi2))*Sm[j]*Sm[k])
        else:
            for j in range(self.M):
                for k in range(self.M):
                    d = min(abs(j-k), self.M-abs(j-k))
                    if d != 0:
                        C = gamma**2/d**3
                        C *= .5
                        H += C*(Sz[j]*Sz[k] - 2*Sx[j]*Sx[k] + Sy[j]*Sy[k])
          
        return H                 

    def H_dipoles1_bose_spin(self, J = 1, U=(1,1,1), gamma=1):
        row, col, data = [], [], []
        if not hasattr(self, 'Sx_j'):
            self.S_j_calc()
        Sx = self.Sx_j
        Sy = self.Sy_j
        Sz = self.Sz_j
        Sp = [Sx[j] + 1j*Sy[j] for j in range(self.M)] 
        Sm = [Sx[j] - 1j*Sy[j] for j in range(self.M)] 
        for k in range(self.D):
            v = self.A[k]
            for i in range(self.M):
                j_range = [(i-1), (i+1)]
                for s in (0,self.M):
                    for j in j_range: 
                        if (j >= self.M or j < 0) and self.bc == 'open':
                            continue
                        j = j % self.M 
                        if v[s+j] >= 1 :
                            u = list(v)
                            u[s+j] -= 1
                            u[s+i] += 1
                            H_ij = - J * np.sqrt(u[s+i] * v[s+j]) #* op_permutation
                                
                            r = self.T[tuple(u)]
                            data.append(H_ij)
                            row.append(r)
                            col.append(k)

                    if s == 0:
                        U_ii = U[0] / 2 * v[i+s] * (v[i+s] - 1)
                    else:
                        U_ii = U[1] / 2 * v[i+s] * (v[i+s] - 1)
                    U_ii += U[2] / 2 * v[i] * v[i+self.M]
                    data.append(U_ii)
                    row.append(k)
                    col.append(k)

        H = sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)
        for j in range(self.M):
            #for k in range(self.M):
            for k in [(j-1) % self.M, (j+1) % self.M]:
                d = (abs(j-k))**3
                if d != 0:
                    C = gamma**2/(4*d)
                    #H += C*(4*Sz[j]*Sz[k] - Sm[j]*Sp[k] - Sp[j]*Sm[k] - 3 * Sp[j]*Sp[k] - 3 * Sm[j]*Sm[k])
                    #H += C*(4*Sz[j]*Sz[k] - Sm[j]*Sp[k] - Sp[j]*Sm[k])# - 3 * Sp[j]*Sp[k] - 3 * Sm[j]*Sm[k])
                    H += 4*C*(Sz[j]*Sz[k] -  2*Sx[j]*Sx[k] + Sy[j]*Sy[k])
                
        return H                 

    def H_dipolar_fermi(self, gamma=1):
        if not hasattr(self, 'Sx_j'):
            self.S_j_calc()
        Sx = self.Sx_j
        Sy = self.Sy_j
        Sz = self.Sz_j
        Sp = [Sx[j] + 1j*Sy[j] for j in range(self.M)] 
        Sm = [Sx[j] - 1j*Sy[j] for j in range(self.M)] 
        H = 0 
        for j in range(self.M):
            for k in range(self.M):
                #for k in [(j-1) % self.M, (j+1) % self.M]:
                d = (abs(j-k))**3
                if d != 0:
                    C = gamma**2/(4*d)
                    H += C*(4*Sz[j]*Sz[k] - Sm[j]*Sp[k] - Sp[j]*Sm[k] - 3 * Sp[j]*Sp[k] - 3 * Sm[j]*Sm[k])
                    #H += C*(4*Sz[j]*Sz[k] - Sm[j]*Sp[k] - Sp[j]*Sm[k])# - 3 * Sp[j]*Sp[k] - 3 * Sm[j]*Sm[k])
                    #H += 4*C*(Sz[j]*Sz[k] -  2*Sx[j]*Sx[k] + Sy[j]*Sy[k])
                
        return H    


    def H_dipolar2_fermi(self, gamma=1):
        if not hasattr(self, 'Sx_j'):
            self.S_j_calc()
        Sx = self.Sx_j
        Sy = self.Sy_j
        Sz = self.Sz_j
        Sp = [Sx[j] + 1j*Sy[j] for j in range(self.M)] 
        Sm = [Sx[j] - 1j*Sy[j] for j in range(self.M)] 
        H = 0 
        for j in range(self.M):
            for k in range(self.M):
                #for k in [(j-1) % self.M, (j+1) % self.M]:
                d = (abs(j-k))**3
                if d != 0:
                    C = gamma**2/(4*d)
                    #H += C*(4*Sz[j]*Sz[k] - Sm[j]*Sp[k] - Sp[j]*Sm[k] - 3 * Sp[j]*Sp[k] - 3 * Sm[j]*Sm[k])
                    H += C*(- Sm[j]*Sp[k] - Sp[j]*Sm[k] - 3 * Sp[j]*Sp[k] - 3 * Sm[j]*Sm[k])
                    #H += C*(4*Sz[j]*Sz[k] - Sm[j]*Sp[k] - Sp[j]*Sm[k])# - 3 * Sp[j]*Sp[k] - 3 * Sm[j]*Sm[k])
                    #H += 4*C*(Sz[j]*Sz[k] -  2*Sx[j]*Sx[k] + Sy[j]*Sy[k])
                
        return H   

    def H_spin_rot(self, J=1, U=1, Omega=1):
        if not hasattr(self, 'Sx_j') or not hasattr(self, 'N_j'):
            self.S_j_calc()
            self.N_j_calc()
        Nj = self.N_j
        Jx, Jy, Jz = self.Sx_j, self.Sy_j, self.Sz_j
        H = 0
        for j in range(self.M):
            d = 1
            if j == self.M-1:
                if self.bc == 'open':
                    continue
                if self.bc == 'periodic':
                    d = -(self.M - 1)
          
            sin_phi2 = np.sin(self.phi*d/2)
            cos_phi2 = np.cos(self.phi*d/2)
            cos_phi = np.cos(self.phi*d)
            sin_phi = np.sin(self.phi*d)

            C_SOC = Omega*(1/2-2*J**2/(U**2-Omega**2)*sin_phi2**2)
            C_dy  = -2*J**2/(U*(U**2-Omega**2))*sin_phi2*cos_phi2
            C_SE  = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*cos_phi2**2)
            C_yz  = 4*J**2/(U*(U**2-Omega**2))*(U**2*cos_phi - Omega**2*cos_phi2**2)
            C_DM  = 4*J**2/(U*(U**2-Omega**2))*(2*U**2 - Omega**2)*2*sin_phi2*cos_phi2
            C_DM  = 4*J**2/(U*(U**2-Omega**2))*(2*U**2 - Omega**2)*sin_phi

            H += C_SOC * (Nj[j]*Jz[(j+1) % self.M] + Jz[j]*Nj[(j+1) % self.M])
            H += C_dy  * (Nj[j]*Jy[(j+1) % self.M] - Jy[j]*Nj[(j+1) % self.M])
            H += C_SE  * (Jx[j]*Jx[(j+1) % self.M] - 1/4*Nj[j]*Nj[(j+1) % self.M])
            H += C_yz  * (Jy[j]*Jy[(j+1) % self.M] + Jz[j]*Jz[(j+1) % self.M])
            H += C_DM  * (Jy[j]*Jz[(j+1) % self.M] - Jz[j]*Jy[(j+1) % self.M])
        
        return H 

    def H_spin(self, J=1, U=1, Omega=1, phi = 0):
        if not hasattr(self, 'Sx_j') or not hasattr(self, 'N_j'):
            self.S_j_calc()
            self.N_j_calc()
        Nj = self.N_j
        Sx, Sy, Sz = self.Sx_j, self.Sy_j, self.Sz_j
        Sp = [Sx[j] + 1j*Sy[j] for j in range(len(Sx))]
        Sm = [Sx[j] - 1j*Sy[j] for j in range(len(Sx))]
        H = 0
        
        cos_phi = np.cos(phi) 
        sin_phi = np.sin(phi) 
        sin_phi2 = np.sin(phi/2)
        cos_phi2 = np.cos(phi/2)

        C_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*sin_phi2**2)
        delta_Omega = Omega - C_SOC
        C_dy   = J**2*Omega/(U**2-Omega**2)*sin_phi
        dOmega = J**2*Omega/(U**2-Omega**2)*sin_phi2
        C_SE  = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*cos_phi2**2)
        C_DM  = 4*J**2/(U*(U**2-Omega**2))*Omega**2*sin_phi2*cos_phi2
        
        d = (self.M-1)
        C_dBound_DM = 4*J**2/(U*(U**2-Omega**2))*(U**2*np.sin(phi*(d-1)) 
                    - .5*Omega**2*(np.sin(phi*(d-1)) - np.sin(phi*d) + sin_phi ))
        C_dBound_SE = 4*J**2/(U*(U**2-Omega**2))*(U**2*(np.cos(phi*(d-1))-1) 
                    + .5*Omega**2*(np.cos(phi*(d-1)) + np.cos(phi*d) - cos_phi - 1) )
        C_open_DM = 4*J**2/(U*(U**2-Omega**2))*(U**2*np.sin(phi*(d-1)) 
                    - .5*Omega**2*(np.sin(phi*(d-1)) - np.sin(phi*d)))
        C_open_SE = 4*J**2/(U*(U**2-Omega**2))*(U**2*np.cos(phi*(d-1)) 
                    - .5*Omega**2*(np.cos(phi*(d-1)) + np.cos(phi*d)) )
        for j in range(self.M):
            #H += C_SOC * (-np.cos(phi*j)*Sx[j] + np.sin(phi*j)*Sy[j])
            H += C_SOC * (np.cos(phi*(j+1))*Sx[j] - np.sin(phi*(j+1))*Sy[j])
            
            if j == self.M-1 and self.bc == 'open':
                '''
                H -= 2 * dOmega * np.sin(phi * (j+.5))*Sx[j]
                H -= 2 * dOmega * np.cos(phi * (j+.5))*Sy[j]
                H += 2 * dOmega * np.sin(phi * (0-.5))*Sx[0]
                H += 2 * dOmega * np.cos(phi * (0-.5))*Sy[0]
                '''
                #'''
                H += 1j* dOmega * np.exp(+1j*phi*(j+.5))*Sp[j]
                H -= 1j* dOmega * np.exp(-1j*phi*(j+.5))*Sm[j]
                H -= 1j* dOmega * np.exp(+1j*phi*(0-.5))*Sp[0]
                H += 1j* dOmega * np.exp(-1j*phi*(0-.5))*Sm[0]
                #'''
                ''' 
                H_bound = 0
                H_bound += C_open_DM * (Sy[j]*Sx[0])
                H_bound -= C_open_DM * (Sx[j]*Sy[0])
            
                H_bound += C_open_SE * (Sx[j]*Sx[0])
                H_bound += C_open_SE * (Sy[j]*Sy[0])
                H_bound += C_SE      * (Sz[j]*Sz[0])
                H_bound -= C_SE / 4  * (Nj[j]*Nj[0])

                H_bound -= C_DM * (Sy[j]*Sx[0])
                H_bound += C_DM * (Sx[j]*Sy[0])
                
                H_bound -= C_SE * (Sx[j]*Sx[0])
                H_bound -= C_SE * (Sy[j]*Sy[0])
                H_bound -= C_SE * (Sz[j]*Sz[0])
                H_bound += C_SE/4*(Nj[j]*Nj[0])
                 
                H += H_bound
                '''

                continue

            H += C_DM * (Sy[j]*Sx[(j+1) % self.M])
            H -= C_DM * (Sx[j]*Sy[(j+1) % self.M])
            
            H += C_SE * (Sx[j]*Sx[(j+1) % self.M])
            H += C_SE * (Sy[j]*Sy[(j+1) % self.M])
            H += C_SE * (Sz[j]*Sz[(j+1) % self.M])
            H -= C_SE/4*(Nj[j]*Nj[(j+1) % self.M])
            
        return H

    def H_spin_defect(self, J=1, U=1, Omega=1, phi = 0):
        if not hasattr(self, 'Sx_j') or not hasattr(self, 'N_j'):
            self.S_j_calc()
            self.N_j_calc()
        Nj = self.N_j
        Sx, Sy, Sz = self.Sx_j, self.Sy_j, self.Sz_j
        Sp = [Sx[j] + 1j*Sy[j] for j in range(len(Sx))]
        Sm = [Sx[j] - 1j*Sy[j] for j in range(len(Sx))]
        H = 0
        
        cos_phi = np.cos(phi) 
        sin_phi = np.sin(phi) 
        sin_phi2 = np.sin(phi/2)
        cos_phi2 = np.cos(phi/2)

        C_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*sin_phi2**2)
        C_SE  = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*cos_phi2**2)
        C_DM  = 4*J**2/(U*(U**2-Omega**2))*Omega**2*sin_phi2*cos_phi2
        
        for j in range(self.M):
            H += C_SOC * (np.cos(phi*(j+1))*Sx[j] - np.sin(phi*(j+1))*Sy[j])
            
            H += C_DM * (Sy[j]*Sx[(j+1) % self.M])
            H -= C_DM * (Sx[j]*Sy[(j+1) % self.M])
            if j == self.M-1 and self.bc == 'open':
                continue
            
            H += C_SE * (Sx[j]*Sx[(j+1) % self.M])
            H += C_SE * (Sy[j]*Sy[(j+1) % self.M])
            H += C_SE * (Sz[j]*Sz[(j+1) % self.M])
            H -= C_SE/4*(Nj[j]*Nj[(j+1) % self.M])
            
        return H
  
    def _H_spin_legacy(self, J_SE=1, J_ud=1, phi = 0):
        if not hasattr(self, 'Sx_j') or not hasattr(self, 'N_j'):
            self.S_j_calc()
            self.N_j_calc()
        Nj = self.N_j
        Sx, Sy, Sz = self.Sx_j, self.Sy_j, self.Sz_j
        Sp = self.Sp_j 
        Sm = self.Sm_j 
        H = 0
        
        for j in range(self.M):
            H += J_ud * (-np.cos(phi*j)*Sx[j] + np.sin(phi*j)*Sy[j])
            
            if j == self.M-1 and self.bc == 'open':
                continue

            H += J_SE * (Sx[j]*Sx[(j+1) % self.M])
            H += J_SE * (Sy[j]*Sy[(j+1) % self.M])
            H += J_SE * (Sz[j]*Sz[(j+1) % self.M])
            H -= J_SE/4*(Nj[j]*Nj[(j+1) % self.M])
            
        return H
    
    def H_spin_simple(self, J_SE=1, J_ud=1, phi = 0, phi_0 = 0):
        if not hasattr(self, 'Sx_j') or not hasattr(self, 'N_j'):
            self.S_j_calc()
            self.N_j_calc()
        Nj = self.N_j
        Sx, Sy, Sz = self.Sx_j, self.Sy_j, self.Sz_j
        Sp = self.Sp_j 
        Sm = self.Sm_j 
        H = 0
        
        for j in range(self.M):
            H -= J_ud * (-np.cos(phi*(j+1)+phi_0)*Sx[j] + np.sin(phi*(j+1)+phi_0)*Sy[j])
            
            if j == self.M-1 and self.bc == 'open':
                continue

            H += J_SE * (Sx[j]*Sx[(j+1) % self.M])
            H += J_SE * (Sy[j]*Sy[(j+1) % self.M])
            H += J_SE * (Sz[j]*Sz[(j+1) % self.M])
            H -= J_SE/4*(Nj[j]*Nj[(j+1) % self.M])
            
        return H
  
    def H_spin_v2(self, J_SE=1, J_ud=1, phi = 0):
        if not hasattr(self, 'Sx_j') or not hasattr(self, 'N_j'):
            self.S_j_calc()
            self.N_j_calc()
        Nj = self.N_j
        Sx, Sy, Sz = self.Sx_j, self.Sy_j, self.Sz_j
        Sp = self.Sp_j 
        Sm = self.Sm_j 
        H = 0
        
        for j in range(self.M):
            H += J_ud * np.cos(phi*(j+1-.5))*Sx[j]
            
            if j == self.M-1 and self.bc == 'open':
                continue

            H += J_SE * (Sx[j]*Sx[(j+1) % self.M])
            H += J_SE * (Sy[j]*Sy[(j+1) % self.M])
            H += J_SE * (Sz[j]*Sz[(j+1) % self.M])
            H -= J_SE/4*(Nj[j]*Nj[(j+1) % self.M])
            
        return H

    def H_SE_tilde(self, J=1):
        if not hasattr(self, 'Sx_j'):
            self.S_j_calc()
        Sx, Sy, Sz = self.Sx_j, self.Sy_j, self.Sz_j
        
        H=0

        if self.bc == 'open':
            j_range = range(self.M-1)
        else:
            j_range = range(self.M)
        
        for j in j_range:
            H -= J*Sz[j]*Sz[(j+1) % self.M]    
            H -= J*Sx[j]*Sx[(j+1) % self.M]    
            H += J*Sy[j]*Sy[(j+1) % self.M]    

        return H

    def H_SE(self, J=1):
        if not hasattr(self, 'Sx_j'):
            self.S_j_calc()
        Sx, Sy, Sz = self.Sx_j, self.Sy_j, self.Sz_j
        
        H=0

        if self.bc == 'open':
            j_range = range(self.M-1)
        else:
            j_range = range(self.M)
        
        for j in j_range:
            H += J*Sz[j]*Sz[(j+1) % self.M]    
            H += J*Sx[j]*Sx[(j+1) % self.M]    
            H += J*Sy[j]*Sy[(j+1) % self.M]    

        return H

    def H_XXZ_inhomo(self, J, U, beta):
        if not hasattr(self, 'Sx_j'):
            self.S_j_calc()
        Sx, Sy, Sz = self.Sx_j, self.Sy_j, self.Sz_j
        
        H = 0
        boundary_limit = self.M
        if self.bc == 'open':
            boundary_limit -= 1
        for j in range(boundary_limit):
            j_next = (j+1) % self.M
            J_aa = -J**2*4*U[0]/(U[0]**2-.25*(beta[j]-beta[j_next])**2)
            J_bb = -J**2*4*U[1]/(U[1]**2-.25*(beta[j]-beta[j_next])**2)
            J_perp = -J**2*4*U[-1]/(U[-1]**2-.25*(beta[j]-beta[j_next])**2)
            J_abm = -J**2*2/(U[-1]-.5*(beta[j]-beta[j_next]))
            J_abp = -J**2*2/(U[-1]+.5*(beta[j]-beta[j_next]))

            J_z = (J_aa+J_bb-J_abm-J_abp)
            H += J_z*Sz[j]*Sz[j_next]
            H += J_perp*Sx[j]*Sx[j_next]
            H += J_perp*Sy[j]*Sy[j_next]
        for j in range(self.M):
            j_next = (j+1) % self.M
            h_p = -J**2*(beta[j]-beta[j_next])/(U[-1]**2-.25*(beta[j]-beta[j_next])**2)
            h_m = +J**2*(beta[j-1]-beta[j])/(U[-1]**2-.25*(beta[j-1]-beta[j])**2)
            h = h_p + h_m
            H += h*Sz[j]
            H += beta[j]*Sz[j]

        if self.bc == 'open':
            h= -J**2*(beta[-1]-beta[0])/(U[-1]**2-.25*(beta[-1]-beta[0])**2)
            H += h*(Sz[0] - Sz[-1])
        
        return H

class HSpinMixin:
    def H_TMM(self, J = 1, U=(1,1,1), gamma=1):
        if not hasattr(self, 'Sx'):
            self.S_calc()
        Sx = self.Sx
        Sy = self.Sy
        Sz = self.Sz
        if not hasattr(self, 'N_j'):
            self.N_j_calc()
        M = self.M if self.M != 1 else self.N
        N = sum(self.N_j)
        C_NN = (U[0]+U[1]+2*U[2])/(8*M)
        C_SS = (U[0]+U[1]-2*U[2])/(2*M)
        C_SN = (U[0]-U[1])/(2*M)
        C_N = (U[0]+U[1])/4
        C_S = (U[0]-U[1])/2
        H  = C_SS * Sz**2
        
        H += C_NN*N**2 + C_SN * Sz * N 
        H -= C_S * Sz + C_N* N 
        
        if self.bc == 'open':
            #H += -2*(M-1)/M*np.sqrt(M)*J*N 
            H += -2*(M-1)/M*J*N 
            #h3 = 0
            #for i in range(M):
            #    for j in range(M):
            #        if j != i:
            #            h3 += 1/abs(i-j)**3
            #eta = 3/2*gamma**2*h3/M**2
            eta = 3*gamma**2/M*(zeta(3,1) - zeta(2,1)/M)
            H -= eta*Sx**2
            #chi = C_SS
        elif self.bc == 'ring':
            H += -2*np.sqrt(M)*J*N 
            h = 0
            for d in range(1, M):
                h += abs(np.sin(np.pi/M)/np.sin(np.pi/M*d))**3
            eta = 3/4*gamma**2*h/M#**2
            H += eta*Sz**2
            chi = C_SS + eta
        else:
            H += -2*J*N 
            #h = 0
            #for d in range(1, (M//2)+1):
            #    h += 1/d**3
            #eta = 6*gamma**2*h/M
            #eta *= .5
            eta = 3*gamma**2*zeta(3,1)/M
            H -= eta*Sx**2

        return H

    def _H_eff_open_legacy(self, J_SE = 1, J_ud = 1, phi = 0, corr = True):
        if not hasattr(self, 'Sx'):
            self.S_calc()
        #al = np.array([np.exp(1j*phi*j) for j in range(1, self.M+1)])
        al = np.array([np.exp(1j*phi*j) for j in range(self.M)])
        d = 0
        if corr:
            d = 1/self.M*sum(al)
        fp = []
        Ep = []
        for q in range(1, self.M):
            #p_lq = np.array([np.cos(np.pi/self.M*(j-.5)*q) for j in range(1, self.M+1)])
            p_lq = np.array([np.cos(np.pi/self.M*(j-.5)*q) for j in range(self.M)])
            fp_q = np.sqrt(2)/self.M*sum(p_lq*al)
            fp.append(fp_q)
            Ep.append(J_SE*(np.cos(np.pi/self.M*q)-1))
        fp = np.array(fp)
        Ep = np.array(Ep)
        c = J_ud**2/(2*(self.M-1))
        F1 = sum(np.abs(fp)**2/Ep)
        F2 = sum(fp*fp/Ep)
        A = J_ud/2*d
        B = c*F1
        C = c*np.real(F2)
        D = c*np.imag(F2)

        linear = A*self.Sp + np.conj(A)*self.Sm
        cuad = (C-B)*self.Sz**2 + 2*C*self.Sx**2 - D*(self.Sx*self.Sy + self.Sy*self.Sx)
        return linear + cuad

    def H_eff_open(self, J_SE = 1, J_ud = 1, phi = 0, phi_0 = 0, corr = True):
        if not hasattr(self, 'Sx'):
            self.S_calc()
        al = np.array([np.exp(1j*(phi*j+phi_0)) for j in range(1, self.M+1)])
        d = 0
        if corr:
            d = 1/self.M*sum(al)
        fp = []
        Ep = []
        for q in range(1, self.M):
            p_lq = np.array([np.cos(np.pi/self.M*(j-.5)*q) for j in range(1, self.M+1)])
            fp_q = np.sqrt(2)/self.M*sum(p_lq*al)
            fp.append(fp_q)
            Ep.append(J_SE*(np.cos(np.pi/self.M*q)-1))
        fp = np.array(fp)
        Ep = np.array(Ep)
        c = J_ud**2/(2*(self.M-1))
        F1 = sum(np.abs(fp)**2/Ep)
        F2 = sum(fp*fp/Ep)
        A = J_ud/2*d
        B = c*F1
        C = c*np.real(F2)
        D = c*np.imag(F2)

        linear = A*self.Sp + np.conj(A)*self.Sm
        cuad = (C-B)*self.Sz**2 + 2*C*self.Sx**2 - D*(self.Sx*self.Sy + self.Sy*self.Sx)
        return linear + cuad

    def H_eff_beta(self, J_SE = 1, J_ud = 1, beta = None):
        if not hasattr(self, 'Sx'):
            self.S_calc()
        d = 1/self.M*sum(beta)
        fp = []
        Ep = []
        j = np.arange(M)+1
        for q in range(1, self.M):
            p_lq = np.cos(np.pi/self.M*(j-.5)*q)
            fp_q = np.sqrt(2)/self.M*sum(p_lq*beta)
            fp.append(fp_q)
            Ep.append(J_SE*(np.cos(np.pi/self.M*q)-1))
        fp = np.array(fp)
        Ep = np.array(Ep)
        c = J_ud**2/((self.M-1))
        F = sum((fp)**2/Ep)
        A = J_ud*d
        B = c*F

        linear = A*self.Sx
        S2 = self.Sx**2 + self.Sy**2 + self.Sz**2
        #cuad = -B*(self.Sx**2-S2)
        cuad = -B*(self.Sx**2)
        return linear + cuad
