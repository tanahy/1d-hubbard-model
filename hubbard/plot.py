import numpy as np
from numpy import pi
from scipy.linalg import expm
from matplotlib import cm, ticker
from matplotlib.ticker import StrMethodFormatter
from matplotlib.gridspec import GridSpec
from matplotlib.colors import TwoSlopeNorm, Normalize
import matplotlib.tri as tri
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage,
                                  AnnotationBbox)
from matplotlib.patches import ConnectionPatch, Ellipse, Circle
from matplotlib.lines import Line2D
import matplotlib.animation as animation

from .utilities import evo_calc, q_function_gen, q_function_parallel_gen

from matplotlib.patches import Circle, RegularPolygon
from matplotlib.path import Path
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection
from matplotlib.spines import Spine
from matplotlib.transforms import Affine2D

def plot_obs(systems, init_states, t_f, num, obs, ls = None, marker = None, show_HL = False):
    assert len(systems) == len(init_states)
    if ls is None:
        ls = ['solid'] + ['None' for i in range(len(systems)-1)]
    if marker is None:
        marker = [''] + [m for m in Line2D.markers][7:]
    if len(obs) == 1:
        fig, ax = plt.subplots(constrained_layout = True)
        axs = [ax]
    elif not isinstance(obs[0], (list, tuple)):
        fig, axs = plt.subplots(len(obs), constrained_layout = True, sharex = True)
    else: 
        rows = len(obs)
        cols = [len(row) for row in obs]

        fig = plt.figure(constrained_layout = True)
        gs = GridSpec(rows, max(cols), figure = fig)
        axs = []
        obs = [item for sublist in obs for item in sublist]
        for i in range(rows):
            for j in range(cols[i]):
                if j < cols[i]-1:
                    ax = fig.add_subplot(gs[i, j])
                else:
                    ax = fig.add_subplot(gs[i, j:max(cols)])
                axs.append(ax)
                ax.sharex(axs[0])
                ax.sharey(axs[sum(cols[:i])])

    for ax, ob in zip(axs, obs):
        ax.set_xlabel('t')
        ax.label_outer()
        if ob == 'squeezing':
            ax.set_ylabel(r'$\xi^2$')
            ax.set_ylim((0, 1.1))
            if show_HL:
                ax.axhline(1/systems[0].N, ls = 'dashed', color = 'grey', label = 'HL')
        else:
            ax.set_ylabel(ob)

    for j in range(len(systems)):
        s = systems[j]
        wf = init_states[j]
        ls_j = ls[j]
        marker_j = marker[j]
        t, data = s.evolve(wf, t_f, num, parameters = obs)
        for i in range(len(obs)):
            axs[i].plot(t, data[i, :], ls = ls_j, marker = marker_j, label = s.label)

    axs[-1].legend()

    return fig, axs

def radar_factory(num_vars, frame='circle'):
    """
    Create a radar chart with `num_vars` axes.

    This function creates a RadarAxes projection and registers it.

    Parameters
    ----------
    num_vars : int
        Number of variables for radar chart.
    frame : {'circle', 'polygon'}
        Shape of frame surrounding axes.

    """
    # calculate evenly-spaced axis angles
    theta = np.linspace(0, 2*np.pi, num_vars, endpoint=False)

    class RadarAxes(PolarAxes):

        name = 'radar'
        # use 1 line segment to connect specified points
        RESOLUTION = 1

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            # rotate plot such that the first axis is at the top
            self.set_theta_zero_location('N')

        def fill(self, *args, closed=True, **kwargs):
            """Override fill so that line is closed by default"""
            return super().fill(closed=closed, *args, **kwargs)

        def plot(self, *args, **kwargs):
            """Override plot so that line is closed by default"""
            lines = super().plot(*args, **kwargs)
            for line in lines:
                self._close_line(line)

        def _close_line(self, line):
            x, y = line.get_data()
            if x[0] != x[-1]:
                x = np.append(x, 0)
                y = np.append(y, 1e-16)
                line.set_data(x, y)

        def set_varlabels(self, labels):
            self.set_thetagrids(np.degrees(theta), labels)

        def _gen_axes_patch(self):
            # The Axes patch must be centered at (0.5, 0.5) and of radius 0.5
            # in axes coordinates.
            if frame == 'circle':
                return Circle((0.5, 0.5), 0.5)
            elif frame == 'polygon':
                return RegularPolygon((0.5, 0.5), num_vars,
                                      radius=.5, edgecolor="k")
            else:
                raise ValueError("Unknown value for 'frame': %s" % frame)

        def _gen_axes_spines(self):
            if frame == 'circle':
                return super()._gen_axes_spines()
            elif frame == 'polygon':
                # spine_type must be 'left'/'right'/'top'/'bottom'/'circle'.
                spine = Spine(axes=self,
                              spine_type='circle',
                              path=Path.unit_regular_polygon(num_vars))
                # unit_regular_polygon gives a polygon of radius 1 centered at
                # (0, 0) but we want a polygon of radius 0.5 centered at (0.5,
                # 0.5) in axes coordinates.
                spine.set_transform(Affine2D().scale(.5).translate(.5, .5)
                                    + self.transAxes)
                return {'polar': spine}
            else:
                raise ValueError("Unknown value for 'frame': %s" % frame)

    register_projection(RadarAxes)
    return theta


def annotate_heatmap(x, y, im, ranking, data=None, valfmt="{x:.2f}",
                     textcolors=("black", "white"),
                     threshold=None, singular_values=False, **textkw):
    
    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array().reshape(ranking.shape)

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              rotation=90,
              verticalalignment="center")
    kw.update(textkw)
    if singular_values:
        kw.update(rotation=0)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    values = []
    for i in range(data.shape[0]):
        if i == data.shape[0]-1:
            va = "top"
            kw.update(verticalalignment=va)
        elif i == 0:
            va = "bottom"
            kw.update(verticalalignment=va)
        for j in range(data.shape[1]):
            if not np.isnan(ranking[i, j]):
                if singular_values:
                    if ranking[i,j] not in values:
                        values.append(ranking[i,j])
                    else:
                        continue
                kw.update(color=textcolors[int(im.norm(data[i, j]) < threshold)])
                text = im.axes.text(x[j], y[i], valfmt(ranking[i, j], None), **kw)
                texts.append(text)

    return texts

def connectAxes(fig,ax1,ax2,coords1=(0,0),coords2=(0,0)):
    con = ConnectionPatch(xyA=coords1, coordsA=ax1.transData,
                          xyB=coords2, coordsB=ax2.transData)
    fig.add_artist(con)

def coord_finder_geo(X, Y, xy, data):
    x, y = xy
    min_distance = None
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            if not np.isnan(data[i,j]):
                xj = X[j]
                yi = Y[i]
                distance = np.sqrt((x-xj)**2 + (y-yi)**2)
                if min_distance is None or distance < min_distance:
                    min_distance = distance
                    x_min, y_min = xj, yi 
                    value = data[i,j]
    return (x_min, y_min), value

def coord_finder(X, Y, xy, data, delta=.5):
    (x_min, y_min), value = coord_finder_geo(X, Y, xy, data)
    i_min = list(Y).index(y_min)
    mask = abs(x_min-X) < delta
    for j in np.arange(len(X))[mask]:
        if not np.isnan(data[i_min,j]):
            if data[i_min,j] < value:
                x_min = X[j] 
                value = data[i_min,j]

    return (x_min, y_min), value

def annotation_box(X, Y, xy, xybox, ranking, evolution, ax, width=30, height=20):
    if isinstance(xy, int):
        rank = xy
        X,Y = np.meshgrid(X, Y)
        mask = ranking == rank
        xy = X[mask], Y[mask]
    else:
        xy, rank = coord_finder(X, Y, xy, ranking)
    y = evolution[int(rank)]
    x = width*np.arange(len(y))/len(y)
    y_min = min(y)
    norm = max(y)-y_min
    y = [height*(yi-y_min)/norm for yi in y]
    line = Line2D(x,y)
    da = DrawingArea(width,height,0,0)
    da.add_artist(line)
    ab = AnnotationBbox(da, xy,
                        xybox=xybox,
                        xycoords='data',
                        boxcoords='data',
                        box_alignment=(0.5, 0.5),
                        arrowprops=dict(arrowstyle="->"))

    ax.add_artist(ab)
    return ab

def find_critical_phi(phi_list,delta):
    phi = list(phi_list)
    while len(phi) > 0:
        delta_new = delta
        phi_i = phi[0]
        while True:
            phi_new = [item for item in phi if abs(item-phi_i) > delta_new]
            if len(phi) > len(phi_new):
                phi = phi_new
                delta_new *= 2
            else:
                delta_new = delta
                break
        
        yield phi_i

def apply_mask(list_og, mask):
    return [list_og[i] for i in range(len(mask)) if mask[i]]

def find_critical_phi_by_domains(phi_list,omega_list,delta):
    rank = list(range(len(phi_list)))
    phi = list(phi_list)
    omega = list(omega_list)
    delta_i = [delta for val in omega]
    omega = omega
    while len(phi) > 0:
        phi_0 = phi[0]
        rank_inside = []
        while True:
            inside = [True 
                    if abs(phi[j]-phi_0) <= delta_i[j]
                    else False 
                    for j in range(len(phi))]
            outside = [not val for val in inside]
            if sum(inside) > 0:
                omega_inside = set(apply_mask(omega, inside))

                for j in range(len(delta_i)):
                    if omega[j] in omega_inside:
                        delta_i[j] += delta

                rank_inside += apply_mask(rank,inside)
                
                phi = apply_mask(phi, outside)
                omega = apply_mask(omega,outside)
                delta_i = apply_mask(delta_i,outside)
                rank = apply_mask(rank, outside)
            else:
                delta_i = [delta for val in delta_i]
                break
        
        yield phi_0, rank_inside

def find_domains_1(x,y,squeezing,time,rank, delta = (5e-1,5e-1)):
    X,Y = np.meshgrid(x,y)

    ssp     = np.copy(squeezing)
    t       = np.copy(time)
    ranking = np.copy(rank)

    x,y = ranking.shape
    x = np.arange(x)
    y = np.arange(y)

    ds, dt = delta

    while (True^np.isnan(ranking)).sum() > 0:
        mask = ranking == np.nanmin(ranking)
        phi_0 = float(X[mask])
        find_count = 1
        while find_count:
            find_count = 0
            for i in x:
                for j in y:
                    if mask[i,j]:
                        i_range = [i+step for step in (-1,0,1) if i+step in x]
                        j_range = [j+step for step in (-1,0,1) if j+step in y]
                        for k in i_range:
                            for p in j_range:
                                if not mask[k,p]:
                                    ds_kp = abs(ssp[k,p]-ssp[i,j]) 
                                    cond1 = ds_kp < ds# and ds_kp > 0
                                    dt_kp = abs(t[k,p]-t[i,j])
                                    cond2 = dt_kp < dt# and dt_kp > 0
                                    if cond1 and cond2:
                                            mask[k,p] = True
                                            find_count += 1
        domain = ranking[mask].flatten()
        ranking[mask] = np.nan
        ssp[mask] = np.nan
        t[mask] = np.nan

        yield phi_0, domain
    
def find_domains_2(x,y,map_array,delta=1e-1):
    X,Y = np.meshgrid(x,y)

    composite = np.copy(map_array)
    composite /= np.nanmax(composite)

    x,y = composite.shape
    x = np.arange(x)
    y = np.arange(y)

    while (True^np.isnan(composite)).sum() > 0:
        mask = composite == np.nanmin(composite)
        phi_0, omega_0 = float(X[mask][0]), float(Y[mask][0])
        origin = composite[mask][0]
        find_count = 1
        while find_count:
            find_count = 0
            for i in x:
                for j in y:
                    if mask[i,j]:
                        i_range = [i+step for step in (-1,0,1) if i+step in x]
                        j_range = [j+step for step in (-1,0,1) if j+step in y]
                        for k in i_range:
                            for p in j_range:
                                if not mask[k,p]:
                                    ds = composite[k,p]-origin
                                    if  abs(ds) < delta:# and ds > 0:
                                        mask[k,p] = True
                                        find_count += 1
        domain = mask
        composite[mask] = np.nan

        yield (phi_0,omega_0), domain

def discrete_pcolormesh(x,y,data,ax,cmap='RdBu',**kwargs):
    #get discrete colormap
    cmap = plt.get_cmap(cmap, int(np.nanmax(data)-np.nanmin(data)+1))
    # set limits .5 outside true range
    mat = ax.pcolormesh(x,y,data,cmap=cmap,vmin = np.nanmin(data)-.5, vmax = np.nanmax(data)+.5, **kwargs)
    #tell the colorbar to tick at integers
    cax = plt.colorbar(mat, ax=ax, ticks=np.arange(np.nanmin(data),np.nanmax(data)+1))

    return mat

def classifier(N, params, t_0, t_f, threshold=1.5, time_scale = 10):
    x, y = evo_calc(N, params, t_0, t_f=t_f)
    behaviour = None
    if np.std(y) < .01:
        behaviour = 1
    elif max(y) < threshold:
        if  t_f/t_0 > 3:
            behaviour = 5
        else:
            behaviour = 6
    else:
        plateau = 0
        while y[plateau] >= y[0]:
            plateau += 1
        if plateau > sum(x<t_0)/time_scale:
            behaviour = 4
        else:
            behaviour = 3

    return behaviour,x,y

class Q_function_representation:
    def __init__(self, f, dt, resolution=50, generator = q_function_gen,
            projection = None, ax = None, cs_array = None, **kwargs):
        self.dt  = dt
        self.projection = projection
        self.Psi     = np.linspace(-np.pi, np.pi, resolution)
        self.Theta   = np.linspace(0, np.pi,      resolution)
        if ax is None:
            self.fig = plt.figure()
            self.ax  = self.fig.add_subplot(111, projection=projection)
        else:
            self.ax = ax

        ref_state = kwargs.pop('ref_state', None)
        if cs_array is None:
            self.cs_array = self.coherent_states(f, ref_state = ref_state)
        else:
            self.cs_array = cs_array
        self.array_gen = generator(f, dt, cs_array, **kwargs)
        q_array = np.zeros((resolution, resolution))
        if projection == '3d':
            self.X = np.outer(np.cos(self.Psi), np.sin(self.Theta))
            self.Y = np.outer(np.sin(self.Psi), np.sin(self.Theta))
            self.Z = np.outer(np.ones(np.size(self.Psi)), np.cos(self.Theta))
            #m = cm.ScalarMappable(cmap=cm.jet)
            #m.set_array(q_array)
            self.im = self.ax.plot_surface(self.X, self.Y, self.Z, cstride = 1, rstride = 1,
                    facecolors=cm.jet(q_array.T),
                    linewidth=0, antialiased=False, shade=False)
            self.ax.set_box_aspect((1, 1, 1))
        else:
            self.X,self.Y = np.meshgrid(self.Psi/np.pi,self.Theta/np.pi)
            self.im = self.ax.pcolormesh(self.X,self.Y,q_array, vmin=0., vmax=1, shading='auto')
            #ellipse = Ellipse((0,np.pi/2), np.pi,np.pi, fill=False)
            #circ = Circle((0,1/2), 1/8, color='red', fill=False)
            #self.ax.add_artist(circ)
            self.ax.set_xlabel(r'$\varphi/\pi$')
            self.ax.set_ylabel(r'$\theta/\pi$')
            #aspect_ratio = (Y[-1,-1]-Y[0,0])/(X[-1,-1]-X[0,0])
            #self.ax.set_aspect(aspect_ratio)
            self.ax.set_aspect(1)
            #self.ax.set_aspect(.5)
            #self.ax.set_aspect((self.Theta[-1]-self.Theta[0])/(self.Psi[-1]-self.Psi[0]))
        #self.cb = self.fig.colorbar(self.im, ax=self.ax)
    
    def coherent_states(self, f, ref_state=None, op='S'):
        w, h = len(self.Psi), len(self.Theta)
        if ref_state is None:
            wf = f.initial_state()
        else:
            wf = ref_state
        Sy = eval(f'f.{op}y')
        Sz = eval(f'f.{op}z')
        self.wf_psi_theta = [[[] for j in range(w)] for i in range(h)]
        rot_psi   = [expm(-1j*Sz*psi) for psi in self.Psi]
        rot_theta = [expm(-1j*Sy*theta) for theta in self.Theta]
        for i in range(h):
            for j in range(w):
                rot = rot_psi[j].dot(rot_theta[i])
                #rot = expm(-1j*Sz*self.Psi[j]).dot(expm(-1j*Sy*self.Theta[i]))
                self.wf_psi_theta[i][j] = rot.dot(wf)
    
    def streamplot(self, M, Omega, phi):
        #dH/dPsi
        U = +Omega*np.sin(self.Y*np.pi)*sum([np.sin(self.X*np.pi+phi*j) for j in range(M)])
        #dH/dTheta
        V = +Omega*np.cos(self.Y*np.pi)*sum([np.cos(self.X*np.pi+phi*j) for j in range(M)])
        speed = np.sqrt(U**2 + V**2)
        lw = 3*speed/speed.max()
        self.ax.streamplot(self.X, self.Y, V, U, density = .6, color='k', linewidth=lw)

    def update(self,i):
        q_array = next(self.array_gen)
        q_array /= np.max(q_array)
        if self.projection == '3d':
            self.ax.clear()
            self.im = self.ax.plot_surface(self.X, self.Y, self.Z, cstride = 1, rstride = 1,
                    facecolors=cm.jet(q_array.T),
                    linewidth=0, antialiased=False, shade=False)
        else:
            self.im.set_array(q_array.ravel())
        title = self.ax.set_title(f't={self.dt*i:.2f}')
        return self.im, title
    
    def animate(self, t_f):
        steps = int(t_f/self.dt)
        anim = animation.FuncAnimation(self.fig, self.update, 
                frames=np.arange(1,steps),
                repeat=False)
        return anim

def corr_plot(N, x, y, z, data, t_corr, style, coords = [], 
        figsize = None, subplot_kw = {}, step = 6, 
        orientation = 'vertical', fig = None, ax = None):
    #z is a 2D array
    #z  = corr[0,0,:,:]
    #data is a 4D array
    #data[:, -1, :, :, :]
    if fig is None:
        fig = plt.figure(figsize=figsize, constrained_layout=True)
    sample_axs = []
    if ax is None:
        if len(coords) > 0:
            len_subplots = len(coords)
            rows = (len_subplots + 1) // 2
            cols = 2 * rows
            gs = GridSpec(rows,cols, figure = fig)
            ax = fig.add_subplot(gs[:,1:-1], **subplot_kw)
            for col in (0, -1):
                for row in range(rows):
                    if row == 0:
                        ax_new = fig.add_subplot(gs[row, col])
                    else:
                        ax_new = fig.add_subplot(gs[row, col], sharey = ax_new, sharex = ax_new)
                    sample_axs.append(ax_new)
                        
        else:
            ax = fig.add_subplot(111, **subplot_kw)

    width  = np.pi/N

    cmap = plt.colormaps['PiYG']

    N_cticks = 10
    corigin = .8
    low, high = 0, 1 #corr.min(), corr.max()
    norm = TwoSlopeNorm(corigin)
    smap_corr = cm.ScalarMappable(norm=norm, cmap=cmap)
    smap_corr.set_clim(low,high)
    cticks = np.concatenate((
        np.linspace(low,  corigin, N_cticks//2 - 1, endpoint=False), 
        np.linspace(corigin, high, N_cticks//2)
        ))
    smap_corr.set_array(cticks)
    levels = cticks

    cb_corr     = fig.colorbar(smap_corr, ax=ax, label = '$R^2$', 
            ticks = cticks, orientation = orientation)
    X,Y = np.meshgrid(x,y)
   
    if style == 'bar':
        for k,r in enumerate(y):
            height = y[k] - y[k-1] if k > 0 else y[k] 
            bar_corr = ax.bar(x, height, 
                    color = cmap(norm(z[k,:])),
                    bottom = y[k]-height/2,
                    width = width,
                    )
            #ax.bar_label(bar_corr, labels = [f'{a:.2f}' for a in z[k,:]], label_type='center')

    elif style == 'pcolormesh':
        patch = ax.pcolormesh(x, y, z,
                cmap = cmap,
                norm = norm,
                label = '$R^2$',
                )
     
    elif style == 'triang':
        patch = ax.tricontourf(triang, z,
                cmap = cmap,
                norm = norm,
                )
        ax.scatter(X,Y, c=z,
                cmap = cmap,
                norm = norm,
                )
     
    elif style == 'contour':
        patch = ax.contour(x, y, z, 
                cmap = cmap, 
                norm = norm,
                #antialiased = False,
                #nchunk = 1,
                levels = 10,#levels,
                )


    elif style == 'contourf':
        patch = ax.contourf(x, y, z, 
                cmap = cmap, 
                norm = norm,
                antialiased = False,
                nchunk = 0,
                levels = 10,#levels,
                )

    if len(coords) > 0:
        len_data = len(data[0, 0, 0, :])
        labels = [ f'({l})' for l in 'abcdefghijklmnop']
        x_tags, y_tags = [], []

        trans_offset = mtransforms.offset_copy(ax.transData, fig=fig,
                                       x=0.0, y=0.10, units='inches')

        for k, coord in enumerate(coords):
            axin = sample_axs[k]
            i, j = coord
            x_tags.append(x[i])
            y_tags.append(y[j])
            axin.plot(data[1, j, i, ::step], label = 'spin')
            axin.plot(data[0, j, i, ::step], label = 'FH', 
                    ls = 'None', marker = 'x') 
            axin.axvline(t_corr / step, ls = 'dashed', alpha = .5)
            axin.set_xticks((0, t_corr / step, len_data /step), 
                    ('0', r'$t_{R^2}$', '$t_\chi$'))
            axin.set_yticklabels([])
            axin.label_outer()
            axin.set_xlim(0, len_data / step)
            axin.set_ylim(0, 1.5)
            ax_label = labels[k]
            axin.text(.7,.1, ax_label, transform = axin.transAxes)
            ax.text(x[i], y[j], ax_label, transform=trans_offset)#, horizontalalignment='center', #verticalalignment='center')
            '''
            if k == 0:
                axin.legend(loc = 'upper left',
                        bbox_to_anchor=(1., 1.),
                        #bbox_transform=fig.transFigure,
                        frameon=False,
                        )
            '''
        ax.plot(x_tags, y_tags, ls="None", marker='o', color = 'black', fillstyle='none')
    
    return fig, ax, (sample_axs, cb_corr)

def corr_plot_axin(N, x, y, z, data, t_corr, style, coords = [], 
        figsize = None, subplot_kw = {}, step = 6):
           
    fig, ax = plt.subplots(1, subplot_kw=subplot_kw, figsize = figsize, constrained_layout=True)

    width  = np.pi/N

    cmap = plt.colormaps['PiYG']

    N_cticks = 10
    corigin = .8
    low, high = 0, 1 #corr.min(), corr.max()
    norm = TwoSlopeNorm(corigin)
    smap_corr = cm.ScalarMappable(norm=norm, cmap=cmap)
    smap_corr.set_clim(low,high)
    cticks = np.concatenate((
        np.linspace(low,  corigin, N_cticks//2 - 1, endpoint=False), 
        np.linspace(corigin, high, N_cticks//2)
        ))
    smap_corr.set_array(cticks)
    levels = cticks

    cb_corr     = fig.colorbar(smap_corr,   ax=ax, label = '$R^2$', ticks = cticks)#, pad = .1)
    X,Y = np.meshgrid(x,y)
    
    if style == 'bar':
        for k,r in enumerate(y):
            height = y[k] - y[k-1] if k > 0 else y[k] 
            bar_corr = ax.bar(x, height, 
                    color = cmap(norm(z[k,:])),
                    bottom = y[k]-height/2,
                    width = width,
                    )
            if len(y) < 15:
                ax.bar_label(bar_corr, labels = [f'{a:.2f}' for a in z[k,:]], label_type='center')

    elif style == 'pcolormesh':
        patch = ax.pcolormesh(x, y, z,
                cmap = cmap,
                norm = norm,
                )
     
    elif style == 'triang':
        patch = ax.tricontourf(triang, z,
                cmap = cmap,
                norm = norm,
                )
        ax.scatter(X,Y, c=z,
                cmap = cmap,
                norm = norm,
                )
     
    elif style == 'contour':
        patch = ax.contour(x, y, z, 
                cmap = cmap, 
                norm = norm,
                #antialiased = False,
                #nchunk = 1,
                levels = 10,#levels,
                )


    elif style == 'contourf':
        patch = ax.contourf(x, y, z, 
                cmap = cmap, 
                norm = norm,
                antialiased = False,
                nchunk = 0,
                levels = 10,#levels,
                )

    if len(coords) > 0:
        len_data = len(data[0, 0, 0, :])
        t = np.arange(0,len_data,step)
        labels = [ f'({l})' for l in 'abcdefghijklmnop']
        x_tags, y_tags = [], []

        len_subplots = len(coords)
        rows = (len_subplots + 1) // 2
        cols = 2 * rows
        l_axin = .3
        ax_pad = .2
        x_cols = (-(l_axin+ax_pad), 1+ax_pad)
        sample_axs = []
        for col in (0, -1):
            for row in range(rows):
                axin_bounds = [x_cols[col], (row+.5)/rows, l_axin, l_axin]
                if row == 0:
                    ax_new = ax.inset_axes(axin_bounds)
                else:
                    ax_new = ax.inset_axes(axin_bounds, sharey = ax_new, sharex = ax_new)
                sample_axs.append(ax_new)
         
        for k, coord in enumerate(coords):
            axin = sample_axs[k]
            i, j = coord
            x_tags.append(x[i])
            y_tags.append(y[j])
            axin.plot(t, data[1, j, i, ::step], label = 'spin')
            axin.plot(t, data[0, j, i, ::step], label = 'FH', 
                    ls = 'None', marker = 'x') 
            axin.axvline(t[-1]*t_corr, ls = 'dashed', alpha = .5)
            axin.set_xticks((0, t[-1]*t_corr, t[-1]), 
                    ('0', r'$t_{R^2}$', '$t_\chi$'))
            axin.set_yticklabels([])
            axin.label_outer()
            axin.set_xlim(0, t[-1])
            axin.set_ylim(0, 1.5)
            ax_label = labels[k]
            axin.text(.7,.1, ax_label, transform = axin.transAxes)
            ax.text(x[i], y[j], ax_label)#, horizontalalignment='center', #verticalalignment='center')

            if k == 0:
                axin.legend(loc = 'lower right')

            '''
            arrowprops = dict(arrowstyle="fancy",
                                    patchA=axin,
                                    #patchB=box, 
                                    color="0.5",
                                    alpha=.8,
                                    #shrinkA=1,
                                    #shrinkB=5,
                                    )
            ax.annotate('',
                    xy = (x[i], y[j]), xycoords = 'data',
                    xytext= center_axin, textcoords = 'axes fraction',
                    arrowprops=arrowprops
                    )
            '''

    ax.plot(x_tags, y_tags, ls="None", marker='o', markersize = .25, color = 'black', fillstyle='none')
    
    return fig, ax, sample_axs


