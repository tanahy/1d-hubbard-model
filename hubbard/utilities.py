import warnings, os, fnmatch
import pickle

import numpy as np

from scipy import interpolate, optimize, sparse
from scipy.sparse import SparseEfficiencyWarning
from scipy.sparse.linalg import expm, expm_multiply
try:
    from quspin.tools.evolution import expm_multiply_parallel
except:
    expm_multiply_parallel = expm_multiply 

from .hubbard import expectation_value

def sparse_to_file(name,A):
    with open(name,'w') as file:
        for i,c,data in zip (A.row, A.col, A.data):
            file.write(f'{i} {c} {data}\n') 

def dot_theta_coeff(phi, N, M, J_SE, Omega, phi_0 = 0):
    al = np.array([np.exp(1j*(phi*j+phi_0)) for j in range(1, M+1)])
    fp = []
    Ep = []
    for q in range(1, M):
        p_lq = np.array([np.cos(np.pi/M*(j-.5)*q) for j in range(1, M+1)])
        fp_q = np.sqrt(2)/M * np.sum(p_lq * al)
        fp.append(fp_q)
        Ep.append(J_SE*(np.cos(np.pi/M*q)-1))
    fp = np.array(fp)
    Ep = np.array(Ep)
    F1 = sum(np.abs(fp)**2/Ep)
    F2 = sum(fp*fp/Ep)
    A = 1/M*al.sum()
    b = Omega**2/(2*(M-1))
    a = b*F1
    
    chi_x = b*F2.real
    chi_z = a-chi_x
    chi_xy = b*F2.imag
    v_x = Omega/4*(A+A.conj())
    v_y = Omega/4*(A-A.conj())*1j

    return (N, v_x.real, v_y.real, chi_x, chi_z, chi_xy)

open_coeff = dot_theta_coeff

def dot_theta(psi, N, v_x, v_y, chi_x, chi_z, chi_xy):
    return v_x * np.sin(psi) - v_y * np.cos(psi) + N * (chi_x * np.sin(2*psi) + chi_xy * np.cos(2*psi))

def ddot_theta(psi, N, v_x, v_y, chi_x, chi_z, chi_xy):
    return v_x * np.cos(psi) + v_y * np.sin(psi) +2*N* (chi_x * np.cos(2*psi) - chi_xy * np.sin(2*psi))

def find_roots(func, args, method = 'secant', steps = 10, fprime = None):
    x = np.linspace(0, 2.1*np.pi, steps)
    y = func(x, *args)
    roots = []
    for i in range(1, steps):
        if y[i] == 0:
            roots.append(x[i])
        elif y[i]/y[i-1] < 0:
            sol = optimize.root_scalar(func, args = args,
                                       method = method,
                                       x0 = x[i-1],
                                       x1 = x[i],
                                       fprime = fprime,
            )
            roots.append(sol.root)
    return roots

def psi_crit_calc(phi, M):
    al = np.array([np.exp(1j*phi*j) for j in range(1, M+1)])
    fp = []
    Ep = []
    for q in range(1, M):
        p_lq = np.array([np.cos(np.pi/M*(j-.5)*q) for j in range(1, M+1)])
        fp_q = np.sqrt(2)/M*sum(p_lq*al)
        fp.append(fp_q)
        #Ep.append(J_SE*(np.cos(np.pi/M*q)-1))
        Ep.append(np.cos(np.pi/M*q)-1)
    fp = np.array(fp)
    Ep = np.array(Ep)
    #F1 = sum(np.abs(fp)**2/Ep)
    F2 = sum(fp*fp/Ep)

    psi_crit = .5*np.arctan(-np.imag(F2)/np.real(F2))
    return psi_crit

def calc_gamma(M, C_SS, eta, bc):
    h3 = 0
    if eta == 0:
        gamma = 0
    else:
        if bc == 'open':
            for i in range(M):
                for j in range(M):
                    if j != i:
                        h3 += 1/abs(i-j)**3
            gamma = np.sqrt(2*M**2*C_SS*eta/h3/3)
        elif bc == 'ring':
            for d in range(1, M):
                h3 += abs(np.sin(np.pi/M)/np.sin(np.pi/M*d))**3
            gamma = np.sqrt(4*M**2*C_SS*eta/h3/3)
        else:
            for d in range(1, (M // 2)+1):
                h3 += 1/d**3
            h3 *= .5
            gamma = np.sqrt(M*C_SS*eta/(6*h3))
    
    return gamma

def J_calc(N, U, J, Omega, phi):
    J_SE = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*np.cos(phi/2)**2)
    J_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*np.sin(phi/2)**2)
    J_DM = 4*J**2/(U*(U**2-Omega**2))*Omega**2*np.cos(phi/2)*np.sin(phi/2)
    J_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*np.sin(phi/2)**2)
    E_phi = J_SE*(1-np.cos(phi))
    chi = J_SOC**2/(E_phi*(N-1))
    if phi != np.pi:
        chi /= 2

    return J_SE, J_SOC, J_DM

def chi_calc(N, phi, U, J, Omega):
    J_SE = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*np.cos(phi/2)**2)
    J_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*np.sin(phi/2)**2)
    E_phi = J_SE*(1-np.cos(phi))
    chi = J_SOC**2/(E_phi*(N-1))
    if phi != np.pi:
        chi /= 2

    return chi#, J_SE, J_SOC

def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    if len(result) == 0:
        raise ValueError(path+pattern)
    return result

def calc_minima(x,y):
    y_min = min(y)
    mask  = y == y_min
    x_min = x[mask][0]
    return x_min, y_min



def evo_calc(model, params, t_0, t_f=None, M=None, bc='open', frame='lab', op='S', measure='squeezing', scaling=False, cutoff=False):
    N = model.N
    if M is None:
        M=N
    if t_f is None:
        t_f = t_0
    wf = model.initial_state_fermi()
    j, omega, u, phi = params
    scale = t_f/t_0
    if scale < 2:
        N_steps = 100
    else:
        N_steps = min(int(10 * scale), 3000)
        #N_steps = int(10 * scale)
    dt = t_f / N_steps
    
    if scaling:
        N_steps     /= scale
        N_scaling   = int(N_steps/2)
        N_steps     = 2*N_scaling
        N_frames    = 4
        for Ni in (N_frames, N_scaling, N_steps):
            assert isinstance(Ni, int)

        time_scaling = N_steps/N_scaling
        A = (N_steps/N_scaling)**N_frames
        t_0 = t_f / A
        dt = t_0 / N_steps

    t_i = t_0
    model.change_frame(frame, phi = phi)
    model.H = model.H_0(J=j) + model.H_int(U=u) + model.H_L(Omega = omega)
    Oy = eval(f'model.{op}y')
    wf2 = expm(-1j*Oy*np.pi/2).dot(wf.T)
    t = 0
    U_dt = model.time_evolution(dt)
    
    x, y = [], []
    while t < t_f:
        if t >= t_i and scaling:
            dt = t_i/N_scaling
            t_i *= time_scaling
            U_dt = model.time_evolution(dt)
        x.append(t)
        t += dt
        if measure == 'squeezing':
            ssp, _ = model.spin_squeezing(wf2, operator = op)
        else:
            ssp = expectation_value(wf2, eval(f'model.{measure}'))
        y.append(ssp)
        wf2 = U_dt.dot(wf2)
        if cutoff:
            found, t_min, ssp_min = first_minima(t_list, ssp_list)
            if found:
                if ssp_min is not None:
                    break
                return [], []

    return x, y

def classifier(params, t_0, t_f, threshold=1.5, time_scale = 10):
    x, y = evo_calc(params, t_0, t_f=t_f)
    behaviour = None
    if np.std(y) < .01:
        behaviour = 1
    elif max(y) < threshold:
        if  t_f/t_0 > 3:
            behaviour = 5
        else:
            plateau = 0
            while y[plateau] >= y[0]:
                plateau += 1
            if plateau > sum(x<t_0)/time_scale:
                behaviour = 4
            else:
                behaviour = 3
    else:
        plateau = 0
        while y[plateau] >= y[0]:
            plateau += 1
        if plateau > sum(x<t_0)/time_scale:
            behaviour = 4
        else:
            behaviour = 3

    return behaviour,x,y

def first_minima(x,y,max_variance=0.01,threshold=.99, t_max=np.inf):
    assert len(x) == len(y)
    found = False
    y_first_max = y[0]
    for i in range(len(x)):
        if y[i] > 1+max_variance:
            found = True
            break
            #return found, None, None
        elif y[i] < y_first_max:
            for j in range(i+1, len(x)):
                #if y[j] >= y_first_max or j == len(t_array)-1:
                if y[j] >= y_first_max or x[j] >= t_max:
                    found = True
                    y_min = min(y[i:j])
                    x_min = x[y.index(y_min)]
                    if y_min < threshold:
                        return found, x_min, y_min
                    else:
                        break
            break
        y_first_max = y[i]
    return found, None, None

def first_minima(x,y,var=(0.1, 0.01),threshold=.99, t_max=np.inf):
    assert len(x) == len(y)
    found = False
    for i in range(len(x)):
        if y[i] > y[0]+var[0]:
            found = True
            break
        elif y[i] < threshold:
            for j in range(i+1, len(x)):
                if y[j] >= threshold+var[1] or x[j] >= t_max:
                    found = True
                    y_min = min(y[i:j])
                    x_min = x[y.index(y_min)]
                    return found, x_min, y_min
            break
    return found, None, None
'''
def first_minima(x,y,max_variance=0.01,threshold=.99, t_max=np.inf):
    found = False
    y_first_max = y[0]
    for i in range(len(x)):
        if y[i] > y[0]+max_variance:
            found = True
            break
            #return found, None, None
        elif y[i] < y[0]:#y_first_max:
            for j in range(i+1, len(x)):
                if y[j] >= y[j-1]:
                    if y[j] >= y[0]-max_variance or x[j] >= t_max:
                        found = True
                        y_min = min(y[i:j])
                        x_min = x[y.index(y_min)]
                        if y_min < threshold:
                            return found, x_min, y_min
                        else:
                            break
            break
        y_first_max = y[i]
    return found, None, None
'''
def calculate_ortho(wf1, wf2):
    prod = np.real(np.vdot(wf1,wf2))
    #print([val if abs(val) > 1e-12 else 0 for val in wf1])
    #print([val if abs(val) > 1e-12 else 0 for val in wf2])
    #print(prod)
    return prod < 1e-12

def psi_loop(psi):
    k = psi / np.pi
    k = (k + 1) % 2 - 1
    return k*np.pi

def critical_psi(M,phi):
    #psi_0 = (2*np.pi-phi)*(M-1)/2 - np.pi/2
    #psi_0 = (np.pi-phi*(M-1))/2
    psi_0 = np.pi-phi/2*(M-1)
    psi_0 = psi_loop(psi_0)
    #if psi_0 < 0:
    #    psi_0 += np.pi
    psi_f = sorted((psi_0, psi_loop(psi_0+np.pi)))
    return psi_f

def evolution_SOC(F, Omega, phi, t):
    coeff_per_site = [0 for i in range(2*F.M)]
    for j in range(F.M):
        for s in (0,F.M):
            sign = 1 if s == 0 else -1
            coeff_per_site[s+j] = 1/np.sqrt(2)*(np.cos(Omega*t/2)+1j*np.sin(Omega*t/2)*np.exp(sign*1j*phi*j))
    coeffs = []
    for i in range(F.D):
        vector = F.A[i]
        factor = 1
        for j in range(len(vector)):
            for k in range(int(vector[j])):
                factor *= coeff_per_site[j]
        coeffs.append(factor)
    coeffs = np.array(coeffs)
    return coeffs

def coherent_states(f, resolution=50, op = 'S', ref_state=None, scheme = 'default'):
    #Coherent states across the Bloch sphere in coordinates theta, psi
    Psi     = np.linspace(-np.pi, np.pi, resolution)
    Theta   = np.linspace(0, np.pi,      resolution)
    
    if op == 'S':
        Oy = f.Sy
        Oz = f.Sz
    else:
        Oy = f.Jy
        Oz = f.Jz
 
    if ref_state is None:
        ref_state = f.initial_state()
        
    cs_array = np.empty((resolution, resolution, len(ref_state)), dtype = np.complex128)

    if scheme == 'default':
        rot_theta = [expm(-1j*Oy*theta) for theta in Theta]
        rot_psi   = [expm(-1j*Oz*psi) for psi in Psi]
        for i in range(resolution):
            for j in range(resolution):
                cs_array[i,j,:] = rot_psi[j].dot(rot_theta[i].dot(ref_state))
 
    elif scheme == 'multiply':
        rot_theta = expm_multiply(-1.j*Oy, ref_state, start=0, stop=np.pi, num = resolution, endpoint = True)
        cs_array = expm_multiply(-1.j*Oz, rot_theta.T, start=-np.pi, stop=np.pi, num = resolution, endpoint = True)
        #cs_array = np.transpose(cs_array, [1, 2, 0])
        #cs_array = np.transpose(cs_array, [2, 1, 0])
        #cs_array = np.transpose(cs_array, [0, 2, 1])
        cs_array = np.transpose(cs_array, [2, 0, 1]) #good
       
    elif scheme == 'parallel':
        rot_theta = expm_multiply_parallel(Oy.tocsr(), a=-1j)
        rot_psi   = expm_multiply_parallel(Oz.tocsr(), a=-1j)
        ref_state = ref_state.astype(np.complex128)
        work_array1 = np.empty((2*len(ref_state),), dtype=ref_state.dtype)
        work_array2 = np.empty((2*len(ref_state),), dtype=ref_state.dtype)
        for i in range(resolution):
            for j in range(resolution):
                rot_theta.set_a(-1.j*Theta[i])
                rot_psi.set_a(-1.j*Psi[j])
                cs_array[i,j,:] = rot_psi.dot(rot_theta.dot(ref_state, work_array = work_array1), work_array = work_array2)
    else:
        raise NameError("No scheme found with that name.")
    
    return cs_array

def q_function_calc(f, t, initial_state, cs_array = None, scheme = 'default', **kwargs):
    simul_calc = False
    if cs_array is None:
        cs_array = coherent_states(f, scheme = scheme, **kwargs)
    elif isinstance(cs_array, bool) and cs_array == False:
        simul_calc = True
    if scheme == 'default':
        wf = expm_multiply(-1j*f.H*t, initial_state)
    elif scheme == 'parallel':
        wf = expm_multiply_parallel(f.H.tocsr(), a=-1j*t).dot(initial_state)
    else:
        raise NameError("No scheme found with that name.")

    if simul_calc:
        resolution = kwargs.pop('resolution', 50)
        rot_theta = expm_multiply_parallel(f.Sy.tocsr(), a=-1j)
        rot_psi   = expm_multiply_parallel(f.Sz.tocsr(), a=-1j)
        ref_state = f.initial_state_fermi()
        ref_state = ref_state.astype(np.complex128)
        work_array1 = np.empty((2*len(ref_state),), dtype=ref_state.dtype)
        work_array2 = np.empty((2*len(ref_state),), dtype=ref_state.dtype)
        Psi     = np.linspace(-np.pi, np.pi, resolution)
        Theta   = np.linspace(0, np.pi,      resolution)
        q_array = np.empty((resolution, resolution))
        for i in range(resolution):
            for j in range(resolution):
                rot_theta.set_a(-1.j*Theta[i])
                rot_psi.set_a(-1.j*Psi[j])
                cs = rot_psi.dot(rot_theta.dot(ref_state, work_array = work_array1), work_array = work_array2)
                q_array[i,j] = abs(cs.conj().dot(wf))**2

    else:
        q_array = np.empty(cs_array.shape[:2])
        for i in range(cs_array.shape[0]):
            for j in range(cs_array.shape[1]):
                q_array[i,j] = abs(cs_array[i,j,:].conj().dot(wf))**2

    return q_array

def q_function_gen(f, dt, cs_array, initial_state=None):
    t = 0
    q_array = np.empty(cs_array.shape[:2])
    if initial_state is None:
        print('No initial state')
        wf = F.initial_state_fermi()
        Oy = eval(f'f.{op}y')
        wf = expm(-1j*Oy*np.pi/2).dot(wf)
    else:
        wf = np.copy(initial_state)

    U_dt = f.time_evolution(dt)
    while True:
        t += dt
        for i in range(cs_array.shape[0]):
            for j in range(cs_array.shape[1]):
                q_array[i,j] = abs(cs_array[i,j,:].conj().dot(wf))**2

        yield q_array
        if f is None:
            wf = U_dt.dot(wf)
        else:
            wf = f(t)
 
def q_function_parallel_gen(f, dt, cs_array, initial_state):
    U_t = expm_multiply_parallel(f.H.tocsr(), a=-1j*dt)
    wf = np.copy(initial_state).astype(np.complex128)
    q_array = np.empty(cs_array.shape[:2])
    work_array = np.empty((2*len(wf),), dtype=wf.dtype)
    while True:
        for i in range(cs_array.shape[0]):
            for j in range(cs_array.shape[1]):
                q_array[i,j] = abs(cs_array[i,j,:].conj().dot(wf))**2
        U_t.dot(wf, work_array = work_array, overwrite_v = True)
        
        yield q_array

def energy_fluc_theo(Psi, Theta, M, Omega, phi):
    w, h = len(Psi), len(Theta)
    E_theo = np.empty((h, w))
    for i in range(h):
        for j in range(w):
            E_theo[i,j] = -Omega*np.sin(Theta[i])*sum([np.cos(Psi[j]+k*phi) for k in range(M)])
    dx, dy = Psi[1]-Psi[0], Theta[1]-Theta[0]
    U, V = np.gradient(E_theo, dy, dx)
    return E_theo, -U, V

def energy_fluc(Psi, Theta, H, wf_array):
    w, h = len(Psi), len(Theta)
    w, h = wf_array.shape[:-1]
    assert w == len(Psi) and h == len(Theta)
    E = np.empty((h, w))
    for i in range(h):
        for j in range(w):
            #E[i,j] = np.real(expectation_value(wf_array[i][j], H))
            E[i,j] = np.real(expectation_value(wf_array[i,j,:], H))
    U, V = np.gradient(E, Psi, Theta)
    return E, -U, V

def stability_analysis(Psi, Theta, H, wf_array, psi_f):
    dx, dy = Psi[1]-Psi[0], Theta[1]-Theta[0]
    _, dp, dq = energy_fluc(Psi, Theta, H, wf_array)
    dp = -dp
    dp2, dpdq = np.gradient(dp, dy, dx)
    dqdp, dq2 = np.gradient(dq, dy, dx)
    M = []
    for d2 in dpdq, dp2, -dq2, -dpdq:
        f = interpolate.RectBivariateSpline(Psi,Theta,d2)
        M.append(f(psi_f, np.pi/2))
    M = np.array(M).reshape((2,2))
    print(M)
    eigenvalues, _ = np.linalg.eig(M)
    return eigenvalues

def energy_fluc_t(Psi, Theta, F, wf_psi_theta):
    w, h = len(Psi), len(Theta)
    E_array = np.empty((h, w))
    def energy_fluc_dt(wf_t):
        for i in range(h):
            for j in range(w):
                E_array[i,j] = np.real(wf_psi_theta[i][j].conj().dot(F.H.dot(wf_t)))
        dx, dy = Psi[1]-Psi[0], Theta[1]-Theta[0]
        U, V = np.gradient(E_array, dx, dy)
        return -U, V

    return energy_fluc_dt

def q_function_SOC(Psi,Theta,Omega,phi,M,dt):
    t = 0
    w,h = len(Psi), len(Theta)
    array = np.empty((h, w))
    while True:
        #c = [[(np.cos(Omega*t/2)+1j*np.sin(Omega*t/2)*np.exp(1j*sign*phi*j))/np.sqrt(2)
        #    for sign in (1,-1)] for j in range(M)]
        a = [np.cos(Omega*t/2) + 1j*np.sin(Omega*t/2)*np.cos(phi*j)
             for j in range(M)]
        b = [-np.sin(Omega*t/2)*np.sin(phi*j)
             for j in range(M)]
        #for i,k in np.ndindex(array.shape):
        for i in range(h):
            for k in range(w):
                psi   = Psi[k]
                theta = Theta[i]
                d = [np.cos(theta/2), np.sin(theta/2)*np.exp(-1j*psi)]
                val = 1
                for j in range(M):
                    val *= abs(d[0]*(a[j]+b[j])/np.sqrt(2) + d[1]*(a[j]-b[j])/np.sqrt(2))**2
                array[i,k] = val
        yield array
        t += dt

class SOC_Evolution:
    def __init__(self, N, theta, psi, Omega, phi):
        self.M = N
        self.theta = theta
        self.psi = psi
        self.Omega = Omega
        self.phi = phi
    def a(self,t):
        return np.array([(np.cos(self.theta/2)*np.cos(self.Omega*t/2) 
                + 1j*np.sin(self.theta/2)*np.exp(1j*(self.psi+self.phi*j))*np.sin(self.Omega*t/2))
                for j in range(self.M)])
    def b(self,t):
        return np.array([(np.sin(self.theta/2)*np.exp(1j*self.psi)*np.cos(self.Omega*t/2) 
                + 1j*np.cos(self.theta/2)*np.exp(-1j*self.phi*j)*np.sin(self.Omega*t/2))
                for j in range(self.M)])
    def sxj(self, t):
        a = self.a(t)
        b = self.b(t)
        #T,J = np.meshgrid(t,np.arange(self.M))
        #return .5*np.sum(np.cos(self.theta)*np.cos(self.Omega*T)*np.sin(self.theta*J)+
        #                 +np.sin(self.theta)*np.sin(self.Omega*T/2)**2*np.cos(self.psi-2*self.phi*J)
        #                 +np.sin(self.theta)*np.cos(self.Omega*T/2)**2*np.cos(self.psi), axis=0)
        return .5*(a.conj()*b + b.conj()*a)
    def syj(self, t):
        a = self.a(t)
        b = self.b(t)
        return -.5j*(a.conj()*b - b.conj()*a)
    def szj(self, t):
        a = self.a(t)
        b = self.b(t)
        return .5*(a.conj()*a - b.conj()*b)
    
    def sx(self, t):
        return np.sum(self.sxj(t), axis=0)
    def sy(self, t):    
        return np.sum(self.syj(t), axis=0)
    def sz(self, t):
        return np.sum(self.szj(t), axis=0)

    def sx2(self, t):
        a = self.a(t)
        b = self.b(t)
        result = 0
        for i in range(self.M):
            for j in range(self.M):
                if i != j:
                    result += (a[i].conj()*b[i] + b[i].conj()*a[i])*(a[j].conj()*b[j]+b[j].conj()*a[j])
        return self.M/4 + result/4
        Sxj = self.sxj(t)
        Sxi = Sxj[:, np.newaxis]
        return np.sum(np.sum(Sxi*Sxj,axis=0),axis=0)
    def sy2(self, t):
        syi = self.syj(t)
        result = 0
        for i in range(self.M):
            for j in range(self.M):
                if i != j:
                    result += syi[i]*syi[j]
        return self.M/4 + result
        Sxj = self.sxj(t)
        Sxi = Sxj[:, np.newaxis]
        return np.sum(np.sum(Sxi*Sxj,axis=0),axis=0)

        #Syj = self.syj(t)
        #Syi = Syj[:, np.newaxis]
        #return np.sum(np.sum(Syi*Syj,axis=0),axis=0)
    def sz2(self, t):
        szi = self.szj(t)
        result = 0
        for i in range(self.M):
            for j in range(self.M):
                if i != j:
                    result += szi[i]*szi[j]
        return self.M/4 + result
        Sxj = self.sxj(t)
        Sxi = Sxj[:, np.newaxis]
        return np.sum(np.sum(Sxi*Sxj,axis=0),axis=0)
        #Szj = self.szj(t)
        #Szi = Szj[:, np.newaxis]
        #return np.sum(np.sum(Szi*Szj,axis=0),axis=0)
    def sxsy(self, t):
        sxi = self.sxj(t)
        syi = self.syj(t)
        result = 0
        for i in range(self.M):
            for j in range(self.M):
                if i != j:
                    result += sxi[i]*syi[j] +syi[i]*sxi[j]
        return result
        #Sxj = self.sxj(t)
        #Sxi = Sxj[:, np.newaxis]
        #return np.sum(np.sum(Sxi*Sxj,axis=0),axis=0)
        Sxi = self.sxj(t)
        Sxj = Sxi[:,np.newaxis]
        Syi = self.syj(t)
        Syj = Syi[:,np.newaxis]
        return np.sum(np.sum(Sxi*Syj+Syi*Sxj,axis=0),axis=0)
    def sysz(self, t):
        szi = self.szj(t)
        syi = self.syj(t)
        result = 0
        for i in range(self.M):
            for j in range(self.M):
                if i != j:
                    result += syi[i]*szi[j] +szi[i]*syi[j]
        return result
        Szi = self.szj(t)
        Szj = Szi[:,np.newaxis]
        Syi = self.syj(t)
        Syj = Syi[:,np.newaxis]
        return np.sum(np.sum(Szi*Syj+Syi*Szj,axis=0),axis=0)
    def szsx(self, t):
        sxi = self.sxj(t)
        szi = self.szj(t)
        result = 0
        for i in range(self.M):
            for j in range(self.M):
                if i != j:
                    result += sxi[i]*szi[j] +szi[i]*sxi[j]
        return result
        Szi = self.szj(t)
        Szj = Szi[:,np.newaxis]
        Sxi = self.sxj(t)
        Sxj = Sxi[:,np.newaxis]
        return np.sum(np.sum(Szi*Sxj+Sxi*Szj,axis=0),axis=0)
        

class SOC_Evolution_Marlena(SOC_Evolution):
    def a(self,t):
        return np.array([(np.cos(self.psi/2)*np.cos(self.Omega*t/2) 
                + 1j*np.exp(1j*self.psi)*np.sin(self.Omega*t/2)*np.cos(self.psi/2 - self.phi*j))
                for j in range(self.M)])
    def b(self,t):
        return np.array([-(1j*np.sin(self.psi/2)*np.cos(self.Omega*t/2) 
                + np.exp(1j*self.psi)*np.sin(self.Omega*t/2)*np.sin(self.psi/2 + self.phi*j))
                for j in range(self.M)])
        return np.array([(1j*np.sin(self.psi/2)*np.cos(self.Omega*t/2) 
                + np.exp(1j*self.psi)*np.sin(self.Omega*t/2)*np.sin(self.psi/2 - self.phi*j))
                for j in range(self.M)])
    def sxj(self, t):
        a = self.a(t)
        b = self.b(t)
        return .5*(a.conj()*a-b.conj()*b)
    def syj(self, t):
        a = self.a(t)
        b = self.b(t)
        return .5j*(a.conj()*b-b.conj()*a)
    def szj(self, t):
        a = self.a(t)
        b = self.b(t)
        return .5*(a.conj()*b+b.conj()*a)

class SOC_Evolution_Marlena_2(SOC_Evolution_Marlena):
    def a(self,t):
        return np.array([(np.cos(self.psi/2)*np.cos(self.Omega*t/2) 
                + 1j*np.exp(1j*self.psi)*np.sin(self.Omega*t/2)*np.cos(self.psi/2 + self.phi*j))
                for j in range(self.M)])
    def b(self,t):
        return np.array([-(1j*np.sin(self.psi/2)*np.cos(self.Omega*t/2) 
                + np.exp(1j*self.psi)*np.sin(self.Omega*t/2)*np.sin(self.psi/2 + self.phi*j))
                for j in range(self.M)])

class SOC_Evolution_two_steps(SOC_Evolution):
    def a(self, t):
        return np.array([
            np.cos(self.theta)*(
            +(1-1j)*np.cos(self.Omega*t/2*(np.sin(self.phi*j)-np.cos(self.phi*j)))
            +(1+1j)*np.cos(self.Omega*t/2*(np.sin(self.phi*j)+np.cos(self.phi*j)))
            )
            +np.sin(self.theta)*np.exp(-1j*self.psi)*(
            +(1+1j)*np.sin(self.Omega*t/2*(np.sin(self.phi*j)+np.cos(self.phi*j)))
            +(1-1j)*np.sin(self.Omega*t/2*(np.sin(self.phi*j)-np.cos(self.phi*j)))
            )
            for j in range(self.M)])
    def b(self, t):
        return np.array([
            np.cos(self.theta)*(
            -(1-1j)*np.sin(self.Omega*t/2*(np.sin(self.phi*j)+np.cos(self.phi*j)))
            -(1+1j)*np.sin(self.Omega*t/2*(np.sin(self.phi*j)-np.cos(self.phi*j)))
            )
            +np.sin(self.theta)*np.exp(-1j*self.psi)*(
            +(1+1j)*np.cos(self.Omega*t/2*(np.sin(self.phi*j)-np.cos(self.phi*j)))
            +(1-1j)*np.cos(self.Omega*t/2*(np.sin(self.phi*j)+np.cos(self.phi*j)))
            )
            for j in range(self.M)]) 
class SOC_Evolution_explicit(SOC_Evolution):
    def sx(self, t):
        return (np.sin(self.theta)/2*(self.M*np.cos(self.Omega*t/2)**2*np.cos(self.psi)+np.sin(self.Omega*t/2)**2*sum(np.cos(self.psi+2*self.phi*np.arange(self.M))))
                + np.cos(self.theta)/2*np.sin(self.Omega*t)*sum(np.sin(self.phi*np.arange(self.M))))
    def sy(self, t):
        return (np.sin(self.theta)/2*(self.M*np.cos(self.Omega*t/2)**2*np.sin(self.psi)-np.sin(self.Omega*t/2)**2*sum(np.sin(self.psi+2*self.phi*np.arange(self.M))))
                + np.cos(self.theta)/2*np.sin(self.Omega*t)*sum(np.cos(self.phi*np.arange(self.M))))
    def sz(self, t):
        return .5*(self.M*np.cos(self.Omega)*np.cos(self.theta)-np.sin(self.Omega*t)*np.sin(self.theta)*sum(np.sin(self.psi+self.phi*np.arange(self.M))))
    def sx2(self, t):
        sum_ij = 0
        for i in range(self.M):
            for j in range(self.M):
                if i != j:
                    sum_ij += np.cos(self.Omega*t/2)**2 + .5*np.sin(self.Omega*t)**2 * (2*np.cos(self.phi*j)+np.cos(self.Omega*t/2)**2*2*np.cos(2*self.phi*i)
                            +np.sin(self.Omega*t/2)**2*(np.cos(self.phi*(i-j))+np.cos(self.phi*(i+j))))
        return self.M/4+1/4*sum_ij


def spin_squeezing(N, expectation_values):
    Sx, Sy, Sz, Sx2, Sy2, Sz2, SxSy, SySz, SzSx = expectation_values
    S2 = Sx**2+Sy**2+Sz**2
    S  = np.sqrt(S2) 

    cos_theta = Sz/S
    sin_theta = np.sqrt(1 - cos_theta**2)
    cos_phi   = Sx/np.sqrt(Sx**2+Sy**2)
    sin_phi   = Sy/np.sqrt(Sx**2+Sy**2)

    cos_2theta = (Sz**2-Sy**2-Sx**2)/S2             #cos_theta**2 - sin_theta**2 
    sin_2theta = 2*Sz*np.sqrt(Sx**2+Sy**2)/S2       #2*cos_theta*sin_theta
    cos_2phi   = (Sx**2 - Sy**2)/(Sx**2 + Sy**2)    #cos_phi**2 - sin_phi**2 
    sin_2phi   = 2*Sx*Sy/(Sx**2 + Sy**2)            #2*cos_phi*sin_phi

    delta_xy = SxSy - 2 * Sx*Sy 
    delta_zx = SzSx - 2 * Sz*Sx 
    delta_yz = SySz - 2 * Sy*Sz 

    delta_x2 = Sx2 - Sx**2 
    delta_y2 = Sy2 - Sy**2 
    delta_z2 = Sz2 - Sz**2 

    A_tilde = ((sin_phi**2 - cos_theta**2 * cos_phi**2) * delta_x2
             + (cos_phi**2 - cos_theta**2 * sin_phi**2) * delta_y2
             - sin_theta**2 * delta_z2 - .5 * (1 + cos_theta**2) * sin_2phi * delta_xy
             + .5 * sin_2theta * cos_phi * delta_zx + .5 * sin_2theta * sin_phi * delta_yz)

    B_tilde = (cos_theta * sin_2phi * (delta_x2 - delta_y2) - cos_theta * cos_2phi * delta_xy
             - sin_theta * sin_phi * delta_zx + sin_theta * cos_phi * delta_yz)

    delta_S2_min = (.5 * (cos_theta**2 * cos_phi**2 + sin_phi**2) * delta_x2
                  + .5 * (cos_theta**2 * sin_phi**2 + cos_phi**2) * delta_y2
                  + .5 * sin_theta**2 * delta_z2 - .25*(sin_theta**2 * sin_2phi * delta_xy
                  + sin_2theta * cos_phi * delta_zx + sin_2theta * sin_phi * delta_yz)
                  - .5 * np.sqrt(A_tilde**2 + B_tilde**2))

    text = f'Sx={Sx}, Sy={Sy}, Sz={Sz}, Deltas={delta_x2}, {delta_y2}, {delta_z2}'
    squeezing = N * delta_S2_min / S2
    #if abs(np.imag(squeezing)) > 1e-10:
    #    warnings.warn(f'Squeezing imaginary part is non negligible: {np.imag(squeezing)}')
    return np.real(squeezing)#, text


def delta_filling(filling, realizations):
    for _ in range(realizations):
        yield np.random.random() < filling


def evolve_sampling_saved(s, M, filling, t_f, N_steps, 
                          model_fn_func, 
                          H_fn_func, 
                          previous_data = None):
    if previous_data is None:
        data = np.zeros((9 + 3*M + 6 + 1, N_steps))
        N_realizations = 0
    else:
        N_realizations, data = previous_data
    
    while True:
        d_p = delta_filling(filling, M)

        fock_state = []
        for delta in d_p:
            fock_state += [int(delta)]

        fock_state += [0 for _ in range(M)]  #Assume initial state 
                                        #is generated with spin ups only
        N = sum(fock_state)

        if N == 0:
            N_realizations += 1
            data = (N_realizations-1)/N_realizations * data
        else:
            hamiltonian_file = H_fn_func(N,M)
            if hasattr(s, 'H'):
                del s.H
            s.H = sparse.load_npz(hamiltonian_file)
            model_file = model_fn_func(N,M)
            s.load_data(model_file)
            obs = [s.Sx, s.Sy, s.Sz, 
                   s.Sx*s.Sx, 
                   s.Sy*s.Sy, 
                   s.Sz*s.Sz, 
                   s.Sx*s.Sy + s.Sy*s.Sx, 
                   s.Sy*s.Sz + s.Sz*s.Sy, 
                   s.Sz*s.Sx + s.Sx*s.Sz
                  ]
            Sx, Sy, Sz       = [], [], []
            Sx2, Sy2, Sz2    = [], [], []
            SxSy, SySz, SzSx = [], [], []
            for j in range(s.M):
                Sx.append(s.Sx_j[j])
                Sy.append(s.Sy_j[j])
                Sz.append(s.Sz_j[j])
                Sx2.append(s.Sx_j[j] * s.Sx_j[j])
                Sy2.append(s.Sy_j[j] * s.Sy_j[j])
                Sz2.append(s.Sz_j[j] * s.Sz_j[j])
                SxSy.append(s.Sx_j[j] * s.Sy_j[j] + s.Sy_j[j] * s.Sx_j[j])
                SySz.append(s.Sy_j[j] * s.Sz_j[j] + s.Sz_j[j] * s.Sy_j[j])
                SzSx.append(s.Sz_j[j] * s.Sx_j[j] + s.Sx_j[j] * s.Sz_j[j])

            local_spin_obs = Sx + Sy + Sz 
            local_spin_obs += [sum(Sx2), sum(Sy2), sum(Sz2), sum(SxSy), sum(SySz), sum(SzSx)]
            obs += local_spin_obs
            
            corr_all = s.Sy_j[0] + 1j*s.Sz_j[0]
            for i in range(1,s.M):
                Sp = s.Sy_j[i] + 1j*s.Sz_j[i]
                corr_all *= Sp
                
            obs.append(corr_all)
     
            wf = np.zeros(s.D)
            ind = s.T[tuple(fock_state)]
            wf[ind] = 1
            wf = s.rotate((s.theta, s.phi), wf)

            t, data_single = s.evolve(wf, t_f, N_steps, obs)
            
            N_realizations += 1
            data = (N_realizations-1)/N_realizations * data
            data += 1/N_realizations * data_single
            
        yield N_realizations, t, data


def evolve_sampling_pickle(M, filling, t_f, N_steps, fn_func, previous_data = None):
    if previous_data is None:
        #data = np.zeros((len(obs), N_steps))
        data = np.zeros((9 + 3*M + 6 + 1, N_steps))
        N_realizations = 0
    else:
        N_realizations, data = previous_data
    
    while True:
        d_p = delta_filling(filling, M)

        fock_state = []
        for delta in d_p:
            fock_state += [int(delta)]

        fock_state += [0 for _ in range(M)]  #Assume initial state 
                                        #is generated with spin ups only
        N = sum(fock_state)

        if N == 0:
            N_realizations += 1
            data = (N_realizations-1)/N_realizations * data
        else:
            with open(fn_func(N,M), 'rb') as f:
                s = pickle.load(f)
            obs = [s.Sx, s.Sy, s.Sz, 
                   s.Sx*s.Sx, 
                   s.Sy*s.Sy, 
                   s.Sz*s.Sz, 
                   s.Sx*s.Sy + s.Sy*s.Sx, 
                   s.Sy*s.Sz + s.Sz*s.Sy, 
                   s.Sz*s.Sx + s.Sx*s.Sz
                  ]
            Sx, Sy, Sz       = [], [], []
            Sx2, Sy2, Sz2    = [], [], []
            SxSy, SySz, SzSx = [], [], []
            for j in range(s.M):
                Sx.append(s.Sx_j[j])
                Sy.append(s.Sy_j[j])
                Sz.append(s.Sz_j[j])
                Sx2.append(s.Sx_j[j] * s.Sx_j[j])
                Sy2.append(s.Sy_j[j] * s.Sy_j[j])
                Sz2.append(s.Sz_j[j] * s.Sz_j[j])
                SxSy.append(s.Sx_j[j] * s.Sy_j[j] + s.Sy_j[j] * s.Sx_j[j])
                SySz.append(s.Sy_j[j] * s.Sz_j[j] + s.Sz_j[j] * s.Sy_j[j])
                SzSx.append(s.Sz_j[j] * s.Sx_j[j] + s.Sx_j[j] * s.Sz_j[j])

            local_spin_obs = Sx + Sy + Sz 
            local_spin_obs += [sum(Sx2), sum(Sy2), sum(Sz2), sum(SxSy), sum(SySz), sum(SzSx)]
            obs += local_spin_obs
            
            corr_all = s.Sy_j[0] + 1j*s.Sz_j[0]
            for i in range(1,s.M):
                Sp = s.Sy_j[i] + 1j*s.Sz_j[i]
                corr_all *= Sp
                
            obs.append(corr_all)
     
            wf = np.zeros(s.D)
            ind = s.T[tuple(fock_state)]
            wf[ind] = 1
            wf = s.rotate((s.theta, s.phi), wf)

            t, data_single = s.evolve(wf, t_f, N_steps, obs)
            
            N_realizations += 1
            data = (N_realizations-1)/N_realizations * data
            data += 1/N_realizations * data_single
            
            del s

        yield N_realizations, t, data


def evolve_sampling(s, filling, t_f, N_steps, previous_data = None):
    obs = [s.Sx, s.Sy, s.Sz, 
           s.Sx*s.Sx, 
           s.Sy*s.Sy, 
           s.Sz*s.Sz, 
           s.Sx*s.Sy + s.Sy*s.Sx, 
           s.Sy*s.Sz + s.Sz*s.Sy, 
           s.Sz*s.Sx + s.Sx*s.Sz
          ]
    Sx, Sy, Sz       = [], [], []
    Sx2, Sy2, Sz2    = [], [], []
    SxSy, SySz, SzSx = [], [], []
    for j in range(s.M):
        Sx.append(s.Sx_j[j])
        Sy.append(s.Sy_j[j])
        Sz.append(s.Sz_j[j])
        Sx2.append(s.Sx_j[j] * s.Sx_j[j])
        Sy2.append(s.Sy_j[j] * s.Sy_j[j])
        Sz2.append(s.Sz_j[j] * s.Sz_j[j])
        SxSy.append(s.Sx_j[j] * s.Sy_j[j] + s.Sy_j[j] * s.Sx_j[j])
        SySz.append(s.Sy_j[j] * s.Sz_j[j] + s.Sz_j[j] * s.Sy_j[j])
        SzSx.append(s.Sz_j[j] * s.Sx_j[j] + s.Sx_j[j] * s.Sz_j[j])

    local_spin_obs = Sx + Sy + Sz 
    local_spin_obs += [sum(Sx2), sum(Sy2), sum(Sz2), sum(SxSy), sum(SySz), sum(SzSx)]
    obs += local_spin_obs
    
    corr_all = s.Sy_j[0] + 1j*s.Sz_j[0]
    for i in range(1,s.M):
        Sp = s.Sy_j[i] + 1j*s.Sz_j[i]
        corr_all *= Sp
        
    obs.append(corr_all)
    
    if previous_data is None:
        data = np.zeros((len(obs), N_steps))
        N_realizations = 0
    else:
        N_realizations, data = previous_data
    
    while True:
        wf = np.zeros(s.D)
        d_p = delta_filling(filling, s.M)

        fock_state = []
        for delta in d_p:
            fock_state += [int(delta)]

        fock_state += [0 for _ in range(s.M)]  #Assume initial state 
                                        #is generated with spin ups only

        ind = s.T[tuple(fock_state)]
        wf[ind] = 1
        wf = s.rotate((s.theta, s.phi), wf)

        t, data_single = s.evolve(wf, t_f, N_steps, obs)
        
        N_realizations += 1
        data = (N_realizations-1)/N_realizations * data
        data += 1/N_realizations * data_single
        
        yield N_realizations, t, data


def spin_squeezing_angle(Sx, Sy, Sz, Sx2, Sy2, Sz2, SxSy, SySz, SzSx):
    theta = np.arccos(Sz/np.sqrt(Sx**2+Sy**2+Sz**2)).real
    phi   = np.sign(Sy)*np.arccos(Sx/np.sqrt(Sx**2+Sy**2)).real

    Jx = np.cos(theta)*(np.cos(phi)*Sx + np.sin(phi)*Sy) - np.sin(theta)*Sz
    Jy = -np.sin(phi)*Sx + np.cos(phi)*Sy

    JxJy = (- np.sin(phi) * np.cos(theta) * np.cos(phi) * 2 * Sx2
            + np.cos(phi) * np.cos(theta) * np.sin(phi) * 2 * Sy2
            + ( np.cos(phi)**2 - np.sin(phi)**2 ) * np.cos(theta) * SxSy
            - np.cos(phi) * np.sin(theta) * SySz
            + np.sin(phi) * np.sin(theta) * SzSx
           )
    Jx2 = (  np.cos(theta)**2 * np.cos(phi)**2 * Sx2 
           + np.cos(theta)**2 * np.sin(phi)**2 * Sy2 
           + np.sin(theta)**2 * Sz2
           + np.cos(theta)**2 * np.cos(phi) * np.sin(phi) * SxSy
           - np.cos(theta) * np.sin(theta) * np.sin(phi) * SySz
           - np.cos(theta) * np.sin(theta) * np.cos(phi) * SzSx 
          )
    Jy2 = (  np.sin(phi)**2 * Sx2 
           + np.cos(phi)**2 * Sy2 
           - np.sin(phi) * np.cos(phi) * SxSy
          )
    
    delta_xy = JxJy - 2 * Jx*Jy 
    delta_x2 = Jx2 - Jx**2 
    delta_y2 = Jy2 - Jy**2 
    '''
    alpha = .5*np.arctan(delta_xy/(delta_x2-delta_y2)).real
    if not hasattr(alpha, '__len__'):
        if -np.sin(2*alpha)/delta_xy < 0:
            alpha += np.pi/2
    else:
        for i in range(len(alpha)):
            if -np.sin(2*alpha[i])/delta_xy[i] < 0:
                alpha[i] += np.pi/2
    '''
    alpha = .5*np.arccos(-(delta_x2-delta_y2)/np.sqrt((delta_x2-delta_y2)**2 + delta_xy**2)).real
            
    return theta, phi, alpha

    
def calc_squeezing_frame(M, exp_values, coords = None):
    # Collective ops structure:
    # Sx, Sy, Sz, Sx2, Sy2, Sz2, SxSy, SySz, SzSx
    coll_spin_evs = exp_values[:9]
    # Local ops structure:
    # Sx[M], Sy[M], Sz[M], Sx2, Sy2, Sz2, SxSy, SySz, SzSx
    local_spin_evs = exp_values[9:]
    
    if coords is None:
        theta, phi, alpha = spin_squeezing_angle(*coll_spin_evs)
    else:
        theta, phi, alpha = coords
    
    Sx, Sy, Sz, Sx2, Sy2, Sz2, SxSy, SySz, SzSx = coll_spin_evs
    
    A = np.cos(theta)*np.cos(phi)*np.cos(alpha) - np.sin(phi)*np.sin(alpha)
    B = np.cos(theta)*np.sin(phi)*np.cos(alpha) + np.cos(phi)*np.sin(alpha)
    C = np.sin(theta)*np.cos(alpha)
    
    if len(local_spin_evs):
        Sx_j = local_spin_evs[:M]
        Sy_j = local_spin_evs[M:2*M]
        Sz_j = local_spin_evs[2*M:3*M]
        Sx2_j, Sy2_j, Sz2_j, SxSy_j, SySz_j, SzSx_j = local_spin_evs[-6:]
        S_min2_off = ( A**2 * (Sx2 - Sx2_j)
                     + B**2 * (Sy2 - Sy2_j)
                     + C**2 * (Sz2 - Sz2_j)
                     + A*B * (SxSy - SxSy_j)
                     - B*C * (SySz - SySz_j)
                     - C*A * (SzSx - SzSx_j)
                     )  
        S_min = []
        
        for j in range(M):
            S_min.append(A * Sx_j[j] + B * Sy_j[j] - C * Sz_j[j])
    else:
        S_min = A * Sx + B * Sy - C * Sz
        S_min2_off = None

    S_min2 = ( A**2 * (Sx2)
             + B**2 * (Sy2)
             + C**2 * (Sz2)
             + A*B * (SxSy)
             - B*C * (SySz)
             - C*A * (SzSx)
             )
        
    S_avg = ( np.sin(theta)*np.cos(phi)*Sx
            + np.sin(theta)*np.sin(phi)*Sy
            + np.cos(theta)*Sz
            )

    return S_avg, S_min, S_min2_off, S_min2


def calc_squeezing_variances(exp_values):
    # Collective ops structure:
    # Sx, Sy, Sz, Sx2, Sy2, Sz2, SxSy, SySz, SzSx
    coll_spin_evs = exp_values[:9]
    
    theta, phi, alpha = spin_squeezing_angle(*coll_spin_evs)
    
    Sx, Sy, Sz, Sx2, Sy2, Sz2, SxSy, SySz, SzSx = coll_spin_evs
    
    A = np.cos(theta)*np.cos(phi)*np.cos(alpha) - np.sin(phi)*np.sin(alpha)
    B = np.cos(theta)*np.sin(phi)*np.cos(alpha) + np.cos(phi)*np.sin(alpha)
    C = np.sin(theta)*np.cos(alpha)
    
    S_min = A * Sx + B * Sy - C * Sz

    S_avg = ( np.sin(theta)*np.cos(phi)*Sx
            + np.sin(theta)*np.sin(phi)*Sy
            + np.cos(theta)*Sz
            )

    S_min2 = ( A**2 * (Sx2)
             + B**2 * (Sy2)
             + C**2 * (Sz2)
             + A*B * (SxSy)
             - B*C * (SySz)
             - C*A * (SzSx)
             )
    
    S_avg2 = ( (np.sin(theta)*np.cos(phi))**2 * Sx2
              + (np.sin(theta)*np.sin(phi))**2 * Sy2
              + np.cos(theta)**2 * Sz2
              + np.sin(theta)**2*np.sin(2*phi)/2 * SxSy
              + np.sin(2*theta)/2*np.sin(phi) * SySz
              + np.sin(2*theta)/2*np.cos(phi) * SzSx
            )

    var_avg = S_avg2 - S_avg**2
    var_min = S_min2 - S_min**2

    return S_avg, S_min, var_avg, var_min

def Bell_opt(M, exp_values):
    S_avg, S_min, S_min2_off, S_min2 = calc_squeezing_frame(M, exp_values)

    if S_min2_off is not None:
        A = np.real(S_min2_off) 
        A -= np.real(sum(S_min))**2
    else:
        A = np.real(S_min2) - np.real(S_min)**2

    B = S_avg.real

    if len(A) > 1:
        Bell = [a*(4+b**2/(4*a**2)) if abs(b/(4*a)) <= 1 and a < 0 else -2*abs(b) for (a,b) in zip(A,B)]
        Bell = np.array(Bell)
        Bell += M #N, actually
    else:
        Bell = A*(4+B**2/(4*A**2)) if abs(B/(4*A)) <= 1 and A < 0 else -2*abs(B)
        Bell += M #N, actually

    #if m2 is True:
    #    C = D = 0
    #    for i in range(self.M):
    #        C += expectation_value(wf, S_avg[i]**2).real
    #        D += expectation_value(wf, S_min[i]**2).real
    #    Bell = B**2/(A-C+D)/4 + 4*(A+D) if abs(B) <= 4*abs(A-C+D) and (A-C+D) < 0 else (-2*abs(B) + 4*C)

    return Bell

def Bell_exact(M, N_theta, exp_values):
    S_avg, S_min, S_min2_off, S_min2 = calc_squeezing_frame(M, exp_values)
 
    if S_min2_off is not None:
        A = np.real(S_min2_off) 
        A -= np.real(sum(S_min))**2
    else:
        A = np.real(S_min2) - np.real(S_min)**2
    
    B = S_avg.real

    Bell = []
    for a,b in zip(A,B): 
        bell = None
        for theta in np.linspace(0, 2*np.pi, N_theta):
            bell_theta = a*4*np.sin(theta)**2 - b*2*np.cos(theta) + M
            if bell is None:
                bell = bell_theta
            else:
                bell = min(bell_theta, bell)

        Bell.append(bell)
    Bell = np.array(Bell)

    return Bell

def combine_separable_states_ev(evs):
    '''
    Combine separable states expectation values to obtain collective expectation values is SU(2) symmetry.
    The assumed structure is Sx, Sy, Sz, Sx2, Sy2, Sz2, SxSy + SySx, SySz + SzSy, SzSx + SxSz.
    '''
    L = len(evs[0])   
    obs = []
    for i in range(min(3, L)):
        a = sum([evs_n[i] for evs_n in evs])
        obs.append(a)

    for i in range(3, min(6, L)):
        j = i-3
        a = obs[j]
        var = sum([evs_n[i] - evs_n[j]**2 for evs_n in evs])
        a2 = a**2 + var
        obs.append(a2)

    for i in range(6, min(9, L)):
        j = i-6
        k = (i-6+1)%3
        a = obs[j]
        b = obs[k]
        var = sum([evs_n[i] - 2*evs_n[j]*evs_n[k] for evs_n in evs])
        ab = a*b + b*a + var
        obs.append(ab)

    return np.array(obs)

def p_l(l, l_0, L, q):
    return np.cos( np.pi/L*q*(l - (l_0-.5)) ) 
     
def evs_OAT(S, chi, v, t):
    ones = np.ones(t.shape)
    zeros = np.zeros(t.shape)
    Sx_OAT = S * np.cos(chi*t)**(2*S-1)
    Sx2_OAT = S/4*((2*S-1)*np.cos(2*chi*t)**(2*S-2) + (2*S+1))
    Sy2_OAT = -S/4*((2*S-1)*np.cos(2*chi*t)**(2*S-2) - (2*S+1))
    Sz2_OAT = S/2*ones
    SxSy_OAT = zeros
    SySz_OAT = S*(2*S-1)*np.cos(chi*t)**(2*S-2)*np.sin(chi*t)
    SzSx_OAT = zeros
    
    Sx = np.cos(v*t)* Sx_OAT
    Sy = np.sin(v*t)* Sx_OAT
    Sz = zeros
    Sx2 = .5*(1+np.cos(2*v*t))*Sx2_OAT + .5*(1-np.cos(2*v*t))*Sy2_OAT
    #Sy2 = .5*(1+np.cos(2*v*t))*Sy2_OAT - .5*(1-np.cos(2*v*t))*Sx2_OAT
    Sy2 = .5*(1+np.cos(2*v*t))*Sy2_OAT + .5*(1-np.cos(2*v*t))*Sx2_OAT
    Sz2 = Sz2_OAT
    #SxSy = (np.cos(v*t)**2 - np.sin(v*t)**2)*SxSy_OAT
    SxSy = np.cos(2*v*t)*SxSy_OAT + np.sin(2*v*t)*(Sx2_OAT - Sy2_OAT)
    SySz = np.cos(v*t)*SySz_OAT + np.sin(v*t)*SzSx_OAT
    SzSx = np.cos(v*t)*SzSx_OAT - np.sin(v*t)*SySz_OAT

    return np.array((Sx, Sy, Sz, Sx2, Sy2, Sz2, SxSy, SySz, SzSx))
   
def evs_chain(l_n, L_n, J, beta, t, S = None):
    if S is None:
        S = L_n/2
    chi = 0
    if L_n > 1: #check if this makes sense
        for q in range(1, L_n):
            E_q = -J*(1-np.cos(np.pi/L_n*q))
            c_q = 0
            for l in range(l_n, l_n + L_n):
                c_q += np.sqrt(2)/L_n*p_l(l, l_n, L_n, q) * beta[l]
                        
            chi += abs(c_q)**2 / E_q
        chi /= (L_n-1)
        chi *= -1
    #v = sum([beta[l] for l in range(l_n, l_n + L_n)])/L_n
    v = beta[l_n: l_n+L_n].sum()/L_n
    
    Sx_OAT = S * np.cos(chi*t)**(2*S-1)
    Sx2_OAT = S/4*((2*S-1)*np.cos(2*chi*t)**(2*S-2) + (2*S+1))
    Sy2_OAT = -S/4*((2*S-1)*np.cos(2*chi*t)**(2*S-2) - (2*S+1))
    Sz2_OAT = S/2*np.ones(len(t))
    SxSy_OAT = np.zeros(len(t))
    SySz_OAT = S*(2*S-1)*np.cos(chi*t)**(2*S-2)*np.sin(chi*t)
    SzSx_OAT = np.zeros(len(t))
    
    Sx = np.cos(v*t)* Sx_OAT
    Sy = np.sin(v*t)* Sx_OAT
    Sz = np.zeros(len(t))
    Sx2 = .5*(1+np.cos(2*v*t))*Sx2_OAT + .5*(1-np.cos(2*v*t))*Sy2_OAT
    #Sy2 = .5*(1+np.cos(2*v*t))*Sy2_OAT - .5*(1-np.cos(2*v*t))*Sx2_OAT
    Sy2 = .5*(1+np.cos(2*v*t))*Sy2_OAT + .5*(1-np.cos(2*v*t))*Sx2_OAT
    Sz2 = Sz2_OAT
    #SxSy = (np.cos(v*t)**2 - np.sin(v*t)**2)*SxSy_OAT
    SxSy = np.cos(2*v*t)*SxSy_OAT + np.sin(2*v*t)*(Sx2_OAT - Sy2_OAT)
    SySz = np.cos(v*t)*SySz_OAT + np.sin(v*t)*SzSx_OAT
    SzSx = np.cos(v*t)*SzSx_OAT - np.sin(v*t)*SySz_OAT

    return np.array((Sx, Sy, Sz, Sx2, Sy2, Sz2, SxSy, SySz, SzSx))

def evs_chain_ani(l_n, L_n, J, U, t, S = None):
    c = 4 * J ** 2 * (2/U[2] - 1/U[0] - 1/U[1])
    if S is None:
        S = L_n/2
    chi = 0
    if L_n > 1: #check if this makes sense
        chi = c / (L_n - 1)

    Sx_OAT = S * np.cos(chi*t)**(2*S-1)
    Sx2_OAT = S/4*((2*S-1)*np.cos(2*chi*t)**(2*S-2) + (2*S+1))
    Sy2_OAT = -S/4*((2*S-1)*np.cos(2*chi*t)**(2*S-2) - (2*S+1))
    Sz2_OAT = S/2*np.ones(len(t))
    SxSy_OAT = np.zeros(len(t))
    SySz_OAT = S*(2*S-1)*np.cos(chi*t)**(2*S-2)*np.sin(chi*t)
    SzSx_OAT = np.zeros(len(t))

    Sx = Sx_OAT
    Sy = np.zeros(len(t))
    Sz = np.zeros(len(t))
    Sx2 = Sx2_OAT
    Sy2 = Sy2_OAT
    Sz2 = Sz2_OAT
    SxSy = SxSy_OAT
    SySz = SySz_OAT
    SzSx = SzSx_OAT

    return np.array((Sx, Sy, Sz, Sx2, Sy2, Sz2, SxSy, SySz, SzSx))

def gen_states_with_holes(N, M):
    assert N <= M
    L = M
    holes = M-N
    v = [1 for _ in range(N)] + [0 for _ in range(holes)]
    yield v
    k=N-1
    while sum(v[holes:]) < N:
        if v[k] > 0 and k < M-sum(v[k:]):
            for i in range(k, M):
                v[i] = 0
            N_left = N - sum(v)
            j = 1
            while N_left > 0:
                l = k+j
                if v[l] == 0:
                    v[l] = 1
                    N_left -= 1
                j += 1
            yield v
            k += j-1
        else:
            k -= 1
    
def evs_ensemble_holes(f, M, max_holes = None, func = None, **kwargs):
    if func is None:
        func = evs_chain
    if max_holes is None:
        max_holes = M-1

    delta = (1-f)/f
    result = 0
    norm = 0
    for sigma in range(max_holes+1):
        for v in gen_states_with_holes(M-sigma, M):
            j = 0
            chains = []
            for i in range(M):
                if v[i] == 0:
                    L = i-j
                    if L > 0:
                        chains.append(func(j, L, **kwargs))
                    j = i + 1
            L = M-j 
            if L > 0:
                chains.append(func(j, L, **kwargs))

            result += (delta ** sigma) * combine_separable_states_ev(chains)
            
            norm += (delta ** sigma) 
    #result *= (f)**M
    #norm *= (f)**M
    
    return result/norm

def ensemble_f(M, max_holes = None, func = None, **kwargs):
    if func is None:
        func = evs_chain
    if max_holes is None:
        max_holes = M-1

    results = []
    norms = []
    for sigma in range(max_holes+1):
        result = 0
        norm = 0
        for v in gen_states_with_holes(M-sigma, M):
            j = 0
            chains = []
            for i in range(M):
                if v[i] == 0:
                    L = i-j
                    if L > 0:
                        chains.append(func(j, L, **kwargs))
                    j = i + 1
            L = M-j 
            if L > 0:
                chains.append(func(j, L, **kwargs))

            result += combine_separable_states_ev(chains)
            norm += 1
        #result *= (f)**M
        #norm *= (f)**M
        results.append(result)
        norms.append(norm)

    def calc_evs(f):
        evs = 0
        delta = (1-f)/f
        total_norm = 0 
        for sigma in range(max_holes+1):
            total_norm += norms[sigma] * (delta ** sigma)
            evs += (delta ** sigma) * results[sigma]
        
        return evs/total_norm

    return calc_evs

def L_simple(M, filling, evs):
    S_mean, S_sq, _, S_sq2 = calc_squeezing_frame(M, evs)
    Var = S_sq2 - (S_sq)**2
    result = []
    for (i,(a,b)) in enumerate(zip(Var, S_mean)):
        r = M - b**2 / ((M - 4*a)) - filling**2 * (M-4*a)
        if abs(1/filling * b / (4*a-M)) <= 1 and filling**2*(4*a - M) < 0:
            result.append(r)
        else:
            result.append(M - 2 * filling * abs(b))
    return result

def L_corrected(M, filling, evs):
    S_mean, S_sq, _, S_sq2 = calc_squeezing_frame(M, evs)
    Var = S_sq2 - (S_sq)**2
    result = []
    for (i,(a,b)) in enumerate(zip(Var, S_mean)):
        #if abs(b) <= abs(M*filling - 4*a) and M*filling - 4*a > 0:
        if abs(b) <= abs(M*filling - 4*a): # Absolute value needed for numerical edge cases
            r = M - b**2 / (M * filling - 4*a) - (M * filling - 4*a)
            result.append(r)
        else:
            result.append(M - 2 * abs(b))
    return result
