import numpy as np
from scipy import sparse

class LocalOpMixin:
    def N_j_calc(self):
        N_j = []
        for j in range(self.M):
            data = np.empty(self.D)
            for k in range(self.D):
                v = self.A[k]
                data[k] = sum([v[self.M * l + j] for l in range(self._n_levels)])
            n_j = sparse.dia_matrix((data, 0), shape=(self.D, self.D), dtype=np.complex128).tocsc()
            N_j.append(n_j)

        self.N_j = N_j 

    def condensate_fraction(self):
        # To implement: discern between bosons and fermions to respect (anti)commutator relations
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for s in range(self._n_levels):
                s *= self.M
                for i in range(self.M):
                    for j in range(self.M):
                        if v[j+s] >= 1:
                            u = list(v)
                            u[j+s] -= 1
                            u[i+s] += 1
                            r = self.T[tuple(u)]
                            f_ij =  np.sqrt(u[i+s] * v[j+s])
                            f_ij /= self.N*self.M
                            data.append(f_ij)
                            row.append(r)
                            col.append(k)

        return sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)
