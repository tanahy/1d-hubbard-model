import numpy as np
from scipy import sparse

class BoseSpinOpMixin:
    def S_calc(self):
        Sp = [] #a_dagga_up[i] * a_down[i]
        Sm = [] #a_dagga_down[i] * a_up[i]
        Nu = [] #a_dagga_up[i] * a_up[i]
        Nd = [] #a_dagga_down[i] * a_down[i]

        for k in range(self.D):
            v = self.A[k]
            for j in range(self.M):
                if v[j] >= 1:
                    u = list(v)
                    u[j] -= 1
                    u[j+self.M] += 1
                    r = self.T[tuple(u)]
                    n3 = v[j]
                    n2 = np.sqrt(u[j+self.M] * v[j])
                    Nu.append([n3,k,k])
                    Sm.append([n2,r,k])
                
                if v[j + self.M] >= 1:
                    u = list(v)
                    u[j + self.M] -= 1
                    u[j] += 1
                    r = self.T[tuple(u)]
                    n4 = v[j+self.M]
                    n1 = np.sqrt(u[j] * v[j+self.M])
                    Nd.append([n4,k,k])
                    Sp.append([n1,r,k])

        Sp = np.array(Sp)
        Sm = np.array(Sm)
        Nu = np.array(Nu)
        Nd = np.array(Nd)

        Sp   = sparse.csc_matrix((Sp[:,0], (Sp[:,1], Sp[:,2])), shape=(self.D, self.D), dtype=np.complex128)
        Sm   = sparse.csc_matrix((Sm[:,0], (Sm[:,1], Sm[:,2])), shape=(self.D, self.D), dtype=np.complex128)
        Nu   = sparse.csc_matrix((Nu[:,0], (Nu[:,1], Nu[:,2])), shape=(self.D, self.D), dtype=np.complex128)
        Nd   = sparse.csc_matrix((Nd[:,0], (Nd[:,1], Nd[:,2])), shape=(self.D, self.D), dtype=np.complex128)

        if self.frame == 'lab':
            self.Sp = Sp
            self.Sm = Sm
            self.Sx = .5*(Sp+Sm)
            self.Sy = -.5j*(Sp-Sm)
            self.Sz = .5*(Nu-Nd)
        elif self.frame == 'rotated':
            self.Jp = Sp
            self.Jm = Sm
            self.Jx = .5*(Sp+Sm)
            self.Jy = -.5j*(Sp-Sm)
            self.Jz = .5*(Nu-Nd)
    # J in rotated frame is calculated as S in lab frame
    J_rot_calc = S_calc
 
    def S_j_calc(self):
        # S calc but per lattice site
        Sp_j = []
        Sm_j = []
        Sx_j = []
        Sy_j = []
        Sz_j = []

        for j in range(self.M):
            Sp = [] #a_dagga_up[i] * a_down[i]
            Sm = [] #a_dagga_down[i] * a_up[i]
            Nu = [] #a_dagga_up[i] * a_up[i]
            Nd = [] #a_dagga_down[i] * a_down[i]
            for k in range(self.D):
                v = self.A[k]
                if v[j] >= 1:
                    u = list(v)
                    u[j] -= 1
                    u[j+self.M] += 1
                    r = self.T[tuple(u)]
                    n3 = v[j]
                    n2 = np.sqrt(u[j+self.M] * v[j])
                    Nu.append([n3,k,k])
                    Sm.append([n2,r,k])
                
                if v[j + self.M] >= 1:
                    u = list(v)
                    u[j + self.M] -= 1
                    u[j] += 1
                    r = self.T[tuple(u)]
                    n4 = v[j+self.M]
                    n1 = np.sqrt(u[j] * v[j+self.M])
                    Nd.append([n4,k,k])
                    Sp.append([n1,r,k])

            Sp = np.array(Sp)
            Sm = np.array(Sm)
            Nu = np.array(Nu)
            Nd = np.array(Nd)
            if len(Nu) == len(Nd) == 0:
                Sp = np.zeros((2,3))
                Sm = np.zeros((2,3))
                Nu = np.zeros((2,3))
                Nd = np.zeros((2,3))
            
            Sp   = sparse.csc_matrix((Sp[:,0], (Sp[:,1], Sp[:,2])), shape=(self.D, self.D), dtype=np.complex128)
            Sm   = sparse.csc_matrix((Sm[:,0], (Sm[:,1], Sm[:,2])), shape=(self.D, self.D), dtype=np.complex128)
            Nu   = sparse.csc_matrix((Nu[:,0], (Nu[:,1], Nu[:,2])), shape=(self.D, self.D), dtype=np.complex128)
            Nd   = sparse.csc_matrix((Nd[:,0], (Nd[:,1], Nd[:,2])), shape=(self.D, self.D), dtype=np.complex128)

            Sx = .5*(Sp+Sm)
            Sy = -.5j*(Sp-Sm)
            Sz = .5*(Nu-Nd)
        
            Sp_j.append(Sp)
            Sm_j.append(Sm)
            Sx_j.append(Sx)
            Sy_j.append(Sy)
            Sz_j.append(Sz)

        self.Sp_j = np.array(Sp_j)
        self.Sm_j = np.array(Sm_j)
        self.Sx_j = np.array(Sx_j)
        self.Sy_j = np.array(Sy_j)
        self.Sz_j = np.array(Sz_j)

class FermiSpinOpMixin:
    def S_calc(self):
        Sp = [] #a_dagga_up[i] * a_down[i]
        Sm = [] #a_dagga_down[i] * a_up[i]
        Nu = [] #a_dagga_up[i] * a_up[i]
        Nd = [] #a_dagga_down[i] * a_down[i]

        for k in range(self.D):
            v = self.A[k]
            for j in range(self.M):
                if v[j] == 1:
                    u = list(v)
                    u[j] -= 1
                    op_permutation = (-1)**sum(v[:j])
                    Nu.append([1,k,k])
                    if u[j+self.M] == 0:
                        u[j+self.M] += 1
                        r = self.T[tuple(u)]
                        op_permutation *= (-1)**sum(v[self.M:j+self.M])
                        Sm.append([op_permutation,r,k])
                
                if v[j + self.M] == 1:
                    u = list(v)
                    u[j + self.M] -= 1
                    op_permutation = (-1)**sum(v[self.M:j + self.M])
                    Nd.append([1,k,k])
                    if u[j] == 0:
                        u[j] += 1
                        r = self.T[tuple(u)]
                        op_permutation *= (-1)**sum(v[:j])
                        Sp.append([op_permutation,r,k])

        Sp = np.array(Sp)
        Sm = np.array(Sm)
        Nu = np.array(Nu)
        Nd = np.array(Nd)

        Sp   = sparse.csc_matrix((Sp[:,0], (Sp[:,1], Sp[:,2])), shape=(self.D, self.D), dtype=np.complex128)
        Sm   = sparse.csc_matrix((Sm[:,0], (Sm[:,1], Sm[:,2])), shape=(self.D, self.D), dtype=np.complex128)
        Nu   = sparse.csc_matrix((Nu[:,0], (Nu[:,1], Nu[:,2])), shape=(self.D, self.D), dtype=np.complex128)
        Nd   = sparse.csc_matrix((Nd[:,0], (Nd[:,1], Nd[:,2])), shape=(self.D, self.D), dtype=np.complex128)

        if self.frame == 'lab':
            self.Sp = Sp
            self.Sm = Sm
            self.Sx = .5*(Sp+Sm)
            self.Sy = -.5j*(Sp-Sm)
            self.Sz = .5*(Nu-Nd)
        elif self.frame == 'rotated':
            self.Jp = Sp
            self.Jm = Sm
            self.Jx = .5*(Sp+Sm)
            self.Jy = -.5j*(Sp-Sm)
            self.Jz = .5*(Nu-Nd)
    # J in rotated frame is calculated as S in lab frame
    J_rot_calc = S_calc

    def S_j_calc(self):
        # S calc but per lattice site
        Sp_j = []
        Sm_j = []
        Sx_j = []
        Sy_j = []
        Sz_j = []

        for j in range(self.M):
            Sp = [] #a_dagga_up[i] * a_down[i]
            Sm = [] #a_dagga_down[i] * a_up[i]
            Nu = [] #a_dagga_up[i] * a_up[i]
            Nd = [] #a_dagga_down[i] * a_down[i]
            for k in range(self.D):
                v = self.A[k]
                if v[j] == 1:
                    u = list(v)
                    u[j] -= 1
                    op_permutation = (-1)**sum(v[:j])
                    Nu.append([1,k,k])
                    if u[j+self.M] == 0:
                        u[j+self.M] += 1
                        r = self.T[tuple(u)]
                        op_permutation *= (-1)**sum(v[self.M:j+self.M])
                        Sm.append([op_permutation,r,k])
                
                if v[j + self.M] == 1:
                    u = list(v)
                    u[j + self.M] -= 1
                    op_permutation = (-1)**sum(v[self.M:j + self.M])
                    Nd.append([1,k,k])
                    if u[j] == 0:
                        u[j] += 1
                        r = self.T[tuple(u)]
                        op_permutation *= (-1)**sum(v[:j])
                        Sp.append([op_permutation,r,k])

            Sp = np.array(Sp)
            Sm = np.array(Sm)
            Nu = np.array(Nu)
            Nd = np.array(Nd)
            if len(Nu) == len(Nd) == 0:
                Sp = np.zeros((2,3))
                Sm = np.zeros((2,3))
                Nu = np.zeros((2,3))
                Nd = np.zeros((2,3))

            Sp   = sparse.csc_matrix((Sp[:,0], (Sp[:,1], Sp[:,2])), shape=(self.D, self.D), dtype=np.complex128)
            Sm   = sparse.csc_matrix((Sm[:,0], (Sm[:,1], Sm[:,2])), shape=(self.D, self.D), dtype=np.complex128)
            Nu   = sparse.csc_matrix((Nu[:,0], (Nu[:,1], Nu[:,2])), shape=(self.D, self.D), dtype=np.complex128)
            Nd   = sparse.csc_matrix((Nd[:,0], (Nd[:,1], Nd[:,2])), shape=(self.D, self.D), dtype=np.complex128)

            Sx = .5*(Sp+Sm)
            Sy = -.5j*(Sp-Sm)
            Sz = .5*(Nu-Nd)
        
            Sp_j.append(Sp)
            Sm_j.append(Sm)
            Sx_j.append(Sx)
            Sy_j.append(Sy)
            Sz_j.append(Sz)

        if self.frame == 'lab':
            self.Sp_j = np.array(Sp_j)
            self.Sm_j = np.array(Sm_j)
            self.Sx_j = np.array(Sx_j)
            self.Sy_j = np.array(Sy_j)
            self.Sz_j = np.array(Sz_j)
        elif self.frame == 'rotated':
            self.Jp_j = np.array(Sp_j)
            self.Jm_j = np.array(Sm_j)
            self.Jx_j = np.array(Sx_j)
            self.Jy_j = np.array(Sy_j)
            self.Jz_j = np.array(Sz_j)

    # J in rotated frame is calculated as S in lab frame
    J_rot_calc = S_calc
    
    def S_rot_calc(self):
        b1 = [] #e^(-i * j * phi) * b_dagga_up[j] * b_down[j]
        b2 = [] #e^(i * j * phi) * b_dagga_down[j] * b_up[j]
        b3 = [] #b_dagga_up[j] * b_up[j]
        b4 = [] #b_dagga_down[j] * b_down[j]

        for k in range(self.D):
            v = self.A[k]
            for j in range(self.M):
                if v[j] == 1:
                    u = list(v)
                    u[j] -= 1
                    op_permutation = (-1)**sum(v[:j])
                    b3.append([1,k,k])
                    if u[j+self.M] == 0:
                        u[j+self.M] += 1
                        r = self.T[tuple(u)]
                        op_permutation *= (-1)**sum(v[self.M:j+self.M])
                        b2.append([np.exp(1j * j * self.phi) * op_permutation,r,k])
                
                if v[j + self.M] == 1:
                    u = list(v)
                    u[j + self.M] -= 1
                    op_permutation = (-1)**sum(v[self.M:j + self.M])
                    b4.append([1,k,k])
                    if u[j] == 0:
                        u[j] += 1
                        r = self.T[tuple(u)]
                        op_permutation *= (-1)**sum(v[:j])
                        b1.append([np.exp(-1j * j * self.phi) * op_permutation,r,k])

        b1 = np.array(b1)
        b2 = np.array(b2)
        b3 = np.array(b3)
        b4 = np.array(b4)

        b1   = sparse.csc_matrix((b1[:,0], (b1[:,1].real, b1[:,2].real)), 
                shape=(self.D, self.D), dtype=np.complex128)
        b2   = sparse.csc_matrix((b2[:,0], (b2[:,1].real, b2[:,2].real)), 
                shape=(self.D, self.D), dtype=np.complex128)
        b3   = sparse.csc_matrix((b3[:,0], (b3[:,1].real, b3[:,2].real)), 
                shape=(self.D, self.D), dtype=np.complex128)
        b4   = sparse.csc_matrix((b4[:,0], (b4[:,1].real, b4[:,2].real)), 
                shape=(self.D, self.D), dtype=np.complex128)

        self.Sx = .5*(b1+b2)
        self.Sy = -.5j*(b1-b2)
        self.Sz = .5*(b3-b4)
        
    def J_calc(self):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for j in range(self.M):
                for s in (self.M, 0):
                    if s == self.M:
                        phase_factor = np.exp(1j*self.phi*j)
                    else:
                        phase_factor = np.exp(-1j*self.phi*j)
                        
                    if v[j+s] == 1:
                        u = list(v)
                        op_permutation = (-1)**sum(v[s:s+j])
                        u[j+s] -= 1
                        if u[j+s-self.M] == 0:
                            op_permutation *= (-1)**sum(v[s-self.M:j+s-self.M])
                            u[j+s-self.M] += 1
                            J_xj = .5 * phase_factor * op_permutation
                            r = self.T[tuple(u)]
                            data.append(J_xj)
                            row.append(r)
                            col.append(k)
                            
        self.Jx = sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)
        
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            for j in range(self.M):
                for s in (self.M, 0):
                    if s == self.M:
                        phase_factor = np.exp(1j*self.phi*j)
                    else:
                        phase_factor = -np.exp(-1j*self.phi*j)
                        
                    if v[j+s] == 1:
                        u = list(v)
                        op_permutation = (-1)**sum(v[s:s+j])
                        u[j+s] -= 1
                        if u[j+s-self.M] == 0:
                            op_permutation *= (-1)**sum(v[s-self.M:j+s-self.M])
                            u[j+s-self.M] += 1
                            J_yj = -.5j * phase_factor * op_permutation
                            r = self.T[tuple(u)]
                            data.append(J_yj)
                            row.append(r)
                            col.append(k)
                            
        self.Jy = sparse.csc_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)
        self.Jz = self.Sz

class TotalSpinOpMixin:
    def S_calc(self):
        Ip, Jp, Vp = [], [], []
        Im, Jm, Vm = [], [], []
        Sz = []
        for k in range(self.D):
            v = self.A[k]
            S = v[0]
            m = v[1]
            Sz.append(m)
            if S >= m+1:
                r = self.T[(S, m+1)]
                Ip.append(r)
                Jp.append(k)
                Vp.append(np.sqrt( S*(S+1) - m*(m+1) ))
            if -S <= m-1:
                r = self.T[(S, m-1)]
                Im.append(r)
                Jm.append(k)
                Vm.append(np.sqrt( S*(S+1) - m*(m-1) ))

        self.Sp = sparse.csc_matrix((Vp, (Ip, Jp)), shape=(self.D, self.D), dtype=np.complex128)
        self.Sm = sparse.csc_matrix((Vm, (Im, Jm)), shape=(self.D, self.D), dtype=np.complex128)
        self.Sz = sparse.dia_matrix((Sz, 0), shape=(self.D, self.D), dtype=np.complex128).tocsc()

        self.Sx = .5*(self.Sp+self.Sm)
        self.Sy = -.5j*(self.Sp-self.Sm)
