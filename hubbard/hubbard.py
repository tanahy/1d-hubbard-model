import warnings
import os
import timeit
from math import prod

import numpy as np
from scipy import sparse
from scipy.sparse import linalg
from scipy.sparse.linalg import expm, expm_multiply
try:
    from quspin.tools.evolution import expm_multiply_parallel
except:
    expm_multiply_parallel = expm_multiply 
import itertools as it

from .spin_op import BoseSpinOpMixin, FermiSpinOpMixin, TotalSpinOpMixin
from .hamiltonians import HBoseMixin, HFermiMixin, HBose2cMixin, HFockSpinMixin, HSpinMixin
from .operators import LocalOpMixin

###############################################################
# Based loosely on
# https://iopscience.iop.org/article/10.1088/0143-0807/31/3/016
###############################################################

def expectation_value(wf, A):
    #expectation  = np.inner(np.conj(norm_psi).T, np.matmul(A,norm_psi) ) 
    L = len(np.shape(wf))
    if L == 1:
        value = np.conj(wf).dot(A.dot(wf))
    elif L == 2:
        rho = wf #density matrix case
        B = A.dot(rho)
        value = B.trace()
    else:
        raise ValueError(f'Expected the first parameter to have dimensions 1 or 2, but got {L}.')
    if abs(np.imag(value)) > 1e-8:
        warnings.warn(f'Expectation value imaginary part is non negligible: {np.imag(value)}')
    return value

def variance(wf, A):
    #return np.sqrt(expectation_value(wf, A.dot(A)).real - expectation_value(wf, A).real**2)
    return expectation_value(wf, A.dot(A)) - expectation_value(wf, A)**2

def sparse_to_dict(arrays_dict, matrix, label):
    if matrix.format in ('csc', 'csr', 'bsr'):
        arrays_dict.update({f'{label}_indices':matrix.indices, f'{label}_indptr':matrix.indptr})
    elif matrix.format == 'dia':
        arrays_dict.update({f'{label}_offsets':matrix.offsets})
    elif matrix.format == 'coo':
        arrays_dict.update({f'{label}_row':matrix.row, f'{label}_col':matrix.col})
    else:
        raise NotImplementedError(f'Save is not implemented for sparse matrix of format {matrix.format}.')
    arrays_dict.update(
            {f'{label}_format':matrix.format.encode('ascii'),
             f'{label}_shape':matrix.shape,
             f'{label}_data':matrix.data
            }
            )
    
    return arrays_dict

def dict_to_sparse(arrays_dict, label):
            matrix_format = arrays_dict[f'{label}_format']
            matrix_format = matrix_format.item()
            
            if not isinstance(matrix_format, str):
                # Play safe with Python 2 vs 3 backward compatibility;
                # files saved with SciPy < 1.0.0 may contain unicode or bytes.
                matrix_format = matrix_format.decode('ascii')
            
            try:
                cls = getattr(sparse, f'{matrix_format}_matrix')
            except AttributeError as e:
                raise ValueError(f'Unknown matrix format "{matrix_format}"') from e

            if matrix_format in ('csc', 'csr', 'bsr'):
                return cls((arrays_dict[f'{label}_data'], arrays_dict[f'{label}_indices'], 
                            arrays_dict[f'{label}_indptr']), shape=arrays_dict[f'{label}_shape'])
            elif matrix_format == 'dia':
                return cls((arrays_dict[f'{label}_data'], arrays_dict[f'{label}_offsets']), 
                           shape=arrays_dict[f'{label}_shape'])
            elif matrix_format == 'coo':
                return cls((arrays_dict[f'{label}_data'], 
                            (arrays_dict[f'{label}_row'], arrays_dict[f'{label}_col'])
                            ), 
                           shape=arrays_dict[f'{label}_shape'])
            else:
                raise NotImplementedError('Load is not implemented for '
                                          'sparse matrix of format {}.'.format(matrix_format))


class BaseModel:
    
    def __init__(self, N, M, basis = 'n', bc = 'periodic', calc_scheme = 'default'):
        self.N = N
        self.M = M
        if hasattr(M, '__iter__'):
            self.M_d = M
            self.M = prod(M) 
        self.frame = 'lab'
        bc = bc.lower()
        self.bc = bc 
        basis = basis.lower()
        self.basis_name = basis
        self._scheme = calc_scheme
        self._spin_op = 'S'
        self._n_levels = 1
        #Eigenbasis
        self.A = []        
        #Coordinates for the rotated frame
        self.theta = None
        self.phi = None
        self.alpha = .5

        self.H = None
        self.label = None

    def n_single_basis(self):
        assert self.N <= self.M
        L = self._n_levels * self.M
        self.A = []
        holes = L-self.N
        v = [1 for _ in range(self.N)] + [0 for _ in range(holes)]
        self.A.append(tuple(v))
        k=self.N-1
        #n = [v[i] + v[i+self.M] for i in range(self.M)]
        n = [sum([v[i+s*self.M] for s in range(self._n_levels)]) for i in range(self.M)]
        while sum(v[holes:]) < self.N:
            if v[k] > 0 and k < L-sum(n[k % self.M:]):
                for i in range(k, L):
                    v[i] = 0
                n = [sum([v[i+s*self.M] for s in range(self._n_levels)]) for i in range(self.M)]
                N_left = self.N - sum(n)
                j = 1
                while N_left > 0:
                    l = (k+j) % self.M
                    if n[l] == 0:
                        n[l] = 1
                        v[k+j] = 1
                        N_left -= 1
                    j += 1
                self.A.append(tuple(v))
                k += j-1
            else:
                k -= 1
        
        self.A = list(self.A)
        self.D = len(self.A)
        self.T = {v: tag for tag, v in enumerate(self.A)} 

    def save_basis(self, filename):
        basis_array = np.array(self.A)
        np.save(filename, basis_array)

    def load_basis(self, basis):
        if isinstance(basis, str):
            basis = np.load(basis)
        self.A = [tuple(v) for v in basis]
        self.D = len(self.A)
        self.T = {v: tag for tag, v in enumerate(self.A)} 

    def extend_basis(self, new_basis):
        if isinstance(new_basis, str):
            new_basis = np.load(new_basis)
            new_basis = [tuple(v) for v in new_basis]
        self.A += new_basis
        self.D = len(self.A)
        self.T = {v: tag for tag, v in enumerate(self.A)} 

    def save_data(self, file, compressed = True):
        data_dict = {}
        basis_array = np.array(self.A)
        data_dict.update(basis = basis_array)
        if hasattr(self, 'Sx'):
            sparse_to_dict(data_dict, self.Sx, 'Sx')
            sparse_to_dict(data_dict, self.Sy, 'Sy')
            sparse_to_dict(data_dict, self.Sz, 'Sz')
        if hasattr(self, 'Sx_j'):
            for j in range(self.M):
                sparse_to_dict(data_dict, self.Sx_j[j], f'Sx_j{j}')
                sparse_to_dict(data_dict, self.Sy_j[j], f'Sy_j{j}')
                sparse_to_dict(data_dict, self.Sz_j[j], f'Sz_j{j}')
        if hasattr(self, 'N_j'):
            for j in range(self.M):
                sparse_to_dict(data_dict, self.N_j[j], f'N_j{j}')
 
        if compressed:
            np.savez_compressed(file, **data_dict)
        else:
            np.savez(file, **data_dict)

    def load_data(self, file):
        if hasattr(self, 'A'):
            del self.A, self.T
        with np.load(file, allow_pickle=False) as loaded:
            self.load_basis(loaded['basis'])
            if 'Sx_format' in loaded.keys():
                if hasattr(self, 'Sx'):
                    del self.Sx, self.Sy, self.Sz
                self.Sx = dict_to_sparse(loaded, 'Sx')
                self.Sy = dict_to_sparse(loaded, 'Sy')
                self.Sz = dict_to_sparse(loaded, 'Sz')
            if 'Sx_j0_format' in loaded.keys():
                if hasattr(self, 'Sx_j'):
                    del self.Sx_j, self.Sy_j, self.Sz_j
                self.Sx_j = [dict_to_sparse(loaded, f'Sx_j{j}') for j in range(self.M)]
                self.Sy_j = [dict_to_sparse(loaded, f'Sy_j{j}') for j in range(self.M)]
                self.Sz_j = [dict_to_sparse(loaded, f'Sz_j{j}') for j in range(self.M)]
            if 'N_j0_format' in loaded.keys():
                if hasattr(self, 'N_j'):
                    del self.N_j
                self.N_j  = [dict_to_sparse(loaded, f'N_j{j}') for j in range(self.M)]

    def set_H(self, H, label = None):
        self.H = H
        self.label = label

    def initial_state(self, **kwargs):
        init_state = np.zeros(len(self.A))
        init_state[0] = 1
        return init_state

    def set_scheme(self, scheme):
        self._scheme = scheme

    def set_spin_op(self, spin_op):
        self._spin_op = spin_op

    def check_spin_op(self):
        if self._spin_op == 'S':
            if not hasattr(self, 'Sx'):
                self.S_calc()
            Ox = self.Sx
            Oy = self.Sy
            Oz = self.Sz
        elif self._spin_op == 'J':
            if not hasattr(self, 'Jx'):
                self.J_calc()
            Ox = self.Jx
            Oy = self.Jy
            Oz = self.Jz
        else:
            raise Exception("Unrecognized operator")
        
        return Ox, Oy, Oz

    def rotate(self, coords, obj = None, **kwargs):
        theta, phi = coords
        if obj is None:
            obj = self.initial_state(**kwargs)

        _, Oy, Oz = self.check_spin_op()
       
        is_matrix = len(obj.shape) == 2 and obj.shape[1] == obj.shape[0]
        if is_matrix: 
            rot_theta = expm(-1j*Oy*theta)
            rot_phi   = expm(-1j*Oz*phi)
            obj_rot   = rot_theta.dot(obj.dot(rot_theta.getH()))
            obj_rot   = rot_phi.dot(obj_rot.dot(rot_phi.getH()))
        else: #rotating a vector, or vector series
            if self._scheme == 'default':
                rot_theta = expm(-1j*Oy*theta)
                rot_phi   = expm(-1j*Oz*phi)
                obj_rot   = rot_phi.dot(rot_theta.dot(obj))
         
            elif self._scheme == 'multiply':
                rot_theta = expm_multiply(-1.j*Oy*theta, obj)
                obj_rot   = expm_multiply(-1.j*Oz*phi, rot_theta)
               
            elif self._scheme == 'parallel':
                rot_theta = expm_multiply_parallel(-1j*Oy.tocsr(), a=theta)
                rot_phi   = expm_multiply_parallel(-1j*Oz.tocsr(), a=phi)
                obj_rot   = rot_phi.dot(rot_theta.dot(obj))
            else:
                raise NameError("No scheme found with that name.")

        return obj_rot

    def calc_coherent_states(self, resolution = 50, ref_state = None, **kwargs):
        if hasattr(resolution, "__len__"):
            res_phi, res_theta = resolution[:2]
        else:
            res_phi = res_theta = resolution
        #Coherent states across the Bloch sphere in coordinates theta, phi
        self._phi_array   = np.linspace(-np.pi, np.pi, res_phi)
        self._theta_array = np.linspace(0, np.pi, res_theta)
       
        _, Oy, Oz = self.check_spin_op()
    
        if ref_state is None:
            ref_state = self.initial_state(**kwargs)
            
        cs_array = np.empty((res_theta, res_phi, len(ref_state)), dtype = np.complex128)

        if self._scheme == 'default':
            rot_theta = [expm(-1j*Oy*theta) for theta in self._theta_array]
            rot_phi   = [expm(-1j*Oz*phi) for phi in self._phi_array]
            for i in range(res_theta):
                for j in range(res_phi):
                    cs_array[i,j,:] = rot_phi[j].dot(rot_theta[i].dot(ref_state))
     
        elif self._scheme == 'multiply':
            rot_theta = expm_multiply(-1.j*Oy, ref_state, 
                                      start=0, stop=np.pi, num = res_theta, endpoint = True)
            cs_array = expm_multiply(-1.j*Oz, rot_theta.T, 
                                     start=-np.pi, stop=np.pi, num = res_phi, endpoint = True)
            #cs_array = np.transpose(cs_array, [1, 2, 0])
            #cs_array = np.transpose(cs_array, [2, 1, 0])
            #cs_array = np.transpose(cs_array, [0, 2, 1])
            cs_array = np.transpose(cs_array, [2, 0, 1]) #good
           
        elif self._scheme == 'parallel':
            rot_theta = expm_multiply_parallel(Oy.tocsr(), a=-1j)
            rot_phi   = expm_multiply_parallel(Oz.tocsr(), a=-1j)
            ref_state = ref_state.astype(np.complex128)
            work_array1 = np.empty((2*len(ref_state),), dtype=ref_state.dtype)
            work_array2 = np.empty((2*len(ref_state),), dtype=ref_state.dtype)
            for i in range(res_theta):
                for j in range(res_phi):
                    rot_theta.set_a(-1.j*self._theta_array[i])
                    rot_phi.set_a(-1.j*self._phi_array[j])
                    cs_array[i,j,:] = rot_psi.dot(
                            rot_theta.dot(ref_state, work_array = work_array1), 
                            work_array = work_array2)
        else:
            raise NameError("No scheme found with that name.")
        
        self.cs_array = cs_array
        self.Phi, self.Theta = np.meshgrid(self._phi_array, self._theta_array)

        return cs_array

    def eigenvalues(self, k=1, **kwargs):
        w, v = linalg.eigsh(self.H, k=k, which='SA', **kwargs)
        self.gs = v[:,0]
        self.w = w
        return w, v

    def calc_energy_fluc(self, **cs_props):
        if not hasattr(self, 'cs_array'):
            self.calc_coherent_states(**cs_props)
        E = np.empty(self.cs_array.shape[:-1])
        for i in range(E.shape[0]):
            for j in range(E.shape[1]):
                E[i,j] = np.real(expectation_value(self.cs_array[i,j,:], self.H))
        self.E_cs = E
        self.U_cs, self.V_cs = np.gradient(E, self._phi_array, self._theta_array)
        self.U_cs *= -1
        return self.E_cs, self.U_cs, self.V_cs

    def stability_analysis(self, **cs_props):
        if not hasattr(self, 'E_cs'):
            self.calc_energy_fluc(**cs_props)
        dp2, dpdq = np.gradient(self.U_cs, self.Theta[:, 0], self.Phi[0, :])
        dqdp, dq2 = np.gradient(self.V_cs, self.Theta[:, 0], self.Phi[0, :])
        M = np.array([[dp2, dpdq], [-dq2, -dqdp]])
        '''
        for d2 in dpdq, dp2, -dq2, -dpdq:
            f = interpolate.RectBivariateSpline(self.Psi,self.Theta,d2)
            M.append(f(psi_f, np.pi/2))
        M = np.array(M).reshape((2,2))
        print(M)
        eigenvalues, _ = np.linalg.eig(M)
        return eigenvalues
        '''
        eigenvalues, _ = np.linalg.eig(M)
        return eigenvalues

    def spin_squeezing(self, wf = None):
        if wf is None:
            self.eigenvalues()
            wf = self.gs
        #elif not isinstance(wf, type(np.array)):
        #    wf = np.array(wf)
        
        Ox, Oy, Oz = self.check_spin_op()

        Sx = expectation_value(wf, Ox)
        Sy = expectation_value(wf, Oy)
        Sz = expectation_value(wf, Oz)
        S2 = Sx**2+Sy**2+Sz**2
        S  = np.sqrt(S2) 
        
        SxSy = Ox * Oy + Oy * Ox
        SzSx = Oz * Ox + Ox * Oz
        SySz = Oy * Oz + Oz * Oy
        delta_xy = expectation_value(wf, SxSy) - 2 * Sx*Sy 
        delta_zx = expectation_value(wf, SzSx) - 2 * Sz*Sx 
        delta_yz = expectation_value(wf, SySz) - 2 * Sy*Sz 

        Sx2 = Ox * Ox
        Sy2 = Oy * Oy
        Sz2 = Oz * Oz
        delta_x2 = expectation_value(wf, Sx2) - Sx**2 
        delta_y2 = expectation_value(wf, Sy2) - Sy**2 
        delta_z2 = expectation_value(wf, Sz2) - Sz**2 
        
        if Sx == 0 and Sy == 0:
            A_tilde = delta_y2 - delta_x2
            B_tilde = np.real(delta_xy)
            delta_S2_min = .5*(2*delta_x2 + A_tilde - np.sqrt(A_tilde**2 + B_tilde**2))
        else:
            cos_theta = Sz/S
            sin_theta = np.sqrt(1 - cos_theta**2)
            cos_phi     = Sx/np.sqrt(Sx**2+Sy**2)
            #sin_phi    = np.sqrt(1- cos_phi**2)              #Sy/np.sqrt(Sx**2+Sy**2)
            sin_phi     = Sy/np.sqrt(Sx**2+Sy**2)
            cos_2phi    = (Sx**2 - Sy**2)/(Sx**2 + Sy**2)    #cos_phi**2 - sin_phi**2 
            sin_2phi    = 2*Sx*Sy/(Sx**2 + Sy**2)            #2*cos_phi*sin_phi

            cos_2theta = (Sz**2-Sy**2-Sx**2)/S2             #cos_theta**2 - sin_theta**2 
            sin_2theta = 2*Sz*np.sqrt(Sx**2+Sy**2)/S2       #2*cos_theta*sin_theta
            #cos_2theta = cos_theta**2 - sin_theta**2 
            #sin_2theta = 2*cos_theta*sin_theta
            #cos_2phi   = cos_phi**2 - sin_phi**2 
            #sin_2phi   = 2*cos_phi*sin_phi
        
            A_tilde = ((sin_phi**2 - cos_theta**2 * cos_phi**2) * delta_x2
                     + (cos_phi**2 - cos_theta**2 * sin_phi**2) * delta_y2
                     - sin_theta**2 * delta_z2 - .5 * (1 + cos_theta**2) * sin_2phi * delta_xy
                     + .5 * sin_2theta * cos_phi * delta_zx + .5 * sin_2theta * sin_phi * delta_yz)

            B_tilde = (cos_theta * sin_2phi * (delta_x2 - delta_y2) - cos_theta * cos_2phi * delta_xy
                     - sin_theta * sin_phi * delta_zx + sin_theta * cos_phi * delta_yz)

            delta_S2_min = (.5 * (cos_theta**2 * cos_phi**2 + sin_phi**2) * delta_x2
                          + .5 * (cos_theta**2 * sin_phi**2 + cos_phi**2) * delta_y2
                          + .5 * sin_theta**2 * delta_z2 - .25*(sin_theta**2 * sin_2phi * delta_xy
                          + sin_2theta * cos_phi * delta_zx + sin_2theta * sin_phi * delta_yz)
                          - .5 * np.sqrt(A_tilde**2 + B_tilde**2))

        squeezing = self.N * delta_S2_min / S2
        #if self.N > self.M:
        #    squeezing = (2*self.M-self.N) * delta_S2_min / S2
        if abs(np.imag(squeezing)) > 1e-8:
            warnings.warn(f'Squeezing imaginary part is non negligible: {np.imag(squeezing)}')
        return np.real(squeezing)

    def spin_squeezing_angle(self, wf = None):
        if wf is None:
            self.eigenvalues()
            wf = self.gs
        #elif not isinstance(wf, type(np.array)):
        #    wf = np.array(wf)
        
        Ox, Oy, Oz = self.check_spin_op()
        
        x = expectation_value(wf, Ox).real
        y = expectation_value(wf, Oy).real
        z = expectation_value(wf, Oz).real

        theta = np.arccos(z/np.sqrt(x**2+y**2+z**2))
        phi   = np.sign(y)*np.arccos(x/np.sqrt(x**2+y**2))

        self.theta = theta
        self.phi = phi

        Jx = np.cos(theta)*(np.cos(phi)*Ox + np.sin(phi)*Oy) - np.sin(theta)*Oz
        Jy = -np.sin(phi)*Ox + np.cos(phi)*Oy
             
        Sx = expectation_value(wf, Jx).real
        Sy = expectation_value(wf, Jy).real
        
        SxSy = Jx * Jy + Jy * Jx
        delta_xy = expectation_value(wf, SxSy).real - 2 * Sx*Sy 

        Sx2 = Jx * Jx
        Sy2 = Jy * Jy
        delta_x2 = expectation_value(wf, Sx2).real - Sx**2 
        delta_y2 = expectation_value(wf, Sy2).real - Sy**2 
       
        #alpha = .5*np.arctan(delta_xy/(delta_x2-delta_y2))
        #if -np.sin(2*alpha)/delta_xy < 0:
        #    alpha += np.pi/2

        alpha = .5*np.arccos(-(delta_x2-delta_y2)/np.sqrt((delta_x2-delta_y2)**2 + delta_xy**2))

        return alpha

    def S_min_var(self, wf = None):
        if wf is None:
            self.eigenvalues()
            wf = self.gs
        #elif not isinstance(wf, type(np.array)):
        #    wf = np.array(wf)
        if not hasattr(self, 'Sx'):
            self.S_calc()

        alpha = self.spin_squeezing_angle(wf)
        
        S_min = ( (np.cos(self.theta)*np.cos(self.phi)*np.cos(alpha) - np.sin(self.phi)*np.sin(alpha))*self.Sx
                    + (np.cos(self.theta)*np.sin(self.phi)*np.cos(alpha) + np.cos(self.phi)*np.sin(alpha))*self.Sy
                    - np.sin(self.theta)*np.cos(alpha)*self.Sz)
        result = variance(wf, S_min)

        return result.real

    def S_mean(self, wf = None):
        if wf is None:
            self.eigenvalues()
            wf = self.gs
        #elif not isinstance(wf, type(np.array)):
        #    wf = np.array(wf)
        if not hasattr(self, 'Sx'):
            self.S_calc()

        alpha = self.spin_squeezing_angle(wf)
        
        Sz = (np.sin(self.theta)*np.cos(self.phi)*self.Sx 
              + np.sin(self.theta)*np.sin(self.phi)*self.Sy 
              + np.cos(self.theta)*self.Sz)
        
        return expectation_value(wf, Sz).real

    def calc_squeezing_frame(self, wf):
        if not hasattr(self, 'Sx_j'):
            self.S_j_calc()
        alpha = self.spin_squeezing_angle(wf)
        
        Sx = [ _ for _ in range(self.M)]
        Sy = [ _ for _ in range(self.M)]
        Sz = [ _ for _ in range(self.M)]
        
        for j in range(self.M):
            Sx[j] = ( (np.cos(self.theta)*np.cos(self.phi)*np.cos(alpha) - np.sin(self.phi)*np.sin(alpha))*self.Sx_j[j]
                    + (np.cos(self.theta)*np.sin(self.phi)*np.cos(alpha) + np.cos(self.phi)*np.sin(alpha))*self.Sy_j[j]
                    - np.sin(self.theta)*np.cos(alpha)*self.Sz_j[j])
            Sy[j] = (-(np.cos(self.theta)*np.cos(self.phi)*np.sin(alpha) + np.sin(self.phi)*np.cos(alpha))*self.Sx_j[j]
                    - (np.cos(self.theta)*np.sin(self.phi)*np.sin(alpha) - np.cos(self.phi)*np.cos(alpha))*self.Sy_j[j] 
                    + np.sin(self.theta)*np.sin(alpha)*self.Sz_j[j])
            Sz[j] = (np.sin(self.theta)*np.cos(self.phi)*self.Sx_j[j] 
                    + np.sin(self.theta)*np.sin(self.phi)*self.Sy_j[j] 
                    + np.cos(self.theta)*self.Sz_j[j])
        
        S_min = Sx
        S_avg = Sz

        return S_avg, S_min, Sy

    def Bell_ineq(self, wf = None, m2 = False):
        if wf is None:
            self.eigenvalues()
            wf = self.gs
        #elif not isinstance(wf, type(np.array)):
        #    wf = np.array(wf)

        S_avg, S_min, _ = self.calc_squeezing_frame(wf)

        theta = self.alpha
        Sa = [S_avg[j]*np.cos(theta) + S_min[j]*np.sin(theta) for j in range(self.M)]
        Sb = [S_avg[j]*np.cos(theta) - S_min[j]*np.sin(theta) for j in range(self.M)]
        M_0 = M_1 = 0
        M2_0 = M2_1 = 0
        C_00 = C_01 = C_10 = C_11 = 0
        for i in range(self.M):
            M_0 += expectation_value(wf, Sa[i])
            M_1 += expectation_value(wf, Sb[i])
            M2_0 += expectation_value(wf, Sa[i]**2)
            M2_1 += expectation_value(wf, Sb[i]**2)
            for j in range(self.M):
                if i != j:
                    C_00 += expectation_value(wf, Sa[i]*Sa[j])
                    C_10 += expectation_value(wf, Sb[i]*Sa[j])
                    C_01 += expectation_value(wf, Sa[i]*Sb[j])
                    C_11 += expectation_value(wf, Sb[i]*Sb[j])

        Bell = C_00 + C_11 - C_01 - C_10 - (M_0 - M_1)**2 - (M_0 + M_1) + self.N
        if m2 is True:
            #Bell += 2*(M2_0+M2_1)-self.N
            Bell = C_00 + C_11 - C_01 - C_10 - (M_0 - M_1)**2 - (M_0 + M_1) + 2*(M2_0+M2_1)

        return Bell.real

    def Bell_simple(self, wf = None):
        if wf is None:
            self.eigenvalues()
            wf = self.gs
        #elif not isinstance(wf, type(np.array)):
        #    wf = np.array(wf)

        S_avg, S_min, _ = self.calc_squeezing_frame(wf)
        A = variance(wf, sum(S_min)).real
        B = expectation_value(wf, sum(S_avg)).real
        Bell = -self.N + 4*A - B**2/(self.N - 4*A)

        return Bell.real


    def Bell_opt(self, wf = None, m2 = False):
        if wf is None:
            self.eigenvalues()
            wf = self.gs
        #elif not isinstance(wf, type(np.array)):
        #    wf = np.array(wf)

        S_avg, S_min, _ = self.calc_squeezing_frame(wf)

        '''
        A = 0
        for i in range(self.M):
            for j in range(self.M):
                if j != i:
                    A += expectation_value(wf, S_min[i]*S_min[j]).real
        '''
        #A -= expectation_value(wf, sum(S_min)).real**2 
        A = variance(wf, sum(S_min)).real
        B = expectation_value(wf, sum(S_avg)).real
        
        Bell = A*(4+B**2/(4*A**2)) if abs(B/(4*A)) <= 1 and A < 0 else -2*abs(B)
        Bell += self.N
        
        if m2 is True:
            C = D = 0
            for i in range(self.M):
                C += expectation_value(wf, S_avg[i]**2).real
                D += expectation_value(wf, S_min[i]**2).real
            Bell = B**2/(A-C+D)/4 + 4*(A+D) if abs(B) <= 4*abs(A-C+D) and (A-C+D) < 0 else (-2*abs(B) + 4*C)

        return Bell.real

    def one_two_body_corr(self, wf = None):
        if wf is None:
            self.eigenvalues()
            wf = self.gs
        #elif not isinstance(wf, type(np.array)):
        #    wf = np.array(wf)
        
        S_avg, S_min, _ = self.calc_squeezing_frame(wf)
       
        theta = self.alpha

        Sa = [S_avg[j]*np.cos(theta) + S_min[j]*np.sin(theta) for j in range(self.M)]
        Sb = [S_avg[j]*np.cos(theta) - S_min[j]*np.sin(theta) for j in range(self.M)]
        
        M = np.zeros(4)
        C = np.zeros((4,4))
        for i in range(self.M):
            M[0] += expectation_value(wf, Sa[i]).real
            M[1] += expectation_value(wf, Sb[i]).real
            M[2] += expectation_value(wf, Sa[i]**2).real
            M[3] += expectation_value(wf, Sb[i]**2).real
            for j in range(self.M):
                if i != j:
                    C[0,0] += expectation_value(wf, Sa[i]*Sa[j]).real
                    C[1,0] += expectation_value(wf, Sb[i]*Sa[j]).real
                    C[0,1] += expectation_value(wf, Sa[i]*Sb[j]).real
                    C[1,1] += expectation_value(wf, Sb[i]*Sb[j]).real
                    
                    C[0,2] += expectation_value(wf, Sa[i]*Sa[j]**2).real
                    C[0,3] += expectation_value(wf, Sb[i]*Sa[j]**2).real
                    C[1,2] += expectation_value(wf, Sa[i]*Sb[j]**2).real
                    C[1,3] += expectation_value(wf, Sb[i]*Sb[j]**2).real
                    
                    C[2,0] += expectation_value(wf, Sa[i]**2*Sa[j]).real
                    C[3,0] += expectation_value(wf, Sb[i]**2*Sa[j]).real
                    C[2,1] += expectation_value(wf, Sa[i]**2*Sb[j]).real
                    C[3,1] += expectation_value(wf, Sb[i]**2*Sb[j]).real
                    
                    C[2,2] += expectation_value(wf, Sa[i]**2*Sa[j]**2).real
                    C[3,2] += expectation_value(wf, Sb[i]**2*Sa[j]**2).real
                    C[2,3] += expectation_value(wf, Sa[i]**2*Sb[j]**2).real
                    C[3,3] += expectation_value(wf, Sb[i]**2*Sb[j]**2).real
        
        return M, C

    def spin_Fisher(self, wf = None):
        if wf is None:
            self.eigenvalues()
            wf = self.gs
        #elif not isinstance(wf, type(np.array)):
        #    wf = np.array(wf)
        
        Ox, Oy, Oz = self.check_spin_op()
        
        Sx = expectation_value(wf, Ox)
        Sy = expectation_value(wf, Oy)
        Sz = expectation_value(wf, Oz)
       
        S = Sx, Sy, Sz
        O = Ox, Oy, Oz
        C = np.empty((3,3), dtype = np.complex128)
        for i in range(C.shape[0]):
            for j in range(i, C.shape[1]):
                SiSj = O[i]*O[j] + O[j]*O[i]
                C[i,j] = .5 * expectation_value(wf, SiSj) - S[i]*S[j] 
                C[j,i] = C[i,j]
        
        w, v = np.linalg.eigh(C)
        return 4*max(w)

    def time_evolution(self, dt, dense = False):
        if dense:
            H = self.H.todense() 
        else:
            H = self.H
        U_dt = expm(-1j*H*dt)
        return U_dt
       
    def evolve(self, wf0, t_f, num = None, parameters = [], save_wf = False, q_calc = False, cs_props = {}):
        if not save_wf and hasattr(self, 'wf_t'):
            del self.wf_t

        if num is None:
            num = 2
            dt = t_f
            self.t = np.array([0, t_f])
        else:
            self.t, dt = np.linspace(0, t_f, num = num, retstep = True)
        data = np.empty( (len(parameters), num), dtype = np.complex128)
        
        if q_calc:
            if not hasattr(self, 'cs_array'):
                self.calc_coherent_states(**cs_props)
            data_q = np.empty( (*self.cs_array.shape[:2], num) )
        
        if save_wf:
            self.wf_t = np.empty( (num, len(wf0)), dtype = np.complex128 )
        
        if len(np.shape(wf0)) == 1: #wave-function evolution
            if self._scheme == 'default':
                wf = np.copy(wf0)
                U_dt = self.time_evolution(dt)
                for i in range(num):
                    self.measure(i, wf, data, parameters)
                    
                    if q_calc:
                        self.measure_q_function(i, wf, data_q)
                    
                    if save_wf:
                        self.wf_t[i, :] = wf
                    wf = U_dt.dot(wf)

            elif self._scheme == 'multiply':
                self.wf_t = expm_multiply(-1.j*self.H, wf0, 
                                          start=0, stop=self.t[-1], num=num, endpoint=True) 
                for i in range(num):
                    wf = self.wf_t[i, :]
                    self.measure(i, wf, data, parameters)

                    if q_calc:
                        self.measure_q_function(i, wf, data_q)

            elif self._scheme == 'parallel':
                wf = np.copy(wf0)#.astype(np.float64)
                work_array = np.empty((2*len(wf),), dtype=wf.dtype)
                U_t = expm_multiply_parallel(self.H.tocsr(), a=-1j*dt, dtype = np.complex128)
                for i in range(num):
                    self.measure(i, wf, data, parameters)

                    if q_calc:
                        self.measure_q_function(i, wf, data_q)
                    
                    if save_wf:
                        self.wf_t[i, :] = wf

                    U_t.dot(wf, work_array = work_array, overwrite_v=True)
            else:
                raise NameError("No scheme found with that name.")
        
        else: #density matrix case
            rho = wf0.copy()
            if self._scheme == 'default':
                U_dt = self.time_evolution(dt)
                U_dagger = U_dt.getH()
                for i in range(num):
                    self.measure(i, rho, data, parameters)
                    rho = U_dt.dot(rho.dot(U_dagger))
            elif self._scheme == 'multiply':
                rho = rho.reshape((self.D**2, 1))
                #rho = rho.toarray()
                identity = sparse.identity(self.D)
                L = (sparse.kron(identity, self.H) 
                     - sparse.kron(self.H, identity)) #Liouvillian
                #self.rho_t = expm_multiply(-1.j*L, rho, 
                #                           start=0, stop=self.t[-1], num=num, endpoint=True) 
                #for i in range(num):
                #    r = self.rho_t[i].reshape(self.D, self.D)
                #    self.measure(i, r, data, parameters)
                for i in range(num): #If .toarray() not used, very slow for M = 5
                    rho_t = expm_multiply(-1.j*L*self.t[i], rho)
                    rho_t = rho_t.reshape(self.D, self.D)
                    self.measure(i, rho_t, data, parameters)

            elif self._scheme == 'parallel':
                rho = rho.reshape((self.D**2, 1))
                rho = rho.toarray()
                #work_array = np.empty((2*self.D**2, ), dtype=np.complex128) #allocates 100Gib for M=10
                identity = sparse.identity(self.D)
                L = (sparse.kron(identity, self.H) 
                     - sparse.kron(self.H, identity)) #Liouvillian
                U_t = expm_multiply_parallel(L.tocsr(), a=-1j*dt, dtype = np.complex128)
                for i in range(num):
                    self.measure(i, rho.reshape((self.D, self.D)), data, parameters)
                    #U_t.dot(rho, work_array = work_array, overwrite_v=True)
                    U_t.dot(rho, overwrite_v=True)
        if q_calc:
            data = (data, data_q)

        return self.t, data

    def measure(self, i, wf, data, parameters):
        for j in range(len(parameters)):
            param = parameters[j]
            if isinstance(param, str):
                if param == 'squeezing':
                    data[j,i] = self.spin_squeezing(wf)
                elif param == 'Fisher':
                    data[j,i] = self.spin_Fisher(wf)
                elif 'Bell' in param:
                    m2 = True if 'm2' in param else False
                    if 'ineq' in param:
                        data[j,i] = self.Bell_ineq(wf, m2)
                    elif 'simple' in param:
                        data[j,i] = self.Bell_simple(wf)
                    else:
                        data[j,i] = self.Bell_opt(wf, m2 = m2)
                elif param == 'angle':
                    data[j,i] = self.spin_squeezing_angle(wf)
                elif param == 'S_mean':
                    data[j,i] = self.S_mean(wf)
                elif param == 'S_min_var':
                    data[j,i] = self.S_min_var(wf)
                else:
                    data[j,i] = getattr(self, param)
            elif callable(param):
                data[j,i] = param(wf)
            else: 
                data[j,i] = expectation_value(wf, param)
        return data
    
    def measure_q_function(self, i, wf, data):
        data[:, :, i] = abs(self.cs_array.conj().dot(wf))**2
        #for j in range(self.cs_array.shape[0]):
        #    for k in range(self.cs_array.shape[1]):
        #        data[j, k, i] = abs(self.cs_array[j,k,:].conj().dot(wf))**2
        return data

    def spdm(self):
        if not hasattr(self, 'gs'):
            self.eigenvalues()
        rho = np.zeros((self.M,self.M))
        for j in range(self.M):
            rho_0j = 0
            for k in range(self.D):
                v = self.A[k]
                if v[j] >= 1:
                    u = list(v)
                    u[j] -= 1
                    u[0] += 1
                    r = self.T[tuple(u)]
                    rho_0j += np.sqrt(u[0] * v[j]) * self.gs[k] * self.gs[r]
            if rho_0j != 0:
                for k in range(self.M):
                    rho[j-k,0-k] = rho[0-k,j-k] = rho_0j                

        return rho

    def rho_ij(self, i, j):
        row, col, data = [], [], []
        for k in range(self.D):
            v = self.A[k]
            if v[j] >= 1:
                u = list(v)
                u[j] -= 1
                u[i] += 1
                r = self.T[tuple(u)]
                data.append(np.sqrt(u[i] * v[j]))
                row.append(r)
                col.append(k)

        return sparse.csr_matrix((data, (row, col)), shape=(self.D, self.D), dtype=np.complex128)

    def spdm_2(self):
        rho = np.zeros((self.M,self.M))
        for j in range(self.M):
            rho_0j = self.rho_ij(0, j)
            rho_0j = rho_0j.dot(self.G).dot(self.G.T)
            if rho_0j != 0:
                for k in range(j, self.M):
                    rho[j-k,0-k] = rho_0j
                    rho[0-k,j-k] = rho_0j

        return rho

    def lambda_1(self):
        lambda1 = 0
        for j in range(self.M):
            rho_0j = self.rho_ij(0, j)
            rho_0j = rho_0j.dot(self.G).dot(self.G.T)
            lambda1 += rho_0j
            #lambda1 += np.dot(np.dot(G, rho_ij(0, j, A)), G.T)
        return lambda1

    def occ_variance(self, i):
        s1, s2 = 0, 0
        for k in range(len(self.G)):
            s1 += self.G[k]**2 * self.A[k][i]**2
            s2 += self.G[k]**2 * self.A[k][i]

        return np.sqrt(s1-s2**2)


class SpinModel(TotalSpinOpMixin, HSpinMixin, BaseModel):

    def __init__(self, N, spin = .5, basis = 'total_spin', bc = 'periodic'):
        self._n_levels = int(2*spin)+1
        super().__init__(N, self._n_levels, basis, bc)
          
        if self.basis_name == 'dicke' or self.basis_name == 'Dicke':
            self.basis = self.Dicke_basis
        else:
            self.basis = self.total_spin_basis
        self.basis()    

    def Dicke_basis(self, spin = .5):
        S = self.N*spin
        self.A = []
        m = S
        while m >= -S:
            v = (S, m)
            self.A.append(v)
            m -= 1

        self.D = len(self.A)
        self.T = {v: tag for tag, v in enumerate(self.A)} 

    def total_spin_basis(self, spin = .5):
        Smax = self.N*spin
        #x = list(range(2*self.M))[::-1]
        self.A = []
        S = Smax
        while S >= 0:
            m = S
            while m >= -S:
                v = (S, m)
                self.A.append(v)
                m -= 1
            S -= 1

        self.D = len(self.A)
        self.T = {v: tag for tag, v in enumerate(self.A)} 


class BoseHubbard(HBoseMixin, LocalOpMixin, BaseModel):
    def __init__(self, N, M, basis = 'n', bc = 'periodic', cutoff = None):
        super().__init__(N, M, basis, bc = bc)           
        self._n_levels = 1
        self.cutoff = cutoff
        
        if self.bc == 'periodic' or self.bc == 'ring':
            self.H_0 = self.H_0_periodic
        elif self.bc == 'open':
            self.H_0 = self.H_0_open
        else:
            raise ValueError('Unrecognised boundary conditions.')
          
        if self.basis_name == 'fock' or self.basis_name == 'n' or self.basis_name == 'number':
            self.basis = self.n_basis
            if self.cutoff is not None:
                self.basis = self.n_basis_cutoff
        elif self.basis_name == 'fock-single' or self.basis_name == 'single':
            self.basis = self.n_single_basis 
        else:
            raise ValueError('Unrecognised eigenbasis name.')
        self.basis()    

    def n_basis(self):
        self.A = []
        vector = [0 for i in range(self.M)]
        vector[0] = self.N
        self.A.append(tuple(vector))
        k = 0
        while vector[-1] < self.N:
            if k < self.M-1 and vector[k] != 0:# and sum(vector[k+1:M-1]) == 0:
                vector[k] -= 1
                vector[k+1] = self.N - sum(vector[:k+1])
                vector[k+2:] = [0 for i in range(k+2,self.M)]
                self.A.append(tuple(vector))
                k += 1
            else:
                k -= 1

        self.A = tuple(self.A)
        self.D = len(self.A)
        self.T = { v: tag for tag, v in enumerate(self.A)  } #set([tuple(v) for v in self.A])
    
    def n_basis_cutoff(self):
        if self.cutoff is None or self.cutoff > self.N:
            self.cutoff = self.N
        self.A = []
        vector = [self.cutoff for _ in range(self.N//self.cutoff)]
        if self.N % self.cutoff:
            vector += [self.N % self.cutoff]
        vector += [0 for _ in range(self.M-len(vector))]
        self.A.append(tuple(vector))
        last_state = tuple(vector[::-1])
        k = self.N // self.cutoff + 1
        while tuple(vector) != last_state:
            if k < self.M-1 and vector[k] != 0 and sum(vector[k+1:]) < self.cutoff*(self.M-1-k):# and sum(vector[k+1:self.M-1]) == 0:
                vector[k] -= 1
                particles_left =  self.N - sum(vector[:k+1])
                cutoff_sites =  particles_left // self.cutoff
                vector[k+1:k+1+cutoff_sites] = [self.cutoff for _ in range(cutoff_sites)]
                vector[k+1+cutoff_sites:] = [0 for i in range(k+1+cutoff_sites,self.M)]
                if particles_left % self.cutoff:
                    vector[k+1+cutoff_sites] = particles_left % self.cutoff
                self.A.append(tuple(vector))
                k += 1+cutoff_sites
            else:
                k -= 1
        self.A = tuple(self.A)
        self.D = len(self.A)
        self.T = { v: tag for tag, v in enumerate(self.A)  } #set([tuple(v) for v in self.A])

class Bose2cHubbard(HBose2cMixin, BoseSpinOpMixin, LocalOpMixin, HFockSpinMixin, HSpinMixin, BaseModel):
    
    def __init__(self, N, M, basis = 'n', bc = 'periodic'):
        super().__init__(N, M, basis, bc = bc)
        self._n_levels = 2
         
        if self.basis_name == 'fock' or self.basis_name == 'n' or self.basis_name == 'number':
            self.basis = self.n_basis
        elif self.basis_name == 'fock-single' or self.basis_name == 'single':
            self.basis = self.n_single_basis 
        else:
            raise ValueError('Unrecognised eigenbasis name.')
        self.basis()
     
    def n_basis(self):
        L = self._n_levels*self.M
        self.A = []
        vector = [0 for i in range(L)]
        vector[0] = self.N
        self.A.append(tuple(vector))
        k = 0
        while vector[-1] < self.N:
            if k < L-1 and vector[k] != 0:
                vector[k] -= 1
                vector[k+1] = self.N - sum(vector[:k+1])
                #vector[k+2:] = [0 for i in range(k+2,L)]
                for l in range(k+2, L):
                    vector[l] = 0
                self.A.append(tuple(vector))
                k += 1
            else:
                k -= 1

        self.A = tuple(self.A)
        self.D = len(self.A)
        self.T = { v: tag for tag, v in enumerate(self.A)  } 

    def initial_state(self, J, U):
        wf = [0 for i in range(self.D)]
        v = [self.N] + [0 for i in range(2*self.M-1)]
        if self.M == 1:
            r = self.T[(self.N, 0)]
            wf[r] = 1
        elif self.M == self.N:
            Ta = self.T.copy()
            Ha = self.H_0(J=J) + self.H_int(U=U)
            select_ind = []
            for key, v in self.T.items():
                if sum(key[self.M:]) != 0:
                    Ta.pop(key)
                else:
                    select_ind.append(v)
            Ha = Ha.tocsr()[select_ind,:].tocsc()[:, select_ind]
            _, v = linalg.eigsh(Ha, k=1, which='SA')
            gs = v[:,0]
            Aa = list(Ta.keys())
            for i, val in enumerate(gs):
                v = Aa[i]
                r = self.T[tuple(v)]
                wf[r] = val
            #v = [1 for i in range(self.M)] + [0 for i in range(self.M)]
            #wf[r] = 1
        else:
            for k in range(self.D):
                v = self.A[k]
                if sum(v[self.M:]) == 0:
                    wf[k] = 1
        
        wf = np.array(wf)
        #wf = np.array(wf) / np.sqrt(sum(abs(wf)**2))
        #assert abs(sum(abs(wf)**2) - 1) < 1e-10
        
        return wf


class FermiHubbard(HFermiMixin, FermiSpinOpMixin, LocalOpMixin, HFockSpinMixin, HSpinMixin, BaseModel):

    def __init__(self, N, M, basis = 'n', bc = 'periodic'):
        super().__init__(N, M, basis, bc)
        self._n_levels = 2
        if self.bc == 'periodic' or self.bc == 'ring':
            self.H_0 = self.H_0_periodic
        elif self.bc == 'anti_periodic':
            self.H_0 = self.H_0_antiperiodic
        elif self.bc == 'open':
            self.H_0 = self.H_0_open
        else:
            raise ValueError('Unrecognised boundary conditions.')
        
        if self.basis_name == 'fock' or self.basis_name == 'n' or self.basis_name == 'number':
            self.basis = self.n_basis
        elif self.basis_name == 'fock-single' or self.basis_name == 'single':
            self.basis = self.n_single_basis 
        else:
            raise ValueError('Unrecognised eigenbasis name.')
        
        self.basis()
    
    def n_basis(self): 
        self.A = []
        L = self._n_levels * self.M
        holes = L-self.N 
        v = [1 for i in range(self.N)] + [0 for i in range(holes)] 
        self.A.append(tuple(v)) 
        k=self.N-1 
        while sum(v[holes:]) < self.N: 
            if v[k] > 0 and k < L-1 and sum(v[k:]) < L-k: 
                N_left = self.N - sum(v[:k+1]) 
                v[k]   = 0 
                v[k+1] = 1 
                for i in range(N_left): 
                    v[k+2+i] = 1 
                for i in range(L-(k+2+N_left)): 
                    v[k+2+N_left+i] = 0 
                self.A.append(tuple(v)) 
                k += 1+N_left 
            else: 
                k -= 1 
                 
        self.A = list(self.A)
        self.D = len(self.A)
        self.T = {v: tag for tag, v in enumerate(self.A)} 
     
    def n_basis_deprecated(self):
        L = self._n_levels * self.M
        assert self.N <= L
        x = list(range(L))[::-1]
        self.A = []
        for item in it.combinations(x,self.N):
            tag = sum([2**j for j in item])
            vector = [int(i) for i in bin(tag)[2:]]
            vector = [0 for i in range(len(x)-len(vector))] + vector
            self.A.append(tuple(vector))

        self.A = list(self.A)
        self.D = len(self.A)
        self.T = {v: tag for tag, v in enumerate(self.A)} 

    def n_single_basis_deprecated(self):
        assert self.N <= 2*self.M
        x = list(range(2*self.M))[::-1]
        self.A = []
        for item in it.combinations(x,self.N):
            tag = sum([2**j for j in item])
            vector = [int(i) for i in bin(tag)[2:]]
            vector = [0 for i in range(len(x)-len(vector))] + vector
            doubled_occupied = sum([(vector[i] + vector[i+self.M]) // 2 for i in range(self.M)])
            if doubled_occupied == 0:
                self.A.append(tuple(vector))
        
        self.A = list(self.A)
        self.D = len(self.A)
        self.T = {v: tag for tag, v in enumerate(self.A)} 

    def initial_state(self, **kwargs):
        wf = [0 for i in range(self.D)]
        v = [1 for i in range(self.N)] + [0 for i in range(2*self.M-self.N)]
        if self.N == self.M:
            r = self.T[tuple(v)]
            wf[r] = 1
        else:
            if self.N < self.M:
                start = 0
                end = self.M
            else:
                start = self.M
                end = 2*self.M
            combinations = set(it.permutations(v[start:end], len(v[start:end])))
            for comb in combinations:
                v[start:end] = comb
                r = self.T[tuple(v)]
                wf[r] = 1

        wf = np.array(wf)
        wf = wf / np.sqrt(sum(abs(wf)**2))
        #assert abs(sum(wf**2) - 1) < 1e-10

        return wf
 
