# `hubbard`: 1D Hubbard Model exact calculations for Python

This python module is made for solving dynamics of the Bose Hubbard and Fermi Hubbard models in 1D with exact calculations. The main focus of the module is to help with two component systems so as to solve spin squeezing dynamics in their simplest case (spin $1/2$). 

Required dependencies:
- `numpy`
- `scipy`

Recommended dependencies for paralellization:
- `quspin`
