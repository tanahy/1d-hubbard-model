from hubbard.utilities import *
from scipy.optimize import root
import numpy as np
import matplotlib.pyplot as plt

pi = np.pi
N = M = 10
J_SE = 0.0032
Omega = 0.004*J_SE
print(J_SE, Omega)
#phi = np.pi-2*np.pi/M
phi = pi-2*np.pi/M #- 1*np.pi/M

#X, Y = np.meshgrid(np.arange(0, np.pi, .1), np.arange(0, np.pi, .1))
X, Y = np.meshgrid(np.arange(0, 2*np.pi, .1), np.arange(0, np.pi, .1))

N, v_x, v_y, chi_x, chi_z, chi_xy = dot_theta_coeff(phi, N, M, J_SE, Omega)
#v_x = -v_x
#v_y = -v_y
args = N, v_x, v_y, chi_x, chi_z, chi_xy
max_arg = max([abs(val) for val in args[1:]])
print('v_x (norm.):', v_x/max_arg)
print('v_y (norm.):', v_y/max_arg)
print(args[1:])

def dtheta(theta, psi):
    return -N/2*np.sin(theta) * ((v_x*np.sin(psi) - v_y*np.cos(psi)) 
                                + N*np.sin(theta)*(chi_x*np.sin(2*psi) + chi_xy * np.cos(2*psi))) 
def dpsi(theta, psi):
    return -N/2*np.cos(theta) * ((v_x*np.cos(psi) + v_y*np.sin(psi)) 
                                + N*np.sin(theta)*(chi_x + chi_z + chi_x*np.cos(2*psi) - chi_xy * np.sin(2*psi))) 

def df(x):
    theta, psi = x[1], x[0]
    #return np.array([dtheta(theta, psi), dpsi(theta, psi)])
    return [dpsi(theta, psi), dtheta(theta, psi)]

def jacobian(x):
    theta, psi = x[1], x[0]
    A = v_x*np.cos(psi) + v_y*np.sin(psi)
    B = v_x*np.sin(psi) - v_y*np.cos(psi)
    C = chi_x*np.cos(2*psi) - chi_xy * np.sin(2*psi)
    D = chi_x*np.sin(2*psi) + chi_xy * np.cos(2*psi)
    
    df1 = - N/2*np.sin(theta)*A + 2*(N/2)**2*np.cos(2*theta)*(chi_x+chi_z+C)
    df2 = - N/2*np.cos(theta)*(B - 4*(N/2)*np.sin(theta)*D)
    dg1 =   N/2*np.cos(theta)*B + 2*(N/2)**2*np.sin(2*theta)*D
    dg2 =   N/2*np.sin(theta)*(A + 2*(N/2)*np.sin(2*theta)*C)

    return np.array([[df1, df2], [dg1, dg2]])


U = dpsi(Y, X)
V = dtheta(Y, X)

speed = np.sqrt(U**2 + V**2)

tol = .05
mask = (abs(U) < tol*abs(U.max())) & (abs(V) < tol*abs(V.max()))

fig1, (ax1, ax2) = plt.subplots(2)
#Q = ax1.quiver(X/pi, Y/pi, U, V, units='width')
#stream = ax1.streamplot(X, Y, U.real, V.real)
stream = ax1.streamplot(X, Y, U, V)
#Q = ax1.streamplot(Y/pi, X/pi, V.real, V.real)
#Q = ax1.quiver(X[mask]/pi, Y[mask]/pi, U[mask], V[mask], color = 'red', units='width')
#qk = ax1.quiverkey(Q, 0.9, 0.9, 2, r'$2 \frac{m}{s}$', labelpos='E',
#                   coordinates='figure')

x0, y0 = [pi*3/2, pi*3/2, pi*3/2, pi*.24], [pi/5, pi/4, pi/4, pi/3]

ax2.plot(speed[:, X.shape[1]//2])
ax2.plot(speed[X.shape[1]//2, :])

fixed_points = []
for z0 in zip(x0, y0):
    #sol = root(df, x0 = z0, jac = jacobian, method = 'broyden2')
    sol = root(df, x0 = z0, method = 'hybr')
    sol.x[0] = sol.x[0] % (2 * np.pi)
    sol.x[1] = sol.x[1] % np.pi
    fixed_points.append(sol.x)

psi_list = find_roots(dot_theta, args = args, method = 'secant', steps = 100)

psi = np.array(psi_list).real % (2*np.pi)

fixed_points = np.array(fixed_points)
print(fixed_points)
print(psi)

ax1.axhline(np.pi/2, color = 'grey')
ax1.scatter(fixed_points[:,0], fixed_points[:,1], color = 'red')
ax1.scatter(psi, [np.pi/2 for _ in psi], color = 'green')

plt.show()
