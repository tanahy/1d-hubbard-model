import sys
import time

import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.patches import Circle
from hubbard.hubbard import *
from hubbard.utilities import *
from hubbard.plot import *

###############################
N = 4
M = N
U = 1 
#t_f = 800
t_f = 100
#Omega = 0.1 
Omega = 0.268
#phi = .18*np.pi
phi = .36*np.pi

psi_fm, psi_fp = critical_psi(M,phi)
psi = 0#psi_fm
#psi = psi_loop(psi_fm +.25*np.pi)

res = 100
N_steps = 100

dt = t_f / N_steps
J = 1 if U != 0 else 0
bc = 'open'
#bc = 'periodic'
frame = 'lab'
op = 'S'
alpha = 1

#timestamps = [0, 200, 416, t_f-dt]
timestamps = [0, 35, 57, t_f-dt]
###############################

timestamp_steps = []
for i,t_i in enumerate(timestamps):
    j = int(t_i // dt)
    timestamp_steps.append(j)
    timestamps[i] = j*dt

filename = f'output/portraits/Q function snapshots, N={N}, J={J}, Omega={Omega}, U={U}, phi={phi/np.pi:.2f}pi, tf={t_f}'
if psi == psi_fm: 
    filename += ', psi=psi_fm'
elif psi == psi_fp: 
    filename += ', psi=psi_fp'
else:
    filename += f', psi={psi/np.pi:.2f}pi'

'''
fig = plt.figure(constrained_layout=True, figsize=(15,20))

rows, cols = 4, 3
gs = GridSpec(rows ,cols, figure=fig)
axs = [fig.add_subplot(gs[i,:cols-1]) for i in range(rows)]
ax2 = fig.add_subplot(gs[:rows//2,cols-1])
ax3 = fig.add_subplot(gs[rows//2:,cols-1])
'''

#fig = plt.figure(constrained_layout=True, figsize=(20,15))
fig = plt.figure(constrained_layout=True, figsize=(12,9))

rows, cols = 3, 2
gs = GridSpec(rows ,cols, figure=fig)
axs = []
for i in range(rows-1):
    for j in range(cols):
        axs.append(fig.add_subplot(gs[i,j]))

ax2 = fig.add_subplot(gs[-1,0])
ax3 = fig.add_subplot(gs[-1,1])




start = time.time()
F = Hubbard(N,M,'Fermi',bc=bc)
F.change_frame(frame, phi = phi, alpha=0)
#F.H = F.H_0(J=J) + F.H_int(U=U) + F.H_L(Omega = Omega)
wf0 = F.initial_state_fermi()
ref_state = wf0

Ox = eval(f'F.{op}x')
Oy = eval(f'F.{op}y')
Oz = eval(f'F.{op}z')

initial_state = expm(-1j*Oz*psi).dot(expm(-1j*Oy*np.pi/2).dot(wf0))
#initial_state = expm(-1j*F.Sy*np.pi/2).dot(wf0)

end = time.time()

print("Initilization:",end-start)

start = time.time()
wf = initial_state #expm(-1j*F.Sz*phi).dot(expm(-1j*F.Sy*np.pi/2).dot(wf0))
Udt = F.time_evolution(dt)
t = []
squeezing = []
s_x = []
s_y = []
s_z = []
for i in range(N_steps):
    ssp = F.spin_squeezing(wf, operator = op)
    t.append(dt*i)
    s_x.append(np.real(expectation_value(wf, Ox))/N)
    s_y.append(np.real(expectation_value(wf, Oy))/N)
    s_z.append(np.real(expectation_value(wf, Oz))/N)
    squeezing.append(ssp)
    wf = Udt.dot(wf)
end = time.time()
print("Evolution:",end-start)

ax2.plot(t,squeezing)

s_label = ['x','y','z']
s = [s_x, s_y, s_z]
for i in range(3):
    ax3.plot(t,s[i], label=f'${op}_{{{s_label[i]}}}$')

ax2.set_ylim((0.,1.5))
ax2.set_ylabel(r'$\xi^2$')
ax2.set_xlabel('$t$')
ax3.set_ylabel('$<S_i>/N$')
ax3.legend()
ax3.set_xlabel('$t$')

'''
#ax1.collections = []
start = time.time()
end = time.time()
print("Stream calculations:",end-start)
'''

#ax1.scatter(psi/np.pi,1/2, marker='+', color='purple', zorder=2.5)
#ax1.scatter(psi/np.pi,1/2, marker='+', color='red', zorder=2.5)


prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color'][3:]
markers = ['x', '1','+', '|', '_']
markers = ['o', 'v','s', 'H', 'd']

for k in range(len(timestamps)):
    t = timestamps[k]
    j = timestamp_steps[k]
    ax2.scatter(t, squeezing[j], 
            marker = markers[k],
            color = colors[k], label = f't={t}')
    ax3.scatter([t for i in range(3)], [s[j] for s in (s_x,s_y,s_z)], 
            marker = markers[k],
            color = colors[k])
    #ax3.axvline(t, color = colors[k])

    q_function = Q_function_representation(dt=dt, ax=axs[k], resolution = res, F = F, 
                                            ref_state = ref_state, initial_state = initial_state)
    Psi, Theta = q_function.Psi, q_function.Theta
    wf_psi_theta = q_function.wf_psi_theta
    X,Y = q_function.X, q_function.Y
    E,u,v = energy_fluc(Psi,Theta, F.H, wf_psi_theta)
    speed = np.sqrt(u**2+v**2)
    axs[k].streamplot(X,Y,u,v,
        density=.6, color='black', linewidth=3*speed/np.max(speed))

    for i in range(N_steps):
        im, title = q_function.update(i)
        if i >= timestamp_steps[k]:
            break

ax2.legend()
#axs[-1].set_title("")
#fig.suptitle(rf'$N={N}, J={J}, U={U}, \Omega={Omega}, \phi={phi/np.pi:.2f}\pi, \psi_0={psi/np.pi:.2f}\pi$')
fig.suptitle(rf'$N={N}, J={J}, U={U}, \Omega={Omega}, \phi={phi/np.pi:.2f}\pi$')
fig.savefig(filename+'.png', dpi = 300, transparent = True)
fig.savefig(filename+'.pdf')

plt.show()
