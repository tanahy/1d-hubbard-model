from matplotlib.gridspec import GridSpec
from matplotlib.colors import TwoSlopeNorm, Normalize
import matplotlib.cm as cm
import matplotlib.tri as tri

from hubbard.hubbard import *
from hubbard.utilities import * 
from hubbard.plot import radar_factory 

N = 8 
M = N

ER = 1# [hbar/Er] = 7e-6

U = ER
y   = np.linspace(1/20,1/2,50)
x = 2*np.pi/N*np.arange(1,N)
x_labels = [fr'$\frac{{ {2*k} }}{{ {N} }}\pi$' if 2*k != N else '$\pi$' for k in range(1,N)]

psi = np.pi/2
theta = np.pi/2
#bc = 'open'
#bc = 'periodic'
bc = 'ring'
frame = 'lab'

only_squeezing = True
TACT = 0
compare_stats  = 1
annotate_plots = 0
save = True

N_steps = 100

projection = 'polar' #'polar'#'radar' #'None', 'polar'
style = 'bar'#'pcolormesh'#'contourf'

l_axin = .3
ax_pad = .2
coords =[ 
        [[ 1, 48], [ -(l_axin+ax_pad),  .75]],
        [[ 1, 29], [ -(l_axin+ax_pad),  .35]],
        [[ 1, 20], [ -(l_axin+ax_pad), -.05]],
        #[[ 2, 48], [-0.4,  .75]],
        #[[ 2, 25], [-0.4,  (.75-.05)/2]],
        #[[ 2, 20], [-0.4, -.05]],
        [[ 0, 48], [1+ax_pad,  .75]],
        [[ 0, 35], [1+ax_pad,  .35]],
        [[ 0, 20], [1+ax_pad, -.05]],
        ]

############################################################

radar = radar_factory(N, frame='circle')

time_scale = 2

print('Plotted coords:')
for ij, _ in coords:
    i, j = ij
    print(x[i], y[j])

model_label = 'OAT'
if TACT:
    model_label = 'TACT'
    time_scale = 3
    if 2 * (N // 2) == N:
        x = np.delete(x, N // 2 - 1)
        x_labels.pop(N // 2 - 1)

subplot_kw = dict(projection = projection)
if projection == 'polar':
    subplot_kw = {
            'projection': projection,
            'rlabel_position': 0,
            'theta_zero_location': 'E',
            }

title = fr'$N={N}, M={M}, U={U}$'
filename = (f'output/r parameter testing for {model_label}, {title.replace("$","")}, ({frame} {bc}),'
             + f'init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')
datafile = filename.replace('output/', 'output/data/') + '.npz'

filename += f'style=({projection}, {style})'
si_tags = ['sx','sy','sz','sx2','sy2','sz2','sxsy','sysz','szsx']

ax_labels = [f'{val.replace("s","S_").replace("2","^2")}' for val in si_tags]
ax_labels.append(r'\xi^2')

fig_w = 5*(1 + 2*(1-only_squeezing) + 3*l_axin*annotate_plots + l_axin*compare_stats)
fig_h = 5*(1 + 3*(1-only_squeezing))
figsize = (fig_w, fig_h)

if only_squeezing:
    ax_labels = ax_labels[-1:]
    parameters = ['squeezing']
    
    fig1, ax1 = plt.subplots(1, subplot_kw=subplot_kw, figsize = figsize, constrained_layout=True)
    fig2, ax2 = plt.subplots(1, subplot_kw=subplot_kw, figsize = figsize, constrained_layout=True)
    axs_A = (ax1,)
    axs_B = (ax2,)
    #axs_C = (axs[2],)
else:
    parameters = si + ['squeezing']
    fig1 = plt.figure(constrained_layout=True, figsize = figsize)
    fig2 = plt.figure(constrained_layout=True, figsize = figsize)
    gs1 = GridSpec(4,3, figure = fig)
    gs2 = GridSpec(4,3, figure = fig)
    axs_A = []
    axs_B = []
    for i in range(3):
        for j in range(3):
            axs_A.append(fig.add_subplot(gs1[i,j], **subplot_kw))
            axs_B.append(fig.add_subplot(gs2[i,j], **subplot_kw))
    axs_A.append(fig.add_subplot(gs[3,:], **subplot_kw))
    axs_B.append(fig.add_subplot(gs[3,:], **subplot_kw))

def chi_square(x, y, axis = 1, aweights = None):
    assert np.shape(x) == np.shape(y)
    if aweights is None:
        norm = np.shape(x)[axis]
        res2 = np.square(y-x)
    else:
        norm = np.sum(aweights)
        res2 = aweights * np.square(y-x)

    return 1/norm * np.sum(res2/x, axis = axis)

def rmse(x, y, axis = 1):
    assert np.shape(x) == np.shape(y)
    if aweights is None:
        norm = np.shape(x)[axis]
        res2 = np.square(y-x)
    else:
        norm = np.sum(aweights)
        res2 = aweights * np.square(y-x)

    return np.sqrt(np.sum(res2/norm, axis = axis))

def corrcoeff2_weighted(m, aweights=None):
    corr = np.empty(m.shape[0]-1)

    cov = np.cov(m, aweights = aweights)
    for i in range(1,cov.shape[0]):
        corr[i-1] = np.square(cov[0,i]) / (cov[0,0] * cov[i,i])

    return corr

try:
    df = np.load(datafile)
    corr = df['corr']
    err  = df['err']
    chi_sq = df['chi_sq']
    data = df['data']

except FileNotFoundError:
    print('No data file found, computing results...')
    f = Hubbard(N, M, 'Fermi', bc = bc)
    f.change_frame(frame, phi = 0)

    Ox = f.Sx
    Oy = f.Sy
    Oz = f.Sz

    sx   = Ox
    sy   = Oy
    sz   = Oz
    sx2  = Ox * Ox
    sy2  = Oy * Oy
    sz2  = Oz * Oz
    sxsy = Ox * Oy + Oy * Ox
    szsx = Oz * Ox + Ox * Oz
    sysz = Oy * Oz + Oz * Oy

    si = [eval(val) for val in si_tags]

    wf = f.initial_state_fermi()
    wf = expm(-1j*Oy*theta).dot(wf)
    wf = expm(-1j*Oz*psi).dot(wf)

    corr   = np.empty((2, len(parameters), len(y), len(x)))
    chi_sq = np.empty((2, len(parameters), len(y), len(x)))
    err    = np.empty((2, len(parameters), len(y), len(x)))
    data   = np.empty((3, len(parameters), len(y), len(x), time_scale*N_steps))

    for i, phi in enumerate(x):
        for j, r in enumerate(y):
            f.phi = phi
            J = r*U
            Omega = r**3*U #alpha

            J_SE = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*np.cos(phi/2)**2)
            J_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*np.sin(phi/2)**2)
            E_phi = J_SE*(1-np.cos(phi))

            chi = J_SOC**2/(E_phi*(N-1))
            if phi != np.pi:
                chi /= 2

            t_f = 1/abs(chi)

            dt = t_f / (time_scale*N_steps)
            
            H_FH    = f.H_0(J=J) + f.H_int(U=U) + f.H_SOC(Omega = Omega) 
            H_eff   = f.H_eff(J=J,U=U,Omega=Omega) 
            H_model = -chi*Ox**2 if phi == np.pi else +chi*Oz**2
            if TACT:
                Omega_2 = Omega/np.sqrt(1-np.cos(phi))
                f_pi    = Hubbard(N, M, 'Fermi', bc = bc)
                f_pi.phi = np.pi
                H_FH    += f_pi.H_0(J=J) + f_pi.H_int(U=U) + f_pi.H_SOC(Omega = Omega_2) 
                H_eff   += f_pi.H_eff(J=J,U=U,Omega = Omega_2) 
                
                J_SE_pi = 4*J**2*U/(U**2-Omega_2**2)
                J_SOC_pi = Omega_2*(1-4*J**2/(U**2-Omega_2**2))
                E_pi    = J_SE_pi*2

                chi_pi = J_SOC_pi**2/(E_pi*(N-1))
                chi_pi /= 2

                t_f = 1/abs(chi_pi)
                dt = t_f / (time_scale*N_steps)

                H_model = chi_pi*(f.Sz**2-f.Sx**2)

            f.H = H_FH
            _, data_exact = f.evolve(wf, dt, t_f, parameter=parameters)
            f.H = H_eff
            _, data_eff   = f.evolve(wf, dt, t_f, parameter=parameters)
            f.H = H_model
            _, data_model   = f.evolve(wf, dt, t_f, parameter=parameters)

            chi_sq_eff   = chi_square(data_exact[:, :N_steps], data_eff  [:, :N_steps])
            chi_sq_model = chi_square(data_exact[:, :N_steps], data_model[:, :N_steps])

            chi_sq_ji = np.array((chi_sq_eff, chi_sq_model))
            chi_sq[:, :, j, i] = chi_sq_ji
            
            err_eff   = rmse(data_exact[:, :N_steps], data_eff  [:, :N_steps])
            err_model = rmse(data_exact[:, :N_steps], data_model[:, :N_steps])

            err_ji = np.array((err_eff, err_model))
            err[:, :, j, i] = err_ji
            
            data[0, :, j, i, :] = data_exact[:, :time_scale*N_steps]
            data[1, :, j, i, :] = data_eff  [:, :time_scale*N_steps]
            data[2, :, j, i, :] = data_model[:, :time_scale*N_steps]

            for k in range(len(parameters)):
                #ax1 = axs_C[k]
                #ax1.scatter(data_exact[k,:], data_eff[k,:], 
                #        color = 'red', 
                #        alpha = 1/len(parameters), 
                #        label = f'$r={r:.2f}, \phi = {phi/np.pi:.2f}\pi$')
                m = np.vstack((data_exact[k,:N_steps], data_eff[k,:N_steps], data_model[k,:N_steps]))
                corr_k = np.corrcoef(m)
                corr[:,k,j,i] = corr_k[0,1:]
            #Let us see if deleting stuff instead of relaying on the garbage collector is enough fo mem. issues
            del data_exact
            del data_eff
            del data_model
            del m
            del H_FH
            del H_model
            del H_eff

    data_dict = {
            'corr': corr,
            'chi_sq': chi_sq,
            'err': err,
            'data': data,
            }
    np.savez(datafile, **data_dict)

corr = corr**2
#mask the rmse and chi2 outliers
#chi_sq  [chi_sq > 1e0]  = np.nan
#err     [err >    1e0]  = np.nan

width  = np.pi/N

cmap = plt.colormaps['PiYG']

N_cticks = 10
corigin = .8
low, high = 0, 1 #corr.min(), corr.max()
norm = TwoSlopeNorm(corigin)
smap_corr = cm.ScalarMappable(norm=norm, cmap=cmap)
smap_corr.set_clim(low,high)
cticks = np.concatenate((
    np.linspace(low,  corigin, N_cticks//2 - 1, endpoint=False), 
    np.linspace(corigin, high, N_cticks//2)
    ))
smap_corr.set_array(cticks)
levels = cticks

if compare_stats:
    width /= 3
    cmap = plt.colormaps['Reds']
    norm = Normalize(vmin = corr.min(), vmax =  corr.max())
    cmap_2 = plt.colormaps['Greens_r']
    norm_2 = Normalize(vmin = np.nanmin(chi_sq), vmax =  np.nanmax(chi_sq))
    cmap_3 = plt.colormaps['Blues_r']
    norm_3 = Normalize(vmin = np.nanmin(err), vmax =  np.nanmax(err))
    smap_corr   = cm.ScalarMappable(norm=norm, cmap=cmap)
    smap_chi_sq = cm.ScalarMappable(norm=norm_2, cmap=cmap_2)
    smap_err    = cm.ScalarMappable(norm=norm_3, cmap=cmap_3)

for fig, ax in ((fig1, axs_A),(fig2, axs_B)):
    if compare_stats:
        cb_corr     = fig.colorbar(smap_corr,   ax=ax, label = '$R^2$'   )
        cb_chi_sq   = fig.colorbar(smap_chi_sq, ax=ax, label = '$\chi^2$')
        cb_err      = fig.colorbar(smap_err,    ax=ax, label = 'RMSE'    )
    else:
        cb_corr     = fig.colorbar(smap_corr,   ax=ax, label = '$R^2$', ticks = cticks)#, pad = .1)
'''
triangles = np.empty((2*(len(x)-1)*len(y),3))

for i,xi in enumerate(x):
        

triang = tri.Triangulation(x, y, triangles)
'''
for i in range(len(parameters)):
    for j, (axs, model) in enumerate(((axs_A, 'eff'), (axs_B, model_label))):
        ax = axs[i]
        z        = corr  [j,i,:,:]
        if compare_stats:
            z_chi_sq = chi_sq[j,i,:,:]
            z_rmse   = err   [j,i,:,:]

        X,Y = np.meshgrid(x,y)
        
        if style == 'bar':
            for k,r in enumerate(y):
                height = y[k] - y[k-1] if k > 0 else y[k] 
                bar_corr = ax.bar(x, height, 
                        color = cmap(norm(z[k,:])),
                        bottom = y[k]-height/2,
                        width = width,
                        )
                if compare_stats:
                    bar_chi_sq = ax.bar(x-width, height, 
                            color = cmap_2(norm_2(z_chi_sq[k,:])),
                            bottom = y[k]-height/2,
                            width = width,
                            )
                    bar_srme = ax.bar(x+width, height, 
                            color = cmap_3(norm_3(z_rmse[k,:])),
                            bottom = y[k]-height/2,
                            width = width,
                            )
                if len(y) < 15:
                    ax.bar_label(bar_corr, labels = [f'{a:.2f}' for a in z[k,:]], label_type='center')

        elif style == 'pcolormesh':
            patch = ax.pcolormesh(x, y, z,
                    cmap = cmap,
                    norm = norm,
                    )
         
        elif style == 'triang':
            patch = ax.tricontourf(triang, z,
                    cmap = cmap,
                    norm = norm,
                    )
            ax.scatter(X,Y, c=z,
                    cmap = cmap,
                    norm = norm,
                    )
         
        elif style == 'contour':
            patch = ax.contour(x, y, z, 
                    cmap = cmap, 
                    norm = norm,
                    #antialiased = False,
                    #nchunk = 1,
                    levels = 10,#levels,
                    )


        elif style == 'contourf':
            patch = ax.contourf(x, y, z, 
                    cmap = cmap, 
                    norm = norm,
                    antialiased = False,
                    nchunk = 0,
                    levels = 10,#levels,
                    )

        if projection is None:
            ax.set_ylabel('r')
        ax.set_xlabel('$\phi$')
        ax.set_title(f'Correlation (${ax_labels[i]}$) {model}-exact')
        ax.set_xticks(x, x_labels)
        #ax.set_yticks((y.min(), y.mean(), y.max()), [r'$\frac{1}{20}$', r'$\frac{1}{10}$', r'$\frac{1}{2}$'])
    '''
    ax1 = axs_A[i]
    ax1.bar(y - width/2, r_err[0,i,:], width, label = 'eff')
    ax1.bar(y + width/2, r_err[1,i,:], width, label = model_label)
    ax1.plot(y, r_err[1,i,:], 
            ls = 'None',
            marker = 'o',
            label = model_label)
    ax1.legend()
    ax1.set_xlabel('r')
    ax1.set_ylabel(f'Covariance (${ax_labels[i]}$)')
    
    for ax, model in ((ax2,'eff'), (ax3,model_label)):
        #ax.legend()
        ax.set_xlabel(f'${ax_labels[i]}$ (exact)')
        ax.set_ylabel(f'${ax_labels[i]}$ ({model})')
    '''
'''
for axs in (axs_C,):
    ax = axs[-1]
    y_b, y_t = ax.get_ylim()
    x_b, x_t = ax.get_xlim()
    ax.set_xlim((max(x_b, y_b, 0), min(1.5, x_t, y_t)))
    ax.set_ylim((max(x_b, y_b, 0), min(1.5, x_t, y_t)))
'''
if annotate_plots:

    step = 6
    t = np.arange(0,time_scale*N_steps,step)
    axs = axs_A[-1], axs_B[-1]
    labels = 'eff', model_label
    for k in range(len(axs)):
        ax = axs[k]
        label = labels[k]
        first_axin = True
        for xy_index, axin_coords in coords:
            i,j = xy_index
            center_axin = [axin_coords[l] + l_axin/2 for l in range(2)]
            axin_bounds = axin_coords + [l_axin, l_axin]
            axin = ax.inset_axes(axin_bounds)
            axin.plot(t, data[k+1, -1, j, i, ::step], label = label)
            axin.plot(t, data[0  , -1, j, i, ::step], label = 'exact', 
                    ls = 'None', marker = '+') 
            axin.axvline(t[-1]/time_scale, ls = 'dashed', alpha = .5)
            axin.set_xticks((0, t[-1]/time_scale, t[-1]), 
                    ('0', fr'$\frac{{t_\chi}}{{ {time_scale} }}$', '$t_\chi$'))
            axin.set_yticklabels([])
            axin.set_xlim(0, t[-1])
            axin.set_ylim(0, 1.5)
            height = y[j] - y[j-1] if j > 0 else y[j] 
            height *= 3
            box = ax.bar(x[i], height, 
                        fill = False,
                        edgecolor = 'blue',
                        linewidth = 1,
                        bottom = y[j]-height/2,
                        width = width,
                        )
            arrowprops = dict(arrowstyle="fancy",
                                    patchA=axin,
                                    #patchB=box, 
                                    color="0.5",
                                    alpha=.8, 
                                    #shrinkA=1,
                                    #shrinkB=5,
                                    )
            ax.annotate('', 
                    xy = (x[i], y[j]), xycoords = 'data',
                    xytext= center_axin, textcoords = 'axes fraction',
                    arrowprops=arrowprops
                    )
            if first_axin:
                first_axin = False
                axin.legend(fontsize=6)


for fig, label in ((fig1, 'eff'), (fig2, model_label)):
    fig.suptitle(title+fr', $\|\Psi_0\rangle = \|\theta={theta/np.pi:.2f}\pi,\phi={psi/np.pi:.2f}\pi \rangle  $')
    
    #fig.set_constrained_layout_pads(w_pad=.5)
    if save:
        if compare_stats:
            label += ', comparing stats'
        fig.savefig(f'{filename} ({label}).pdf')

#plt.show()
