import sys,os
#os.environ['OMP_NUM_THREADS']='12' # set number of OpenMP threads to run in parallel
#os.environ['MKL_NUM_THREADS']='6'  # set number of MKL threads to run in parallel

import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from hubbard.hubbard import *
from hubbard.utilities import coherent_states, q_function_calc, chi_calc, J_calc
from hubbard.plot import *
from hubbard.bloch import *

###############################

N = 6
bc = 'ring'
phi_ind = 1
if len(sys.argv) >= 3:
    N = int(sys.argv[1])
    bc = str(sys.argv[2])
elif len(sys.argv) >= 4:
    N = int(sys.argv[1])
    bc = str(sys.argv[2])
    phi_ind = int(sys.argv[3])

M = N

time_scale = 7e-6 #[hbar/ER] = 7 micro seconds
ER = 1

N_steps = 1000

res = 100


include_FH = True
light_calc = False
if light_calc:
    include_FH = False
scheme = 'parallel'
calc_q = False

initial_states = 'y',
initial_states_coords = pi/2 * np.array(((1,1),))

N_range = False

N_range = range(4, N+1, 2) if N_range else (N,)

for N in N_range:
    U = .5/ER
    J = 0.02
    J_SE = 4*J**2/U
    phi = np.pi*(1+2/N)
    Omega = 0.04*J_SE
    #print(J_calc(N, U, J, Omega, phi))
    #print(1/chi_calc(N, phi, U, J, Omega))
    phi_list = [2*pi/N*k for k in range(1,N)]
    phi = phi_list[phi_ind-1]

    if light_calc:
        f = FermiHubbard(N,M,bc=bc, basis = 'single')
    else:
        f = FermiHubbard(N,M,bc=bc)
    f.set_scheme(scheme)
    for model in ('OAT', 'TACT'):
        for i, phi in enumerate(phi_list):
            f.phi = phi
            Omega_2   = Omega
            Omega_1   = Omega / np.sqrt(1-np.cos(phi))

            chi_pi  = chi_calc(N, np.pi, U, J, Omega_1)
            chi_phi = chi_calc(N,   phi, U, J, Omega_2)

            #assert .95 < chi_pi / chi_phi < 1.05

            chi = chi_phi #/ 2
            t_f = 1/chi #* 3/5
            if model=='TACT':
                t_f *= .2*N/(N-1)
            else:
                t_f *= .4*N/(N-1)
            #if bc == 'open':
            #    t_f *= .25
            #if J/U < .05:
            #    t_f *= .01
            
            for j in range(len(initial_states)):
                theta, psi = initial_states_coords[j]
                datafile = (f'output/data/{model} N={N}, U={U}, J={J}, '
                        +f'Omega={Omega}, ini={initial_states[j]}, phi={phi/pi:.4f}pi, bc={bc}'
                        + (', light_calc' if light_calc else '')
                        + (', excl_FH' if not include_FH and not light_calc else '')
                        +'.npz')
                data_loaded = False

                saved_arrays = dict()

                ref_state = f.initial_state()
                initial_state = f.rotate((theta, psi), ref_state)

                if model == 'TACT':
                    H0 = chi*(f.Sz**2-f.Sx**2)
                    H1  = f.H_spin(J/2,U/2,Omega_1, np.pi)
                    H2  = f.H_spin_simple(J/2,U/2,Omega_1, np.pi)
                    H1 += f.H_spin(J/2,U/2,Omega_2, phi)
                    H2 += f.H_spin_simple(J/2,U/2,Omega_2, phi)

                else:
                    if phi == np.pi:
                        H0 = chi*f.Sx**2
                    else:
                        H0 = chi*f.Sz**2
                    H1  = f.H_spin(J, U, Omega_2, phi)
                    H2  = f.H_spin_simple(4*J**2/U, Omega_2, phi)

                H = [H0, H1, H2]

                if include_FH:
                    if model == 'TACT':
                        H3 = f.H_0(J) + f.H_int(U) + f.H_SOC(Omega_1, np.pi) 
                        H3 += f.H_SOC(Omega_2, phi)              
                    else:
                        H3 = f.H_0(J) + f.H_int(U) + f.H_SOC(Omega_2, phi) 
                    H += [H3]

                data = []
                data_2D = []
                for H_i in H:
                    f.H = H_i
                    t, data_i = f.evolve(initial_state, t_f, N_steps,
                                         parameters = ['squeezing'])
                    data.append(data_i)
                data = np.vstack(data)
                
                ind = list(data[0,:]).index(data[0,:len(t) // 2].min())
                t_best = t[ind]
                xi_best = data[-1,:][ind]

                if calc_q:
                    _, (_, data_q) = f.evolve(initial_state, t_best,
                                               q_calc = True, 
                                               cs_props = {'resolution': res},
                                               )
                    data_2D = data_q[:, :, -1]

                saved_arrays['t']      = t 
                saved_arrays['phi']      = phi
                saved_arrays['xy_best'] = np.array((t_best, xi_best))
                saved_arrays['data']   = data
                saved_arrays['data_2D'] = data_2D
                np.savez(datafile, **saved_arrays)

