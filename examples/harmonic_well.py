from hubbard.hubbard import *
import matplotlib.pyplot as plt

N = 6

f = Hubbard(N, N, 'Fermi')

#print(f.A)
f.S_calc()
f.N_j_calc()
m = 2
w = 1
Omega = 0 
U = 0 

dV = .5*m*w**2*(N-1)//2*( N-1-(N-1)//2 )
dV = .5*m*w**2*(N-1)//2*np.ceil((N-1)/2)

#U = dV

V = f.V_harmonic_fermi(m, w) + Omega*f.Sz + f.H_int(U) 
diag = V.diagonal()

coords = []
for k in range(f.D):
    v = f.A[k]
    nk = [Nj[k,k] for Nj in f.N_j]
    x = nk.count(2)
    y = V[k,k] - diag.mean()
    coord = (x, y.real)
    coords.append(coord)

coords_set = set(coords)
counts = []
for coord in coords_set:
    m = coords.count(coord)
    counts.append(m)

x = [v[0] for v in coords_set]
y = [v[1] for v in coords_set]
plt.scatter(x, y, s=counts)
plt.ylabel('V')
plt.xlabel('# of doublons')
#plt.hist(diag, bins=int(max(diag)-min(diag)))

plt.show()

'''
s = Hubbard(N, N, 'total_spin')

s.S_calc()

print(s.T)
print(s.Sp.real)
print()
print(s.Sz.real)
#print(s.Sm.todense().real)
'''
