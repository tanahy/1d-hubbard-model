import sys
import time

import numpy as np
import matplotlib.pyplot as plt
from hubbard.hubbard import *
from hubbard.utilities import *
from hubbard.bloch import *

###############################
N = 10
M = N
res = 200
psi = 0
theta = 0
n_snapshots = 6
xi = 1
t_f = .25 / xi #18400
scheme = 'parallel'
###############################

filename = f'output/portraits/bloch_snapshot'

def gaussian_2d(x,y,x0,y0,A=1,sigma=(1,1)):
    return A*np.exp(-((x-x0)**2/(2*sigma[0]) + (y-y0)**2/(2*sigma[1])))

s = SpinModel(N,M)
s.set_scheme(scheme)
s.S_calc()

s.H = s.Sm

wf0 = s.initial_state()

ref_state = wf0 
initial_state = s.rotate((theta, psi), wf0)

wf = initial_state

fig = plt.figure(figsize=(6,6)) 
ax = fig.add_subplot(projection='3d') 

s.calc_coherent_states(resolution= res)

axes_style = dict(span = 0,#.25,#0,# 
        margin = 0.1,#.25,# 
        color = 'red',
        fontsize = 15,
                  )
b = Bloch_Sphere(alpha = .1,
        ax = ax,
        cmap = cm.viridis,
        draw_axes = True,
        axes_style = axes_style
        )
b.ax.view_init(15, 30)

def q_array(wf):
    data = s.cs_array.conj().dot(wf)
    data = abs(data)**2
    data /= data.max()
    return data

wf_1 = ref_state

wf_2 = ((s.Sm)**(N//2)).dot(ref_state)
wf_2 /= wf_2.max()

wf_3 = ((s.Sm)**(N)).dot(ref_state)
wf_3 /= wf_3.max()

b.plot_surface(q_array(wf_1))
#fig.savefig(f'{filename}_m=N2.png')
b.plot_surface(q_array(wf_2))
#fig.savefig(f'{filename}_m=0.png')
b.plot_surface(q_array(wf_3))
#fig.savefig(f'{filename}_m=-N2.png')

plt.show()
