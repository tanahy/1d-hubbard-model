import sys
import time

import numpy as np
import matplotlib.pyplot as plt
from hubbard.hubbard import *
from hubbard.utilities import *
from hubbard.bloch import *

###############################
N = 6
M = N
res = 100
scheme = 'parallel'
scheme = 'default'
scheme = 'multiply'
###############################

filename = f'output/portraits/bloch_sws'

s = FermiHubbard(N,M, basis = 'single')
#s = FermiHubbard(N,M)
s.set_scheme(scheme)
s.S_calc()
s.S_j_calc()

initial_state = s.initial_state() 

ref_state = initial_state

fig = plt.figure(figsize=(6,6)) 
ax = fig.add_subplot(projection='3d') 

s.calc_coherent_states(resolution= res)

axes_style = dict(span = 0,#.25,#0,# 
        margin = 0.1,#.25,# 
        color = 'red',
        fontsize = 15,
                  )
b = Bloch_Sphere(alpha = .1,
        ax = ax,
        #cmap = cm.inferno, 
        #cmap = cm.magma_r, 
        #cmap = cm.jet, 
        #cmap = cm.hot_r,
        cmap = cm.viridis,
        draw_axes = True,
        axes_style = axes_style
        )
#b.ax.view_init(90-theta*180/np.pi, psi*180/np.pi)
b.ax.view_init(15, 30)

'''
t, (data, wf_array) = s.evolve(initial_state, dt, t_f,
                               parameters = ['Sz'],
                               scheme = 'parallel',
                               save_wf = True,
                               )
'''

def q_array(wf):
    data = cs_array.conj().dot(wf)
    data = abs(data)**2
    data /= data.max()
    return data
'''
for i, t in enumerate(t):
    print(i)
    
    data = q_array(wf_array[:, i])

    #data = gaussian_2d(X,Y,1/(2*5),1/2,A=1,sigma=(1,.05))

    b.plot_surface(data)
    #b.plot_linear_move((0,np.pi/2),(0,0))
    #b.plot_linear_move((np.pi/2,np.pi/2),(0,psi))

    #fig.savefig(f'{filename}_{i}.svg')
    fig.savefig(f'{filename}_{i}.png')
'''


#for m in np.arange(-N/2, -N/2+2, 1):
wf = initial_state
for m in np.arange(N/2, -N/2, -1):
    print('m:', m)
    #wf = ((s.Sm)**(N/2-m)).dot(ref_state)
    wf = s.Sm.dot(wf)
    wf /= np.sqrt(wf.conj().dot(wf))
    #for n in range(1, M):
    #for n in range(0, 3):
    for n in (0, M//2, M-1,):
        q = 2*np.pi*n/M
        wf_n = sum([ np.exp(1j*q*(j+1))*s.Sp_j[j].dot(wf) for j in range(M)])
        wf_n /= np.sqrt(wf_n.conj().dot(wf_n))
        arrows = []
        for j in range(M):
            x = expectation_value(wf_n, s.Sx_j[j]).real
            y = expectation_value(wf_n, s.Sy_j[j]).real
            z = expectation_value(wf_n, s.Sz_j[j]).real
            print(x, y, z, b.r)
            arrow = Arrow3D((0, x), (0, y), (0, z), color = 'green')
            arrows.append(arrow)
            b.ax.add_artist(arrow)
        b.plot_surface(q_array(wf_n))
        fig.savefig(f'{filename}_N={N}_m={m}_n={n}.png')
        for arrow in arrows:
            arrow.remove()

#plt.show()
