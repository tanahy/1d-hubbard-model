from scipy.linalg import eig 

from hubbard.hubbard import *

N = 4
#bc = 'periodic'
bc = 'open'
stat = 'Fermi-single-ocupancy'#'Fermi'
stat = 'Fermi'

phi = np.pi
J       = .01
U       = 1
J_SE    = 4*J**2/U
Omega   = J_SE/40

F = Hubbard_SW(N=N,M=N,stat=stat,bc=bc)
F.change_frame('lab', phi=phi)

F.S_fermi_calc_j()

if bc == 'periodic':
    j_range = range(N)
else:
    j_range = range(N-1)


def H_XX(alpha):
    H = 0
    for i in range(N):
        for j in range(N):
            if j != i:
                C = -J/abs(i-j)**alpha
                H += C*(F.Sx_j[i]*F.Sx_j[j] + F.Sy_j[i]*F.Sy_j[j])
    return H

#H_XX_inf = sum([F.Sx_j[j]*F.Sx_j[(j+1) % N] + F.Sy_j[j]*F.Sy_j[(j+1) % N] for j in j_range])
#H_XX_inf = -J*H_XX_inf

#H_1 = H_XX(alpha=1)
#H_2 = H_XX(alpha=3)
#H_3 = H_XX_inf
#H_2 = F.H_0(J) + F.H_int(U) + F.H_L(Omega)
H_eff = F.H_SE(J_SE,Omega)
H_OAT = J*F.Sz**2
h     = H_OAT-H_eff
#H_0 = -J*(F.Sx*F.Sx + F.Sy*F.Sy)

H_list = [
        H_eff,
        H_OAT,
        h,
        ]

H_labels = [
        '$H_{eff}$',
        '$H_{OAT}$',
        '$h=H_{OAT}-H_{eff}$',
        ]
#CSS = expm(-1j*np.pi/2*F.Sy).dot(F.initial_state_fermi())
fig, ax = plt.subplots()#(1,len(H_list), figsize=(5*len(H_list), 5))

bins = 50#np.arange(-.2,.2,50)
eigenvectors = []
for i in range(len(H_list)):
    w, v = eig(H_list[i].toarray(), check_finite=False)
    #ax.scatter(w,i*np.ones(len(w)), label = H_labels[i])
    ax.hist(w, bins=bins, range=(-.2,.2),label=H_labels[i], alpha=.3)
    eigenvectors.append(v)

#threshold = 1e-5
threshold = .95

for i,v in enumerate(F.A):
    print(i,v)

counter = 0
for i in range(len(eigenvectors[2][0,:])):
    v0 = eigenvectors[2][:,i]
    found = True
    for j in range(2):
        for k in range(len(eigenvectors[j][0,:])):
            v1 = eigenvectors[j][:,k]

            #if sum(abs(v0-v1)**2) < threshold:
            if abs(v0.conj().dot(v1)) > threshold:
                found = False
    if found:
        coeffs = v0#(v0/max(v0)).real
        print([f'({coeffs[a]:.3f}, {a})' for a in range(len(coeffs)) if coeffs[a] > 1e-4])
        counter += 1
print(f'Number of unique eigenstates found: {counter}/{eigenvectors[2].shape[1]}')


ax.set_xlabel('E')
ax.set_ylabel('degeneracy')
ax.set_title(f'N={N}, {stat}')
ax.legend()
plt.show()
