import sys

from hubbard.hubbard import *
from hubbard.utilities import * 

N = 6
M = N
U = .5
J = .02, #.0014#U*66/1700
J_SE = 4*J[0]**2/U
Omega = 0.04*J_SE, 0.004*J_SE, 1e-5*J_SE #1/20, 7/100, 1/10, 1/5000#,0,1,#0.54,#30,.004#np.sqrt(2*U**2)#0, 4e-4, 4e-2 
phi = 2*np.pi/N #/50#1.578#3.20571
#J_par = J**2/U*(U**2*np.cos(phi)-Omega**2*np.cos(phi/2)**2)/(U**2-Omega**2)
#J_per = J**2/U*(U**2-Omega**2*np.cos(phi/2)**2)/(U**2-Omega**2)
#xi = 2*(J_par-J_per)/(M-1)
t_f = 1e0 #*2*np.pi/J#4/xi
#Omega = Omega,
#U = U,
theta = np.pi/2
psi = np.pi/2
#bc = 'periodic'
#bc = 'ring'
bc = 'open'
frame = 'lab'
alpha = 1

display_theo = False
display_imag = False
ground_state = False
save = True

scheme = 'parallel'

###
# Input Format
# N J Omega U phi | t_f | psi | bc frame
###
if len(sys.argv) >= 6:
    print('Parameters introduced by command line')
    N = int(sys.argv[1])
    M = N
    J, Omega, U = [float(arg.strip(',')) for arg in sys.argv[2:5]]
    phi = sys.argv[5]
    if 'pi' in phi:
        phi = eval(phi.replace('pi','*np.pi'))
    else:
        phi = float(phi)
    Omega = Omega,
    U = U,
    if len(sys.argv) >= 7:
        t_f = float(sys.argv[6])
    if len(sys.argv) >= 8:
        psi = sys.argv[7]
        if 'pi' in psi:
            psi = eval(psi.replace('pi','*np.pi'))
        else:
            psi = float(psi)
        #psi_fm, psi_fp  = critical_psi(M,phi)
        #psi = psi_loop(psi_fm+psi) 
    if len(sys.argv) >= 10:
        bc, frame = [arg.strip(',') for arg in sys.argv[8:10]]

N_steps = 500
dt = t_f / N_steps
F = Hubbard(N, M, 'Fermi', bc = bc)
#F = Hubbard(N, M, 'Bose_spin', bc = bc)
F.change_frame(frame, phi = phi, alpha = alpha)

if frame == 'lab':
    spin_op = 'S'
    Ox     = F.Sx
    Oy     = F.Sy
    Oz     = F.Sz
elif frame == 'rotated':
    spin_op = 'J'
    Ox     = F.Jx
    Oy     = F.Jy
    Oz     = F.Jz

'''
F.S_fermi_calc_j()
Sx = F.Sx_j
Sy = F.Sy_j
Sz = F.Sz_j

Sp = [Sx[j] + 1j*Sy[j] for j in range(M)]
Sm = [Sx[j] - 1j*Sy[j] for j in range(M)]

H_Lplus = sum([np.exp(1j*phi*j)*Sp[j] for j in range(N)]) 
'''

for omega in Omega:
    fig, (ax1, ax2) = plt.subplots(2)
    for j in J:
        print(j, omega)
        #F.H = F.H_0(J=.01) + F.H_int(U=1) + F.H_dipolar_fermi(gamma = omega)# + F.H_L(Omega = omega) 
        #F.H = F.H_0(J=.01) + F.H_int(U=1) + F.H_L(Omega = 0.0001)# + F.H_L(Omega = omega) 
        #S2 = F.Sz**2 + F.Sx**2 + F.Sy**2
        #F.H = S2 + F.H_L(Omega = 1)# + F.H_L(Omega = omega) 
        #F.H = S2 - F.Sz**2 + F.H_L(Omega = omega)# + F.H_L(Omega = omega) 
        #F.H = F.Sz**2 + F.H_L(Omega = omega)# + F.H_L(Omega = omega) 
        #F.H = F.Sz + omega*H_Lplus# + F.H_L(Omega = omega) 
        #chi = 1e-3
        #F.H = F.Sz - chi*S2 -chi*F.Sz**2# + F.H_L(Omega = omega) 
        #F.H = F.Sz - chi*F.Sz**2# + F.H_L(Omega = omega) 
        #F.H = F.H_dipoles1_bose_spin(J=J, U = (u,u,u), gamma = omega)
        #F.H = np.conj(F.R().T)*F.H*F.R()
        #F.H = F.H_SE(J=4*J**2/u, Omega=omega, w=0)
        #F.H = F.H_SE(J=100/4, Omega=1/2, w=0)
        #F.H = F.Sz**2
        #F.H = np.conj(F.R().T)*F.H*F.R()
        F.H = F.H_eff(j, U, omega)
        chi = chi_calc(N, phi, U, j, omega)
        t_f = 1/abs(chi)
        dt = t_f / N_steps
        #print(F.H)
        '''
        S2 = Ox * Ox + Oy * Oy + Oz * Oz 
        #S2 = F.Sx * F.Sx + F.Sy * F.Sy + F.Sz * F.Sz 
        if u==2:
            u = 1
            #H_spin = -u/M*S2 + F.B_fermi_pos_ring(J=J,a=1)  
            #H_spin = -u/M*S2 + F.B_fermi_pos_open(J=J,a=1)  
            H_spin = -F.H_int(U=u) + F.B_fermi_pos(J=J,a=1)  
            F.H = H_spin
        if u==3:
            u = 1
            H_spin = -u/M*S2 + F.B_fermi(J=J,a=1)
            F.H = H_spin
        '''
        if ground_state:
            F.eigenvalues()
            wf = F.G
        else:
            wf = F.initial_state_fermi()
        #F.H = F.H_xi(J)#F.H_0(J=J) + F.H_int(U=u) + F.H_L(Omega = omega) 
        #F.H = F.Sz*F.Sz

        assert calculate_ortho(wf, expm(-1j*Oy*np.pi).dot(wf))
        
        #wf2 = F.R.dot(wf)
        wf2 = expm(-1j*Oy*theta).dot(wf)
        wf2 = expm(-1j*Oz*psi).dot(wf2)
        
        t, data = F.evolve(wf2, dt, t_f, parameters = ['squeezing', Ox, Oy, Oz], scheme = scheme)
        s = data[0, :]
        x = data[1, :]
        y = data[2, :]
        z = data[3, :]

        ax1.plot(t, s, label = rf'$\xi^2, J = {j}$')
        ax2.plot(t, np.real(x), ls = 'solid', label = f'${spin_op}_x, J = {j}$')
        ax2.plot(t, np.real(y), ls = 'dashed', label = f'${spin_op}_y, J = {j}$')
        ax2.plot(t, np.real(z), ls = 'dotted', label = f'${spin_op}_z, J = {j}$')
        if display_imag:
            ax2.plot(t, np.imag(x), label = f'$\Im ({spin_op}_x), U = {u}$')
            ax2.plot(t, np.imag(y), label = f'$\Im ({spin_op}_y), U = {u}$')
            ax2.plot(t, np.imag(z), label = f'$\Im ({spin_op}_z), U = {u}$')
        if display_theo:#J == 0 and u == 0:
            print('Theoretical results for <S_i> included')
            T = SOC_Evolution_plus(N, np.pi/2, psi, omega, phi)
            t_list = np.array(t_list)
            ax2.plot(t, np.real(T.sx(t)), linestyle = 'dashed', label = f'Sx (theo), U = {u}')
            ax2.plot(t, np.real(T.sy(t)), linestyle = 'dashed', label = f'Sy (theo), U = {u}')
            ax2.plot(t, np.real(T.sz(t)), linestyle = 'dashed', label = f'Sz (theo), U = {u}')
            T = SOC_Evolution_Marlena(N, np.pi/2, psi, omega, phi)
            ax2.plot(t, np.real(T.sx(t)), linestyle = 'dotted', label = f'Sx (Marlena), U = {u}')
            ax2.plot(t, np.real(T.sy(t)), linestyle = 'dotted', label = f'Sy (Marlena), U = {u}')
            ax2.plot(t, np.real(T.sz(t)), linestyle = 'dotted', label = f'Sz (Marlena), U = {u}')
        ax1.set_title(fr'$ N={N}, M={M}, \phi = {phi/np.pi:.2f}\pi, \Omega = {omega}, J = {j:.2f}$')
        #plt.title(fr'$ N={N}, M={M}, \phi = \pi/{round(np.pi/phi)}, \Omega = {omega}, J= {J}$, {F.frame} frame')
    ax1.set_ylim((0,1.5))
    #plt.ylabel('$<J_x>_r$')
    ax1.legend()
    ax2.legend(loc='center right')
    ax1.set_ylabel(rf'$\xi^2$, {spin_op} op.')
    #plt.ylabel(f'$<S_->$')
    ax2.set_xlabel('time')
    if save:
        title = f'output/N={N}, M={M}, phi={phi/np.pi:.2f}pi, J={j:.2f}, U={U}, Omega={omega}, {spin_op} op ({frame} {bc})'
        if psi != 0:
            title += f'psi={psi/np.pi:.2f}pi'
        if ground_state:
            title += ', Psi(0)=GS'
        #title += '(S-)'
        plt.savefig(title+'.pdf')
        np.savetxt(title+'.txt',np.array([t,s]).T)
plt.show()
