from scipy.integrate import solve_ivp
from matplotlib.gridspec import GridSpec

from hubbard.hubbard import *
from hubbard.utilities import * 

psi = np.pi/2
theta = np.pi/2
bc = 'periodic'
frame = 'lab'

N_range = [int(val) for val in np.logspace(2, 6, 1000)]
#N_range = [int(val) for val in np.linspace(100, 100000, 40)]

alpha = 1
beta_range = -np.logspace(-1, 0, 10) 
beta_range = -np.logspace(np.log10(.3), 0, 8) 
#beta_range = [-1] 

only_squeezing = True#False
save = True

N_steps = 1000
stats = 'Fermi'
#stats = 'Bose_spin'

si_tags = ['sx','sy','sz','sx2','sy2','sz2','sxsy','sysz','szsx']

parameters = si_tags + ['squeezing']

ax_labels = [f'${val.replace("s","S_").replace("2","^2")}$' for val in si_tags]
#ax_labels.append(r'$F_Q$')
ax_labels.append(r'$\xi^2$')
if only_squeezing:
    ax_labels = ax_labels[-1:]
    parameters = parameters[-1:]

def ode_asym_TACT_0(t, y, a, b):
    x, y, z, x2, y2, z2, xy, yz, zx = y
    dx = -a*yz
    dy = (a-b)*zx
    dz = b*xy
    dx2 = -2*a*(zx*y+xy*z+yz*x-4*x*y*z)
    dy2 = 2*(a-b)*(zx*y+xy*z+yz*x-4*x*y*z)
    dz2 = 2*b*(zx*y+xy*z+yz*x-4*x*y*z)
    dxy = 4*(a-b)*(x2*z+zx*x-2*x**2*z) - 4*a*(y2*z+yz*y-2*y**2*z)
    dyz = 4*(a-b)*(z2*x+zx*z-2*z**2*x) + 4*b*(y2*x+xy*y-2*y**2*x)
    dzx = -4*a*(z2*y+yz*z-2*z**2*y) + 4*b*(x2*y+xy*x-2*x**2*y)

    return dx, dy, dz, dx2, dy2, dz2, dxy, dyz, dzx

def Kajtoch_2015(t, y0):
    x0, y0, z0, x20, y20, z20, xy0, yz0, zx0 = y0
    y = y0 - (np.cosh(4*y0*t)-1)/(4*y0)
    squeezing = np.exp(-4*y0*t + (np.sinh(4*y0*t)-t)/(4*y0**2))
    return y, squeezing

xi_bbeta = []
t_bbeta = []
for beta in beta_range:
    if only_squeezing:
        fig, ax = plt.subplots(1, constrained_layout=True)
        axs = [ax,]
    else:
        fig = plt.figure(constrained_layout=True, figsize=(15,20))
        cols = 3
        rows = len(si_tags)//cols + 1
        gs = GridSpec(rows,cols, figure = fig)
        axs = []
        for i in range(rows-1):
            for j in range(cols):
                axs.append(fig.add_subplot(gs[i,j]))
        #axs.append(fig.add_subplot(gs[rows-1,:1]))
        axs.append(fig.add_subplot(gs[rows-1,:]))
    
    fig.suptitle(fr'$\beta={beta}$')
    lines = ['solid','dashed', 'dashdot']+12*['None']
    markers = 3*['None']+['+', '^', 's', 'o', '*', 'x','<','>','1','2','3','4']

    xi_best = []
    t_best = []
    for N in N_range:
        calc_exact = True
        if N > 10:
            calc_exact = False
        norm = 1 #abs(alpha)+abs(beta)#np.sqrt(abs(alpha)**2 + abs(beta)**2)
        #norm = np.sqrt(abs(alpha)**2 + abs(beta)**2)
        scaling_N = 1
        scaling_N = (np.log(N)/N)
        if beta/alpha == -1:
            scaling_N = (np.log(N)/N)
        elif beta == 0:
            scaling_N = N**(-2/3)
        t_f = 1/norm*scaling_N
        M = N
        #along y
        y0 = (0, N/2, 0, N/4, N**2/4, N/4, 0, 0, 0)
        #along x
        #y0 = (N/2, 0, 0, N**2/4, N/4, N/4, 0, 0, 0)

        H = []
        if calc_exact:
            f = Hubbard(N, M, stats, bc = bc)
            f.change_frame(frame, phi = 0)

            dt = t_f / N_steps

            if frame == 'lab':
                spin_op = 'S'
                Ox = f.Sx
                Oy = f.Sy
                Oz = f.Sz
            elif frame == 'rotated':
                spin_op = 'J'
                Ox = f.Jx
                Oy = f.Jy
                Oz = f.Jz

            sx   = Ox
            sy   = Oy
            sz   = Oz
            sx2  = Ox * Ox
            sy2  = Oy * Oy
            sz2  = Oz * Oz
            sxsy = Ox * Oy + Oy * Ox
            szsx = Oz * Ox + Ox * Oz
            sysz = Oy * Oz + Oz * Oy

            si = [eval(val) for val in si_tags]
            parameters = si + ['Fisher', 'squeezing']
            parameters = si + ['squeezing']

            H_1 = 1/norm*(alpha * f.Sz**2 + beta * f.Sx**2) 

            H = [H_1]
            H_labels = ['exact']
            
            wf = f.initial_state_fermi()
            wf = expm_multiply(-1j*Oy*theta, wf)
            wf = expm_multiply(-1j*Oz*psi, wf)

            for k in range(len(H)):
                wf2 = np.copy(wf)

                f.H = H[k]
                
                t, data = f.evolve(wf, dt = dt, t_f = t_f, parameters = parameters, scheme = 'parallel')
                #t *= 1/t_f
                for i in range(len(parameters)):
                    axs[i].plot(t, data[i], ls = lines[k], marker = markers[k], label = H_labels[k])
        else:
            t = np.linspace(0,t_f, N_steps)

        args = (alpha/norm, beta/norm)
        #y0 = np.array(y0, dtype = np.complex128)
        funcs = (ode_asym_TACT_0,)
        #methods = 'RK45', 'RK23', 'DOP853', 'Radau', 'BDF', 'LSODA'
        #methods = 'DOP853', 
        methods = 'RK45', 

        for j, method in enumerate(methods):
            for k, fun in enumerate(funcs):
                exp_values = [None for i in range(len(y0))]
                k = k + j + len(H)
                label = fun.__name__ + ', ' + method + f', N={N}'
                sol = solve_ivp(fun, t_span = (0, t_f), 
                        y0 = y0, 
                        method = method, 
                        t_eval = t, 
                        args = args)
                for i in range(len(y0)):
                    exp_values[i] = sol.y[i]
                    if not only_squeezing:
                        axs[i].plot(sol.t, sol.y[i].real, ls = lines[k], marker = markers[k], label = label)

                y = spin_squeezing(N, exp_values)
                threshold = y > .95
                threshold[0] = False
                for i in range(1,len(y)-1):
                    if all(y[:i] >= .95):
                        threshold[i] = False
                    elif y[i] <= y[i-1]:
                        threshold[i] = False
                    elif y[i] > y[i-1] and y[i] > y[i+1]:
                        threshold[i] = True
                        break

                threshold[-1] = True
                i_max = [i for i, x in enumerate(threshold) if x][0]
                x, y = sol.t[:i_max], y[:i_max]
                x_min, y_min = calc_minima(x,y)
                xi_best.append(y_min)
                t_best.append(x_min)
                axs[-1].plot(x, y, ls = lines[k], marker = markers[k], label = label)
                #axs[-1].plot(x, y, ls = lines[k], marker = markers[k+3], label = label)

    t_bbeta.append(t_best)
    xi_bbeta.append(xi_best)
    for i,ax in enumerate(axs):
        ax.set_xlabel('$t$')
        ax.set_ylabel(ax_labels[i])
        '''
        if isinstance(parameters[i], str) and parameters[i] == 'Fisher':
            for k in range(1,N+1):
                s = N // k
                r = N - k * s
                y = s*k**2 + r**2
                ax.axhline(y, alpha = .33)
                ax.text(t[N_steps//10], y, f'k={k}')
        '''
    #axs[-1].legend()
    #axs[-1].set_ylim((0,1.5))
    #axs[-1].set_yscale('log')
    #axs[-1].set_xscale('log')
    #axs[-1].set_ylim((1e-2,1.))
    axs[-1].set_ylim((0,1.))
    title = fr'$N={N}, M={M}, \alpha = {alpha}, \beta = {beta}$'
    #title = fr'$N={N}, M={M}, \Omega = {Omega}, J= {J:.2f}, U = {U}$'
    
    #plt.close(fig)
    #fig.suptitle(title+fr', $\|\Psi_0\rangle = \|\theta={theta/np.pi:.2f}\pi,\phi={psi/np.pi:.2f}\pi \rangle  $')
    '''
    if save:
        title = title.replace("\\","").replace('$',"")
        main_title = 'Spin Squeezing' if only_squeezing else 'Expectation values' 
        title = (f'output/ODE vs Exact, {main_title}, {title}, '
                 + f'init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')
        plt.savefig(title+'.pdf')
    '''

fig2, ax2 = plt.subplots(2,4, figsize = (20,10), sharex = 'col', constrained_layout = True)
ax2[1,0].set_xlabel('N')
ax2[1,1].set_xlabel('N')
ax2[1,2].set_xlabel('N')
ax2[1,3].set_xlabel(r'$\beta$')
ax2[1,0].set_ylabel(r'$\xi_\mathrm{best}^2$')
ax2[1,1].set_ylabel(r'$\Delta\xi_\mathrm{best}^2$')
ax2[1,2].set_ylabel(r'$\Delta\xi_\mathrm{best}^2 \frac{N}{\log N}$')
ax2[1,3].set_ylabel(r'$\Delta\xi_\mathrm{best}^2$')
ax2[0,0].set_ylabel('$t_\mathrm{best}$')
ax2[0,1].set_ylabel('$\Delta t_\mathrm{best}$')
ax2[0,2].set_ylabel(r'$\Delta t_\mathrm{best} \frac{N}{\log N}$')
ax2[0,3].set_ylabel(r'$\Delta t_\mathrm{best}$')

N_range = np.array(N_range)

for ax in ax2.flatten():
    ax.set_xscale('log')
    ax.set_yscale('log')

for i in (0, 1):
    for j in (3,):
        ax = ax2[i,j]
        ax.set_xscale('linear')
        ax.set_yscale('linear')

t_best = np.array(t_bbeta)
xi_best = np.array(xi_bbeta)
scaling = N_range/np.log(N_range)
for i, beta in enumerate(beta_range):
    ax2[0,0].plot(N_range, t_best[i], label = fr'$\beta = {beta}$')
    ax2[1,0].plot(N_range, xi_best[i], label = fr'$\beta = {beta}$')
    ax2[0,1].plot(N_range, abs(t_best[i]-t_best[-1]), label = fr'$\beta = {beta}$')
    ax2[1,1].plot(N_range, abs(xi_best[i]-xi_best[-1]), label = fr'$\beta = {beta}$')
    ax2[0,2].plot(N_range, scaling*abs(t_best[i]-t_best[-1]), label = fr'$\beta = {beta}$')
    ax2[1,2].plot(N_range, scaling*abs(xi_best[i]-xi_best[-1]), label = fr'$\beta = {beta}$')

#ax2[0, 1].plot(N_range, 1e-5*N/np.log(N_range), label='N/logN')
#ax2[0, 1].plot(N_range, 1/N_range, label='1/N')
ax2[0, 1].plot(N_range, .9*np.log10(N_range)/N_range, ls = 'dashed', label=r'$0.9 \frac{\logN}{N}$')
ax2[0, 1].legend()

ax2[0, 3].plot(beta_range[:-1], abs(t_best[:-1,-1] - t_best[-1,-1]), label = '$N=N_\mathrm{max}$' )
ax2[0, 3].legend()

if save:
    main_title = 'Non-iso-TACT-scaling' 
    title = (f'output/{main_title}')
    plt.savefig(title+'.pdf')
#plt.show()
