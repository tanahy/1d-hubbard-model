import sys

import matplotlib
import matplotlib.pyplot as plt

from hubbard.hubbard import *
from hubbard.utilities import * 


matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})


N = 4
if len(sys.argv) == 2:
    N = int(sys.argv[1])

M = N

U = .05 
U = (U, U, .95*U)
J = 1

psi = np.pi/2
theta = np.pi/2

bc = 'periodic'

C_SS = (U[0]+U[1]-2*U[2])/(2 * M)

figwidth = 3.416*2

fig, ax = plt.subplots(1, constrained_layout=True, figsize=(figwidth, figwidth/2))

lines = iter(['solid','dashed', 'dashdot']+12*['None'])
markers = iter(3*['None']+['+', '^', 's', 'o', '*', 'x','<','>','1','2','3','4'])

for model in ('BHM', 'TMM'):
    for eta in (0, .5, 1):
        gamma = calc_gamma(M, C_SS, eta, bc)
        title = fr'N={N}, M={M}, gamma = {gamma:.4f}, J= {J:.2f}, U = {U}'
        title = (f'output/squeezing for {model}, {title},'
             + f'init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')
        
        filename = title + '.npz'
        label = f'{model}, $\gamma = {gamma:.3f}$ ($\eta = {eta}$)'
        ls = next(lines)
        marker = next(markers)
        
        df = np.load(filename)
        t = df['t']
        data = df['data']
        ax.plot(t, data[-1], ls = ls, marker = marker, label = label)

ax.set_ylabel(r'$\xi^2$')
ax.set_xlabel('$t$')

ax.legend()
ax.set_ylim((0,1.1))
plt.savefig(title+'.pdf')
plt.savefig(title+'.pgf', backend = 'pgf')
plt.show()
