import sys

from matplotlib.gridspec import GridSpec

from hubbard.hubbard import *
from hubbard.utilities import * 

N = 6
M = N

#####
## Define parameters -> beta --> (alpha, xi)
#####
ER = 1# [hbar/Er] = 7e-6
r = .45#1/10
U = ER
J = r*U
Omega = r**3*U #alpha

#phi = 2*np.pi/N
phi = np.pi

J_SE = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*np.cos(phi/2)**2)
J_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*np.sin(phi/2)**2)
E_phi = J_SE*(1-np.cos(phi))

chi = J_SOC**2/(E_phi*(N-1))
if phi != np.pi:
    chi /= 2

#assert J/U < 1
#assert Omega/U < 1

psi = np.pi/2
theta = np.pi/2
#bc = 'open'
bc = 'periodic'
#bc = 'ring'
frame = 'lab'

only_squeezing = True
save = True

print('U:', U)
print('J:', J)
print('Omega:', Omega)
print('chi:', chi)

t_f = 1/abs(chi) 

N_steps = 100
dt = t_f / N_steps

#f = Hubbard(N, M, 'Fermi-single-ocupancy', bc = bc)
f = Hubbard(N, M, 'Fermi', bc = bc)
f.change_frame(frame, phi = phi)

if frame == 'lab':
    spin_op = 'S'
    Ox = f.Sx
    Oy = f.Sy
    Oz = f.Sz
elif frame == 'rotated':
    spin_op = 'J'
    Ox = f.Jx
    Oy = f.Jy
    Oz = f.Jz

sx   = Ox
sy   = Oy
sz   = Oz
sx2  = Ox * Ox
sy2  = Oy * Oy
sz2  = Oz * Oz
sxsy = Ox * Oy + Oy * Ox
szsx = Oz * Ox + Ox * Oz
sysz = Oy * Oz + Oz * Oy

si_tags = ['sx','sy','sz','sx2','sy2','sz2','sxsy','sysz','szsx']
si = [eval(val) for val in si_tags]

ax_labels = [f'${val.replace("s","S_").replace("2","^2")}$' for val in si_tags]
ax_labels.append(r'$\xi^2$')
if only_squeezing:
    ax_labels = ax_labels[-1:]

H_OAT = +chi*f.Sx**2 if phi == np.pi else -chi*f.Sz**2
H_TACT = -chi*(f.Sz**2-f.Sy**2) 

H_eff = f.H_eff(J=J,U=U,Omega=Omega) 
H_FH = f.H_0(J=J) + f.H_int(U=U) + f.H_SOC(Omega = Omega) 

H = [
        H_OAT, 
        #H_TACT, 
        H_eff, 
        H_FH
        ]
H_labels = [
        'OAT', 
        #'TACT',
        'eff', 
        'FH+SOC'
        ]

if only_squeezing:
    fig, ax = plt.subplots(1, constrained_layout=True)
    axs = [ax,]
else:
    fig = plt.figure(constrained_layout=True, figsize=(15,20))
    gs = GridSpec(4,3, figure = fig)
    axs = []
    for i in range(3):
        for j in range(3):
            axs.append(fig.add_subplot(gs[i,j]))
    axs.append(fig.add_subplot(gs[3,:]))

lines = iter(['solid','dashed', 'dashdot']+12*['None'])
markers = iter(3*['None']+['+', '^', 's', 'o', '*', 'x','<','>','1','2','3','4'])

wf = f.initial_state_fermi()
wf = expm(-1j*Oy*theta).dot(wf)
wf = expm(-1j*Oz*psi).dot(wf)
#assert calculate_ortho(wf, expm(-1j*Oy*np.pi).dot(wf))

for k in range(len(H)):
    wf2 = np.copy(wf)

    f.H = H[k]
    label = H_labels[k]
    print(label)
    #print(F.H)
    ls = next(lines)
    marker = next(markers)
    
    U_dt = f.time_evolution(dt)
    t = 0
    t_list = []
    ssp_list = []
    si_list = [[] for i in range(9)]
    for i in range(N_steps):
        for j in range(9):
            s = expectation_value(wf2, si[j])
            si_list[j].append(s)
        ssp = f.spin_squeezing(wf2, operator = spin_op)
        ssp_list.append(ssp)
        t_list.append(t)
        t += dt
        wf2 = U_dt.dot(wf2)
    
    t_list = np.array(t_list)
    axs[-1].plot(t_list, ssp_list, ls = ls, marker = marker, label = label)
    if not only_squeezing:
        si_theo_list = []
        for i in range(9):
            axs[i].plot(t_list, np.real(si_list[i]), ls = ls, marker = marker, label = label)

for i,ax in enumerate(axs):
    ax.legend()
    ax.set_xlabel('t')
    ax.set_ylabel(ax_labels[i])
axs[-1].set_ylim((0,1.5))
title = fr'$N={N}, M={M}, \phi = {phi/np.pi:.2f}\pi, \Omega = {Omega}, J= {J:.2f}, U = {U}$'
#title = fr'$N={N}, M={M}, \Omega = {Omega}, J= {J:.2f}, U = {U}$'

fig.suptitle(title+fr', $\|\Psi_0\rangle = \|\theta={theta/np.pi:.2f}\pi,\phi={psi/np.pi:.2f}\pi \rangle  $')
if save:
    title = title.replace("\\","").replace('$',"")
    main_title = 'Spin Squeezing' if only_squeezing else 'Expectation values' 
    title = (f'output/{main_title}, {title}, {spin_op} op ({frame} {bc}),'
             + f'init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')
    plt.savefig(title+'.pdf')
    results = [t_list]+[si for si in si_list]
    results.append(ssp_list)
    header = ['time']+si_tags+['ssp']
    header = '  '.join(header)
    #np.savetxt(title+'.txt',np.array(results).T.real, header=header)
plt.show()
