import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as spa

from hubbard.hubbard import *

N = 4
M = 6

b = Hubbard(N,M,'Bose','periodic')

basis = list()
for n in range(N+1):
    b.N = n
    b.n_basis_bose()
    print(n, b.A)
    basis += list(b.A)

b.A = list(basis)
b.D = len(b.A)
b.T = {v: tag for tag, v in enumerate(b.A)}

def H_SOC_q(b):
    row, col, data = [], [], []
    for k in range(b.D):
        v = b.A[k]
        i = b.M//2
        q = 2*np.pi/M*i
        for j in (1,-1):
            u = list(v)
            u[i] += j
            H_ij = 1 * np.sqrt(u[i]) if j == 1 else 1 * np.sqrt(v[i])
            if tuple(u) in b.T:
                r = b.T[tuple(u)]
                data.append(H_ij)
                row.append(r)
                col.append(k)

    return sparse.csc_matrix((data, (row, col)), shape=(b.D, b.D), dtype=np.complex128)

def H_q(b):
    row, col, data = [], [], []
    for k in range(b.D):
        v = b.A[k]
        for i in range(b.M):
            q = 2*np.pi/M*i
            if v[i] != 0:
                H_ii = 2*(-1+np.cos(q))*v[i]
                data.append(H_ii)
                row.append(k)
                col.append(k)
            
            '''
            for j in range(b.M):
                for l in range(b.M):
                    for p in range(b.M):
                        q1 = q
                        q2 = 2*np.pi/M*j
                        q3 = 2*np.pi/M*l
                        q4 = 2*np.pi/M*p
                        #if (q1-q3) % b.M == -(q2-q4) % b.M:
                        #if (q1+q2) % b.M == (q3+q4) % b.M:
                        if q1+q2 == q3+q4:
                            u = list(v)
                            sq = 1
                            u[p] -= 1
                            if u[p] < 0:
                                continue
                            sq *= np.sqrt(u[p]+1)
                            u[l] -= 1
                            if u[l] < 0:
                                continue
                            sq *= np.sqrt(u[l]+1)
                            u[j] += 1
                            sq *= np.sqrt(u[j]-1)
                            u[i] += 1
                            sq *= np.sqrt(u[i]-1)
                            H_ij = 2*np.exp(1j*(q2-q4))*sq
                            if tuple(u) in b.T:
                                r = b.T[tuple(u)]
                                data.append(H_ij)
                                row.append(r)
                                col.append(k)
            
            '''
            for j in range(b.M):
                for l in range(b.M):
                    q1 = q
                    q2 = 2*np.pi/M*j
                    q3 = 2*np.pi/M*l
                    u = list(v)
                    sq = 1
                    u[l] -= 1
                    if u[l] < 0:
                        continue
                    sq *= np.sqrt(u[l]+1)
                    u[j] -= 1
                    if u[j] < 0:
                        continue
                    sq *= np.sqrt(u[j]+1)
                    u[l-i] += 1
                    sq *= np.sqrt(u[l-i])
                    u[(j+i) % b.M] += 1
                    sq *= np.sqrt(u[(j+i) % b.M])
                    H_ij = 2*np.exp(-1j*(q1))*sq
                    if tuple(u) in b.T:
                        r = b.T[tuple(u)]
                        data.append(H_ij)
                        row.append(r)
                        col.append(k)

    return sparse.csc_matrix((data, (row, col)), shape=(b.D, b.D), dtype=np.complex128)

def H_q_eff(b):
    pi = b.M // 2
    row, col, data = [], [], []
    for k in range(b.D):
        v = b.A[k]
        H_ii = v[0]**2 + v[pi]*(v[pi]-1) + v[0]*v[pi]
        data.append(H_ii)
        row.append(k)
        col.append(k)
            
        u = list(v)
        c = 1

        if u[0] > 1:
            c *= np.sqrt(u[0])
            u[0] -= 1
            c *= np.sqrt(u[0])
            u[0] -= 1
            
            u[pi] += 1
            c *= np.sqrt(u[pi])
            u[pi] += 1
            c *= np.sqrt(u[pi])
            H_ij = c
            if tuple(u) in b.T:
                r = b.T[tuple(u)]
                data.append(H_ij)
                row.append(r)
                col.append(k)
            
            u = list(v)
            c = 1

        if u[pi] > 1:
            c *= np.sqrt(u[pi])
            u[pi] -= 1
            c *= np.sqrt(u[pi])
            u[pi] -= 1
            
            u[0] += 1
            c *= np.sqrt(u[0])
            u[0] += 1
            c *= np.sqrt(u[0])
            H_ij = c
            if tuple(u) in b.T:
                r = b.T[tuple(u)]
                data.append(H_ij)
                row.append(r)
                col.append(k)

    return sparse.csc_matrix((data, (row, col)), shape=(b.D, b.D), dtype=np.complex128)

basis = b.A
group_label = ['vac','q=0','$q=\pi$','others']
groups = [[] for i in group_label]
print('Basis:')
for i,v in enumerate(basis):
    print (f'|{i}>', v)
    if sum(v) == 0:
        groups[0].append(i)
    elif sum(v) == v[0]:
        groups[1].append(i)
    elif sum(v) == v[M//2]:
        groups[2].append(i)
    else:
        groups[3].append(i)

H_1 = H_q(b).toarray()
H_1 = H_q_eff(b).toarray()
H_2 = H_SOC_q(b).toarray()
H = H_1, H_2

fig, axs = plt.subplots(2,2)

for i in range(2):
    for j in range(2):
        ax = axs[i,j]
        H_ij = H[i].real if j else H[i].imag
        #im = axs[i,j].pcolormesh(H_ij)
        im = axs[i,j].matshow(H_ij)
        for k in groups[2]:
            ax.axhline(k)
            ax.axvline(k)
        fig.colorbar(im,ax=ax)
        ax.set_title(f'$H_{i}, Real$' if j else f'$H_{i}, Imag$')
'''
def recursive_arrow(H, i, j, x, y):
    D = H.shape[0]
    for k range(i, D):
'''
def trans_drawer(H, ax, x, y, order):
    for i in range(H.shape[0]):
        for j, value in enumerate(H[i,:]):
            if value != 0:
                k = order[i]
                l = order[j]
                dx = x[l]-x[k]
                dy = y[l]-y[k]
                ax.arrow(x[k],y[k],dx,dy, linewidth = value/H.max())

def trans_crawler(H, i, ax, x, y, order, unexplored_states):
    D = H.shape[0]
    for j in range(D):
        value = H[i, j]
        if value != 0 and i != j:
            k = order[i]
            l = order[j]
            dx = x[l]-x[k]
            dy = y[l]-y[k]
            ax.arrow(x[k],y[k],dx,dy, linewidth = value/H.max())
            if j in unexplored_states:
                unexplored_states.remove(j)
                trans_crawler(H, j, ax, x, y, order, unexplored_states) 


#fig2= plt.figure()
#ax1 = fig2.add_subplot()
#ax2 = fig2.add_subplot()
#ax3 = fig2.add_subplot(projection="polar", facecolor="lightgoldenrodyellow")

#fig2, (ax1, ax2, ax3) = plt.subplots(3)
from matplotlib.gridspec import GridSpec
fig = plt.figure(constrained_layout=True)

gs = GridSpec(2, 2, figure=fig)
ax1 = fig.add_subplot(gs[0, 0])
ax2 = fig.add_subplot(gs[0, 1])
ax3 = fig.add_subplot(gs[1, 0])
ax4 = fig.add_subplot(gs[1, 1])

ax3.set_aspect('equal','box')
ax4.set_aspect('equal','box')

epsilon = 1e-1
H = H_1 - epsilon * H_2

state_numbers = np.arange(b.D)
state_energies = np.diag(H).real
state_energies, state_labels = zip(*sorted(zip(list(state_energies), list(state_numbers))))
state_energies, state_labels = np.array(state_energies), np.array(state_labels)
#state_labels = [f'{i}' for i in state_numbers]
    
ax1.matshow(abs(H))
#ax2.scatter(state_numbers, state_energies)
#ax2.scatter(state_numbers, state_energies)

theta = 2*np.pi/b.D*np.array(state_numbers)
x = np.cos(theta)
y = np.sin(theta)
#ax3.scatter(x,y)

trans_drawer(abs(H), ax3, x, y, state_numbers)
D = H.shape[0]
unexplored_states = list(range(D))
trans_crawler(abs(H), 0, ax4, x, y, state_numbers, unexplored_states)
#print(iterations)

for i in range(len(state_numbers)):
    x1 = state_numbers[i]
    y1 = state_energies[i]
    x2 = x[i]
    y2 = y[i]
    s = state_labels[i]

    ax2.text(x1,y1,s, va='center', ha='center')
    ax3.text(x2,y2,x1, va='center', ha='center')
    #ax4.text(x2,y2,x1, va='center', ha='center')


for i,group in enumerate(groups[:-1]):
    mask1 = [j in group for j in state_labels]
    mask2 = [j in group for j in state_numbers]
    x1 = state_numbers[mask1]
    y1 = state_energies[mask1]
    x2 = x[mask2]
    y2 = y[mask2]
    ax2.scatter(x1,y1,label=group_label[i])
    ax3.scatter(x2,y2,label=group_label[i])
    ax4.scatter(x2,y2,label=group_label[i])

ax2.set_ylabel('<H>')
ax2.set_xticklabels([])
ax2.legend()

plt.show()

