import sys

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from hubbard.hubbard import *
from hubbard.utilities import * 

import matplotlib

matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})

N = 6
model = 'OAT'
phi_index = int(N//2+1)
if len(sys.argv) == 2:
    N = int(sys.argv[1])
elif len(sys.argv) == 3:
    N = int(sys.argv[1])
    model = sys.argv[2]
elif len(sys.argv) >= 4:
    N = int(sys.argv[1])
    model = sys.argv[2]
    if 'pi' in sys.argv[3]:
        phi = eval(sys.argv[3].replace('pi', '*np.pi'))
        phi_index = None
    else:
        phi_index = int(sys.argv[3])-1

M = N
#phi_available = [2*np.pi/M * (k+1+.5) for k in range(M)]
phi_available = [2*np.pi/M * (k+1) for k in range(M)]
#phi_available[-2] = np.pi
U = .5
#J = 0.02#*U
J = 0.0032
Omega = 0.004 * 0.0032

if phi_index is not None:
    phi = phi_available[phi_index]

#psi = np.pi/2
psi = 0
psi = psi_crit_calc(phi, M)+1*np.pi/2
theta = np.pi/2
#bc = 'anti-periodic'
#bc = 'periodic'
bc = 'open'
#bc = 'ring'
frame = 'lab'

'''
#U = (U, U, .95*U)
#C_SS = (U[0]+U[1]-2*U[2])/(2 * M)
eta = 1
h3 = 0
if eta == 0:
    gamma = 0
else:
    if bc == 'open':
        for i in range(M):
            for j in range(M):
                if j != i:
                    h3 += 1/abs(i-j)**3
        gamma = np.sqrt(2*M**2*C_SS*eta/h3/3)
    elif bc == 'ring':
        for d in range(1, M):
            h3 += abs(np.sin(np.pi/M)/np.sin(np.pi/M*d))**3
        gamma = np.sqrt(4*M**2*C_SS*eta/h3/3)
    else:
        for d in range(1, (M // 2)+1):
            h3 += 1/d**3
        h3 *= .5
        gamma = np.sqrt(M*C_SS*eta/(6*h3))

#gamma = 1
'''

only_squeezing = True
save = False

N_steps = 200

#chi = chi_calc(N, phi, U, J, Omega)
chi = Omega**2/(2*J*(1-np.cos(phi))*(N-1))
#chi *= 10
#if phi == np.pi:
#    chi *= 2
#chi /= 2
#chi /= 10
print('phi:', phi)
print('chi:', chi)

t_f = 1/abs(chi)*np.log(N)/(2*N) #/2

#t_f = 1/(C_SS + gamma**2*h3/(2*M)) if bc == 'ring' else 1/(C_SS)

print('tf:', t_f)

si_tags = ['sx','sy','sz','sx2','sy2','sz2','sxsy','sysz','szsx']

ax_labels = [f'${val.replace("s","S_").replace("2","^2")}$' for val in si_tags]
ax_labels.append(r'$F_Q$')
ax_labels.append(r'$\xi^2$')

title_tail = ""
if model == 'TACT':
    f = FermiHubbard(N, M, bc = bc)
    f.phi = phi
    f.S_calc()
    H_1 = chi*(f.Sz**2-f.Sx**2) 
    H_2 = f.H_eff(J=J/2,U=U/2,Omega=Omega)
    H_3 = f.H_0(J=J) + f.H_int(U=U) + f.H_SOC(Omega = Omega) 
    f.phi = np.pi
    Omega_2 = Omega / np.sqrt(1-np.cos(phi))
    H_2 += f.H_eff(J=J/2,U=U/2,Omega=Omega_2)
    H_3 += f.H_SOC(Omega = Omega_2) 
    f.phi = phi
    H = [H_3, H_2, H_1]
    systems = [f, f, f]
elif model == 'OAT':
    J_SE, J_ud, _ = J_calc(N, U, J, Omega, phi)
    E = J_SE*(1-np.cos(phi))
    print('SW condition:', J_ud/E)
    print('New SW condition:', np.sqrt(N)*J_ud/E)

    f = FermiHubbard(N, M, bc = bc)
    f.phi = phi
    f.S_calc()
    #k0 = 2*U/((N-1)//2*np.ceil((N-1)/2))
    #k1 = 8*U/(N-1)**2
    H_1 = -chi*f.Sx**2 if phi == np.pi else chi*f.Sz**2
    #H_1 += f.V_harmonic_fermi(m=k0, w=1)
    H_2 = f.H_eff(J=J,U=U,Omega=Omega)# + f.V_harmonic_fermi(m=k0, w=1)
    #H_31 = f.H_0(J=J) + f.H_int(U=U) + f.H_SOC(Omega = Omega) + f.V_harmonic(m=k0, w=1) 
    #H_32 = f.H_0(J=J) + f.H_int(U=U) + f.H_SOC(Omega = Omega) + f.V_harmonic(m=2*k0, w=1) 
    #H_33 = f.H_0(J=J) + f.H_int(U=U) + f.H_SOC(Omega = Omega) + f.V_harmonic(m=3*k0, w=1) 
    #H_4 = f.H_0(J=J) + f.H_int(U=U) + f.H_SOC(Omega = Omega) 
    H_3 = f.H_0(J=J) + f.H_int(U=U) + f.H_L(Omega = Omega) 
    #H = [H_4, H_31, H_32, H_33, H_2, H_1]
    #H = [H_4, H_31, H_2, H_1]
    #H = [H_3, H_2, H_1]
    H = [H_2, H_1]
    #H_labels = ['FH', r'FH+$V_\mathrm{ext}(k=k_0)$', r'FH+$V_\mathrm{ext}(k=2 k_0)$', r'FH+$V_\mathrm{ext}(k=3 k_0)$','Spin', 'OAT']
    #H_labels = ['FH', r'FH+$V_\mathrm{ext}(k=k_0)$','Spin', 'OAT']
    #H_labels = ['FH', 'Spin', 'OAT']
    H_labels = ['Spin', 'OAT']
    systems = [f for _ in range(len(H))]
elif model == 'eff':
    f = FermiHubbard(N, M, bc = bc)
    f.phi = phi
    f.S_calc()
    H = []
    H_labels = [f'k={i+1}' for i in range(N-1)]
    for phi in phi_available[:-1]:
        f.phi = phi
        H_i = f.H_eff(J=J,U=U,Omega=Omega) 
        H.append(H_i)
    systems = [f for i in range(N-1)]
elif model == 'TMM':
    f = FermiHubbard(N, M, bc = bc)
    f.phi = phi
    m = Bose2cHubbard(N, 1, bc = bc)
    H1 = f.H_0_bose_spin(J=J) + f.H_int_bose_spin(U=U) + f.H_dip_bose_spin(gamma=gamma)
    #f.bc = 'ring'
    H2 = f.H_TMM(J,U=U,gamma=gamma)
    H3 = m.H_TMM(J,U=U,gamma=gamma)
    systems = [f, f, m]
    H = [H1, H2, H3]
    H_labels = ['BHM', 'TMM (coord.)','TMM (mom.)']
elif model == 'OAT_open':
    #f = FermiHubbard(N, M, bc = bc)
    #f.phi = phi
    #f.S_calc()
    spin = SpinModel(N, M, bc = bc)
    spin.phi = phi
    spin.S_calc()
    #H0 = f.H_spin(J, Omega)
    #H0 = f.H_spin_v2(J, Omega)
    #comm = f.Sm*H0 - H0*f.Sm
    #print('comm', comm)
    #H1 = f.H_spin_1(J, Omega)
    #E = J*(np.cos(phi)-1)
    #chi = Omega**2/(2*(N-1)*E)
    #H1 = chi*f.Sx**2
    H2 = spin.H_eff_open(J, Omega)
    #systems = [f, spin]
    #H = [H0, H2]
    #H_labels = ['spin', 'eff']
    systems = [spin]
    H = [H2]
    H_labels = ['eff']
elif model == 'OAT_open_v2':
    f = FermiHubbard(N, M, bc = bc)
    f.phi = phi
    f.S_calc()
    spin = SpinModel(N, M, bc = bc)
    spin.phi = phi
    spin.S_calc()
    H0 = f.H_spin_v2(J, Omega)
    E = J*(np.cos(phi)-1)
    H1 = spin.H_eff_open_v2(J, Omega)
    systems = [f, spin]
    H = [H0, H1]
    H_labels = ['spin', 'eff']
elif model == 'OAT_mag':
    f = FermiHubbard(N, M, bc = bc)
    f.phi = phi
    f.S_calc()
    spin = SpinModel(N, M, bc = bc)
    spin.phi = phi
    spin.S_calc()
    H0 = f.H_SE(J)
    f.S_j_calc()
    j = np.arange(M)+1
    q = N-1
    beta = Omega * np.cos(np.pi / M * q * (j-.5))
    #beta = Omega * np.cos(phi * j)
    mag_random = np.random.randn(M)
    #mag_random[-1] = -mag_random[:-1].sum()
    beta = Omega * mag_random
    H0 += sum(beta*np.array(f.Sz_j))# + 10 * Omega * sum(f.Sz_j)
    c_q = np.empty(M-1)
    E_q = np.empty(M-1)
    p_l = lambda q_tilde: np.cos(np.pi / M * q_tilde * (j-.5))
    for i in range(M-1):
        q_tilde = i+1
        c_q[i] = np.sqrt(2) / M * sum(p_l(q_tilde) * beta)
        E_q[i] = J*(np.cos(np.pi / M * q_tilde) - 1)
    chi = 1/(N-1) * sum(abs(c_q)**2/E_q)
    H1 = chi * (-spin.Sz**2)
    t_f = abs(1 / chi * .5)
    systems = [f, spin]
    H = [H0, H1]
    #H_labels = [fr'SE+Random Magnetic $(\sum \beta_j = {beta.sum():.2e})$', 'OAT']
    #H_labels = [fr'SE+Magnetic $(\beta_j = \Omega \cos(\pi/M(j-1/2)q))$', 'OAT']
    H_labels = [r'SE+Magnetic', 'OAT']
    title_tail = f', rand field, beta_sum = {beta.sum():.2e}'
    #title_tail = ', mag cos field'

else:
    raise ValueError('Unrecognized model.')

figwidth = 3.416*2#*.45

if only_squeezing:
    #fig, ax = plt.subplots(1, constrained_layout=True, figsize=(figwidth, figwidth*2/3))
    fig, ax = plt.subplots(1, constrained_layout=True, figsize=(figwidth, figwidth*2/3))
    axs = [ax,]
    '''
    fig, ax = plt.subplots(2, 1, constrained_layout=True, figsize=(figwidth, figwidth))
    axs = [ax[0],]
    ax[1].bar(j, beta)
    ax[1].set_ylabel(r'$\beta_j$')
    ax[1].set_xlabel('$j$')
    ax[1].tick_params(left = False, bottom = False, labelleft = False, labelbottom = False)
    '''
else:
    fig = plt.figure(constrained_layout=True, figsize=(figwidth, figwidth))
    cols = 3
    rows = len(si_tags)//cols + 1
    gs = GridSpec(rows,cols, figure = fig)
    axs = []
    for i in range(rows-1):
        for j in range(cols):
            axs.append(fig.add_subplot(gs[i,j]))
    axs.append(fig.add_subplot(gs[rows-1,:1]))
    axs.append(fig.add_subplot(gs[rows-1,1:]))

lines = iter(['solid', 'dashed', 'dashdot', 'dotted']+12*['None'])
markers = iter(4*['None']+['+', 'o', 's', '^', '*', 'x','<','>','1','2','3','4'])
psi_crit = psi_crit_calc(phi, M)
#psi_list = [psi] + [psi_crit + i*np.pi/2 for i in range(4)]
psi_list = [psi_crit + i*np.pi/2 for i in range(4)]
#psi_list = [psi_crit]
psi_list = [psi]
for k in range(len(H)):
    s = systems[k]
    #s.change_frame(frame, phi = None)

    if frame == 'lab':
        spin_op = 'S'
        Ox = s.Sx
        Oy = s.Sy
        Oz = s.Sz
    elif frame == 'rotated':
        spin_op = 'J'
        Ox = s.Jx
        Oy = s.Jy
        Oz = s.Jz

    sx   = Ox
    sy   = Oy
    sz   = Oz
    sx2  = Ox * Ox
    sy2  = Oy * Oy
    sz2  = Oz * Oz
    sxsy = Ox * Oy + Oy * Ox
    szsx = Oz * Ox + Ox * Oz
    sysz = Oy * Oz + Oz * Oy

    si = [eval(val) for val in si_tags]
    parameters = si + ['Fisher', 'squeezing']
    if only_squeezing:
        ax_labels = ax_labels[-1:]
        parameters = parameters[-1:]

    s.H = H[k]
    for psi in psi_list:
        wf = s.initial_state(J=J, U=U)
      
        wf = expm_multiply(-1j*Oy*theta, wf)
        wf = expm_multiply(-1j*Oz*psi, wf)
        
        label = H_labels[k] #+ f'$ (\psi = {psi/np.pi}\pi)$'
        print(label)
        ls = next(lines)
        marker = next(markers)
        
        t, data = s.evolve(wf, t_f = t_f, num = N_steps, parameters = parameters)
        #t *= chi
        for i in range(len(parameters)):
            axs[i].plot(t, data[i], ls = ls, marker = marker, label = label)

t_best, xi_best = calc_minima(t, data[-1])
mask = t == t_best

if not only_squeezing:
    for i in range(2):
        i = -(i+1)
        axs[i].axvline(t[mask], color = 'tab:grey', alpha = .5)
        axs[i].scatter(t[mask], data[i][mask])
        axs[i].text(t[mask], data[i][mask], f'{ax_labels[i]}={data[i][mask][0]}')

for i,ax in enumerate(axs):
    #ax.set_xlabel('$\chi t$')
    ax.set_xlabel('$t$')
    ax.set_ylabel(ax_labels[i])
    if isinstance(parameters[i], str) and parameters[i] == 'Fisher':
        for k in range(1,N+1):
            s = N // k
            r = N - k * s
            y = s*k**2 + r**2
            ax.axhline(y, alpha = .33)
            ax.text(t[N_steps//10], y, f'k={k}')

axs[-1].axhline(1/N, c='grey', ls='dashed', label = 'HL')
axs[-1].legend()
axs[-1].set_ylim((0,1.5))
axs[-1].ticklabel_format(axis = 'x', scilimits = (-5, 4)) 
title = fr'$N={N}, M={M}, \phi = {phi/np.pi:.3f}\pi, \Omega = {Omega}, J = {J}$'#, U = {U}$'
#title = fr'$N={N}, M={M}, \gamma = {gamma:.4f}, J= {J:.2f}, U = {U}$'
#title = fr'$N={N}, M={M}, \Omega = {Omega}, J= {J:.2f}, U = {U}$'

#fig.suptitle(title+fr', $\|\Psi_0\rangle = \|\theta={theta/np.pi:.2f}\pi,\phi={psi/np.pi:.2f}\pi \rangle  $')
if save:
    title = title.replace("\\","").replace('$',"")
    main_title = 'Spin Squeezing' if only_squeezing else 'Expectation values' 
    title = (f'output/{main_title}, {title}, {model},'# {spin_op} op ({frame} {bc}),'
             + f'init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')
    title += title_tail
    plt.savefig(title+'.pdf')
    #plt.savefig(title+'.pgf', backend = 'pgf')
plt.show()
