import sys

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from hubbard.hubbard import *
from hubbard.utilities import * 

import matplotlib

matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})

N = 21
model = 'SOC_open'
phi_index = int(N//2+1)
if len(sys.argv) == 2:
    N = int(sys.argv[1])
elif len(sys.argv) == 3:
    N = int(sys.argv[1])
    model = sys.argv[2]
elif len(sys.argv) >= 4:
    N = int(sys.argv[1])
    model = sys.argv[2]
    phi_model = sys.argv[3]

M = N
if phi_model == 'comm':
    phi_list = [2*np.pi/M * (k+1) for k in range(M//2)]
elif phi_model == 'inc':
    phi_list = [np.pi/M * (k+1) for k in range(M)]
elif 'samples=' == phi_model[:8]:
    #phi_list = np.linspace(np.pi-4*np.pi/M, np.pi-2*np.pi/M, int(phi_model[8:]))
    phi_list = np.linspace(2*np.pi*(M//2-2)/M, 2*np.pi*(M//2-1)/M, int(phi_model[8:]))
else:
    phi_model = 'comm'
    phi_list = [2*np.pi/M * (k+1) for k in range(M//2)]
U = .5
#J = U*0.04
J = 0.0032
Omega = .004*0.0032

#psi = np.pi/2
psi = 0#np.pi/2
theta = np.pi/2
#bc = 'anti-periodic'
#bc = 'periodic'
bc = 'open'
#bc = 'ring'
frame = 'lab'

save = True

N_steps = 500
spin = SpinModel(N, M, bc = bc)

spin.S_calc()
    
figwidth = 3.416*2#*.45

fig, (ax1, ax2) = plt.subplots(2, 1, 
                               sharex = 'col',
                               constrained_layout=True, 
                               figsize=(figwidth, figwidth*2/3)
                               )

t_best = []
xi_best = []
for phi in phi_list:
    spin.phi = phi
    spin.H = spin.H_eff_open(J, Omega)

    chi = Omega**2/(2*J*(1-np.cos(phi))*(N-1))
    chi *= 20
    #if phi == np.pi:
    #    chi *= 2
        #N_steps *= 2

    t_f = 1/abs(chi)#/2
    dt = t_f / N_steps

    if phi_model == 'comm':
        psi_crit = psi_crit_calc(J, phi, M)
        psi_list = [psi_crit + i*np.pi/2 for i in range(4)]
        theta_list = [np.pi/2 for _ in range(len(psi_list))]
        #psi_list += [0]
        #theta_list += [0]
        psi_list = [0]
        theta_list = [0]
    else:
        args = dot_theta_coeff(phi, N, M, J, Omega)
        psi_list = find_roots(dot_theta, args = args, method = 'secant', steps = 100)
        #print([psi/np.pi for psi in psi_list])
        for psi_1 in range(4):
            psi_1 *= np.pi/2
            keep = True
            for psi_2 in psi_list:
                if abs(psi_2 - psi_1) < np.pi/4:
                    if keep:
                        keep = False
                    else:
                        psi_list.remove(psi_2)
        theta_list = [np.pi/2 for _ in range(len(psi_list))]
        #psi_list += [np.pi/2]
        #theta_list += [0]
        psi_list += [0]#, 0.005*np.pi, np.pi/2]
        theta_list += [0]#, np.pi/2, np.pi*.78]
        psi_list = [0]
        theta_list = [0]
        #psi_list += [0.54761905*np.pi, 0.54761905*np.pi, 0.04761905*np.pi, 0.04761905*np.pi]
        #theta_list += [np.pi, 0, np.pi/2, np.pi*3/2]
        #[1.         0.54761905]
        #[3.38031473e-24 5.47619048e-01]
        #[4.5        0.04761905]
        #[1.5        0.04761905]
        #theta_list = [0 for _ in range(len(psi_list))]
    t_best_phi = None
    xi_best_phi = 1
    for theta, psi in zip(theta_list, psi_list):
        wf = spin.initial_state()
        wf = expm_multiply(-1j*spin.Sy*theta, wf)
        wf = expm_multiply(-1j*spin.Sz*psi, wf)
        
        t, data = spin.evolve(wf, dt = dt, t_f = t_f, parameters = ['squeezing'],
                scheme = 'parallel')
        #t *= chi
        #t_best, xi_best = calc_minima(t, data[-1])
        #mask = t == t_best
        if t_best_phi is None or data.min() < xi_best_phi:
            t_best_phi = t[np.argmin(data)]
            xi_best_phi = data.min()

    t_best.append(t_best_phi)
    xi_best.append(xi_best_phi)

ax1.set_ylabel(r'$\xi^2_\mathrm{best}$')
ax2.set_ylabel('$t_\mathrm{best}$')

ax2.set_xlabel('$\phi$')

phi_list = np.array(phi_list) / np.pi
ax1.plot(phi_list, xi_best, '-o')
ax2.plot(phi_list, t_best, '-^')     

title = fr'$N={N}, M={M}, \Omega = {Omega}, J = {J}$'#, U = {U}$'

#fig.suptitle(title+fr', $\|\Psi_0\rangle = \|\theta={theta/np.pi:.2f}\pi,\phi={psi/np.pi:.2f}\pi \rangle  $')
if save:
    title = title.replace("\\","").replace('$',"")
    main_title = 'Optimal Spin Squeezing'
    title = (f'output/{main_title}, {title}, {model}, phi_sampling: {phi_model}'# {spin_op} op ({frame} {bc}),'
             #+ f' init_state = ({theta/np.pi:.2f}pi, {psi/np.pi:.2f}pi)'
             )
    #title += title_tail
    plt.savefig(title+'.pdf')
    #plt.savefig(title+'.pgf', backend = 'pgf')
plt.show()
