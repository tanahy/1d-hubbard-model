import sys

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from hubbard.hubbard import *
from hubbard.utilities import * 

import matplotlib

matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})


N = 6
phi_index = int(N//2)
if len(sys.argv) == 2:
    N = int(sys.argv[1])
elif len(sys.argv) >= 3:
    N = int(sys.argv[1])
    if 'pi' in sys.argv[2]:
        phi = eval(sys.argv[2].replace('pi', '*np.pi'))
        phi_index = None
    else:
        phi_index = int(sys.argv[2])-1

M = N
phi_available = [2*np.pi/M * (k+1) for k in range(M)]
J = 0.0032
Omega = 0.004*0.0032

if phi_index is not None:
    phi = phi_available[phi_index]

def psi_crit_calc(phi):
    al = np.array([np.exp(1j*phi*j) for j in range(1, M+1)])
    fp = []
    Ep = []
    for q in range(1, M):
        p_lq = np.array([np.cos(np.pi/M*(j-.5)*q) for j in range(1, M+1)])
        fp_q = np.sqrt(2)/M*sum(p_lq*al)
        fp.append(fp_q)
        Ep.append(J*(np.cos(np.pi/M*q)-1))
    fp = np.array(fp)
    Ep = np.array(Ep)
    F1 = sum(np.abs(fp)**2/Ep)
    F2 = sum(fp*fp/Ep)

    psi_crit = .5*np.arctan(-np.imag(F2)/np.real(F2))
    return psi_crit

psi_crit_list = []
for phi_i in phi_available:
    psi_crit_list.append(psi_crit_calc(phi_i))
pcl = np.array(psi_crit_list)
psi_crit = psi_crit_calc(phi)
psi_list = [psi_crit + i*2*np.pi/4 for i in range(4)]
'''
for i in range(4):
    plt.plot(np.array(phi_available)/np.pi, pcl+i*2*np.pi/4)
plt.xlabel('$\phi [\pi]$')
plt.ylabel('$\psi$')
plt.scatter([phi/np.pi]*4,psi_list)
'''

psi = np.pi/2
theta = np.pi/2
bc = 'open'
frame = 'lab'

only_squeezing = True
save = True

N_steps = 100
s = Hubbard(N, M, 'total_spin', bc = bc)
s.phi = phi

#chi = chi_calc(N, phi, U, J, Omega)
chi = Omega**2/(2*J*(1-np.cos(phi))*(N-1))
if phi == np.pi:
    chi *= 2
chi *= 10
#chi /= 10
print('phi:', phi)
print('chi:', chi)

t_f = 1/abs(chi)#/2

print('tf:', t_f)
dt = t_f / N_steps

si_tags = ['sx','sy','sz','sx2','sy2','sz2','sxsy','sysz','szsx']

ax_labels = [f'${val.replace("s","S_").replace("2","^2")}$' for val in si_tags]
ax_labels.append(r'$F_Q$')
ax_labels.append(r'$\xi^2$')

s.S_calc()
H1 = s.H_eff_open(J, Omega)
H = [H1]
H_labels = ['eff']

figwidth = 3.416*2

if only_squeezing:
    fig, ax = plt.subplots(1, constrained_layout=True, figsize=(figwidth, figwidth))
    axs = [ax,]
else:
    fig = plt.figure(constrained_layout=True, figsize=(figwidth, figwidth))
    cols = 3
    rows = len(si_tags)//cols + 1
    gs = GridSpec(rows,cols, figure = fig)
    axs = []
    for i in range(rows-1):
        for j in range(cols):
            axs.append(fig.add_subplot(gs[i,j]))
    axs.append(fig.add_subplot(gs[rows-1,:1]))
    axs.append(fig.add_subplot(gs[rows-1,1:]))

lines = iter(['solid', 'dashed', 'dashdot', 'dotted']+12*['None'])
markers = iter(4*['None']+['+', 'o', 's', '^', '*', 'x','<','>','1','2','3','4'])

for k in range(len(H)):
    if frame == 'lab':
        spin_op = 'S'
        Ox = s.Sx
        Oy = s.Sy
        Oz = s.Sz
    elif frame == 'rotated':
        spin_op = 'J'
        Ox = s.Jx
        Oy = s.Jy
        Oz = s.Jz

    sx   = Ox
    sy   = Oy
    sz   = Oz
    sx2  = Ox * Ox
    sy2  = Oy * Oy
    sz2  = Oz * Oz
    sxsy = Ox * Oy + Oy * Ox
    szsx = Oz * Ox + Ox * Oz
    sysz = Oy * Oz + Oz * Oy

    si = [eval(val) for val in si_tags]
    parameters = si + ['Fisher', 'squeezing']
    if only_squeezing:
        ax_labels = ax_labels[-1:]
        parameters = parameters[-1:]
    
    s.H = H[k]
    label = H_labels[k]
    print(label)

    psi_list = [psi]+psi_list
    #for psi in np.linspace(psi, psi_crit, 10):
    for psi in psi_list:
        wf = s.initial_state()
        wf = expm_multiply(-1j*Oy*theta, wf)
        wf = expm_multiply(-1j*Oz*psi, wf)
        label = fr'$\theta={theta/np.pi:.3f}\pi, \psi={psi/np.pi:.3f}\pi$'
        ls = next(lines)
        marker = next(markers)
    
        t, data = s.evolve(wf, dt = dt, t_f = t_f, parameters = parameters,
            scheme = 'parallel')
            #scheme = 'default')
        for i in range(len(parameters)):
            axs[i].plot(t, data[i], ls = ls, marker = marker, label = label)

t_best, xi_best = calc_minima(t, data[-1])
mask = t == t_best

if not only_squeezing:
    for i in range(2):
        i = -(i+1)
        axs[i].axvline(t[mask], color = 'tab:grey', alpha = .5)
        axs[i].scatter(t[mask], data[i][mask])
        axs[i].text(t[mask], data[i][mask], f'{ax_labels[i]}={data[i][mask][0]}')

for i,ax in enumerate(axs):
    #ax.set_xlabel('$\chi t$')
    ax.set_xlabel('$t$')
    ax.set_ylabel(ax_labels[i])
    if isinstance(parameters[i], str) and parameters[i] == 'Fisher':
        for k in range(1,N+1):
            s = N // k
            r = N - k * s
            y = s*k**2 + r**2
            ax.axhline(y, alpha = .33)
            ax.text(t[N_steps//10], y, f'k={k}')

axs[-1].legend()
axs[-1].set_ylim((0,1.5))
title = fr'$N={N}, M={M}, \phi = {phi/np.pi:.3f}\pi, \Omega = {Omega}, J= {J}$'
#fig.suptitle(title+fr', $\|\Psi_0\rangle = \|\theta={theta/np.pi:.2f}\pi,\phi={psi/np.pi:.2f}\pi \rangle  $')
if save:
    title = title.replace("\\","").replace('$',"")
    main_title = 'Spin Squeezing' if only_squeezing else 'Expectation values' 
    title = (f'output/{main_title}, {title}, {spin_op} op ({frame} {bc}),'
             + f'init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')
    plt.savefig(title+'.pdf')
    #plt.savefig(title+'.pgf', backend = 'pgf')
plt.show()
