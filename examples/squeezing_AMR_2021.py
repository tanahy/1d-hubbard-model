from matplotlib.gridspec import GridSpec

from hubbard.hubbard import *
from hubbard.utilities import * 

N = 6
M = N

U = 1
Omega = .85
J = .01 * np.sqrt(Omega*U) 

phi = 2*np.pi/M

cols = 3
rows = 2
phi_range = phi*np.logspace(0, -6, cols * rows)
print(phi_range/phi)

print('U:', U)
print('J:', J)
print('Omega:', Omega)

assert J/U < 1
#assert Omega/U < 1

psi = np.pi/2
theta = np.pi/2
#bc = 'open'
#bc = 'periodic'
bc = 'ring'
frame = 'rotated'

only_squeezing = True
save = True

N_steps = 100

f = Hubbard(N, M, 'Fermi', bc = bc)

fig, axs = plt.subplots(rows, cols, figsize = (5*cols, 5*rows), constrained_layout=True)
axs = axs.flatten()

f.change_frame(frame, phi = phi)

wf = f.initial_state_fermi()
wf = expm(-1j*f.Jy*theta).dot(wf)
wf = expm(-1j*f.Jz*psi).dot(wf)

for i, phi in enumerate(phi_range):
    f.phi = phi
    print('phi:', phi)

    # Eq. 105 from Gedeminas notes
    J_para  = J**2/U * (U**2*np.cos(phi) - Omega**2*np.cos(phi/2)**2) / (U**2 - Omega**2)
    J_perp  = J**2/U * (U**2 - Omega**2*np.cos(phi/2)**2) / (U**2 - Omega**2)
    J_DM    = J**2/U * (Omega**2 - 2*U**2) * np.sin(phi) / (2*(U**2 - Omega**2))
    J_Omega = Omega/2 * (1 - 2*J**2*Omega*np.sin(phi/2)**2 / (U**2 - Omega**2))
    print('J_Omega:', J_Omega) 
    print('J_para+J_perp:', J_para+J_perp) 
    chi = 2*(J_para - J_perp) / (M-1)
    print('     chi:', chi)

    t_f = 1/abs(chi) 
    dt = t_f / N_steps

    f.change_frame('lab', phi=phi)
    H_SE = 0

    H_XXZ = 0
    f.S_fermi_calc_j()
    Sx, Sy, Sz = f.Sx_j, f.Sy_j, f.Sz_j
    for j in range(N):

        H_SE += 4*J_para*Sx[j]*Sx[(j+1)%M]
        H_SE += 4*J_para*Sy[j]*Sy[(j+1)%M]
        H_SE += 4*J_perp*Sz[j]*Sz[(j+1)%M]
        H_SE += 4*J_DM*(Sx[j]*Sy[(j+1)%M] - Sy[j]*Sx[(j+1)%M])
        H_SE += 2*J_Omega*Sx[j]
        
        H_XXZ += 4*J_para*Sx[j]*Sx[(j+1)%M] 
        H_XXZ += 2*(J_perp + J_para)*(Sy[j]*Sy[(j+1)%M] + Sz[j]*Sz[(j+1)%M]) 
        H_XXZ += 2*J_Omega * Sx[j]
    f.change_frame('rotated', phi=phi)

    H_pOAT = chi * f.Jx**2 + 2*J_Omega*f.Jx
    H_OAT = chi * f.Jx**2
    H_linear = 2*J_Omega*f.Jx

    H_FH = f.H_0(J=J) + f.H_int(U=U) + f.H_L(Omega = Omega) 

    H = [
            H_FH,
            H_SE,
            H_XXZ, 
            H_pOAT, 
            H_OAT, 
            H_linear, 
            ]
    H_labels = [
            '$H_{FH+SOC}$ (AMR (A2))',
            '$H_{SE}$ (AMR (4))', 
            '$H_{XXZ}$ (AMR (6))', 
            '$H_{pOAT}$ (AMR (9))', 
            '$H_{OAT}$',
            '$2 J_\Omega J^x$',
            ]

    lines = iter(['solid','dashed', 'dashdot']+12*['None'])
    markers = iter(3*['None']+['+', '^', 's', 'o', '*', 'x','<','>','1','2','3','4'])

    for k in range(len(H)):
        f.H = H[k]
        label = H_labels[k]
        print('     '+label)
        #print(F.H)
        ls = next(lines)
        marker = next(markers)
        
        t, data = f.evolve(wf, dt, t_f, parameter=['squeezing'], operator = 'J')
        data = data.flatten()
        
        axs[i].plot(t, data, ls = ls, marker = marker, label = label)

    axs[i].legend()
    axs[i].set_xlabel('t')
    axs[i].set_ylabel(r'$\xi^2$')
    axs[i].set_ylim((0,1.5))
    axs[i].set_title(f'$\phi={phi/np.pi:e}\pi$')

title = fr'AMR 2021 $N={N}, M={M}, \phi = {phi_range[0]/np.pi:.2e}\pi, \Omega = {Omega:.2f}, J= {J:.2e}, U = {U}$'

fig.suptitle(title+fr', $\|\Psi_0\rangle = \|\theta={theta/np.pi:.2f}\pi,\phi={psi/np.pi:.2f}\pi \rangle  $')
if save:
    title = title.replace("\\","").replace('$',"")
    title = (f'output/{title}, ({frame} {bc}),'
             + f'init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')
    plt.savefig(title+'.pdf')
plt.show()
