import matplotlib.pyplot as plt
from scipy.linalg import eig 

from hubbard.hubbard import *

N = 6
#bc = 'periodic'
bc = 'open'
stat = 'Fermi-single-ocupancy'#'Fermi'

phi = np.pi
J       = .1
U       = 1
J_SE    = 4*J**2/U
Omega   = J_SE/40

F = FermiHubbard(N=N,M=N,bc=bc)
F.phi = phi
F.S_calc()
F.S_j_calc()

if bc == 'periodic':
    j_range = range(N)
else:
    j_range = range(N-1)


def H_XX(alpha):
    H = 0
    for i in range(N):
        for j in range(N):
            if j != i:
                C = -J/abs(i-j)**alpha
                H += C*(F.Sx_j[i]*F.Sx_j[j] + F.Sy_j[i]*F.Sy_j[j])
    return H

H_XX_inf = sum([F.Sx_j[j]*F.Sx_j[(j+1) % N] + F.Sy_j[j]*F.Sy_j[(j+1) % N] for j in j_range])
H_XX_inf = -J*H_XX_inf

H_1 = H_XX(alpha=1)
H_2 = H_XX(alpha=3)
H_3 = H_XX_inf
#H_2 = F.H_0(J) + F.H_int(U) + F.H_L(Omega)
H_eff = -F.H_eff_simple(J_SE,Omega)
H_OAT = -J*F.Sz**2
H_0 = -J_SE*(F.Sx*F.Sx + F.Sy*F.Sy)

E0 = J_SE

H_list = [
        #H_1,
        #H_2,
        #H_3,
        H_eff,
        #H_eff,
        H_0,
        #H_OAT,
        ]

H_labels = [
        #r'$\alpha = 1$',
        #r'$\alpha = 3$',
        #r'$\alpha = \infty$',
        #'H_XX',
        #'H_FH',
        '$-H_{eff}$',
        #'$-H_{eff}$',
        r'$\alpha = 0$',
        #'$H_{OAT}$',
        ]

S2 = F.Sx*F.Sx+F.Sy*F.Sy+F.Sz*F.Sz
#S2 = F.Sx*F.Sx+F.Sy*F.Sy

J_max = N/2

CSS = expm(-1j*np.pi/2*F.Sy).dot(F.initial_state())

fig, axs = plt.subplots(4,len(H_list), figsize=(8*len(H_list), 5*4))
for i,v in enumerate(F.A):
    print(i,v)

for i in range(len(H_list)):
    F.H = H_list[i]
    w, v = eig(F.H.toarray(), check_finite=False)
    #w, v = linalg.eigsh(F.H.toarray(), k=F.D, which='LM')
    #w, v = F.eigenvalues(k=F.D-2)
    Sz_list = []
    Sz2_list = []
    S2_list = []
    fidelity = []
    for j in range(len(w)):
        wf = v[:,j]
        print(w[j]/E0,(wf/max(wf)).real) 
        text = [f'{wf[k]:.2f},{k}' for k in range(len(wf))]# if abs(wf[k]) > 1e-8]
        Sz = expectation_value(wf, F.Sz)
        Sz2 = expectation_value(wf, F.Sz*F.Sz)
        #Sz2 = expectation_value(wf, F.Sz)**2
        #J2 = expectation_value(wf, F.Sx)**2 + expectation_value(wf,  F.Sy)**2 + expectation_value(wf,  F.Sz)**2
        J2 = expectation_value(wf, S2)/(J_max**2+J_max)
        f = abs(np.conj(wf).dot(CSS))
        #print('j', 'E/J', 'Sz', 'S2')
        #print(f'{j}, {w[j]/J:.2f}, {Sz:.3f}, {J2:.3f}', text)
        Sz_list.append(Sz)
        Sz2_list.append(Sz2)
        S2_list.append(J2)
        fidelity.append(f)

    S2_list = np.array(S2_list)
    Sz_list = np.array(Sz_list)
    Sz2_list = np.array(Sz2_list)
    fidelity = np.array(fidelity)

    mask_TOS  = S2_list > .8
    mask_rest = np.invert(mask_TOS)
    #axs[0,i].set_title(H_labels[i]+f' ({mask_TOS.sum()} in ToS)')
    axs[0,i].set_title(H_labels[i])
    
    #if i == 0:
    #    mask_range = (mask_TOS,)
    #    for j in range(len(mask_TOS)):
    #        if mask_TOS[j]:
    #            print(v[:,j].real) 
    #else:
    mask_range = (mask_TOS, mask_rest)
    for mask in mask_range:
        axs[0,i].scatter(Sz2_list[mask], w[mask]/E0)
        axs[1,i].scatter(w[mask]/E0, S2_list[mask])
        axs[2,i].scatter(Sz_list[mask], fidelity[mask]) 
        axs[3,i].scatter(S2_list[mask], fidelity[mask]) 

    E_lims = [w[mask_TOS].min()/E0, w[mask_TOS].max()/E0]
    dE = (E_lims[1]-E_lims[0])/20
    E_lims[0] -= dE
    E_lims[1] += dE

    J_lims = [S2_list[mask_TOS].min(), S2_list[mask_TOS].max()]
    J_lims[0] -= 1e-3
    J_lims[1] += 1e-3
    
    axs[1,i].set_ylim(J_lims)
    
    axs[0,i].set_ylim(E_lims)
    axs[1,i].set_xlim(E_lims)
    axs[0,i].set_xlabel('$J_z^2$')
    axs[1,i].set_xlabel('$E/E_0$')
    axs[2,i].set_xlabel('$J_z$')
    axs[3,i].set_xlabel('$<J^2>/(J_{max}^2+J_{max})$')


    
    axs[0,0].set_ylabel('$E/E_0$')
    axs[1,0].set_ylabel('$J^2/(J_{max}^2+J_{max})$')
    axs[2,0].set_ylabel('$|<n|{CSS}_x>|$')
    axs[3,0].set_ylabel('$|<n|{CSS}_x>|$')

#v = np.array([0,0,1,0,0,0])
#print(expectation_value(v,  F.Sx)**2 + expectation_value(v,  F.Sy)**2 + expectation_value(v,  F.Sz)**2)
fig.savefig(f'output/quantum_scars,{stat},N={N},J={J},Omega={Omega}, {len(H_list)} Hamiltonians.pdf') 
plt.show()
