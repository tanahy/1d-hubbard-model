#!/usr/bin/env python
# coding: utf-8
import sys, os

from hubbard.hubbard import *
from hubbard.utilities import * 

N = M = 16
frac = 10#, 5
filling = .99
label = 0
path = 'output'
J = 1
U = 24.4*J
model = 'B'

if len(sys.argv) > 1:
    N = M   = int(sys.argv[1])
    J       = float(sys.argv[2])
    U       = float(sys.argv[3])
    #frac    = float(sys.argv[4])
    frac    = eval(sys.argv[4])
    path    = sys.argv[5]
    model   = sys.argv[6]

psi = 0
theta = np.pi/2
bc = 'open'

N_steps = 100

scheme = 'parallel'
#scheme = 'multiply'

#t_J = True if 't-J' in sys.argv else False
t_J = 't-J_' if 't-J' in sys.argv else ''
calc_one_case_per_N = True if 'break' in sys.argv else False

model_fn_func = lambda N, M: f'{path}/models_saved/B2c_N={N}_M={M}.npz' 

if model == 'B':
    J_SE = 4*J**2/U
    U = U, U, U
    q0 = M-1
    E_gap = J_SE * (1-np.cos(np.pi/M*q0))
    Omega = E_gap / frac
    
    j = np.arange(M)+1

    q = np.arange(1, M)
    beta = Omega * np.cos(np.pi/M*(j-.5)*q0)

    Eq = J_SE*(np.cos(np.pi*q/M) - 1)
    x, y = np.meshgrid(j,q)
    p = np.cos(np.pi/N*(x-.5)*y)

    cq = np.sqrt(2)/N*np.sum(p*beta, axis=1)
    chi = 1/(N-1)*sum(abs(cq)**2/Eq)

    H_fn_func     = lambda N, M: f'{path}/models_saved/B2c_N={N}_M={M},H_{t_J}inhomo_J={J}_U={U[-1]}_frac={frac}.npz' 

    filename = f'{path}/evs_{t_J}M={M}_J={J}_U={U[-1]}_frac={frac}.pickle'

elif model == 'contact':
    U = U, U, U / frac
    J_SE = 4*J**2/U[-1]
    delta = U[2]/U[0] + U[2]/U[1] - 1
    beta = np.zeros(M)
    chi =  J_SE * (1-delta) / (M-1)

    H_fn_func     = lambda N, M: f'{path}/models_saved/B2c_N={N}_M={M},H_{t_J}inhomo_J={J}_U={U}.npz' 

    filename = f'{path}/evs_{t_J}M={M}_J={J}_U={U}.pickle'

else:
    raise ValueError(f'Unrecognized model: {model}.')

#t_f = np.pi/abs(chi)
t_f = .5/abs(chi)
if J < 1.0:
    t_f = np.pi/abs(chi)
    N_steps = 500


b = Bose2cHubbard(1, 2, bc = bc, basis = 'single')
b.M = M
b.set_scheme(scheme)
b.theta = theta
b.phi = psi

for n in range(1, N+1):
    model_file = model_fn_func(n,M)
    hamiltonian_file = H_fn_func(n,M)
    if not os.path.exists(hamiltonian_file):
        b.N = n
        if os.path.exists(model_file):
            b.load_data(model_file)
        else:
            del b.A, b.T
            b.basis()
            if hasattr(b, 'Sx'):
                del b.Sx, b.Sy, b.Sz
                del b.Sx_j, b.Sy_j, b.Sz_j
            b.S_j_calc()
            b.S_calc()

            b.save_data(model_file)
        if hasattr(b, 'H'):
            del b.H
        b.H = b.H_XXZ_inhomo(J, U, beta)
        if t_J:
            b.H += b.H_0(J)
        sparse.save_npz(hamiltonian_file, b.H)
        
s = b

results = {n: {'realizations': 0, 'data': None} for n in range(M, 0, -1)}
results['t'] = np.linspace(0, t_f, N_steps)

if os.path.exists(filename):
    with open(filename, 'rb') as f:
        results = pickle.load(f)

results['chi'] = chi
with open(filename, 'wb') as f:
    pickle.dump(results, f, protocol=pickle.HIGHEST_PROTOCOL)

states_left = True
while states_left:
    states_left = False
    for n in range(M, 0, -1):
        N_realizations = results[n]['realizations']
        data = results[n]['data']
        
        model_file = model_fn_func(n,M)
        s.load_data(model_file)
        hamiltonian_file = H_fn_func(n,M)
        if hasattr(s, 'H'):
            del s.H
        s.H = sparse.load_npz(hamiltonian_file)
        
        initial_states = [v for v in s.A if sum(v[M:]) == 0]
        print('N:', n, 
              'realizations:', N_realizations, 
              'Initial states:', len(initial_states)
              )

        obs = [s.Sx, s.Sy, s.Sz, 
               s.Sx*s.Sx, 
               s.Sy*s.Sy, 
               s.Sz*s.Sz, 
               s.Sx*s.Sy + s.Sy*s.Sx, 
               s.Sy*s.Sz + s.Sz*s.Sy, 
               s.Sz*s.Sx + s.Sx*s.Sz
              ]
        Sx, Sy, Sz       = [], [], []
        Sx2, Sy2, Sz2    = [], [], []
        SxSy, SySz, SzSx = [], [], []
        for j in range(s.M):
            Sx.append(s.Sx_j[j])
            Sy.append(s.Sy_j[j])
            Sz.append(s.Sz_j[j])
            Sx2.append(s.Sx_j[j] * s.Sx_j[j])
            Sy2.append(s.Sy_j[j] * s.Sy_j[j])
            Sz2.append(s.Sz_j[j] * s.Sz_j[j])
            SxSy.append(s.Sx_j[j] * s.Sy_j[j] + s.Sy_j[j] * s.Sx_j[j])
            SySz.append(s.Sy_j[j] * s.Sz_j[j] + s.Sz_j[j] * s.Sy_j[j])
            SzSx.append(s.Sz_j[j] * s.Sx_j[j] + s.Sx_j[j] * s.Sz_j[j])

        local_spin_obs = Sx + Sy + Sz 
        local_spin_obs += [sum(Sx2), sum(Sy2), sum(Sz2), sum(SxSy), sum(SySz), sum(SzSx)]
        obs += local_spin_obs
        
        corr_all = s.Sy_j[0] + 1j*s.Sz_j[0]
        for i in range(1,s.M):
            Sp = s.Sy_j[i] + 1j*s.Sz_j[i]
            corr_all *= Sp
            
        obs.append(corr_all)

        for i in range(N_realizations, len(initial_states)):
            print('Calculate evolution...', i)
            wf = np.zeros(s.D)
            ind = s.T[initial_states[i]]
            wf[ind] = 1
            wf = s.rotate((s.theta, s.phi), wf)

            _, data_single = s.evolve(wf, t_f, N_steps, obs)
            
            if N_realizations == 0:
                N_realizations = 1
                data = data_single
            else:
                N_realizations += 1
                data = (N_realizations-1)/N_realizations * data
                data = data + 1/N_realizations * data_single
            
            results[n]['realizations'] = N_realizations
            results[n]['data'] = data

            with open(filename, 'wb') as f:
                pickle.dump(results, f, protocol=pickle.HIGHEST_PROTOCOL)

            if calc_one_case_per_N:
                break

        if N_realizations < len(initial_states):
            states_left = True
