import sys
import time

import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.patches import Circle
from hubbard.hubbard import *
from hubbard.utilities import *
from hubbard.plot import *

###############################
N = 10
M = N

psi = 0#np.pi/2
theta = 0#np.pi/2

res = 100
N_steps = 200

#J = 1 if U != 0 else 0
bc = 'periodic'
frame = 'lab'

save_anim = False 
ground_state = False
snapshot = False
quiver_scale = 1
mf_styles = 'quiver', 'stream'
projections = [None, "aitoff", "hammer", "lambert", "mollweide"]
mf_style  = mf_styles[1]
proj = projections[0]
N_quiver = 10

model = 'TACT'
model = 'spin_open'

chi = 1
if model == 'R':
    t_sq = 1/ chi
elif model == 'OAT':
    t_sq = 1/(chi)*N**(-2/3) 
elif model == 'TACT':
    #t_sq =  1 / chi * np.log(N) / N
    #t_sq =  1 / chi * np.log(2*N) / N
    t_sq =  1 / chi * np.log(N) / (2 * N)
elif model == 'spin_open':
    phi = np.pi-2*np.pi/N#-np.pi/N
    U = .5
    J = 0.04*U
    J_SE = 4*J**2/U
    Omega = 0.004*J_SE#(4*J**2/U)
    J_ud = Omega
    #J_SE, J_ud, _ = J_calc(N, U, J, Omega, phi)
    print(J_SE, J_ud)
    chi = chi_calc(N, phi, U, J, Omega)
    t_sq =  1 / chi * np.log(N) / (2 * N)

else:
    t_sq = 1 / chi
t_f = 2 * t_sq
t_snapshot = t_sq

###############################
filename = f'output/movies/Q function {model}, N={N}, M={M}, tf={t_f:.2e}, chi={chi:.2e}, proj={proj}, mf_style={mf_style}'
if ground_state:
    filename += ', Psi(0) = GS'

fig = plt.figure(constrained_layout=True, dpi=300)

gs = GridSpec(2,2, figure=fig)
ax1 = fig.add_subplot(gs[0, :], projection = proj)
ax2 = fig.add_subplot(gs[1,0])
ax3 = fig.add_subplot(gs[1,1])

start = time.time()
s = SpinModel(N,M)
s.S_calc()
if model == 'R':
    H = chi*(s.Sz)
elif model == 'OAT':
    H = chi*(s.Sz**2)
elif model == 'TACT':
    H = chi*(s.Sz**2-s.Sy**2)
elif model == 'spin_open':
    _, v_x, v_y, chi_x, chi_z, chi_xy = dot_theta_coeff(phi, N, M, J_SE, J_ud)
    H = v_x*s.Sx + v_y*s.Sy + 2*chi_x*s.Sx**2 - chi_z*s.Sz**2 - chi_xy*(s.Sx*s.Sy + s.Sy*s.Sx)
s.H = H

if ground_state:
    s.eigenvalues()
    wf0 = s.gs
else:
    wf0 = s.initial_state()
ref_state = wf0

initial_state = s.rotate((theta, psi), wf0)
s.calc_coherent_states(resolution = res)
X,Y = s.Phi, s.Theta

q_array = abs(s.cs_array.dot(initial_state))**2
q_array /= q_array.max()

end = time.time()

print("Initialization:",end-start)

start = time.time()

t, data = s.evolve(initial_state, t_f, N_steps, 
                               parameters = ['squeezing', 'Sx', 'Sy', 'Sz'], 
                               save_wf = True, 
                               )

squeezing = data[0,:]
s_x = data[1,:] / N
s_y = data[2,:] / N 
s_z = data[3,:] / N 

end = time.time()
print("Evolution:",end-start)

#im = ax1.pcolormesh(X,Y, q_array, vmin=0., vmax=1, shading='auto')
im = ax1.contour(X, Y, q_array)
#fig.colorbar(im, ax = ax1)

ax2.plot(t,squeezing)

s_label = ['x','y','z']
s_op = [s_x, s_y, s_z]
for i in range(3):
    ax3.plot(t,s_op[i], label=f'$S_{{{s_label[i]}}}$')
m = np.arange(N/2, -(N/2+1), -1)
#ax3.bar(m, abs(initial_state[:N+1]))

ax2.set_ylim((0.,1.5))
ax2.set_ylabel(r'$\xi^2$')
ax2.set_xlabel('$t$')

ax3.set_ylabel('$<S_i>/N$')
ax3.legend()
ax3.set_xlabel('$t$')


#path = im.collections[0].get_paths()[0]
#print(im.levels)

def find_nearest_points(X, Y, polygon, N_points = 20):
    mask = np.zeros(X.shape, dtype = bool).flatten()
    step = max(1, len(polygon)//N_points)
    for coord in polygon[::step]:
        dis = (X-coord[0])**2 + (Y-coord[1])**2
        index = dis.argmin()
        mask[index] = True

    return mask.reshape(X.shape)

cont_line = im.allsegs[1][0]
mask = find_nearest_points(X, Y, cont_line)

#ax1.collections = []
start = time.time()
E,u,v = s.calc_energy_fluc()
speed = np.sqrt(u**2+v**2)
if mf_style == 'quiver':
    quiver = ax1.quiver(X[mask], Y[mask], u[mask], v[mask])
elif mf_style == 'stream':
    stream = ax1.streamplot(X, Y, u, v, 
                        #density=.3, 
                        color='grey', 
                        #linewidth=3*speed/np.max(speed),
                        #arrowsize = 0,
                        #transform = ax1.transAxes,
                        #transform = ax1.transData,
                        )
    cf = ax1.contourf(X, Y, abs(E), cmap = 'Reds')
end = time.time()
print("Stream calculations:",end-start)

#ax1.set_axis_off()
ax1.axes.xaxis.set_ticklabels([])
#ax1.axes.yaxis.set_ticklabels([])
ax1.grid()
   
vline1 = ax2.axvline(0, color='red', linestyle='dashed')
vline2 = ax3.axvline(0, color='red', linestyle='dashed')

ax1.axhline(np.pi/2)

def update(i):
    global im
    wf = s.wf_t[i]
    q_array = abs(s.cs_array.conj().dot(wf))**2
    q_array /= q_array.max()

    for c in im.collections:
        c.remove()
    im = ax1.contour(X, Y, q_array)
   
    if mf_style == 'quiver':
        global quiver
        quiver.remove()

        if quiver_scale == 1:
            cont_line = im.allsegs[1][0]
            if len(cont_line) > 0:
                mask = find_nearest_points(X, Y, cont_line, N_quiver)
            else:
                mask = np.zeros(X.shape, dtype = bool)
        else:
            level = im.levels[1] * quiver_scale
            cont_aux = ax1.contour(X, Y, q_array, levels = [level])
            quiver_line = cont_aux.allsegs[0][0]
            mask = find_nearest_points(X, Y, quiver_line, N_quiver)
            for col in cont_aux.collections:
                col.remove()
        
        quiver = ax1.quiver(X[mask], Y[mask], u[mask], v[mask], 
                            alpha = .33, 
                            zorder = 10,
                            )
    vline1.set_xdata(s.t[i])
    vline2.set_xdata(s.t[i])
    return *im.collections, vline1, vline2

if snapshot:
    ind_snapshot = int(t_snapshot / t_f * N_steps)
    update(ind_snapshot)
    ax1.set_title("")
    #fig.suptitle(rf'$N={N}, J={J}, U={U}, \Omega={Omega}, \phi={phi/np.pi:.2f}\pi, \psi_0={psi/np.pi:.2f}\pi$')
    psi_label = '0' if psi == 0 else f'{psi/np.pi:.2f}\pi' 
    theta_label = '0' if theta == 0 else f'{theta/np.pi:.2f}\pi' 
    fig.suptitle(fr'{model}, N={N}, M={M}, $\left|\psi(t=0)\right> = \left|\theta = {theta_label}, \phi = {psi_label}\right>$')
    timestap = f' (t={s.t[ind_snapshot]:.2e})'
    #fig.savefig(filename.replace('movies','portraits')+timestap+'.png', dpi = 300)
    fig.savefig(filename.replace('movies','portraits')+timestap+'.pdf')

start = time.time() 
steps = range(N_steps)
ani = animation.FuncAnimation(fig, update, steps, blit=True)
end = time.time()
print("Animation:",end-start)

if save_anim:
    #ani.save(filename+'.mp4',dpi=800)
    ani.save(filename+'.mp4')
    #Writer = animation.writers['ffmpeg']
    #writer = Writer(fps=15, metadata=dict(artist='Tanausu Hdez'), bitrate=1800)
    #ani.save(filename,writer=writer, dpi=800)

plt.show()
