import sys
import argparse

from numpy import pi
from matplotlib import cm
from hubbard.hubbard import *
from hubbard.utilities import *

###################################################
######
###### Optional parameters
######
###################################################
incremental_dt = True

N_range = 2,3,4
t_0 = 10

threshold = .95  
variance = (.2,0.02)
t_max = 1e10#np.inf

Phi     = (7/6*np.pi,1/2*np.pi)#np.linspace(0,2*np.pi,201)
#Omega   = np.logspace(1,3,5)
Omega   = np.logspace(-3,-1,3)
#Omega   = np.logspace(-3,-1,1)
#Omega   = np.logspace(-1,1,15)

U = (.1,1,10)
U = (1,)

J = 1
###################################################

if len(sys.argv) >= 3:
    print('Parameters introduced by command line')
    N_range = eval(sys.argv[1])
    t_0 = float(sys.argv[2])

if incremental_dt:
    if len(sys.argv) >= 5:
        time_scaling = float(sys.argv[3])
        N_steps = int(sys.argv[4])
    else:
        time_scaling = 2 #A**(1./N_frames)
        N_steps = 50
    
    assert time_scaling > 1

    t_i = t_0
    print('N steps:',N_steps)
    print('scaling:',time_scaling)

else:
    t_i = np.inf
    N_steps = 100
    time_scaling = 2

bc = 'open'

print('U =',U)
print('J =',J)
print('Phi =',Phi)
print('Omega =',Omega)

frame = 'lab'
op = 'S'

k=1
for phi in Phi:
    parameter_set = []
    t_m = []
    minima = []
    ssp_t = []
    fig, axs =plt.subplots(len(Omega)+1,len(N_range),figsize=(10,15))#,sharex=True)
    fig.suptitle(f'$\phi={phi/np.pi:.2f}\pi$')
    for j,N in enumerate(N_range):
        M = N
        F = Hubbard(N, M, 'Fermi', bc=bc)
        wf = F.initial_state_fermi()
        F.change_frame(frame, phi = phi)
        for i,omega in enumerate(Omega):
            if len(Omega) > 1 and len(N_range) > 1:
                ax = axs[i,j]
                ax2 = axs[-1,j]
                ax.sharex(axs[i,0])
            elif len(Omega) == 1:
                ax = axs[0,j]
                ax2 = axs[-1,j]
                ax.sharex(axs[0,0])
            elif len(N_range) == 1:
                ax = axs[i]
                ax2 = axs[-1]
                #ax.sharex(axs[0])
            ax.set_title(f'$N={N}, \Omega={omega}$')
            ax.set_ylim((0,1.5))
            for u in U:
                print(f'phi={phi:.4f}, N={N}, Omega={omega}, U={u}')
                print(f'{k}/{len(U)*len(N_range)*len(Omega)*len(Phi)}')
                k += 1
                F.H = F.H_0(J=J) + F.H_int(U=u) + F.H_L(Omega = omega) 
                Oy = eval(f'F.{op}y')
                wf2 = expm(-1j*Oy*np.pi/2).dot(wf.T)
                ssp_list = []
                t = 0
                t_list = []
                if incremental_dt:
                    t_i = t_0
                dt = t_0 / N_steps
                U_dt = F.time_evolution(dt)
                t_f = t_max
                while t < t_f:
                    if t >= t_i:
                        t_i *= time_scaling
                        print(f'    New time scale:{t_i}')
                        dt = t_i/N_steps
                        U_dt = F.time_evolution(dt)
                    t_list.append(t)
                    t += dt
                    #ssp = expectation_value(wf2, eval(f'F.{op}x'))
                    ssp, _ = F.spin_squeezing(wf2, operator = op)
                    ssp_list.append(ssp)
                    wf2 = U_dt.dot(wf2)
                    found, t_min, ssp_min = first_minima(t_list, ssp_list,
                                            var=variance,
                                            threshold=threshold, t_max=t_f)
                    if found and t_f == t_max:
                        if ssp_min is not None:
                            t_f = 2*t 
                            ssp_t.append(ssp_list)
                            parameter_set.append((N,J,omega,u,phi))
                            minima.append(ssp_min)
                            t_m.append(t_min)
                        else:
                            break
                ax.plot(t_list, ssp_list, label=f'$U={u}$')
                ax2.plot(np.array(t_list)/t_list[-1], ssp_list, label=f'U={u}, $\Omega={omega}$')
            ax2.set_title(f'N={N}, $\Omega$=All, U=All')
            ax.legend()
            ax2.legend()
    if len(minima) == 0:
        print("Couldn't find any results.")
        continue
    #minima, t_m, parameter_set, ssp_t = zip(*sorted(zip(minima, t_m, parameter_set, ssp_t)))
    title = f'evolution dependence, phi={phi/np.pi:.2f}pi, N={N_range}, U={U}, Omega={Omega}'
    filename = 'output/'+title

    print('Number of minima found:',len(minima))
    '''
    np.savez(filename, 
            ssp_min = minima,
            t_min = t_m,
            parameters = parameter_set,
            evolution = np.array(ssp_t,dtype=object),
            Omega = Omega,
            phi = Phi)
    parameters = np.array(parameter_set).T
    header = 'J     Omega       U       phi     t_min       ssp_min'
    np.savetxt(filename+'.txt', np.vstack((parameters, t_m, minima)).T, header=header)
    '''
    fig.tight_layout()
    fig.savefig(filename+'.png')
plt.show()
