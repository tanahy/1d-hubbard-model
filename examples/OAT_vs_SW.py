import sys

from hubbard.hubbard import *
from hubbard.utilities import * #calculate_ortho, psi_loop, critical_psi, SOC_Evolution, SOC_Evolution_Marlena

N_range = [2, 4, 6]

U = 1
J = .01
Omega = 0.1
phi = np.pi/10

Xi = J**2/(2*U*(U**2-Omega**2))
Xi2 = J**2*U*(np.cos(phi)-1)/(U**2-Omega**2)
time_scale = abs(np.pi/Xi2)
print(np.pi/Xi)
print(time_scale)
t_f = time_scale#/2**10#8e4

psi = 0
bc = 'periodic'
#bc = 'ring'
scaling_factor = 2, 1.95, 1.5
#bc = 'open'
#scaling_factor = 1, 1, 1
frame = 'lab'
alpha = 1
save = True

N_steps = 200
dt = t_f / N_steps

fig = plt.figure(constrained_layout=True, figsize=(10,7))
ax1 = fig.add_subplot()
filename_oat = '../OAT_Emilia/OATN'


for k,N in enumerate(N_range):
    filename = filename_oat+f'{N}.dat'
    data = np.loadtxt(filename)
    
    ax1.plot(data[:,0], data[:,1], ls='solid', label = f'$H_{{OAT}}, N={N}$')
    #ax1.plot(time_scale*data[:,0], data[:,1], ls='solid', label = f'$H_{{OAT}}, N={N}$')

    M = N
    F = Hubbard_SW(N, M, 'Fermi', bc = bc)
    F.change_frame(frame, phi = phi, alpha = alpha)

    if frame == 'lab':
        spin_op = 'S'
        Ox     = F.Sx
        Oy     = F.Sy
        Oz     = F.Sz
    elif frame == 'rotated':
        spin_op = 'J'
        Ox     = F.Jx
        Oy     = F.Jy
        Oz     = F.Jz

    lines = iter(['dashed','dotted','dashdot'])
    markers = iter(['+','1','x'])

    H_1 = F.H_0_SW_rot_T(J=J) + F.H_int(U=U) + Omega * Oz 
    #H_2 = F.H_eff_rot_T(J=J,U=U,Omega=Omega) 
    H_2 = F.H_eff_rot_T(J=J,U=U,Omega=Omega) 

    H = [
            #H_1, 
            H_2, 
            ]
    H_labels = [
            #'H_{FH, rot}', 
            'H_{eff}',
            ]

    for j in range(len(H)):
        F.H = H[j]
        wf = F.initial_state_fermi()

        print(H_labels[j])
        print(F.H)
        assert calculate_ortho(wf, expm(-1j*Oy*np.pi).dot(wf))
        
        dt = t_f / N_steps / scaling_factor[k] #((N-1)/2)
        wf2 = expm(-1j*Oy*np.pi/2).dot(wf)
        #wf2 = expm(-1j*Oz*psi).dot(wf2)
        U_dt = F.time_evolution(dt)
        t = 0
        t_list = []
        ssp_list = []
        for i in range(N_steps):
            ssp, _ = F.spin_squeezing(wf2, operator = spin_op)
            ssp_list.append(ssp)
            t_list.append(t)
            t += dt
            wf2 = U_dt.dot(wf2)
        
        ls = next(lines)
        t_list = np.array(t_list)
        t_list /= time_scale
        t_list *= scaling_factor[k]
        #ax1.plot(t_list, ssp_list, ls=ls, label = f'${H_labels[j]}, N={N}$')
        ax1.scatter(t_list, ssp_list, marker=next(markers), label = f'${H_labels[j]}, N={N}$')
ax1.set_title(fr'$U = {U},\Omega = {Omega}, J = {J:.2f}, \phi = {phi/np.pi:.2f}\pi$')
ax1.set_ylim((0,1.5))
ax1.legend()
ax1.set_ylabel(rf'$\xi^2$')
ax1.set_xlabel(r'$t \tilde{\chi}/\pi$')

if save:
    title = f'output/SW vs OAT N={N_range}, phi={phi/np.pi:.2f}pi, J={J:.2f}, U={U}, Omega={Omega}, ({bc})'
    if psi != 0:
        title += f'psi={psi/np.pi:.2f}pi'
    plt.savefig(title+'.pdf')
    #np.savetxt(title+'.txt',data.T, header='t   H_FH    t   H_eff')
plt.show()
