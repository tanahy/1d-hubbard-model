import os, fnmatch
import string
import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.patches as mpatches
from matplotlib.gridspec import GridSpec

from hubbard.bloch import *
from hubbard.utilities import chi_calc, find, calc_minima
###############################

time_scale = 1 #7e-6 #[hbar/ER] = 7 micro seconds
ER = 1
N_example = 10
phi_examples = [2*np.pi/N_example*k for k in range(1,N_example//2)]
phi_examples = [2*np.pi/N_example*(N_example//2+1)]
#phi_examples = [np.pi*(1-2/N_example), np.pi*(1+2/N_example)]
U = .5/ER
J = 0.02
Omega = 0.00012800000000000002
initial_state = 'y'

bc_example = 'ring', 'ring'

fig_scale = 2
plt.rcParams["text.usetex"] = True
plt.rcParams["font.family"] = 'serif'
plt.rcParams["font.serif"] = 'Computer Modern Roman'
plt.rcParams.update({'font.size': fig_scale*10})
width   = fig_scale*3.416
#width   = 15

figsize = width, .65*width

show_Marcin_evo = False
show_Tana_results = True
show_H = True, False, True, True

len_props = len(show_H)

ls = ['solid', 'None', 'None', 'dotted'], ['solid', 'None', 'None', 'dashed']
ls_2 = 'solid', 'dashed', 'dotted'
fill = ['full']*len_props, ['none']*len_props
markers = [',', 'x', 'o', ','], [',', '+', 's', ',']
lw = [[fig_scale]*len_props]*2

N_ticks_x = 8 
N_ticks_y = 4 

ratios = (2.5, 1)
style = 'cols'
#style = 'rows'
include_Bloch = False
###############################

if initial_state == 'y':
    theta = np.pi/2
    psi = np.pi/2

for model in ('OAT', 'TACT'):
    H_labels = [
            #f'$\hat{{H}}_\mathrm{{ {model} }}$',
            '$\hat{H}_\mathrm{eff}$',
            '$\hat{H}_\mathrm{spin}$',
            '$\hat{H}_\mathrm{spin}$',
            #'$\hat{H}_\mathrm{spin,\ simple}$',
            '$\hat{H}_\mathrm{FHM}$',
            ]
    #H_labels = model, 'spin', 'spin, simple', 'FH+L'
    #bc = bc_example[0] if model == 'OAT' else bc_example[1]
    #filename = f'output/portraits/plot scaling {model}, N={N_example}, bc={bc}, style={style}'
    filename = f'output/portraits/plot scaling {model}, N={N_example}, style={style}'

    fig = plt.figure(figsize = figsize,
            constrained_layout = True)

    #if model == 'TACT':
    #    phi_examples = phi_examples[:-1]
    tags = list(string.ascii_letters)
    if style == 'cols':
        gs = GridSpec(2, 2, width_ratios = ratios, figure = fig)
        subgridspec_args = {'hspace': 0}
        if include_Bloch:
            gs1 = gs[:,0].subgridspec(1+len(phi_examples), 1, **subgridspec_args)
            axs_evo = [fig.add_subplot(gs1[i+1,0]) for i in range(len(phi_examples))]
            ax0 = fig.add_subplot(gs1[-1,0], projection='3d', facecolor='#FFAAAA')

            ax0.view_init(90-theta*180/np.pi, psi*180/np.pi)
            ax0.patch.set_alpha(0)
            axes_style = dict(span = .25,#0,# 
                        margin = 0.1,#.25,#
                        fontsize = fig_scale * 6,
                        color = 'red')
            bs = Bloch_Sphere(alpha = .1, ax = ax0, 
                        #cmap = cm.inferno, 
                        cmap = cm.jet, 
                        #cmap = cm.hot, 
                        axes_style = axes_style
                        )  
            #ax0.set_axis_on()
            lims = np.array([-1,1])*.9
            lims = np.array([-.6,.95])
            ax0.set_xlim(lims)
            ax0.set_ylim(lims)
            ax0.set_zlim(lims)

            #ax0.text2D(.05, .90, '(b)', transform=ax0.transAxes, va='top')
            l = lims[1]-lims[0]
            ax0.text(2.40*(lims[1]-.05*l), 1, lims[1]*.90, '(b)', zdir='x', va='top')
            tags.pop(1)
        else:
            gs1 = gs[:,0].subgridspec(len(phi_examples), 1, **subgridspec_args)
            axs_evo = [fig.add_subplot(gs1[i,0]) for i in range(len(phi_examples))]
        ax2 = fig.add_subplot(gs[0,1])
        ax3 = fig.add_subplot(gs[1,1], sharex = ax2)

        #ax2.tick_params(labelbottom=False)
    elif style == 'rows':
        gs = GridSpec(2, 2, height_ratios = ratios,figure = fig)
        subgridspec_args = {'hspace': 0}
        gs1 = gs[0,:].subgridspec(len(phi_examples), 1, **subgridspec_args)
        axs_evo = [fig.add_subplot(gs1[i,0]) for i in range(len(phi_examples))]
        ax2 = fig.add_subplot(gs[1,0])
        ax3 = fig.add_subplot(gs[1,1], sharex = ax2)
    else:
        continue

    axs = axs_evo + [ax2, ax3]
    y_text = [.3]*len(axs_evo) + [.25, .90]
    #if model == 'TACT':
    #    y_text = [.25]*len(axs_evo) + [.25, .5]
    for i, ax in enumerate(axs):
        ax.text(.015, y_text[i], f'({tags[i]})', transform=ax.transAxes, va='top')

    for i, ax in enumerate(axs_evo):
        if i == 0:
            label = fr'\frac{{\pi}}{{ {N_example//2} }}' 
        elif (i+1)*2 == N_example:
            label = '\pi'
        else:
            label = fr'\frac{{ {i+1}\pi}}{{ {N_example//2} }}'
        #phi = phi_examples[i]
        #label = fr'\frac{{ {N_example*phi/(2*np.pi):n} }}{{ {N_example//2} }}\pi'
        ax.set_ylabel(fr'$\xi^2 (\phi = {label})$')
    #axs_evo[-1].set_xlabel(r'$\chi_\phi t$')
    axs_evo[-1].set_ylabel(fr'$\xi^2 (\phi = \pi + \frac{{\pi}}{{ {N_example//2} }})$')
    axs_evo[-1].set_xlabel(r'$t [\hbar/E_R]$')
    ax2.set_xlabel('$N$')
    ax2.set_ylabel(r'$\xi^2_\mathrm{best}$')
    ax3.set_xlabel('$N$')
    ax3.set_ylabel(r'$t_\mathrm{best}\ [\hbar/E_R]$')
    
    #'''
    if show_Marcin_evo and model == 'TACT':
        fig_2, axs_2 = plt.subplots(1,3,
                constrained_layout = True)

        for i, ax in enumerate(axs_2):
            ax.set_title(H_labels[i])
            ax.set_ylim((0, 1))

        #for N in range(4,21,2):
        for N in (10,):
            phi = 2*np.pi/N * (N//2+1)
            chi = chi_calc(N,phi, U, J, Omega)
            #print('N:', N)
            #print('chi:', chi)
            l = len(str(N))
            N_text = '0'*(3-l)+str(N)
            path      = 'Marcin/uj_004/'
            #path_1      = 'Marcin/Squeezing_results/FH_results/'
            #pattern_FH   = f'xiSquare_{model}_FH_SOC_PBC_N.{N_text}_J.1_U.10.000_factor.50.000_PBC.0_t_max.*_dt.0.050_BondDim.*_sweeps_tdvp.001_K.010_Cutoff_tdvp_exponent.010.txt'
            N_text = '0'*(2-l)+str(N)
            #path_2      = 'Marcin/Squeezing_results/spin_and_collective_results/'
            pattern_SE_PBC = f'xiSquare_general_{model}_H_SE_PBC_sparse_N.{N_text}_phi_flag.1_axis_rotation.x_k.*.dat'
            pattern_SE_OBC = f'xiSquare_general_{model}_H_SE_OBC_sparse_N.{N_text}_phi_flag.1_axis_rotation.x_k.*.dat'
            pattern_eff    = f'xiSquare_general_{model}_H_sparse_N.{N_text}_phi_flag.1_axis_rotation.x_k.*.dat'

            #df1 = np.loadtxt(find(pattern_eff, path_2)[0])
            #df2 = np.loadtxt(find(pattern_SE_PBC, path_2)[0])
            #df3 = np.loadtxt(find(pattern_FH, path_1)[0])
            df1 = np.loadtxt(find(pattern_eff, path)[0])
            df2 = np.loadtxt(find(pattern_SE_PBC, path)[0])
            df3 = np.loadtxt(find(pattern_SE_OBC, path)[0])

            dfs = df1, df2, df3
            for ax, df in zip(axs_2, dfs):
                ax.plot(df[:, 0], df[:, 1],
                        lw = 1*fig_scale,
                        label = f'N={N}')

            if N == N_example:
                x_max = 0
                y_max = 0
                for i in range(3):
                    df = dfs[i]
                    #if model == 'TACT':
                    #    t_scale = 1 if i == 2 else 0.65
                    #else:
                    #    t_scale = 1 if i == 2 else 0.046
                    #t_scale *= chi
                    #t_scale *= 1e-3
                    t_scale = 1
                    x_i_max = max(df[:,0]) * t_scale
                    y_i_max = max(df[:,1])
                    if x_i_max > x_max:
                        x_max = x_i_max
                    if y_i_max > y_max:
                        y_max = min(y_i_max, 1.1)
                    step = 8 if i == 1 else 4 if i == 2 else 1
                    axs_evo[-1].plot(df[::step, 0]*t_scale, df[::step, 1],
                            color = f'C{6+i}', 
                            fillstyle = 'none',
                            lw = lw[0][i],
                            ls = ls[0][i],
                            zorder = i+5,
                            marker = markers[0][i],
                            label = H_labels[i]+'(Marcin)')
    #'''
    x_max = 0
    y_max = 1.0
    chi_pi = chi_calc(N_example, np.pi, U, J, Omega)
    chi_min = min([chi_calc(N_example, phi, U, J, Omega) for phi in phi_examples])
    for j, phi in enumerate(phi_examples):
        for k, bc in enumerate(('ring', 'open')):
            datafile_evo = (f'output/data/{model} N={N_example}, U={U}, J={J}, '
                +f'Omega={Omega}, ini={initial_state}, phi={phi/pi:.4f}pi, bc={bc}.npz')
            chi = chi_calc(N_example, phi, U, J, Omega)
            df = np.load(datafile_evo)
            t = df['t']#*chi
            phi_file = df['phi']
            t_best, xi_best = df['xy_best']
            data = df['data']
            data_2D = df['data_2D']

            superscript = '^\mathrm{(PBC)}$' if bc == 'ring' else '^\mathrm{(OBC)}$'
            scale = chi / chi_min
            h = data.shape[0]
            for i in range(h):
                step = 1
                if markers[k][i] != ',':
                    step = int(np.ceil(scale))#+2
                    if bc == 'open':
                        if len(t) < 200:
                            step *= 4
                        else:
                            step *= 10
                if i == 0 and bc == 'open':
                    continue
                if not show_H[i]:
                    continue
                y = data[i, :]
                mask  = y < y_max
                x_max = max(x_max, t[mask][-1])
                y_max = min(y_max, max(y))
                l = sum(show_H[:i])+k*(sum(show_H[:h])-1)
                axs_evo[j].plot(t[::step], y[::step],
                        fillstyle = fill[k][i],
                        color = f'C{l}',
                        #zorder = -i,
                        lw = lw[k][i],
                        ls = ls[k][i],
                        marker = markers[k][i],
                        label = H_labels[i][:-1]+superscript)
                #if i==0:
                #    axs_evo[j].text(t[-10], y[-10], 'PBC')
       
    #x_max = x_max*chi_min/chi_pi
    if style == 'Bloch':
        bs.plot_surface(data_2D)

    '''
    path = 'Marcin/Squeezing_results/spin_and_collective_results/'
    title = f'_{model}_phi_flag.1_xiSquare_best_and_tau_best_vs_N_{model}_H_'
    datafile_1 = path+title+'phi_axis_rotation.x_k.0.5L_plus_1.dat'
    datafile_2 = path+title+'SE_PBC_phi_axis_rotation.x_k.0.5L_plus_1.dat'
    datafile_3 = path+title+'SE_OBC_phi_axis_rotation.x_k.0.5L_plus_1.dat'

    df_1 = np.loadtxt(datafile_1)
    df_2 = np.loadtxt(datafile_2)
    df_3 = np.loadtxt(datafile_3)

    dfs = df_1, df_2, df_3
    scaling_labels = [
            f'{H_labels[2][:-1]}^\mathrm{{(PBC)}}$',
            f'{H_labels[1][:-1]}^\mathrm{{(PBC)}}$',
            f'{H_labels[1][:-1]}^\mathrm{{(OBC)}}$',
            ] 
    for i, df in enumerate(dfs):
        for k in range(1,3):
            axs[len(axs_evo)-1+k].plot(df[:,0], df[:,k],
                    ls = ls_2[i],
                    color = colors[i],
                    fillstyle = fill[i],
                    #zorder = (-1)**i*i,
                    #markerfacecolor='white',
                    lw = lw[i],
                    marker = markers_2[i],
                    label = scaling_labels[i], 
                    ) 
    '''

    for ax in axs_evo:
        ax.label_outer()
        ax.set_xlim((0, x_max))
        ax.set_ylim((0, y_max))
        ax.ticklabel_format(axis='x', style='sci', scilimits=(-2,2))
        #ax.tick_params(bottom=False, labelbottom=False)
        ax.yaxis.set_major_locator(ticker.MaxNLocator(N_ticks_y))
        major_ticks = ax.xaxis.get_majorticklocs()
        minor_ticks = [.5 * (major_ticks[i]+major_ticks[i+1]) for i in range(len(major_ticks)-1)]
        #ax.xaxis.set_major_locator(ticker.FixedLocator(major_ticks))
        #ax.xaxis.set_minor_locator(ticker.FixedLocator(minor_ticks))
        ax.xaxis.set_major_locator(ticker.MaxNLocator(7))
        ax.xaxis.set_minor_locator(ticker.MaxNLocator(14))
    axs_evo[0].legend(loc='lower right', ncol = 2,fontsize = fig_scale*6, 
            frameon=False)
    #axs_evo[0].legend(handles = handles[::-1], loc='upper right', ncol = 3,fontsize = fig_scale*8)
    #ax2.label_outer()
    #if model == 'OAT':
    #ax3.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
    #ax2.set_yscale('log')
    #ax2.set_xscale('log')
    #ax3.set_xscale('log')
    #ax3.set_yscale('log')
    #ax2.tick_params(labelbottom=False)
    #ax1.legend()
    #ax2.legend()
    calc_methods = 'light',#'full'
    bcs = 'ring', 'open'
    if show_Tana_results:
        fig_2, axs_2 = plt.subplots(len(calc_methods), len(bcs), sharey = True, constrained_layout = True)
        axs_2 = axs_2.reshape((len(calc_methods), len(bcs)))
        axs_2[0,0].set_ylim((0, 1.5))
        for j,calc in enumerate(calc_methods):
            N_max   = 14 if calc == 'light' else 0
            N_range = range(4, N_max+1, 2)
            for k,bc in enumerate(bcs):
                xi_best = [[] for i in range(4)]
                t_best = [[] for i in range(4)]
                                    
                for N in N_range:
                    phi = np.pi*(1+2/N)
                    path = (f'output/data/{model} N={N}, U={U}, J={J}, '
                        +f'Omega={Omega}, ini={initial_state}, phi={phi/pi:.4f}pi, bc={bc}')
                    if calc == 'light':
                        datafile = path + ', light_calc.npz'
                    elif calc =='full':
                        datafile = path + ', excl_FH.npz'
                    else:
                        datafile = path + '.npz'
                    df = np.load(datafile)
                    t = df['t']
                    data = df['data']
                    for i in range(data.shape[0]):
                        axs_2[j, k].plot(t, data[i,:], label = H_labels[i]+f' (N={N})', 
                                ls = ls_2[i])
                                #ls = ls[k][i])
                        t_i, xi_i = calc_minima(t, data[i,:])
                        t_best[i].append(t_i)
                        xi_best[i].append(xi_i)
                h = data.shape[0]
                for i in range(h):
                    if not show_H[i]:
                        continue
                    if i==0 and bc != 'ring':
                        continue
                    l = sum(show_H[:i])+k*sum(show_H[:h])
                    #l = sum(show_H[:i])*(1+k*h)
                    
                    stripped_label = H_labels[i][17:-2]
                    title = (f'{model}, {stripped_label}-{bc}, U={U}, J={J}, '
                            +f'Omega={Omega}, ini={initial_state}')
                    np.savetxt('output/data/scaling for '+title+'.txt', np.array([N_range, xi_best[i], t_best[i]]).T)
                    #data = np.loadtxt('output/data/combined scaling for '+title+'.txt')
                    #data = np.genfromtxt('output/data/combined scaling for '+title+'.txt')
                    data = np.loadtxt('output/data/combined scaling for '+title+'.txt', 
                            converters = {2: lambda s: eval(s)})
                            #converters = {2: lambda s:np.product([float(val) for val in str(s).split('*')])})
                    print(data)
                    N_range_comb = data[:,0]
                    xi_best_comb = data[:,1]
                    t_best_comb = data[:,2]

                    #for ax, y in ((ax2, xi_best), (ax3, t_best)):
                    for ax, y in ((ax2, xi_best_comb), (ax3, t_best_comb)):
                        #ax.plot(N_range, y[i], ls = ls[k][i], 
                        ax.plot(N_range_comb, y, ls = ls[k][i], 
                                marker = markers[k][i], 
                                color = f'C{l}',
                                fillstyle = fill[k][i],
                                lw = lw[k][i], 
                                label = H_labels[i] + f' ({bc})')
                axs_2[j,k].set_title(f'{model}, {calc}, {bc}')
                axs_2[j,k].legend(fontsize = 7)

        #ax2.legend(fontsize = 10)
        #ax2.legend()
    #ax3.xaxis.set_major_locator(ticker.MaxNLocator(N_ticks_x))
    #ax3.xaxis.set_minor_locator(ticker.MaxNLocator(2*N_ticks_x))
    ax3.xaxis.set_major_locator(ticker.FixedLocator(N_range_comb[::2]))
    ax3.xaxis.set_minor_locator(ticker.FixedLocator(range(5,20)))
    ax2.set_ylim((0, y_max))
    ax2.yaxis.set_major_locator(ticker.MaxNLocator(5))
    ax3.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
    '''
    x = np.floor(np.linspace(4,21,N_ticks_x))
    x = np.floor(np.logspace(np.log2(4),np.log2(20), N_ticks_x, base = 2))
    x = [2*(val // 2 + int(val % 2)) for val in x]
    x_labels = [f'{int(val)}' for val in x]
    ax3.set_xticks(x, labels = x_labels)
    ax3.xaxis.set_minor_locator(ticker.FixedLocator(range(4,21,2)))
    ax3.xaxis.set_minor_formatter(ticker.NullFormatter())
    '''
    #ax3.set_xticklabels(x_labels)
    #ax3.xaxis.set_major_locator(ticker.LogLocator(base=10, numticks=N_ticks_x))
    #ax3.xaxis.set_minor_locator(ticker.LogLocator(base=2, numticks=2*N_ticks_x))
    #ax3.xaxis.set_major_formatter(ticker.ScalarFormatter())
    #ax3.ticklabel_format(axis='x', style='plain')
    #x = np.arange(4,21,2)
    #ax3.plot(x, 1.7*1e4/4*x**(1/3))
    #fig.tight_layout()
    fig.savefig(filename+'.png', dpi = 300)
    fig.savefig(filename+'.pdf')
#plt.show()

