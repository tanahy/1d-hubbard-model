import sys

from matplotlib.gridspec import GridSpec

from hubbard.hubbard import *
from hubbard.utilities import * 

N = 6
phi_index = 0
if len(sys.argv) == 2:
    N = int(sys.argv[1])
elif len(sys.argv) == 3:
    N = int(sys.argv[1])
    model = sys.argv[2]
elif len(sys.argv) >= 4:
    N = int(sys.argv[1])
    model = sys.argv[2]
    phi_index = int(sys.argv[3])-1

M = N
phi_available = [2*np.pi/M * (k+1) for k in range(M)]
U = 1 
r = 1e-1
J = U*r
Omega = U*r**3

phi = phi_available[phi_index]

print('U:', U)
print('J:', J)
print('Omega:', Omega)
print('phi:', phi/np.pi,'* pi')

#bc = 'open'
#bc = 'periodic'
bc = 'ring'

save = False

N_steps = 100
f = Hubbard(N, M, 'Fermi', bc = bc)
f.change_frame('lab', phi = phi)

chi = chi_calc(N, phi, U, J, Omega)

t_f = 1/abs(chi)
t_f = 1e3
dt = t_f / N_steps

print('chi:', chi)
print('tf:', t_f)

if phi == np.pi:
    model = '$\chi \hat{S}_x^2$'
    psi = 0
    theta = np.pi/2
    H_1 = chi*f.Sx**2 
else:
    model = '$\chi \hat{S}_z^2$'
    psi = 0#np.pi/2
    theta = 0#np.pi/2
    H_1 = chi*f.Sz**2

H_2 = f.H_eff(J=J,U=U,Omega=Omega) 
H_3 = f.H_0(J=J) + f.H_int(U=U) + f.H_SOC(Omega = Omega) 

H = [H_3, H_2, H_1]
H_labels = ['FH+SOC', 'Spin', model]

fig, ax = plt.subplots(1, constrained_layout=True)

lines = iter(['solid','dashed', 'dashdot']+12*['None'])
markers = iter(3*['None']+['+', '^', 's', 'o', '*', 'x','<','>','1','2','3','4'])

wf = f.initial_state_fermi()
wf = expm_multiply(-1j*f.Sy*theta, wf)
wf = expm_multiply(-1j*f.Sz*psi, wf)
#assert calculate_ortho(wf, expm(-1j*Oy*np.pi).dot(wf))
obs = f.Sx*f.Sz + f.Sz*f.Sx
for k in range(len(H)):
    f.H = H[k]
    label = H_labels[k]
    ls = next(lines)
    marker = next(markers)
    
    t, data = f.evolve(wf, dt = dt, t_f = t_f, parameters = [obs], scheme = 'parallel')
    #data /= abs(data)
    #data *= 4/(N*abs(chi))
    t *= chi
    ax.plot(t, data[0], ls = ls, marker = marker, label = label)

ax.legend()
#ax.set_ylim((-1.1,1.1))
#ax.set_ylabel(r'$\frac{<\hat{H}_\mathrm{model}>}{\|<\hat{H}_\mathrm{model}>\|}$')
#ax.set_ylabel(r'$4\frac{<\hat{H}_\mathrm{model}>}{N\chi}$')
ax.set_xlabel('$\chi t$')
title = fr'$N={N}, M={M}, \phi = {phi/np.pi:.2f}\pi, \Omega = {Omega:.4f}, J= {J:.2f}, U = {U}$'
#title = fr'$N={N}, M={M}, \Omega = {Omega}, J= {J:.2f}, U = {U}$'

#fig.suptitle(title+fr', $\|\Psi_0\rangle = \|\theta={theta/np.pi:.2f}\pi,\phi={psi/np.pi:.2f}\pi \rangle  $')
if save:
    title = title.replace("\\","").replace('$',"")
    main_title = 'proj_model'
    title = (f'output/{main_title}, {title}, {model}, {bc},'
             + f'init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')
    plt.savefig(title+'.pdf')
plt.show()
