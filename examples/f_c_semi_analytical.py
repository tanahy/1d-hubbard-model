#!/usr/bin/env python
# coding: utf-8
import sys, os

import pandas as pd
import numpy as np

from hubbard.utilities import *

frac = 50
ani = .98

J = 1
U = 24.4*J
sigma = None
error = 1e-8

path = '../output'

if len(sys.argv) > 1:
    N = M   = int(sys.argv[1])
    J       = float(sys.argv[2])
    U       = float(sys.argv[3])
    sigma   = sys.argv[4]
    if sigma == 'None':
        sigma = None
    else:
        sigma = int(sigma)
    path    = sys.argv[5]
    model   = sys.argv[6]

def calc_params(m, J, U):
    if model == 'B':
        J_SE = 4 * J**2 / U
        #E_gap = J_SE * (1 - np.cos(np.pi/m*(m-1)))
        E_gap = 2 * J_SE
        j = np.arange(m)+1
        beta = E_gap / frac * np.cos(np.pi/m*(j-.5)*(m-1))
        
        chi = 0        
        for q in range(1, m):
            E_q = -J_SE*(1-np.cos(np.pi/m*q))
            c_q = 0
            for l in range(m):
                c_q += np.sqrt(2)/m*p_l(l, 0, m, q) * beta[l]
            chi += abs(c_q)**2 / E_q
        chi /= (m-1)
    elif model == 'contact':
        J_SE = 4 * J**2 / U[2]
        delta = U[2]/U[0] + U[2]/U[1] - 1
        beta = np.zeros(m)
        chi =  J_SE * (1-delta) / (m-1)
    
    t_min_theo = 3**(1/6)*(m)**(-2/3)/abs(chi) # Youcef Baamaraa, Alice Sinatraa, Manuel Gessner (2021)
    t_f = 3*t_min_theo
    #t_f = 1/abs(chi)
    
    t = np.linspace(0, t_f, 200)

    return beta, t


def L_simple_min(M, filling, func):
    evs = func(filling)
    S_mean, S_sq, _, S_sq2 = calc_squeezing_frame(M, evs)
    Var = S_sq2 - (S_sq)**2
    result = []
    for (i,(a,b)) in enumerate(zip(Var, S_mean)):
        r = M - b**2 / ((M - 4*a)) - filling**2 * (M-4*a)
        if abs(1/filling * b / (4*a-M)) <= 1 and filling**2*(4*a - M) < 0:
            result.append(r)
        else:
            #print(i, 'not accepted')
            result.append(-2 * filling * abs(b) + M)
    return min(result)

def L_corrected_min(M, filling, func):
    evs = func(filling)
    S_mean, S_sq, _, S_sq2 = calc_squeezing_frame(M, evs)
    Var = S_sq2 - (S_sq)**2
    result = []
    for (i,(a,b)) in enumerate(zip(Var, S_mean)):
        r = M - b**2 / (M * filling - 4*a) - (M * filling - 4*a)
        if b <= M*filling - 4*a:
            result.append(r)
        else:
            #print(i, 'not accepted')
            result.append(M -2 * abs(b))
    return min(result)

def p_c_calc(M, func):
    evs = func(1)
    S_mean, S_sq, _, S_sq2 = calc_squeezing_frame(M, evs)
    Var = S_sq2 - (S_sq)**2
    result = np.sqrt(M*(M-4*Var)-S_mean**2)/(M-4*Var)

    for i in range(len(S_mean)):
        result[i] = result[i] if result[i] >= S_mean[i]/(M-4*Var[i]) else M/(2*S_mean[i])

    return min(result)

def first_passage(bounds, func, error, max_iter = None):
    x_0, x_f = bounds
    count = 0
    x = [x_0]
    y = [func(x_0)]
    if y[0] > 0:
        return [None], [None]
        
    while True:
        x_i = (x_0 + x_f)/2
        y_i = func(x_i)
        if y_i <= 0:
            x.append(x_i)
            y.append(y_i)
            x_0 = x_i
        else:
            x_f = x_i
            
        if abs(x_0 - x_f)/2 < error:
            return x, y
            
        count += 1
        if max_iter is not None:
            if count > max_iter:
                return None


if model == 'B':
    beta, t = calc_params(M, J, U)
    J_SE = 4*J**2/U
    calc_evs = ensemble_f(M, sigma, J = J_SE, beta = beta, t = t)
    tail = f'J={J}_U={U}_frac={frac}_sigma={sigma}'
elif model == 'contact':
    U = U, U, ani*U
    beta, t = calc_params(M, J, U)
    calc_evs = ensemble_f(M, sigma, evs_chain_ani, J = J, U = U, t = t)
    tail = f'J={J}_U={U}_sigma={sigma}'
else:
    raise ValueError

bell_min_1 = lambda f: L_simple_min(M, f, calc_evs)
bell_min_2 = lambda f: L_corrected_min(M, f, calc_evs)
x_1, y_1 = first_passage((1, 0), bell_min_1, error)
x_2, y_2 = first_passage((1, 0), bell_min_2, error)
p_c_lower = p_c_calc(M, calc_evs)

df_1 = pd.DataFrame({'f': x_1, 'Bell_min': y_1})
fn = f'output/bell_cut_simple_{tail}_M={M}.csv'
df_1.to_csv(fn)
    
df_2 = pd.DataFrame({'f': x_2, 'Bell_min': y_2})
fn = f'output/bell_cut_corrected_{tail}_M={M}.csv'
df_2.to_csv(fn)

df_f_c = pd.DataFrame({'M': [M], 
                        'p_c_simp': [x_1[-1]],
                        'p_c_corr': [x_2[-1]], 
                        'p_c_lower': [p_c_lower]}) 

filename = f'output/f_c_{tail}'

df_f_c.to_csv(f'{filename}.csv', header = False, index = False, mode='a')
