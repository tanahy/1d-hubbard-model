import matplotlib.pyplot as plt
import numpy as np
from hubbard.utilities import *



path = 'output/data/'
pattern = 'OAT*U=0.5, J=0.02, Omega=0.000128*=open, light_calc.npz*'

results = find(pattern, path)

for fn in results:
    df = np.load(fn)
    params = fn.replace(path, '').split(',')
    phi = params[5].split('=')[1]
    N   = params[0].split('=')[1]
    print(N, phi)
    t = df['t']
    data = df['data']
    y = data[1, :]
    filename = f'spin model for N={N}, phi={phi}, U=0.5, J=0.02, Omega=0.000128, bc=open, ini=y.txt'
    np.savetxt(path+filename, np.vstack((t, y)).T)
