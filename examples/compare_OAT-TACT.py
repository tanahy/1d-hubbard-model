import sys
import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.gridspec import GridSpec

#from hubbard.hubbard import *
#from hubbard.utilities import coherent_states, q_function_calc
from hubbard.plot import *
from hubbard.bloch import *

###############################

if len(sys.argv) >= 3:
    N = int(sys.argv[1])
    bc = str(sys.argv[2])
else:
    N = 8
    bc = 'ring'

M = N

time_scale = 1 #7e-6 #[hbar/ER] = 7 micro seconds
ER = 1
r = 1/10
U = 1/ER
J = r*U
Omega = r**3*U

initial_states = 'y',#'x','y','z'
initial_states_coords = (pi/2, pi/2),#pi/2 * np.array(((1,0), (1,1), (0,0)))

phi_list = [2*pi/N, pi, 2*pi/N]
phi_labels = fr'OAT $(\frac{{\pi}}{{ {N//2} }})$', 'OAT $(\pi)$', 'TACT'
model_types = 'non_pi', 'pi', 'TACT'

N_steps = 100
cols = 3
rows = len(initial_states)

fig_scale = 2
plt.rcParams["text.usetex"] = True
plt.rcParams["font.family"] = 'serif'
plt.rcParams["font.serif"] = 'Computer Modern Roman'
plt.rcParams.update({'font.size': fig_scale*10})
width   = fig_scale*3.416

figsize = width, width / 2
fig, axs = plt.subplots(rows, cols, 
        figsize = figsize,
        constrained_layout=True,
        sharey = True,
        )

aspect_ratio = figsize[0]/figsize[1] * rows / cols

connect_styles = 'zoom', 'arrow_box', 'arrow', 'debug', 'none'
connect_style = connect_styles[4]

filename = f'output/portraits/Compare OAT-TACT N={N}, U={U}, J={J}, Omega={Omega}, {bc}, style={connect_style}'

ls = 'solid', 'dashed', 'dotted', 'dashdot', '-', '--', '-.', ':', 'None', ' ', ''
markers = 'None', 'None', 'None','+', 'x'

time_scales = [.25, .5, .25]
if N == 8:
    time_scales = [.15, .5, .15]
elif N == 10:
    time_scales = [.0675, .385, .0675]
    time_scales = [.38, .38, .17]

###############################

def chi_calc(phi):
    J_SE = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*np.cos(phi/2)**2)
    J_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*np.sin(phi/2)**2)
    E_phi = J_SE*(1-np.cos(phi))

    chi = J_SOC**2/(E_phi*(N-1))
    if phi != pi:
        chi /= 2

    return chi

xi_best_all = 1
for i, phi in enumerate(phi_list):
    for j in range(len(initial_states)):
        theta, psi = initial_states_coords[j]
        if rows == 1:
            ax = axs[i]
        else:
            ax = axs[j, i]
        #ax.set_box_aspect(1/aspect_ratio)
        #aspect = ax.get_box_aspect()
        #print(aspect)
        model_type = model_types[i]
        if model_types[i] == 'TACT':
            datafile = (f'output/data/TACT N={N}, U={U}, J={J}, '
                +f'Omega={Omega}, ini={initial_states[j]}, phi={phi/pi:.4f}pi, bc={bc}.npz')
        else:
            datafile = (f'output/data/OAT N={N}, U={U}, J={J}, '
                +f'Omega={Omega}, ini={initial_states[j]}, phi={phi/pi:.4f}pi, bc={bc}.npz')

        df = np.load(datafile)
        
        t = df['t']
        phi_file = df['phi']
        t_best, xi_best = df['xy_best']
        data = df['data'] 
        data_2D = df['data_2D']
       
        xi_best_all = min(data.min(), xi_best_all)

        model_label = ''
        chi_labels = r'','', r''
        if model_type == 'pi':
            model_label = '$-\chi_\pi\hat{{S}}_x^2$'
            ax.set_xlabel(r'$\chi_\pi t$')
        elif model_type == 'non_pi':
            model_label = r'$\chi_\phi\hat{{S}}_z^2$'
            ax.set_xlabel(r'$\frac{\chi_\phi}{2} t$')
            ax.set_xlabel(r'$\chi_\phi t$')
        elif model_type == 'TACT':
            model_label = r'$\chi_\phi\left(\hat{{S}}_z^2-\hat{{S}}_x^2\right)$'
            ax.set_xlabel(r'$\chi_\phi t$')
        #ax.set_xlabel(r'$\chi t$')

        H_labels = model_label, r'$\hat{H}_{spin}$', '$\hat{H}$'

        if data[0,:].std() < 1e-3:
            ind = (len(t) * 3) // 4 - 1
            t_best = t[ind]
            xi_best = data[-1,:][ind]

        ind = list(t).index(t_best)
        step = [j for j in range(2,3) if (ind % j) == 0]
        step = step[-1] if len(step) > 0 else 2
        steps = 1, step, step
        steps = 1, 1, 1
        #ind for N=8:
        #23,23,12

        #origin = [3,3,0][i]
        origin = 0
        #step = [5,2,3][i]
        #steps = 1, step, step
        ori = 0, origin, origin
        ax.set_ylim((0, 1.2))

        theta_label = '0' if theta == 0 else fr'\frac{{\pi}}{{ {pi/theta:n} }}' 
        psi_label = '0' if psi == 0 else fr'\frac{{\pi}}{{ {pi/psi:n} }}' 
        #ini_label_bloch = fr'\|\Psi_0\rangle = \|\theta={theta_label},\phi={psi_label} \rangle'
        ini_label_bloch = fr'\|\theta_0, \phi_0\rangle = \|{theta_label},{psi_label} \rangle'
        #ax.set_title(f'{phi_labels[i]}')
        if i == 0:
            ax.set_ylabel(r'$\xi^2$')
        ax.locator_params(axis='y', nbins=6)
        ax.locator_params(axis='x', nbins=3)
        #ax.set_ylabel(rf'$\xi^2$ [{phi_labels[i]}]')
        
        chi = chi_calc(phi)
        if model_type == 'TACT':
            chi /= 2
        
        t = chi*t
        t_best = chi*t_best
        ax.set_xlim((0, t[-1]*time_scales[i]))
        #n_ticks = 4
        #ax.set_xticks(np.linspace(0,tf,n_ticks))
        #ax.set_xticklabels(np.round(np.linspace(0,tf_scaled*10,n_ticks)/10,1))
        #ax.set_xticks([0, t_best, t_f])
        #ax.set_xticklabels(['0', r'$t_{best}$', t_f_label])
        #ax.set_xticklabels(['0', r'$t_{best}$', f'{t_f_scaled:.2f}'])
        #ax.legend(fontsize=14, loc = 'lower right')
        #ax.legend(loc = 'upper right')
        tags = ['a','b','c', 'd', 'e', 'f']
        #ax.text(.05, .95, f'({tags[cols*i+j]})', fontsize=14, transform=ax.transAxes, va='top')
        ax.text(.05, .15, f'({tags[cols*j+i]})', transform=ax.transAxes, va='top')
        #ax.set_xlabel(r'$\chi t$')
        
        for k in range(data.shape[0]): 
            ax.plot(t[ori[k]::steps[k]], data[k,ori[k]::steps[k]],
                    lw = 2,
                    ls = ls[k], marker = markers[k], label = H_labels[k])
        #ax.legend(loc = 'lower right', ncol=2, fontsize=10)
      
        side = .80
        h = side / rows * aspect_ratio
        w = side / cols
        w_box = .90*w
        h_box = .90*h
        #dx = .045 
        dx = .093 
        dy = .04
        x0 = .08 / cols
        y0 = .35 / rows
        x_axin = dx + x0 + (1-dx) * i/cols
        y_axin = 1 + dy + y0 - (1-dy) * (j+1)/rows
        xy_axin = [x_axin, y_axin]
        #xy_frame = [xy_axin[0]+(w-w_box)/2 - 0.005, xy_axin[1]+(h-h_box)/2 + .02]
        xy_frame = [xy_axin[0]+(w-w_box)/2,#- 0.005, 
                    xy_axin[1]+(h-h_box)/2]#+ .02]
        
        w_small = t[-1]/len(t)*step
        h_small = 1.5/len(t)*step
        xy_small = [t_best - w_small/2, xi_best - h_small/2]
 
        #sq_bloch = [xy_axin[0]-w/3.2, xy_axin[1]-h/2.0, 1.7*w, 1.7*h] 
        #axin = fig.add_axes(sq_bloch, anchor='SW', projection='3d', azim = 45, zorder=6)
        axin = fig.add_axes(xy_axin+[w, h], anchor='SW', projection='3d', azim = 45, zorder=6)
        axin.view_init(90-theta*180/np.pi, psi*180/np.pi)
        axin.patch.set_alpha(0.)
        axes_style = dict(span = .25,#0,# 
                margin = 0.1,#.25,#
                fontsize = fig_scale * 6,
                color = 'red')
        bs = Bloch_Sphere(alpha = .1, ax = axin, 
                #cmap = cm.inferno, 
                cmap = cm.jet, 
                #cmap = cm.hot, 
                axes_style = axes_style
                )
        if connect_style == 'debug':
            axin_area = mpatches.Rectangle(xy_axin, w, h, 
                                        fill = False, edgecolor = 'red')
            fig.add_artist(axin_area)

            frame = mpatches.Rectangle(xy_frame, w_box, h_box, 
                                        fill = True,
                                        alpha = .5,
                                        #zorder=100,
                                        edgecolor = 'blue')
            fig.add_artist(frame)

            frame_small = mpatches.Rectangle(xy_small, w_small, h_small, 
                                        fill = False, 
                                        zorder = 10,
                                        edgecolor = 'green')
            ax.add_artist(frame_small)

            arrowprops = dict(arrowstyle="<->",
                                    patchA=frame,
                                    patchB=frame_small,
                                    shrinkA=0,
                                    shrinkB=0,
                                    )
            ax.annotate("",
                    xy=(t_best, xi_best), xycoords='data',
                    xytext=(xy_axin[0]+w/2, xy_axin[1]+h/2), textcoords='figure fraction',
                    arrowprops=arrowprops)

        elif connect_style == 'zoom':
            #axin = ax.inset_axes([.5, .1, .3, .3], projection='3d', azim = 30)
            #sq = mpatches.Rectangle((rect[0], rect[1]), rect[2], rect[3], 
            #                            fill = False, edgecolor = 'black')
            #fig.add_artist(sq)
            frame = mpatches.Rectangle(xy_frame, w_box, h_box, 
                                        fill = False, 
                                        #zorder=100,
                                        edgecolor = 'black')
            fig.add_artist(frame)

            frame_small = mpatches.Rectangle(xy_small, w_small, h_small, 
                                        fill = False, 
                                        zorder = 10,
                                        edgecolor = 'black')
            ax.add_artist(frame_small)
            arrowprops = dict(arrowstyle="-",
                                    patchA=frame,
                                    patchB=frame_small,
                                    shrinkA=0,
                                    shrinkB=0,
                                    )
            ax.annotate("",
                    xy=(xy_small[0], xy_small[1]+h_small), xycoords='data',
                    xytext=xy_frame, textcoords='figure fraction',
                    arrowprops=arrowprops)
            ax.annotate("",
                    xy=(xy_small[0]+w_small, xy_small[1]+h_small), xycoords='data',
                    xytext=(xy_frame[0]+w_box, xy_frame[1]), textcoords='figure fraction',
                    arrowprops=arrowprops)
        elif connect_style == 'arrow_box':
            frame = mpatches.Rectangle(xy_frame, w_box, h_box, 
                                        fill = True, 
                                        facecolor = 'white',
                                        edgecolor = 'black',
                                        alpha = .5,
                                        #zorder = 5,
                                        )
            fig.add_artist(frame)
            arrowprops = dict(arrowstyle="simple",
                                    patchA=frame,
                                    #patchB=frame_small,
                                    color="0.5",
                                    shrinkA=1,
                                    shrinkB=5,
                                    )
            ax.annotate("",
                    xy=(t_best, xi_best), xycoords='data',
                    xytext=(xy_axin[0]+w/2, xy_axin[1]+h/2), textcoords='figure fraction',
                    arrowprops=arrowprops)

        elif connect_style == 'arrow':
            xy_ellipse = xy_small[0] + w_small/2, xy_small[1] + h_small/2 
            circle_fraction = 20
            w_ellipse = ax.get_xlim()[1]/circle_fraction
            h_ellipse = ax.get_ylim()[1]/circle_fraction*aspect_ratio
            frame = mpatches.Ellipse(xy_ellipse, width = w_ellipse, height=h_ellipse, 
                                        fill = False, 
                                        edgecolor = 'grey',
                                        zorder=5,
                                        )
            ax.add_patch(frame)
            ax.annotate("",
                    xy=(t_best, xi_best), xycoords='data',
                    #xytext=(xy_axin[0]+w/2, xy_frame[1]), textcoords='figure fraction',
                    xytext=(xy_axin[0]+w/2, xy_axin[1]+h/5), textcoords='figure fraction',
                    arrowprops=dict(arrowstyle="fancy",
                                    color="0.5",
                                    patchB=frame,
                                    #patchA=axin,
                                    #shrinkA=10*h/2,
                                    shrinkB=2,
                                    connectionstyle="arc3,rad=0.1",
                                    ),
                    )
        if len(data_2D.shape) > 2:
            print(data_2D.shape)
            bs.plot_surface(data_2D[ind,:,:,-1])
        else:
            bs.plot_surface(data_2D)

#for ax in axs:
#    ax.axhline(xi_best_all, c='grey', ls = (0, (1, 10)))

#fig.savefig(filename+'.png')
fig.savefig(filename+'.pdf')
#plt.show()
