import sys

from hubbard.hubbard import *

N = 4
M = N

t_f = 10
N_steps = 100
dt = t_f / N_steps
F = Hubbard(N, M, 'Fermi', bc='open')
wf = F.initial_state_fermi()
Omega = 10.#np.linspace(.1,100,5)#np.linspace(.5,2,3) 
phi =7/6*np.pi#np.pi 2, 
F.change_frame('lab', phi = phi)
U = 1 #2, 6, 8, 10
J=1

H_rot = np.array([[0,0,Omega/2,-Omega/2,0,0],
    [0,U,-2*np.exp(-1j*phi/2)*J,-2*np.exp(1j*phi/2)*J,0,0],
    [Omega/2,-2*np.exp(1j*phi/2)*J,0,0,-2*np.exp(1j*phi/2)*J,-Omega/2],
    [-Omega/2, -2*np.exp(-1j*phi/2)*J,0,0,-2*np.exp(-1j*phi/2)*J,Omega/2],
    [0,0,-2*np.exp(-1j*phi/2)*J,-2*np.exp(1j*phi/2)*J,U,0],
    [0,0,-Omega/2, Omega/2, 0,0]])
H_rot = sparse.csc_matrix(H_rot, shape=(6, 6))

H_lab = np.array([[0,0,np.exp(1j*phi)*Omega/2,-Omega/2,0,0],
    [0,U,-J,-J,0,0],
    [np.exp(-1j*phi)*Omega/2,-J,0,0,-J,-Omega/2],
    [-Omega/2, -J,0,0,-J,np.exp(1j*phi)*Omega/2],
    [0,0,-J,-J,U,0],
    [0,0,-Omega/2, np.exp(-1j*phi)*Omega/2, 0,0]])
H_lab = sparse.csc_matrix(H_lab, shape=(6, 6))

U = U,
Omega = Omega,

for u in U:
    plt.figure()
    for omega in Omega:
        F.H = F.H_0(J=J) + F.H_int(U=u) + F.H_L(Omega = omega) 
        #print('Difference',F.H  != H_lab)
        wf2 = expm(-1j*F.Sy*np.pi/2).dot(wf.T)
        U_dt = F.time_evolution(dt)
        t = 0
        t_list = []
        ssp_list = []
        for i in range(N_steps):
            ssp, _ = F.spin_squeezing(wf2, operator = 'S')
            ssp_list.append(ssp)
            t_list.append(t)
            t += dt
            wf2 = U_dt.dot(wf2)
            #if ssp > 1.1:
            #    break
        
        plt.plot(t_list, ssp_list, label = f'Omega = {omega}')
        plt.title(f'$ N={N}, M={M}, \phi = \pi/{round(np.pi/phi)}, U = {u}$')
        plt.legend()
        plt.ylim((0,1.5))
    if omega == 0.04:
        plt.xlim((0,500))
    #plt.savefig(f'output/N={N}, M={M}, U={u}, Omega={omega} (lab frame).pdf')
    #np.savetxt(f'output/N={N}-lab-frame.txt', np.array([t_list,ssp_list]).T)
plt.show()
