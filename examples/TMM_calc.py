import sys

from hubbard.hubbard import *
from hubbard.utilities import calc_gamma

model = 'TMM'
N = 4
eta = 1
if len(sys.argv) == 2:
    N = int(sys.argv[1])
elif len(sys.argv) == 3:
    N = int(sys.argv[1])
    model = sys.argv[2]
elif len(sys.argv) >= 4:
    N = int(sys.argv[1])
    model = sys.argv[2]
    eta = float(sys.argv[3])

M = N
U = .01 
U = (U, U, .95*U)
J = 1
C_SS = (U[0]+U[1]-2*U[2])/(2 * M)

psi = np.pi/2
theta = np.pi/2
#bc = 'anti-periodic'
bc = 'periodic'
#bc = 'open'
#bc = 'ring'
frame = 'lab'

gamma = calc_gamma(M, C_SS, eta, bc)

N_steps = 300

if model == 'BHM':
    s = Hubbard(N, M, 'Bose_spin', bc = bc)
    s.change_frame(frame, phi = None)
    s.H = s.H_0_bose_spin(J=J) + s.H_int_bose_spin(U=U) + s.H_dip_bose_spin(gamma=gamma)
elif model == 'TMM':
    s = Hubbard(N, 1, 'Bose_spin', bc = bc)
    s.change_frame(frame, phi = None)
    s.H = s.H_TMM(J,U=U,gamma=gamma)
else:
    raise ValueError('Unrecognized model.')

t_f = 1/(C_SS + gamma**2*h3/(2*M)) if bc == 'ring' else 1/(C_SS)
dt = t_f / N_steps

parameters = ['squeezing']

wf = s.initial_state(J=J, U=U)
wf = expm_multiply(-1j*s.Sy*theta, wf)
wf = expm_multiply(-1j*s.Sz*psi, wf)

t, data = s.evolve(wf, dt = dt, t_f = t_f, parameters = parameters, scheme = 'parallel')

title = fr'N={N}, M={M}, gamma = {gamma:.4f}, J= {J:.2f}, U = {U}'

title = (f'output/squeezing for {model}, {title},'
             + f'init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')

np.savez(title, t = t, data = data)
