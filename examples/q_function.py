import sys
import time

import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.patches import Circle
from hubbard.hubbard import *
from hubbard.utilities import *
from hubbard.plot import *

###############################
N = 4
U = 0 
t_f = 20
Omega = 1
N_steps = 300
dt = t_f / N_steps
J = 1 if U != 0 else 0
M = N
res = 100
phi = .01*np.pi
#phi = .41*np.pi
#<H> = -Omega sin theta sum_j cos(psi + j*phi)

psi_fm, psi_fp  = critical_psi(M,phi)

psi0 = 0#psi_fm
#Phi = [0,.05,.2,.3,.49]
Phi = [0]#,.25,.5]
#Phi = [0, -psi0/np.pi, .41]
#Phi = [psi0 + np.pi*val for val in Phi]
#Phi = [psi_loop(val) for val in Phi]
ground_state = False 
energy_distr = False
stability    = False
initial_husimi = False
show_spins   = True
single_spins_per_axis = False 
###############################
colors = ['r','g','b','orange', 'purple', 'yellow', 'brown']
#ls = ['dotted','dashed','solid','dashdot', 'densely dashed']
ls = 'dashed', 'dashdot', 'dotted','-', '--', '-.', ':', 'None', ' ', ''
filename = f'output/portraits/Q function, N={N}, U={U}, tf={t_f}, Omega={Omega}, phi={phi/np.pi:.2f}pi'
if psi0 == psi_fm: 
    filename += ', psi=psi_fm'
    phi_ref = r'\varphi_{{f-}}' 
elif psi0 == psi_fp: 
    filename += ', psi=psi_fm'
    phi_ref = r'\varphi_{{f+}}' 
else:
    filename += f', psi={psi0/np.pi:.2f}pi'
    phi_ref = rf'{psi0/np.pi:.2f}\pi'
if ground_state:
    filename += ', Psi(0) = GS'
filename += '.pdf'

'''
fig = plt.figure(constrained_layout=True)

gs = GridSpec(3,2, figure=fig)
ax1 = fig.add_subplot(gs[0,:])
ax2 = fig.add_subplot(gs[1,:])
ax3 = fig.add_subplot(gs[2,:])
'''
if show_spins:
    if single_spins_per_axis:
        rows = 5
    else:
        rows = 2 + len(Phi) 
else:
    rows = 4

fig, axs = plt.subplots(rows,1, constrained_layout = True, figsize=(5,5*(rows//2)))

axs[0].set_title(f'$N={N}, J={J}, U={U}, \Omega={Omega}, \phi={phi/np.pi:.2f}pi$')

start = time.time()
F = Hubbard(N,M,'Fermi',bc='open')
F.change_frame('lab', phi = phi)

#F.H = F.H_0(J=J) + F.H_int(U=U) + F.H_L(Omega = Omega)
H_0 = F.H_0(J=J) 
H_int = F.H_int(U=U)
H_L = F.H_L(Omega = Omega)

F.H = H_0 + H_int + H_L
Udt = F.time_evolution(dt)

if ground_state:
    F.eigenvalues()
    wf0 = F.G
else:
    wf0 = F.initial_state_fermi()

ref_state = wf0 
initial_state = expm(-1j*F.Sz*Phi[0]).dot(expm(-1j*F.Sy*np.pi/2).dot(wf0))

ortho = calculate_ortho(ref_state, expm(-1j*np.pi*F.Sy).dot(ref_state))
assert ortho


if ground_state or energy_distr:
    q_function = Q_function_representation(dt=dt, ax=axs[0], resolution = res, F = F,
                                            initial_state=initial_state, ref_state = ref_state)
    wf_psi_theta = q_function.wf_psi_theta
else:
    q_function = Q_function_representation(dt=dt, ax=axs[0], resolution = res, 
                generator = q_function_SOC, calc_q = False, Omega=Omega, phi=phi, M=M)
Psi, Theta = q_function.Psi, q_function.Theta
X,Y = q_function.X, q_function.Y
end = time.time()
print("Initilization:",end-start)

if stability:
    print('phi:', phi)
    print('psi0:', psi0)
    if not 'wf_psi_theta' in dir(q_function):
        q_function.coherent_states(F, ref_state = ref_state)
        wf_psi_theta = q_function.wf_psi_theta
    eigenvalues = stability_analysis(Psi,Theta,F.H,wf_psi_theta,psi0)
    print('Stability Analysis eigenvalues:', eigenvalues)

if initial_husimi:
    q_function.update(0)
else:
    axs[0].collections = []

start = time.time()
if ground_state or energy_distr:
    E,u,v = energy_fluc(Psi,Theta, F.H, wf_psi_theta)
    #ax1.contour(X, Y, E, colors='green')
    speed = np.sqrt(u**2+v**2)
    axs[0].streamplot(X,Y,u,v,
            density=.6, color='k', linewidth=3*speed/np.max(speed))
else:
    q_function.streamplot(M, Omega, phi)

end = time.time()
print("Stream calculations:",end-start)

for i,psi in enumerate(Phi):
    start = time.time()

    wf = expm(-1j*F.Sz*psi).dot(expm(-1j*F.Sy*np.pi/2).dot(wf0))
    
    t = []
    squeezing = []
    if show_spins:
        s_x = []
        s_y = []
        s_z = []
    else:
        E_0 = []
        E_int = []
        E_L = []
    for k in range(N_steps):
        ssp = F.spin_squeezing(wf, operator = 'S')
        if show_spins:
            s_x.append(np.real(expectation_value(wf, F.Sx))/N)
            s_y.append(np.real(expectation_value(wf, F.Sy))/N)
            s_z.append(np.real(expectation_value(wf, F.Sz))/N)
        else:
            E_0.append(np.real(expectation_value(wf, H_0)))
            E_int.append(np.real(expectation_value(wf, H_int)))
            E_L.append(np.real(expectation_value(wf, H_L)))
        t.append(dt*k)
        squeezing.append(ssp)
        wf = Udt.dot(wf)
    end = time.time()
    print("Evolution:",end-start)
    axs[0].scatter(psi/np.pi,1/2, color=colors[i], marker='+')
    label = rf'$\varphi={phi_ref}+{(psi-psi0)/np.pi:.2f}\pi$'
    line1,  = axs[1].plot(t,squeezing, color=colors[i], linestyle=ls[i], label=label)
    
    if show_spins:
        s = (s_x, s_y, s_z)
        tag = ('x','y','z')
        if single_spins_per_axis:
            for k in range(len(s)):
                axs[2+k].plot(t, s[k], color = colors[i], linestyle=ls[i],label = label)
                axs[2+k].set_ylabel(rf'$S_{{{tag[k]}}}/N$')
                axs[2+k].legend()
        else:
            for k in range(len(s)):
                axs[2+i].plot(t, s[k], linestyle=ls[k],label = tag[k].upper())
            axs[2+i].legend()
            axs[2+i].set_ylabel(r'$S_{i}/N, $'+label)
    else:
        #axs[2].plot(t, E_0, color = colors[i], linestyle=ls[0],label = rf'$E_0, $'+label)
        #axs[2].plot(t, E_int, color = colors[i], linestyle=ls[1],label = rf'$E_{{int}}, $'+label)
        axs[2].plot(t, [E_0[j] + E_int[j] for j in range(len(t))], color = colors[i], linestyle=ls[1],label = rf'$ E_0 + E_{{int}}, $'+label)
        axs[2].plot(t, E_L, color = colors[i], linestyle=ls[2],label = rf'$E_L, $'+label)
        axs[3].plot(t, [E_0[j]+E_int[j]+E_L[j] for j in range(len(t))], color = colors[i], label=label)
        
        axs[2].set_ylabel('E')
        axs[3].set_ylabel('$E_{total}$')

axs[1].set_ylim((0.,1.5))
axs[1].set_ylabel(r'$\xi^2$')

axs[-1].set_xlabel('$t$')

#ax2.set_aspect(ax1.get_aspect())
for ax in axs[1:]:
    ax.sharex(axs[1])
    if ax != axs[-1]:
        plt.setp(ax.get_xticklabels(), visible=False)
    handles, labels = ax.get_legend_handles_labels()
    if len(handles) > 6:
        dots = ax.scatter([],[], alpha=0) 
        handles = handles[:7] + [dots]
        labels  = labels[:7] + ['...']
    ax.legend(handles, labels)

fig.savefig(filename)


if energy_distr:
    fig2, axs = plt.subplots(3,2)
    #fig2, axs = plt.subplots(2,2, figsize=(20,10))

    axs[0,0].set_title(r'$-\frac{\partial <H>}{\partial \theta}$')
    im = axs[0,0].contourf(X,Y,u)
    plt.colorbar(im, ax = axs[0,0])

    axs[0,1].set_title(r'$\frac{\partial <H>}{\partial \varphi}$')
    im = axs[0,1].contourf(X,Y,v)
    plt.colorbar(im, ax = axs[0,1])

    #axs[1,0].set_title(r'$dH = \frac{\partial <H>}{\partial \theta}d\theta + \frac{\partial <H>}{\partial \varphi} d\varphi$')
    #dx, dy = Psi[1]-Psi[0], Theta[1]-Theta[0]
    #im = axs[1,0].contourf(X,Y,-U*dy+V*dx)
    #plt.colorbar(im, ax = axs[1,0])

    axs[1,0].set_title('$<H>$')
    im = axs[1,0].contourf(X,Y,E)
    #im2 = axs[1,0].contour(X,Y,E_theo,colors='k')
    plt.colorbar(im, ax = axs[1,0])

    axs[1,1].streamplot(X,Y,u,v, color='blue', density=.3)
    axs[1,0].streamplot(X,Y,u,v, color='k', density=.3)
    steps = 3
    axs[1,1].quiver(X[::steps],Y[::steps],u[::steps],v[::steps], color='k')

    axs[2,0].set_title('$<H_int> + <H_0>$')
    E_int_0, u, v = energy_fluc(Psi, Theta, H_int + H_0, wf_psi_theta)
    im = axs[2,0].contourf(X,Y,E_int_0)
    plt.colorbar(im, ax = axs[2,0])

    axs[2,1].set_title('$<H_L>$')
    E_L, u, v = energy_fluc(Psi, Theta, H_L, wf_psi_theta)
    im = axs[2,1].contourf(X,Y,E_L)
    plt.colorbar(im, ax = axs[2,1])

    for ax in axs.flatten():
        ax.set_aspect(1)
    
    for i,phi in enumerate(Phi):
        for j in range(2):
            axs[1,j].scatter(phi/np.pi,1/2, color=colors[i], marker='+')

    fig2.tight_layout()

    fig2.savefig(filename.replace('Q function','Energy'))

plt.show()
