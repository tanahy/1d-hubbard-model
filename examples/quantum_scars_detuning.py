from scipy.linalg import eig 

from hubbard.hubbard import *

N = 10
bc = 'periodic'
#bc = 'open'
stat = 'Fermi-single-ocupancy'#'Fermi'
#stat = 'Fermi'

J       = .1
U       = 1
J_SE    = 4*J**2/U
phi_list = np.pi*np.array([1,49/50, 1/2,1/50,0])
Omega_list   = J_SE*np.linspace(.1,2,4)
C_SE    = 4*J**2*U/(U**2-Omega_list**2)

F = Hubbard_SW(N=N,M=N,stat=stat,bc=bc)
F.change_frame('lab', phi=phi_list[0])

E0 = J_SE

S2 = F.Sx*F.Sx+F.Sy*F.Sy+F.Sz*F.Sz

J_max = N/2

quant_axis = 'z'
init_axis = 'x'
rot_axis = 'y'
CSS = expm(-1j*np.pi/2*eval(f'F.S{rot_axis}')).dot(F.initial_state_fermi())

def plot_col(axs, col, H, title, ToS=False): 
    w, v = eig(H.toarray(), check_finite=False)
    w = w.real
    Sz_list = []
    Sz2_list = []
    S2_list = []
    fidelity = []
    for j in range(len(w)):
        wf = v[:,j]
        #print(w[j]/E0,(wf/max(wf)).real)
        Sz = expectation_value(wf, F.Sz).real
        Sz2 = expectation_value(wf, F.Sz*F.Sz).real
        J2 = expectation_value(wf, S2).real/(J_max**2+J_max)
        f = abs(np.conj(wf).dot(CSS))
        Sz_list.append(Sz)
        Sz2_list.append(Sz2)
        S2_list.append(J2)
        fidelity.append(f)

    S2_list = np.array(S2_list)
    Sz_list = np.array(Sz_list)
    Sz2_list = np.array(Sz2_list)
    fidelity = np.array(fidelity)

    mask_TOS  = S2_list >= sorted(S2_list)[-(N+1)]
    mask_rest = np.invert(mask_TOS)
    axs[0,col].set_title(title)
    if ToS:
        mask_range = (mask_TOS,)# mask_rest)
        #x = Sz2_list[mask_TOS]
        #axs[0,col].set_xlim(x.min(), x.max())
    else:
        mask_range = (mask_TOS, mask_rest)
    
    for mask in mask_range:
        if ToS:
            d = 4#1 if  col == 0 else 3
            y = w[mask]/E0
            x1 = Sz2_list[mask]
            x2 = S2_list[mask]
            p1 = np.polyfit(y, x1, d)
            p2 = np.polyfit(y, x2, d)
            yn  = np.linspace(y.min(),y.max(),20)
            x1n = np.polyval(p1, yn)
            x2n = np.polyval(p2, yn)
            axs[0,col].plot(x1n, yn, c='orange')
            axs[0,col].scatter(x1, y)
            axs[1,col].plot(x2n, yn, c='orange')
            axs[1,col].scatter(x2, y)
        else:
            #axs[0,i].scatter(Sz2_list[mask], w[mask]/E0)
            #axs[1,i].scatter(w[mask]/E0, S2_list[mask])
            #axs[1,i].scatter(S2_list[mask], w[mask]/E0)
            axs[0,col].scatter(Sz2_list[mask], w[mask]/E0)
            axs[1,col].scatter(S2_list[mask], w[mask]/E0)

        axs[2,col].scatter(Sz_list[mask], fidelity[mask]) 
        axs[3,col].scatter(S2_list[mask], fidelity[mask]) 

    axs[0,col].set_xlabel('$J_'+quant_axis+'^2$')
    axs[1,col].set_xlabel('$J^2/(J_{max}^2+J_{max})$')
    axs[2,col].set_xlabel('$J_z$')
    axs[3,col].set_xlabel('$<J^2>/(J_{max}^2+J_{max})$')

    #axs[1,0].set_ylabel('$J^2/(J_{max}^2+J_{max})$')
    #axs[1,i].set_xlabel('$E/E_0$')
    axs[0,0].set_ylabel('$E/E_0$')
    axs[1,0].set_ylabel('$E/E_0$')
    axs[2,0].set_ylabel('$|<n|{CSS}_'+init_axis+'>|$')
    axs[3,0].set_ylabel('$|<n|{CSS}_'+init_axis+'>|$')


fig, axs = plt.subplots(4,len(Omega_list), figsize=(3*len(Omega_list), 4*4), sharey='row')
for i, Omega in enumerate(Omega_list):
    H_eff = -F.H_SE(C_SE[i],Omega)
    title = rf'$\Omega={Omega/J_SE:.2f}J_{{SE}}$'
    plot_col(axs, i, H_eff, title)
axs[0,0].set_ylim(top=0)

fig2, axs = plt.subplots(4,len(phi_list), figsize=(3*len(phi_list), 4*4), sharey='row')
Omega = Omega_list[0]
for i, phi in enumerate(phi_list):
    F.change_frame('lab', phi=phi)
    H_eff = -F.H_eff_T(J, U, Omega, beta=0)#np.pi/2)
    title = rf'$\phi={phi/np.pi:.2f}\pi$'
    plot_col(axs, i, H_eff, title)
axs[0,0].set_ylim(top=-2)

fig3, axs = plt.subplots(4,len(phi_list), figsize=(3*len(phi_list), 4*4))#, sharey='row')
Omega = Omega_list[0]
for i, phi in enumerate(phi_list):
    F.change_frame('lab', phi=phi)
    H_eff = -F.H_eff_T(J, U, Omega, beta=0)#np.pi/2)
    title = rf'$\phi={phi/np.pi:.2f}\pi$'
    plot_col(axs, i, H_eff, title, ToS=True)
#axs[1,0].set_xlim(left=.975)

#fig.savefig(f'output/quantum_scars_detuning,{stat},N={N},J={J},len(Omega)={len(Omega_list)},phi={phi_list[0]:.2f}.pdf') 
#fig2.savefig(f'output/quantum_scars_detuning,{stat},N={N},J={J},Omega={Omega:.3f},len(phi)={len(phi_list)}.pdf') 
#fig3.savefig(f'output/quantum_scars_detuning,{stat},N={N},J={J},Omega={Omega:.3f},len(phi)={len(phi_list)},ToS.pdf') 
plt.show()
