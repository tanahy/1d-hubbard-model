#!/usr/bin/env python
# coding: utf-8
import sys, os

from hubbard.hubbard import *
from hubbard.utilities import * 

N = M = 16
frac = 10#, 5
filling = .99
label = 0
path = 'output'
J = 1
U = 24.4*J

if len(sys.argv) > 1:
    N = M   = int(sys.argv[1])
    J       = float(sys.argv[2])
    U       = float(sys.argv[3])
    frac    = float(sys.argv[4])
    filling = float(sys.argv[5])
    label   = sys.argv[6]
    path    = sys.argv[7]

t_J = True if 't-J' in sys.argv else False
J_SE = 4*J**2/U
U = U, U, U
q0 = M-1
E_gap = J_SE * (1-np.cos(np.pi/M*q0))
Omega = E_gap / frac

psi = 0
theta = np.pi/2
bc = 'open'

N_steps = 100

scheme = 'parallel'

model_fn_func = lambda N, M: f'{path}/models_saved/B2c_N={N}_M={M}.npz' 
H_fn_func     = lambda N, M: f'{path}/models_saved/B2c_N={N}_M={M},H_inhomo_J={J}_U={U[-1]}_frac={frac}.npz' 

filename = f'{path}/evs_N={N}_J={J}_U={U[-1]}_frac={frac}_filling={filling}_label={label}'
if t_J:
    H_fn_func = lambda N, M: f'{path}/models_saved/B2c_N={N}_M={M},H_tJ_inhomo_J={J}_U={U[-1]}_frac={frac}.npz' 
    filename  = f'{path}/evs_tJ_N={N}_J={J}_U={U[-1]}_frac={frac}_filling={filling}_label={label}'

j = np.arange(M)+1

q = np.arange(1, M)
beta = Omega * np.cos(np.pi/M*(j-.5)*q0)

Eq = J_SE*(np.cos(np.pi*q/M) - 1)
x, y = np.meshgrid(j,q)
p = np.cos(np.pi/N*(x-.5)*y)

cq = np.sqrt(2)/N*np.sum(p*beta, axis=1)
chi = 1/(N-1)*sum(abs(cq)**2/Eq)
#t_f = np.pi/abs(chi)
t_f = .5/abs(chi)

b = Bose2cHubbard(1, 2, bc = bc, basis = 'single')
b.M = M
b.set_scheme(scheme)
b.theta = theta
b.phi = psi

for n in range(1, N+1):
    model_file = model_fn_func(n,M)
    hamiltonian_file = H_fn_func(n,M)
    if not os.path.exists(hamiltonian_file):
        b.N = n
        if os.path.exists(model_file):
            b.load_data(model_file)
        else:
            del b.A, b.T
            b.basis()
            if hasattr(b, 'Sx'):
                del b.Sx, b.Sy, b.Sz
                del b.Sx_j, b.Sy_j, b.Sz_j
            b.S_j_calc()
            b.S_calc()

            b.save_data(model_file)
        if hasattr(b, 'H'):
            del b.H
        b.H = b.H_XXZ_inhomo(J, U, beta)
        if t_J:
            b.H += b.H_0(J)
        sparse.save_npz(hamiltonian_file, b.H)
        
realization = evolve_sampling_saved(b, M, filling, t_f, N_steps, model_fn_func, H_fn_func)
while True:
    N_real, t, evs = next(realization)
    np.savez(filename, N_realizations = N_real, t = t, evs = evs)
