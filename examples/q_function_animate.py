import sys
import time

import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.patches import Circle
from hubbard.hubbard import *
from hubbard.utilities import *
from hubbard.plot import *

###############################
N = 100
M = N

psi = 0#np.pi/2
theta = np.pi/2

res = 200
N_steps = 100

#J = 1 if U != 0 else 0
bc = 'periodic'
frame = 'lab'

save_anim = True 
ground_state = False
snapshot = True
chi = 1
#t_f = 5/(chi)*N**(-2/3) 
#t_snapshot = 1/chi*N**(-2/3)
#t_sq =  1 / chi * np.log(N) / N
#t_sq =  1 / chi * np.log(2*N) / N
t_sq =  1 / chi * np.log(N) / (2 * N)
t_f = 5 * t_sq
t_snapshot = t_sq
dt = t_f / N_steps

model = 'TACT'

###############################
filename = f'output/movies/Q function {model}, N={N}, M={M}, tf={t_f}, chi={chi}'
if ground_state:
    filename += ', Psi(0) = GS'

fig = plt.figure(constrained_layout=True, dpi=300)

gs = GridSpec(2,2, figure=fig)
ax1 = fig.add_subplot(gs[0, :])
ax2 = fig.add_subplot(gs[1,0])
ax3 = fig.add_subplot(gs[1,1])

start = time.time()
s = SpinModel(N,M)
s.S_calc()
if model == 'OAT':
    H = chi*(s.Sz**2)
elif model == 'TACT':
    H = chi*(s.Sz**2-s.Sy**2)
s.H = H

if ground_state:
    s.eigenvalues()
    wf0 = s.gs
else:
    wf0 = s.initial_state()
ref_state = wf0

initial_state = expm_multiply(-1j*s.Sz*psi, expm_multiply(-1j*s.Sy*theta, wf0))
cs_array = coherent_states(s, resolution = res, scheme = 'parallel')
q_function = Q_function_representation(s, dt, ax=ax1, resolution = res,
        generator = q_function_parallel_gen,
        cs_array = cs_array,
        ref_state = ref_state, initial_state = initial_state)
Psi, Theta = q_function.Psi, q_function.Theta
X,Y = q_function.X, q_function.Y
end = time.time()

print("Initialization:",end-start)

start = time.time()

t, data = s.evolve(initial_state, dt, t_f, 
        parameters = ['squeezing', 'Sx', 'Sy', 'Sz'], scheme = 'parallel')
squeezing = data[0,:]
s_x = data[1,:] / N
s_y = data[2,:] / N 
s_z = data[3,:] / N 

end = time.time()
print("Evolution:",end-start)

ax2.plot(t,squeezing)

s_label = ['x','y','z']
s = [s_x, s_y, s_z]
for i in range(3):
    ax3.plot(t,s[i], label=f'$S_{{{s_label[i]}}}$')

ax2.set_ylim((0.,1.5))
ax2.set_ylabel(r'$\xi^2$')
ax2.set_xlabel('$t$')
ax3.set_ylabel('$<S_i>/N$')
ax3.legend()
ax3.set_xlabel('$t$')

'''
#ax1.collections = []
start = time.time()
E,u,v = energy_fluc(Psi,Theta, s.H, wf_psi_theta)
speed = np.sqrt(u**2+v**2)
ax1.streamplot(X,Y,u,v,
    density=.6, color='black', linewidth=3*speed/np.max(speed))
#q_function.streamplot(M, Omega, phi)
end = time.time()
print("Stream calculations:",end-start)
'''

#ax1.scatter(psi/np.pi,1/2, marker='+', color='purple', zorder=2.5)
#ax1.scatter(psi/np.pi,1/2, marker='+', color='red', zorder=2.5)
   
vline1 = ax2.axvline(0, color='red', linestyle='dashed')
vline2 = ax3.axvline(0, color='red', linestyle='dashed')

def update(i):
    #ax1.collections = ax1.collections[:-1]
    #ax1.patches = []
    im, title = q_function.update(i)
    vline1.set_xdata(i*dt)
    vline2.set_xdata(i*dt)
    #stream = ax1.streamplot(q_function.X,q_function.Y,u_dt[i],v_dt[i],
    #        density=.6, color='red', linewidth=speed/np.max(speed))
    #return im, title, vline1, vline2, stream.lines, stream.arrows
    return im, title, vline1, vline2
    #return vline1, vline2

if snapshot:
    #im, title = q_function.update(0)
    update(0)
    steps_snapshot = int(t_snapshot/dt)
    for i in range(steps_snapshot):
        update(i)
    ax1.set_title("")
    #fig.suptitle(rf'$N={N}, J={J}, U={U}, \Omega={Omega}, \phi={phi/np.pi:.2f}\pi, \psi_0={psi/np.pi:.2f}\pi$')
    psi_label = '0' if psi == 0 else f'{psi/np.pi:.2f}\pi' 
    theta_label = '0' if theta == 0 else f'{theta/np.pi:.2f}\pi' 
    fig.suptitle(fr'{model}, N={N}, M={M}, $\left|\psi(t=0)\right> = \left|\theta = {theta_label}, \phi = {psi_label}\right>$')
    timestap = f' (t={dt*steps_snapshot:.1f})'
    fig.savefig(filename.replace('movies','portraits')+timestap+'.png', dpi = 300)
    steps = range(steps_snapshot, N_steps)
else:
    steps = range(N_steps)

if save_anim:
    start = time.time() 
    ani = animation.FuncAnimation(fig, update, steps, blit=True)
    #ani.save(filename+'.mp4',dpi=800)
    ani.save(filename+'.mp4')
    #Writer = animation.writers['ffmpeg']
    #writer = Writer(fps=15, metadata=dict(artist='Tanausu Hdez'), bitrate=1800)
    #ani.save(filename,writer=writer, dpi=800)
    end = time.time()
    print("Animation:",end-start)

#plt.show()
