import sys,os

import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from mpl_toolkits.mplot3d.axes3d import Axes3D

from hubbard.hubbard import *
from hubbard.utilities import coherent_states, q_function_calc
from hubbard.plot import *
from hubbard.bloch import *

###############################

if len(sys.argv) == 3:
    N = int(sys.argv[1])
    M = N
    bc = str(sys.argv[2])
elif len(sys.argv) == 4:
    N = int(sys.argv[1])
    M = int(sys.argv[2])
    bc = str(sys.argv[3])
else:
    N = 8
    M = N
    bc = 'ring'


time_scale = 7e-6 #[hbar/ER] = 7 micro seconds
ER = 1
r = 1/10
U = 1/ER
J = r*U
Omega = r**3*U

N_steps = 100

res = 100

light_calc = False
scheme = 'parallel'
calc_all_q = False

initial_states = 'x','y','z'
initial_states_coords = pi/2 * np.array(((1,0), (1,1), (0,0)))
#e_inv = 4#10
#N=4
epsilon = 0.279774
#N=6
#epsilon = 0.275856
#N=inf
#epsilon=0.272832566778296#1/e_inv
epsilon_2 = 0.01

phi_list = [2*pi/N, pi]#, pi*(1+epsilon), pi*(1+epsilon_2)]
phi_labels = fr'\frac{{\pi}}{{ {N//2} }}', '\pi'#, f'{{ {1+epsilon:.3f} }}\pi', f'{{ {1+epsilon_2:.3f} }}\pi' 
#fr'\frac{{ {e_inv+1} }}{{ {e_inv} }}\pi'
#fr'\pi(1+\epsilon)=\frac{{ {e_inv+1} }}{{ {e_inv} }}\pi'
phi_types = 'non_pi', 'pi', 'TACT_2', 'TACT_2'

cols = len(initial_states)
rows = len(phi_list)

fig_scale = 1
plt.rcParams["text.usetex"] = True
plt.rcParams["font.family"] = 'serif'
plt.rcParams["font.serif"] = 'Computer Modern Roman'
plt.rcParams.update({'font.size': fig_scale*10})
width = 7.067 * fig_scale
figsize = width, width * rows/cols
aspect_ratio = figsize[0]/figsize[1] * rows / cols

fig, axs = plt.subplots(rows, cols, 
        figsize = figsize,
        constrained_layout=True,
        sharey = 'row'
        )

connect_styles = 'zoom', 'arrow_box', 'arrow', 'debug', 'none'
connect_style = connect_styles[4]

filename = f'output/portraits/OAT (xyz) N={N}, M={M}, U={U}, J={J}, Omega={Omega}, {bc}, style={connect_style}'

#'dashed', 'dotted', 'dashed', 'dashdot', 'dotted','-', '--', '-.', ':', 'None', ' ', ''
#ls = 'solid', 'None', 'None' 
#markers = ',', 'o', 's'
ls = 'solid', 'dashed', 'dotted' 
markers = ',', ',', ','
lw = [1, 1, 1] 
lw = [val * fig_scale for val in lw]
#colors = 'tab:gray', 'tab:blue', 'tab:orange', 'tab:green', 'tab:red'
colors = 'tab:blue', 'tab:orange', 'tab:green', 'tab:red'


###############################

x_scale = .4
if N > 10:
    calc_all_q = False

def chi_calc(phi):
    J_SE = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*np.cos(phi/2)**2)
    J_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*np.sin(phi/2)**2)
    E_phi = J_SE*(1-np.cos(phi))

    chi = J_SOC**2/(E_phi*(N-1))
    if phi != pi:
        chi /= 2

    return chi

for i, phi in enumerate(phi_list):
    phi_type = phi_types[i]

    J_SE = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*np.cos(phi/2)**2)
    J_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*np.sin(phi/2)**2)
    E_phi = J_SE*(1-np.cos(phi))

    xi = J_SOC**2/(E_phi*(N-1))
    if phi_type == 'non_pi':
        xi /= 2
    elif phi_type == 'TACT_2':
        J_SE = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*np.cos(pi/2)**2)
        J_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*np.sin(pi/2)**2)
        E_phi = J_SE*(1-np.cos(pi))
        xi = J_SOC**2/(E_phi*(N-1))
        xi /= 3

    t_f = 1/xi #* 3/5
    
    t_f_scaled = t_f * time_scale
    #power = int(np.floor(np.log10(t_f_scaled)))
    #val   = round(t_f_scaled / (10 ** power), 1)
    #t_f_scaled = val*10**power
    #t_f_label  = f'${val}\cdot 10^{power}$'
    dt = t_f / N_steps
    
    for j in range(len(initial_states)):
        theta, psi = initial_states_coords[j]
        ax = axs[i,j]
        datafile = (f'output/data/OAT N={N}, U={U}, J={J}, '
                +f'Omega={Omega}, ini={initial_states[j]}, phi={phi/pi:.4f}pi, bc={bc}.npz')
        #datafile = (f'output/data/OAT N={N}, M={M}, U={U}, J={J}, '
        #        +f'Omega={Omega}, ini={initial_states[j]}, phi={phi/pi:.4f}pi, bc={bc}.npz')
        data_loaded = False

        try:
            df = np.load(datafile)
            data_loaded = True
            
            t = df['t']
            phi_file = df['phi']
            t_best, xi_best = df['xy_best']
            data = df['data'] 
            data_2D = df['data_2D']

            assert phi == phi_file

        except FileNotFoundError:
            if light_calc:
                f = Hubbard(N,M,'Fermi-single-ocupancy',bc=bc)
            else:
                f = Hubbard(N,M,'Fermi',bc=bc)
            f.change_frame('lab', phi=phi)
            saved_arrays = dict()

            wf0 = f.initial_state_fermi()
            ref_state = wf0
            #initial_state = expm(-1j*f.Sz*psi).dot(expm(-1j*f.Sy*theta).dot(wf0))
            rot_z = expm_multiply_parallel(f.Sz, a=-1j*psi)
            rot_y = expm_multiply_parallel(f.Sy, a=-1j*theta)
            initial_state = rot_z.dot(rot_y.dot(wf0))

            if phi_type == 'pi':
                H0 = -xi*f.Sx**2
            elif phi_type == 'non_pi':
                H0 = xi*f.Sz**2
            elif phi_type == 'TACT_2':
                H0 = xi*(f.Sz**2-f.Sx**2)

            H1 = f.H_eff(J,U,Omega)
            H = [H0, H1]

            if not light_calc:
                H2 = f.H_0(J) + f.H_int(U) + f.H_SOC(Omega) #f.H_L(Omega)             
                H += [H2]

            data = []
            data_2D = []
            if calc_all_q:
                cs_array = coherent_states(f, resolution= res, ref_state = ref_state, scheme = scheme)
                for H_i in H:
                    f.H = H_i
                    t, data_i, data_2D_i = f.evolve(initial_state, dt, t_f+2*dt, 
                            scheme = scheme, cs_array = cs_array)
                    data.append(data_i)
                    data_2D.append(data_2D_i)
                data = np.vstack(data)
                data_2D = np.stack(data_2D, axis=3)
            else:
                for H_i in H:
                    f.H = H_i
                    t, data_i = f.evolve(initial_state, dt, t_f+2*dt, 
                            scheme = scheme)
                    data.append(data_i)
                data = np.vstack(data)
            
            ind = list(data[0,:]).index(data[0,:].min())
            t_best = t[ind]
            xi_best = data[-1,:][ind]

            if not calc_all_q:
                data_2D = q_function_calc(f, t_best, initial_state=initial_state,
                        cs_array = False,
                        scheme = scheme,
                        resolution = res, 
                        ref_state = ref_state)

            saved_arrays['t']      = t 
            saved_arrays['phi']      = phi
            saved_arrays['xy_best'] = np.array((t_best, xi_best))
            saved_arrays['data']   = data
            saved_arrays['data_2D'] = data_2D
            np.savez(datafile, **saved_arrays)
  
        chi = chi_calc(phi)
        
        t = time_scale*t
        t_best = time_scale*t_best
        ax.set_xlim((0, t[-1]*x_scale))

        model_label = ''
        chi_labels = r'','', r''
        if phi_type == 'pi':
            model_label = '$-\chi_\pi\hat{{S}}_x^2$'
        elif phi_type == 'non_pi':
            model_label = r'$\frac{\chi_\phi}{2}\hat{{S}}_z^2$'
        elif phi_type == 'TACT_2':
            model_label = r'$\frac{\chi_\pi}{3}\left(\hat{{S}}_z^2-\hat{{S}}_x^2\right)$'

        H_labels = model_label, '$\hat{H}_{eff}$', '$\hat{H}_{FH}+\hat{H}_{SOC}$'

        if data[0,:].std() < 1e-3:
            #ind = (len(t) * 3) // 4 - 1
            ind = int(len(t) * x_scale ) // 2 - 1
            t_best = t[ind]
            xi_best = data[-1,:][ind]

        ind = list(t).index(t_best)
        step = [j for j in range(2,7) if (ind % j) == 0]
        step = step[-1] if len(step) > 0 else 3
        steps = 1, step, step
        steps = 1, 2, 2
        steps = 1, 1, 1
        for k in range(data.shape[0]): 
            ax.plot(t[::steps[k]], data[k,::steps[k]], 
                    #zorder = -k,
                    ls = ls[k], 
                    lw = lw[k], 
                    color = colors[k], 
                    marker = markers[k], 
                    label = H_labels[k])
        
        ax.set_ylim((0, 1.2))

        theta_label = '0' if theta == 0 else fr'\frac{{\pi}}{{ {pi/theta:n} }}' 
        psi_label = '0' if psi == 0 else fr'\frac{{\pi}}{{ {pi/psi:n} }}' 
        #ini_label_bloch = fr'\|\Psi_0\rangle = \|\theta={theta_label},\phi={psi_label} \rangle'
        #ini_label_bloch = fr'\left|\theta, \varphi\right\rangle = \left|{theta_label},{psi_label} \right\rangle'
        ini_label_bloch = fr'\left|\theta={theta_label},\varphi={psi_label} \right\rangle'
        #ax.set_title(f'$\phi={phi_labels[i]}\qquad {ini_label_bloch}$')
        if i == 0:
            ax.set_title(f'${ini_label_bloch}$')
        if j == 0:
            ax.set_ylabel(r'$\xi^2$')
        
        #t_f *= 3/5

        if i == rows-1:
            ax.set_xlabel('$t\ [s]$')
        #ax.set_xticks([0, t_best, t_f])
        #ax.set_xticklabels(['0', r'$t_{best}$', t_f_label])
        #ax.set_xticklabels(['0', r'$t_{best}$', f'{t_f_scaled:.2f}'])
        #ax.legend(loc = 'lower right')
        tags = ['a','b','c', 'd', 'e', 'f']
        ax.text(.05, .95, f'({tags[cols*i+j]})', transform=ax.transAxes, va='top')
        
        '''
        h = .45/rows
        w = .45/cols
        #xy_axin = [(i+.33)/rows, 1-(j+.5)/cols]
        xy_axin = [(j)/cols, 1-(i+1)/rows]
        w_box = .90*w
        h_box = .90*h
        xy_axin[0] += .13/cols
        xy_axin[1] += .48/rows
        #xy_frame = [xy_axin[0]+(w-w_box)/2 - 0.005, xy_axin[1]+(h-h_box)/2 + .02]
        xy_frame = [xy_axin[0]+(w-w_box)/2,#- 0.005, 
                    xy_axin[1]+(h-h_box)/2]#+ .02]
        
        w_small = dt*step
        h_small = 1.5/N_steps*step
        xy_small = [t_best - w_small/2, xi_best - h_small/2]
        ''' 
        
        #side = .45
        side = .60
        h = side / rows * aspect_ratio
        w = side / cols
        #xy_axin = [(i+.33)/rows, 1-(j+.5)/cols]
        xy_axin = [(j)/cols, 1-(i+1)/rows]
        #xy_axin = [(i)*b, 1-(j+1)*a]
        w_box = .95*w
        h_box = .95*h
        dx = .050 
        dy = .080
        x0 = .18 / cols
        y0 = -.13
        y0 = (.20+y0) / rows if xi_best < .8 else (.0+y0) / rows
        x_axin = dx + x0 + (1-dx) * j/cols 
        y_axin = 1 + dy + y0 - (1-dy) * (i+1)/rows
        xy_axin = [x_axin, y_axin]
        #xy_frame = [xy_axin[0]+(w-w_box)/2 - 0.005, xy_axin[1]+(h-h_box)/2 + .02]
        xy_frame = [xy_axin[0]+(w-w_box)/2,#- 0.005, 
                    xy_axin[1]+(h-h_box)/2]#+ .02]
        
        w_small = t[-1]/len(t)*step
        h_small = 1.5/len(t)*step
        xy_small = [t_best - w_small/2, xi_best - h_small/2]
         
        if connect_style == 'debug':
            axin_area = mpatches.Rectangle(xy_axin, w, h, 
                                        fill = False, edgecolor = 'red')
            fig.add_artist(axin_area)

            frame = mpatches.Rectangle(xy_frame, w_box, h_box, 
                                        fill = True,
                                        alpha = .5,
                                        #zorder=100,
                                        edgecolor = 'blue')
            fig.add_artist(frame)

            frame_small = mpatches.Rectangle(xy_small, w_small, h_small, 
                                        fill = False, 
                                        zorder = 10,
                                        edgecolor = 'green')
            ax.add_artist(frame_small)

            arrowprops = dict(arrowstyle="<->",
                                    patchA=frame,
                                    patchB=frame_small,
                                    shrinkA=0,
                                    shrinkB=0,
                                    )
            ax.annotate("",
                    xy=(t_best, xi_best), xycoords='data',
                    xytext=(xy_axin[0]+w/2, xy_axin[1]+h/2), textcoords='figure fraction',
                    arrowprops=arrowprops)

        elif connect_style == 'zoom':
            #axin = ax.inset_axes([.5, .1, .3, .3], projection='3d', azim = 30)
            #sq = mpatches.Rectangle((rect[0], rect[1]), rect[2], rect[3], 
            #                            fill = False, edgecolor = 'black')
            #fig.add_artist(sq)
            frame = mpatches.Rectangle(xy_frame, w_box, h_box, 
                                        fill = False, 
                                        #zorder=100,
                                        edgecolor = 'black')
            fig.add_artist(frame)

            frame_small = mpatches.Rectangle(xy_small, w_small, h_small, 
                                        fill = False, 
                                        zorder = 10,
                                        edgecolor = 'black')
            ax.add_artist(frame_small)
            arrowprops = dict(arrowstyle="-",
                                    patchA=frame,
                                    patchB=frame_small,
                                    shrinkA=0,
                                    shrinkB=0,
                                    )
            ax.annotate("",
                    xy=(xy_small[0], xy_small[1]+h_small), xycoords='data',
                    xytext=xy_frame, textcoords='figure fraction',
                    arrowprops=arrowprops)
            ax.annotate("",
                    xy=(xy_small[0]+w_small, xy_small[1]+h_small), xycoords='data',
                    xytext=(xy_frame[0]+w_box, xy_frame[1]), textcoords='figure fraction',
                    arrowprops=arrowprops)
        elif connect_style == 'arrow_box':
            frame = mpatches.Rectangle(xy_frame, w_box, h_box, 
                                        fill = True,
                                        alpha = .5,
                                        facecolor = 'white',
                                        edgecolor = 'black',
                                        #zorder = 5,
                                        )
            fig.add_artist(frame)
            arrowprops = dict(arrowstyle="simple",
                                    patchA=frame,
                                    #patchB=frame_small,
                                    color="0.5",
                                    shrinkA=1,
                                    shrinkB=5,
                                    )
            ax.annotate("",
                    xy=(t_best, xi_best), xycoords='data',
                    xytext=(xy_axin[0]+w/2, xy_axin[1]+h/2), textcoords='figure fraction',
                    arrowprops=arrowprops)

        elif connect_style == 'arrow':
            ax.annotate("",
                    xy=(t_best, xi_best), xycoords='data',
                    xytext=(xy_axin[0]+w/2, xy_frame[1]), textcoords='figure fraction',
                    arrowprops=dict(arrowstyle="fancy",
                                    color="0.5",
                                    #patchB=el,
                                    shrinkA=0,
                                    shrinkB=8,
                                    connectionstyle="arc3,rad=0.1",
                                    ),
                    )

        axin = fig.add_axes(xy_axin+[w, h], anchor='SW', projection='3d', azim = 45, zorder=6)
        #axin = Axes3D(fig, xy_axin+[w, h], anchor='SW', azim = 45, zorder=6)
        #ax.add_artist(axin)
        axin.view_init(90-theta*180/np.pi, psi*180/np.pi)
        #axin.view_init(20, 80)
        axin.patch.set_alpha(0.)
        axes_style = dict(span = .25,#0,# 
                margin = 0.1,#.25,#
                fontsize = 6,
                color = 'red')
        b = Bloch_Sphere(alpha = .1, ax = axin, 
                #cmap = cm.inferno, 
                cmap = cm.jet, 
                #cmap = cm.hot, 
                axes_style = axes_style
                )
        if len(data_2D.shape) > 2:
            print(data_2D.shape)
            b.plot_surface(data_2D[ind,:,:,-1])
        else:
            b.plot_surface(data_2D)

fig.savefig(filename+'.png', dpi = 300)
fig.savefig(filename+'.pdf')
#plt.show()
