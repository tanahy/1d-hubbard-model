from matplotlib.gridspec import GridSpec
from matplotlib.colors import TwoSlopeNorm, Normalize
import matplotlib.cm as cm
import matplotlib.tri as tri
from matplotlib import ticker
import matplotlib.pyplot as plt

from hubbard.hubbard import *
from hubbard.utilities import * 
from hubbard.plot import radar_factory, corr_plot 

plt.rcParams["text.usetex"] = True
plt.rcParams["font.family"] = 'serif'
plt.rcParams["font.serif"] = 'Computer Modern Roman'

N = 8
M = N

ER = 1# [hbar/Er] = 7e-6

U = ER
J = 1/10 * U
x = 2*np.pi/N*np.arange(1,N)
x_labels = [fr'$\frac{{ {2*k} }}{{ {N} }}\pi$' if 2*k != N else '$\pi$' for k in range(1,N)]
#For N=6
if N == 6:
    y = np.linspace(1/20, 0.25, 50)
elif N == 8: #For N=8
    y = np.linspace(.01, 0.33, 50)
else: #For N=10
    y = np.linspace(.01, 0.5, 50)

psi = np.pi/2
theta = np.pi/2
#bc = 'open'
#bc = 'periodic'
bc = 'ring'
frame = 'lab'

only_squeezing = True
TACT = 0
annotate_plots = 1
save = True

N_steps = 100

projection = 'polar'#'radar' #'None', 'polar'
style = 'bar' #'bar'#'pcolormesh'#'contourf'

l_axin = .3
ax_pad = .2

coords =[ 
        [ 0,  0],
        [ 1, 16],
        [ N//2-1, -3],
        [ N//2, -15],
        [-2, -20],
        #[-1,  4],
        [-1,  -6],
        ]

time_scale = N_steps // 2

scheme = 'parallel'

############################################################

radar = radar_factory(N, frame='circle')

def corr2(data, lim):
    shape = list(data.shape[:-1])
    shape[0] -= 1
    corr   = np.empty(shape)
    for k in range(data.shape[1]):
        for i in range(data.shape[2]):
            for j in range(data.shape[3]):
                #m = np.vstack((data[0, k, j, i, :N_steps], data[l, k, j, i, :N_steps]))
                m = data[:, k, i, j, :lim]
                corr_k = np.corrcoef(m)
                corr[:,k,i,j] = corr_k[0,1:]

    return corr**2

model_label = 'OAT'
if TACT:
    model_label = 'TACT'
    if 2 * (N // 2) == N:
        x = np.delete(x, N // 2 - 1)
        x_labels.pop(N // 2 - 1)

subplot_kw = dict(projection = projection)
if projection == 'polar':
    subplot_kw = {
            'projection': projection,
            'rlabel_position': 0,
            'theta_zero_location': 'E',
            }

f = Hubbard(N, M, 'Fermi', bc = bc)
f.set_scheme(scheme)
f.change_frame(frame, phi = 0)

title = fr"$N={N}, M={M}, U={U}, J={J}$"
title_rf = title.replace("$","").replace("\\", "")
filename = (fr'output/corr for spin-eff, {title_rf}, ({frame} {bc}),'
        + f' init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')
datafile = filename.replace('output/', 'output/data/') + '.npz'

filename += f'style=({projection}, {style})'
si_tags = ['sx','sy','sz','sx2','sy2','sz2','sxsy','sysz','szsx']

ax_labels = [f'{val.replace("s","S_").replace("2","^2")}' for val in si_tags]
ax_labels.append(r'\xi^2')

fig_w = 5*(1 + 3*(1-only_squeezing) + 3*l_axin*annotate_plots)
fig_h = 5*(1 + 3*(1-only_squeezing))
figsize = (fig_w, fig_h)

ax_labels = ax_labels[-1:]
parameters = ['squeezing']
try:
    df = np.load(datafile)
    data = df['data']

except FileNotFoundError:
    print('No data file found, computing results...')
    f.change_frame(frame, phi = 0)

    #wf = f.initial_state_fermi()
    #wf = expm(-1j*Oy*theta).dot(wf)
    #wf = expm(-1j*Oz*psi).dot(wf)
    
    ref_state = f.initial_state_fermi()
    rot_z = expm_multiply_parallel(f.Sz, a=-1j*psi)
    rot_y = expm_multiply_parallel(f.Sy, a=-1j*theta)
    wf = rot_z.dot(rot_y.dot(ref_state))
    data   = np.empty((2, len(parameters), len(y), len(x), N_steps))

    for i, phi in enumerate(x):
        for j, r in enumerate(y):
            f.phi = phi
            Omega = r*8*(J/U)**2*U 

            chi  = chi_calc(N, phi, U, J, Omega)

            t_f = 1/abs(chi)

            #H_FH    = f.H_0(J=J) + f.H_int(U=U) + f.H_L(Omega = Omega)
            H_eff   = f.H_eff(J=J,U=U,Omega=Omega) 
            H_model = -chi*f.Sx**2 if phi == np.pi else +chi*f.Sz**2

            H = [H_eff, H_model]
            
            for k in range(len(H)):
                f.H = H[k]
                _, result   = f.evolve(wf, t_f, num = N_steps, parameters=parameters)
                data[k, :, j, i, :] = result[:, :N_steps]


    data_dict = {
            'data': data,
            }
    np.savez(datafile, **data_dict)

corr = corr2(data, time_scale)

fig, ax, sample_axs = corr_plot(N, x, y, z = corr[0, 0, :, :], data = data[:, 0, :, :, :], 
        t_corr = time_scale, style = style, coords = coords, 
        figsize = figsize, subplot_kw = subplot_kw, step = 4)

dx = np.pi/N
L = 200
x_shade = np.linspace(x[0]-dx, x[-1]+dx, L)
y_lim = np.zeros(L)
lim = 0.25 if N == 6 else 0.33 if N == 8 else 0.5
for i in range(L):
    mask = x_shade[i] - dx <= x
    xi = x[mask][0]
    y_lim[i] = lim*(1-np.cos(xi))/2

shade = ax.fill_between(x_shade, y_lim, 1, alpha = .3,
        zorder = 0,
        label = fr'$J_{{\uparrow\downarrow}} \geq {lim} J_\mathrm{{SE}} (1-\cos\phi)$')

ax.legend(#handles=[shade],
        loc = 'lower left',
        bbox_to_anchor=(.55, -0.10),
        #bbox_transform=ax.transAxes,
        #frameon=False,
        )


ax.yaxis.set_major_formatter(ticker.PercentFormatter(xmax = 1, decimals = 0))
ax.set_xlabel('$\phi$')
ax.set_ylabel(r'$J_{\uparrow\downarrow} [2 J_\mathrm{SE}]$', labelpad = 10)
ax.yaxis.set_label_position("right")
ax.set_xticks(x, x_labels)
ax.set_ylim((0, max(y))) 
#ax.set_title(f'Correlation (${ax_labels[0]}$)')
#second_title = r', $\Omega = r 2 \frac{4J^2}{U}$'
#fig.suptitle(title+second_title+
#        fr', $\|\Psi_0\rangle = \|\theta={theta/np.pi:.2f}\pi,\phi={psi/np.pi:.2f}\pi \rangle  $')
    
if save:
    fig.savefig(f'{filename}.png')

#plt.show()
