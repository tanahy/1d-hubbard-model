from hubbard.utilities import *
from scipy.optimize import root
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

pi = np.pi
N = M = 100
J = 0.04*0.5
Omega = 0.004*J
#phi = np.pi-2*np.pi/M
phi = pi-1.5*np.pi/M

X, Y = np.meshgrid(np.arange(0, 2 * np.pi, .1), np.arange(0, np.pi, .1))

N, v_x, v_y, chi_x, chi_z, chi_xy = dot_theta_coeff(phi, N, M, J, Omega)
args = N, v_x, v_y, chi_x, chi_z, chi_xy

def dtheta(theta, psi, args):
    N, v_x, v_y, chi_x, chi_z, chi_xy = args
    return N/2*np.sin(theta) * ((v_x*np.sin(psi) - v_y*np.cos(psi)) 
                                + N*np.sin(theta)*(chi_x*np.sin(2*psi) + chi_xy * np.cos(2*psi))) 
def dpsi(theta, psi, args):
    N, v_x, v_y, chi_x, chi_z, chi_xy = args
    return N/2*np.cos(theta) * ((v_x*np.cos(psi) + v_y*np.sin(psi)) 
                                + N*np.sin(theta)*(chi_x + chi_z + chi_x*np.cos(2*psi) - chi_xy * np.sin(2*psi))) 

def df(x, args):
    theta, psi = x[1], x[0]
    #return np.array([dtheta(theta, psi), dpsi(theta, psi)])
    return [dpsi(theta, psi, args), dtheta(theta, psi, args)]

def jacobian(x):
    theta, psi = x[1], x[0]
    A = v_x*np.cos(psi) + v_y*np.sin(psi)
    B = v_x*np.sin(psi) - v_y*np.cos(psi)
    C = chi_x*np.cos(2*psi) - chi_xy * np.sin(2*psi)
    D = chi_x*np.sin(2*psi) + chi_xy * np.cos(2*psi)
    
    df1 = - N/2*np.sin(theta)*A + 2*(N/2)**2*np.cos(2*theta)*(chi_x+chi_z+C)
    df2 = - N/2*np.cos(theta)*(B - 4*(N/2)*np.sin(theta)*D)
    dg1 =   N/2*np.cos(theta)*B + 2*(N/2)**2*np.sin(2*theta)*D
    dg2 =   N/2*np.sin(theta)*(A + 2*(N/2)*np.sin(2*theta)*C)

    return np.array([[df1, df2], [dg1, dg2]])


U = dpsi(X, Y, args)
V = dtheta(X, Y, args)

tol = .1

fig1, ax1 = plt.subplots()

N_steps = 50
phi_array = np.linspace(2*np.pi*(M//2-2)/M, 2*np.pi*(M//2-1)/M, N_steps)

#q = ax1.quiver(X/pi, Y/pi, U, V, units='width')
mask = (abs(U) < tol*abs(U.max())) & (abs(V) < tol*abs(V.max()))
#q2 = ax1.quiver(X[mask]/pi, Y[mask]/pi, U[mask], V[mask], color = 'red', units='width')
print(type(U))
stream = ax1.streamplot(X/pi, Y/pi, U.real, V.real)

x0, y0 = [pi*3/2, pi*3/2, pi*3/2, pi*.24], [pi/5, pi/4, pi/4, pi/3]

dummy = np.zeros(X.flatten().shape)
points_1 = ax1.scatter(dummy, dummy)
points_2 = ax1.scatter(dummy, dummy, c = 'red')

def update(i):
    args = _, v_x, v_y, chi_x, chi_z, chi_xy = dot_theta_coeff(phi_array[i], N, M, J, Omega)
    U = dpsi(X, Y, args)
    V = dtheta(X, Y, args)
    
    #global q2
    #q2.remove()
    
    #q.set_UVC(U, V)
    mask = (abs(U) < tol*abs(U.max())) & (abs(V) < tol*abs(V.max()))
    #q2 = ax1.quiver(X[mask]/pi, Y[mask]/pi, U[mask], V[mask], color = 'red', units='width')
    global stream
    #stream.remove()
    stream = ax1.streamplot(X/pi, Y/pi, U.real, V.real)

    points_2.set_offsets((X[mask]/pi, Y[mask]/pi))
    print(X[mask].shape)
    fixed_points = []
    for z0 in zip(x0, y0):
        #sol = root(df, x0 = z0, jac = jacobian, method = 'broyden2')
        sol = root(df, x0 = z0, args = (args,),  method = 'hybr')
        sol.x[0] = sol.x[0].real % (2 * np.pi)
        sol.x[1] = sol.x[1].real % np.pi
        fixed_points.append(sol.x)

    psi_list = find_roots(dot_theta, args = args, method = 'secant', steps = 100)

    psi = np.array(psi_list).real/pi

    fixed_points = np.array(fixed_points)/pi
    #print(fixed_points)
    #print(psi)
    points_1.set_offsets((psi, [1/2 for _ in psi]))

    #ax1.scatter(fixed_points[:,0], fixed_points[:,1])

    #return q, q2, points_1
    #return q, points_1, points_2
    return stream, points_1, points_2

ani = animation.FuncAnimation(fig1, update, N_steps, blit=True)

plt.show()
