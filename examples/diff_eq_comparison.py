import sys

from scipy.integrate import solve_ivp
from matplotlib.gridspec import GridSpec

from hubbard.hubbard import *
from hubbard.utilities import * 

N = 6
alpha = 1
beta  = -1
if len(sys.argv) == 2:
    N = int(sys.argv[1])
elif len(sys.argv) == 3:
    N = int(sys.argv[1])
    beta = float(sys.argv[2])
elif len(sys.argv) >= 4:
    N = int(sys.argv[1])
    alpha = float(sys.argv[2])
    beta  = float(sys.argv[3])

norm = 1 #abs(alpha)+abs(beta)#np.sqrt(abs(alpha)**2 + abs(beta)**2)
scaling_N = 1
scaling_N = (np.log(N)/N)
if beta/alpha == -1:
    scaling_N = (np.log(N)/N)
elif beta == 0:
    scaling_N = N**(-2/3)
t_f = 1/norm*scaling_N
M = N

psi = np.pi/2
theta = np.pi/2
bc = 'periodic'
frame = 'lab'

#along y
y0 = (0, N/2, 0, N/4, N**2/4, N/4, 0, 0, 0)
#along x
#y0 = (N/2, 0, 0, N**2/4, N/4, N/4, 0, 0, 0)

only_squeezing = True#False
save = True

N_steps = 100
stats = 'Fermi'
#stats = 'Bose_spin'

si_tags = ['sx','sy','sz','sx2','sy2','sz2','sxsy','sysz','szsx']

calc_exact = True
if N > 10:
    calc_exact = False

parameters = si_tags + ['squeezing']
H = []
if calc_exact:
    f = Hubbard(N, M, stats, bc = bc)
    f.change_frame(frame, phi = 0)

    if frame == 'lab':
        spin_op = 'S'
        Ox = f.Sx
        Oy = f.Sy
        Oz = f.Sz
    elif frame == 'rotated':
        spin_op = 'J'
        Ox = f.Jx
        Oy = f.Jy
        Oz = f.Jz

    sx   = Ox
    sy   = Oy
    sz   = Oz
    sx2  = Ox * Ox
    sy2  = Oy * Oy
    sz2  = Oz * Oz
    sxsy = Ox * Oy + Oy * Ox
    szsx = Oz * Ox + Ox * Oz
    sysz = Oy * Oz + Oz * Oy

    si = [eval(val) for val in si_tags]
    parameters = si + ['Fisher', 'squeezing']
    parameters = si + ['squeezing']

    H_1 = 1/norm*(alpha * f.Sz**2 + beta * f.Sx**2) 

    H = [H_1]
    H_labels = ['exact']

ax_labels = [f'${val.replace("s","S_").replace("2","^2")}$' for val in si_tags]
#ax_labels.append(r'$F_Q$')
ax_labels.append(r'$\xi^2$')
if only_squeezing:
    ax_labels = ax_labels[-1:]
    parameters = parameters[-1:]

def ode_asym_TACT_0(t, y, a, b):
    x, y, z, x2, y2, z2, xy, yz, zx = y
    dx = -a*yz
    dy = (a-b)*zx
    dz = b*xy
    dx2 = -2*a*(zx*y+xy*z+yz*x-4*x*y*z)
    dy2 = 2*(a-b)*(zx*y+xy*z+yz*x-4*x*y*z)
    dz2 = 2*b*(zx*y+xy*z+yz*x-4*x*y*z)
    dxy = 4*(a-b)*(x2*z+zx*x-2*x**2*z) - 4*a*(y2*z+yz*y-2*y**2*z)
    dyz = 4*(a-b)*(z2*x+zx*z-2*z**2*x) + 4*b*(y2*x+xy*y-2*y**2*x)
    dzx = -4*a*(z2*y+yz*z-2*z**2*y) + 4*b*(x2*y+xy*x-2*x**2*y)

    return dx, dy, dz, dx2, dy2, dz2, dxy, dyz, dzx

def ode_asym_TACT_1(t, y, a, b):
    x, y, z, x2, y2, z2, xy, yz, zx = y
    dx = -a*yz
    dy = (a-b)*zx
    dz = b*xy
    dx2 = -2*a*zx*y
    dy2 = 2*(a-b)*zx*y
    dz2 = 2*b*zx*y
    dxy = -4*a*yz*y
    dyz = 4*b*xy*y
    dzx = -4*(a*z2*y-b*x2*y)

    return dx, dy, dz, dx2, dy2, dz2, dxy, dyz, dzx

def ode_asym_TACT_2(t, y, a, b):
    x, y, z, x2, y2, z2, xy, yz, zx = y
    dx = -0*yz
    dy = (a-b)*zx
    dz = -0*xy
    dx2 = -2*a*zx*y
    dy2 = 2*(a-b)*zx*y
    dz2 = 2*b*zx*y
    dxy = -0*yz*y
    dyz = -0*xy*y
    dzx = -4*(a*z2*y-b*x2*y)

    return dx, dy, dz, dx2, dy2, dz2, dxy, dyz, dzx

def Kajtoch_2015(t, y0):
    x0, y0, z0, x20, y20, z20, xy0, yz0, zx0 = y0
    y = y0 - (np.cosh(4*y0*t)-1)/(4*y0)
    squeezing = np.exp(-4*y0*t + (np.sinh(4*y0*t)-t)/(4*y0**2))
    return y, squeezing

def OAT_analytical(N, t, theta=np.pi/2, phi=np.pi/2, chi=1):
    #https://iopscience.iop.org/article/10.1088/1367-2630/11/7/073049/pdf
    S = N/2
    ones = np.ones(len(t))
    p = S*np.sin(theta)*np.exp(1j*phi)*(np.cos(chi*t)+1j*np.cos(theta)*np.sin(chi*t))**(2*S-1)
    z = S*np.cos(theta)*ones
    x = np.real(p)
    y = np.imag(p)
    z2 = (S/2 + S*(S-.5)*np.cos(theta)**2)*ones
    A = S/2*(S-.5)*(1-np.cos(2*chi*t)**(2*S-2))
    B = 2*S*(S-.5)*np.cos(chi*t)**(2*S-2)*np.sin(chi*t)
    C = A+S
    xi2 = (C-np.sqrt(A**2 + B**2))/S
    return x, y, z, z2, xi2

def Kitagawa_Ueda_1993(N, t, theta=np.pi/2, phi=np.pi/2, chi=1):
    S = N/2
    A = 1-np.cos(2*chi*t)**(2*S-2)
    B = 4*np.sin(chi*t)*np.cos(chi*t)**(2*S-2)
    Vm = .5*S*((1+.5*(S-.5)*A) - .5*(S-.5)*np.sqrt(A**2 + B**2))
    return 2*Vm/S

if only_squeezing:
    fig, ax = plt.subplots(1, constrained_layout=True)
    axs = [ax,]
else:
    fig = plt.figure(constrained_layout=True, figsize=(15,20))
    cols = 3
    rows = len(si_tags)//cols + 1
    gs = GridSpec(rows,cols, figure = fig)
    axs = []
    for i in range(rows-1):
        for j in range(cols):
            axs.append(fig.add_subplot(gs[i,j]))
    #axs.append(fig.add_subplot(gs[rows-1,:1]))
    axs.append(fig.add_subplot(gs[rows-1,:]))

lines = ['solid','dashed', 'dashdot']+12*['None']
markers = 3*['None']+['+', '^', 's', 'o', '*', 'x','<','>','1','2','3','4']

if calc_exact:
    wf = f.initial_state_fermi()
    wf = expm_multiply(-1j*Oy*theta, wf)
    wf = expm_multiply(-1j*Oz*psi, wf)

    for k in range(len(H)):
        wf2 = np.copy(wf)

        f.H = H[k]
        
        t, data = f.evolve(wf, t_f = t_f, num = N_steps, parameters = parameters)
        #t *= 1/t_f
        for i in range(len(parameters)):
            axs[i].plot(t, data[i], ls = lines[k], marker = markers[k], label = H_labels[k])
else:
    t = np.linspace(0,t_f, N_steps)

args = (alpha/norm, beta/norm)
args = (alpha, beta)
#y0 = np.array(y0, dtype = np.complex128)
funcs = (ode_asym_TACT_0, ode_asym_TACT_1, ode_asym_TACT_2,)
funcs = (ode_asym_TACT_0, ode_asym_TACT_2,)
funcs = (ode_asym_TACT_0,)
methods = 'RK45', 'RK23', 'DOP853', 'Radau', 'BDF', 'LSODA'
methods = 'RK45', 
#methods = 'DOP853', 

for j, method in enumerate(methods):
    for k, fun in enumerate(funcs):
        exp_values = [None for i in range(len(y0))]
        k = k + j + len(H)
        label = fun.__name__ + ', ' + method
        sol = solve_ivp(fun, t_span = (0, t_f), 
                y0 = y0, 
                method = method, 
                t_eval = t, 
                args = args)
        for i in range(len(y0)):
            exp_values[i] = sol.y[i]
            if not only_squeezing:
                axs[i].plot(sol.t, sol.y[i].real, ls = lines[k], marker = markers[k], label = label)

        y = spin_squeezing(N, exp_values)
        axs[-1].plot(sol.t, y, ls = lines[k], marker = markers[k], label = label)
        
        R = np.array([[1/2+sol.y[2]/N, (sol.y[0]+1j*sol.y[1])/N], [(sol.y[0]-1j*sol.y[1])/N, 1/2 - sol.y[2]/N]])

        ev = []
        for i in range(len(sol.t)):
            Ri = R[:,:,i]
            w,v = np.linalg.eigh(Ri)
            ev.append(w)
        ev = np.array(ev)
        axs[-1].plot(sol.t, ev[:,0], ls = lines[k], marker = markers[k], label = r'$\lambda_1$')
        axs[-1].plot(sol.t, ev[:,1], ls = lines[k], marker = markers[k], label = r'$\lambda_2$')



if beta/alpha == -1:
    y, xi2 = Kajtoch_2015(t, y0)
    label = 'Kajtoch, Witkowska 2015'
    if not only_squeezing:
        axs[1].plot(t, y.real, ls = lines[-1], marker = markers[-1], label = label)
    axs[-1].plot(t, xi2.real, ls = lines[-1], marker = markers[-1], label = label)
elif beta == 0:
    x, y, z, z2, xi2 = OAT_analytical(N, t, theta, psi, chi=alpha)
    label = 'Guang-Ri Jin 2009'
    if not only_squeezing:
        axs[0].plot(t, x, ls = lines[-1], marker = markers[-1], label = label)
        axs[1].plot(t, y, ls = lines[-1], marker = markers[-1], label = label)
        axs[2].plot(t, z, ls = lines[-1], marker = markers[-1], label = label)
        axs[5].plot(t, z2, ls = lines[-1], marker = markers[-1], label = label)
    axs[-1].plot(t, xi2, ls = lines[-1], marker = markers[-1], label = label)
    xi2 = Kitagawa_Ueda_1993(N, t, theta, psi, chi=alpha)
    label = Kitagawa_Ueda_1993.__name__
    axs[-1].plot(t, xi2, ls = lines[-2], marker = markers[-2], label = label)
    

'''
t_best, xi_best = calc_minima(t, data[-1])
mask = t == t_best
if not only_squeezing:
    for i in range(2):
        i = -(i+1)
        axs[i].axvline(t[mask], color = 'tab:grey', alpha = .5)
        axs[i].scatter(t[mask], data[i][mask])
        axs[i].text(t[mask], data[i][mask], f'{ax_labels[i]}={data[i][mask][0]}')
'''
for i,ax in enumerate(axs):
    ax.set_xlabel('$t$')
    ax.set_ylabel(ax_labels[i])
    '''
    if isinstance(parameters[i], str) and parameters[i] == 'Fisher':
        for k in range(1,N+1):
            s = N // k
            r = N - k * s
            y = s*k**2 + r**2
            ax.axhline(y, alpha = .33)
            ax.text(t[N_steps//10], y, f'k={k}')
    '''
axs[-1].legend()
#axs[-1].set_ylim((0,1.5))
#axs[-1].set_yscale('log')
#axs[-1].set_xscale('log')
axs[-1].set_ylim((1e-2,1.))
title = fr'$N={N}, M={M}, \alpha = {alpha}, \beta = {beta}$'
#title = fr'$N={N}, M={M}, \Omega = {Omega}, J= {J:.2f}, U = {U}$'

#fig.suptitle(title+fr', $\|\Psi_0\rangle = \|\theta={theta/np.pi:.2f}\pi,\phi={psi/np.pi:.2f}\pi \rangle  $')
if save:
    title = title.replace("\\","").replace('$',"")
    main_title = 'Spin Squeezing' if only_squeezing else 'Expectation values' 
    title = (f'output/ODE vs Exact, {main_title}, {title}, '
             + f'init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')
    plt.savefig(title+'.pdf')
plt.show()
