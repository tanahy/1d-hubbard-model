from hubbard.hubbard import *
from hubbard.utilities import * 

from memory_profiler import profile

N_range = np.arange(2,11) 

ER = 1# [hbar/Er] = 7e-6

r = 1/20

U = ER
J = r*U
Omega = r**3*U

phi = np.pi/3
#bc = 'open'
#bc = 'periodic'
bc = 'ring'
frame = 'lab'

save = True

N_steps = 100
dt      = 1e-3
############################################################

datafile = 'output/mem_HFH.txt'

#fig, ax = plt.subplots(1, constrained_layout=True)

#@profile
def system_gen(N):
    f = Hubbard(N, N, 'Fermi', bc = bc)
    f.change_frame(frame, phi = phi)

    H_FH    = f.H_0(J=J) + f.H_int(U=U) + f.H_SOC(Omega = Omega) 
    H_eff   = f.H_eff(J=J,U=U,Omega=Omega) 
    #H_model = -chi*Ox**2 if phi == np.pi else +chi*Oz**2
    f.H = H_FH
    wf = f.initial_state_fermi()
    wf = expm(-1j*f.Sy*np.pi/2).dot(wf)
    t, data = f.evolve(wf, dt, dt*N_steps) 
    del f
    del t
    del data
    del H_FH
    return None
'''
try:
    data = np.load(datafile)

except FileNotFoundError:
    print('No data file found, computing results...')
'''

if __name__ == '__main__':
    for N in N_range:
        print('N: ', N)
        system_gen(N)


#if save:
#    fig.savefig(datafile.replace('txt','pdf')

#plt.show()
