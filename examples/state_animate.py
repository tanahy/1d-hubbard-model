import sys
import time

import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.patches import Circle
from hubbard.hubbard import *
from hubbard.utilities import *
from hubbard.plot import *

###############################
N = 10
M = N

psi = 0#np.pi/2
theta = np.pi/2

res = 200
N_steps = 100

#J = 1 if U != 0 else 0
bc = 'periodic'
frame = 'lab'

save_anim = True 
ground_state = False
snapshot = False
model = 'OAT'
show_trace = True
chi = 1
#t_sq = 1/(chi)*N**(-2/3) 
#t_snapshot = 1/chi*N**(-2/3)
#t_sq =  1 / chi * np.log(N) / N
#t_sq =  1 / chi * np.log(2*N) / N
t_sq = 1/chi
if model == 'OAT':
    t_sq = 1/(chi)*N**(-2/3) 
elif model == 'TACT':
    t_sq =  1 / chi * np.log(N) / (2 * N)
t_f = 35 * t_sq
t_snapshot = t_sq
dt = t_f / N_steps


###############################
filename = f'output/movies/state evolution {model}, N={N}, M={M}, tf={t_f}, chi={chi}'
if ground_state:
    filename += ', Psi(0) = GS'

fig = plt.figure(constrained_layout=True, dpi=300)

gs = GridSpec(2,2, figure=fig)
ax1 = fig.add_subplot(gs[0,0])
ax2 = fig.add_subplot(gs[0,1])
ax3 = fig.add_subplot(gs[1,0])
ax4 = fig.add_subplot(gs[1,1])

start = time.time()
s = SpinModel(N,M)
s.S_calc()
if model == 'OAT':
    H = chi*(s.Sz**2)
elif model == 'TACT':
    #H = chi*(s.Sz**2-s.Sy**2)
    H = chi*(s.Sz**2-s.Sy**2)
s.H = H

if ground_state:
    s.eigenvalues()
    wf0 = s.gs
else:
    wf0 = s.initial_state()
ref_state = wf0

initial_state = expm_multiply(-1j*s.Sz*psi, expm_multiply(-1j*s.Sy*theta, wf0))
end = time.time()

print("Initialization:",end-start)

start = time.time()

t, (data, wf_array) = s.evolve(initial_state, dt, t_f, 
        parameters = ['squeezing', 'Sx', 'Sy', 'Sz'], scheme = 'parallel',
        save_wf = True)
squeezing = data[0,:]
s_x = data[1,:] / N
s_y = data[2,:] / N 
s_z = data[3,:] / N 

end = time.time()
print("Evolution:",end-start)

ax2.plot(t,squeezing)

ax2.set_ylim((0.,1.5))
ax2.set_ylabel(r'$\xi^2$')
ax2.set_xlabel('$t$')

r"""
s_label = ['x','y','z']
s_op = [s_x, s_y, s_z]
for i in range(3):
    ax3.plot(t,s_op[i], label=f'$S_{{{s_label[i]}}}$')

ax3.set_ylabel('$<S_i>/N$')
ax3.legend()
ax3.set_xlabel('$t$')
"""

'''
#ax1.collections = []
start = time.time()
E,u,v = energy_fluc(Psi,Theta, s.H, wf_psi_theta)
speed = np.sqrt(u**2+v**2)
ax1.streamplot(X,Y,u,v,
    density=.6, color='black', linewidth=3*speed/np.max(speed))
#q_function.streamplot(M, Omega, phi)
end = time.time()
print("Stream calculations:",end-start)
'''

#ax1.scatter(psi/np.pi,1/2, marker='+', color='purple', zorder=2.5)
#ax1.scatter(psi/np.pi,1/2, marker='+', color='red', zorder=2.5)
  
S_max = N / 2
scatter_size = [1 + abs(s.A[i][1]) / S_max for i in range(N+1)]
scatter_size = np.array(scatter_size)
state_coeff = ax1.scatter(wf0.real[:N+1], wf0.imag[:N+1], 
                          s = scatter_size*plt.rcParams['lines.markersize'] ** 2, 
                          #c = [s.A[i][0] / S_max for i in range(s.D)],
                          zorder = 5,
                          ) 

c_max = abs(wf_array).max()
ax1.set_xlim((-c_max, c_max))
ax1.set_ylim((-c_max, c_max))

state_labels = [ax1.text(wf0[i].real, wf0[i].imag, 
                         s = fr'$\|{s.A[i][0]:1.0f}, {s.A[i][1]:1.0f}\rangle$',
                         #s = '',
                         ) 
                for i in range(N+1)]

vline1 = ax2.axvline(0, color='red', linestyle='dashed')
m_array = -(np.arange(N+1) - S_max)

m_lines = [ax1.plot(val.real, val.imag)[0] for val in wf0[:N+1]]

line1, = ax3.plot(m_array, abs(wf0[:N+1])) 
line2, = ax4.plot(m_array, np.angle(wf0[:N+1])) 
#line2, = ax4.plot(m_array, np.arctan(wf0[:N+1].imag/wf0[:N+1].real)) 
#vline2 = ax3.axvline(0, color='red', linestyle='dashed')

ax3.set_ylim((0, 1))
ax4.set_ylim((-np.pi, np.pi))

#text_offset = state_labels[0].get_fontsize()
#print(text_offset)
tol = 1e-3
def update(i):
    #ax1.collections = ax1.collections[:-1]
    #ax1.patches = []
    re, im = wf_array[:N+1, i].real, wf_array[:N+1, i].imag
    state_coeff.set_offsets(np.c_[re, im])
    if show_trace:
        for m, line in enumerate(m_lines):
            line.set_data(wf_array[m, :i].real, wf_array[m, :i].imag)
    for j in range(len(state_labels)):
        state = state_labels[j]
        '''
        count = 0
        dx, dy = 0, 0
        for k in range(j):
            if abs(re[k]-re[j]) < tol and abs(im[j]-im[k]) < tol:
                dx += 0
                dy += 0#1
        state.set_position((re[j] + dx, im[j] + dy))
        '''
        state.set_position((re[j], im[j]))

    vline1.set_xdata(i*dt)
    line1.set_data(m_array, abs(wf_array[:N+1, i]))
    line2.set_data(m_array, np.angle(wf_array[:N+1, i]))

    #vline2.set_xdata(i*dt)
    #stream = ax1.streamplot(q_function.X,q_function.Y,u_dt[i],v_dt[i],
    #        density=.6, color='red', linewidth=speed/np.max(speed))
    #return im, title, vline1, vline2, stream.lines, stream.arrows
    return *state_labels, state_coeff, vline1, line1, line2, *m_lines
    #return vline1, vline2

if snapshot:
    #im, title = q_function.update(0)
    update(0)
    steps_snapshot = int(t_snapshot/dt)
    for i in range(steps_snapshot):
        update(i)
    ax1.set_title("")
    #fig.suptitle(rf'$N={N}, J={J}, U={U}, \Omega={Omega}, \phi={phi/np.pi:.2f}\pi, \psi_0={psi/np.pi:.2f}\pi$')
    psi_label = '0' if psi == 0 else f'{psi/np.pi:.2f}\pi' 
    theta_label = '0' if theta == 0 else f'{theta/np.pi:.2f}\pi' 
    fig.suptitle(fr'{model}, N={N}, M={M}, $\left|\psi(t=0)\right> = \left|\theta = {theta_label}, \phi = {psi_label}\right>$')
    timestap = f' (t={dt*steps_snapshot:.1f})'
    fig.savefig(filename.replace('movies','portraits')+timestap+'.png', dpi = 300)
    steps = range(steps_snapshot, N_steps)
else:
    steps = range(N_steps)

if save_anim:
    start = time.time() 
    ani = animation.FuncAnimation(fig, update, steps, blit=True)
    #ani.save(filename+'.mp4',dpi=800)
    ani.save(filename+'.mp4')
    #Writer = animation.writers['ffmpeg']
    #writer = Writer(fps=15, metadata=dict(artist='Tanausu Hdez'), bitrate=1800)
    #ani.save(filename,writer=writer, dpi=800)
    end = time.time()
    print("Animation:",end-start)

#plt.show()
