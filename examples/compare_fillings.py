import sys,os
#os.environ['OMP_NUM_THREADS']='12' # set number of OpenMP threads to run in parallel
#os.environ['MKL_NUM_THREADS']='6'  # set number of MKL threads to run in parallel

import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from mpl_toolkits.mplot3d.axes3d import Axes3D

from hubbard.hubbard import *
from hubbard.utilities import coherent_states, q_function_calc
from hubbard.plot import *

plt.rcParams["text.usetex"] = True
plt.rcParams.update({'font.size': 10})

###############################

if len(sys.argv) >= 3:
    M = int(sys.argv[1])
    bc = str(sys.argv[2])
else:
    M = 8
    bc = 'ring'

N_range = M-2, M, M+2

time_scale = 1 #7e-6 #[hbar/ER] = 7 micro seconds
ER = 1
r = 1/20
U = 1/ER
J = r*U
Omega = r**3*U

N_steps = 100

light_calc = False
scheme = 'parallel'

initial_states = 'x','y','z'
initial_states_coords = pi/2 * np.array(((1,0), (1,1), (0,0)))

phi_labels = fr'\frac{{\pi}}{{ {M//2} }}', '\pi'
phi_types = 'non_pi', 'pi'

phi = 2*pi/M
phi_label = phi_labels[0]
phi_type = phi_types[0]

cols = len(initial_states)
rows = len(N_range)

width = 7.067
figsize = width, width * rows/cols
aspect_ratio = figsize[0]/figsize[1] * rows / cols

fig, axs = plt.subplots(rows, cols, 
        figsize = figsize,
        constrained_layout=True,
        sharey = 'row'
        )

ls = 'solid', 'dashed', 'dotted', 'dashed', 'dashdot', 'dotted','-', '--', '-.', ':', 'None', ' ', ''
markers = 'None', 'None', 'None'

###############################
x_scale = 1#.4

def chi_calc(phi):
    J_SE = 4*J**2/(U*(U**2-Omega**2))*(U**2 - Omega**2*np.cos(phi/2)**2)
    J_SOC = Omega*(1-4*J**2/(U**2-Omega**2)*np.sin(phi/2)**2)
    E_phi = J_SE*(1-np.cos(phi))

    chi = J_SOC**2/(E_phi*(M-1))
    if phi != pi:
        chi /= 2

    return chi

chi = chi_calc(phi)

t_f = 1/chi
t_f_scaled = t_f * time_scale

model_label = ''
chi_labels = r'','', r''
if phi_type == 'pi':
    model_label = '$-\chi_\pi\hat{{S}}_x^2$'
elif phi_type == 'non_pi':
    model_label = r'$\frac{\chi_\phi}{2}\hat{{S}}_z^2$'
elif phi_type == 'TACT_2':
    model_label = r'$\frac{\chi_\pi}{3}\left(\hat{{S}}_z^2-\hat{{S}}_x^2\right)$'

H_labels = model_label, '$\hat{H}_{eff}$', '$\hat{H}_{FH}+\hat{H}_{SOC}$'

        
if light_calc:
    basis = 'Fermi-single-ocupancy'
else:
    basis = 'Fermi'

for k,N in enumerate(N_range):
    print('N:', N)
    f = Hubbard(N,M,basis,bc=bc)
    f.set_scheme(scheme)
    print('D:', f.D)
    f.change_frame('lab', phi=phi)
    if phi_type == 'pi':
        H0 = -chi*f.Sx**2
    elif phi_type == 'non_pi':
        H0 = chi*f.Sz**2

    H1 = f.H_eff(J,U,Omega)

    H2 = f.H_0(J) + f.H_int(U) + f.H_SOC(Omega)
    
    H = [H0, H1, H2] 
    wf0 = f.initial_state_fermi()

    print('z',expectation_value(wf0, f.Sz))
    print('y',expectation_value(wf0, f.Sy))
    print('x',expectation_value(wf0, f.Sx))
    print('var z:',expectation_value(wf0, f.Sz*f.Sz) - expectation_value(wf0, f.Sz)**2)
    print('var y:',expectation_value(wf0, f.Sy*f.Sy) - expectation_value(wf0, f.Sy)**2)
    print('var x:',expectation_value(wf0, f.Sx*f.Sx) - expectation_value(wf0, f.Sx)**2)
    
    for j in range(len(initial_states)):
        print('initial state:',initial_states[j])
        theta, psi = initial_states_coords[j]
        rot_z = expm_multiply_parallel(f.Sz, a=-1j*psi)
        rot_y = expm_multiply_parallel(f.Sy, a=-1j*theta)
        initial_state = rot_z.dot(rot_y.dot(wf0))

        for i in range(len(H_labels)):
            print('Hamiltonian:',H_labels[i])
            ax = axs[i,j]
            f.H = H[i]
            
            t, data = f.evolve(initial_state, t_f, num = N_steps)
    
            t = time_scale*t
            
            ax.plot(t, data[0,:], 
                    ls = ls[k], 
                    #marker = markers[k], 
                    label = f'N={N}')
    
            ax.set_xlim((0, t[-1]*x_scale))

tags = ['a','b','c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k']
for j in range(len(initial_states)):
    theta, psi = initial_states_coords[j]
    theta_label = '0' if theta == 0 else fr'\frac{{\pi}}{{ {pi/theta:n} }}' 
    psi_label = '0' if psi == 0 else fr'\frac{{\pi}}{{ {pi/psi:n} }}' 
    ini_label_bloch = fr'\left|\theta_0, \phi_0\right\rangle = \left|{theta_label},{psi_label} \right\rangle'
    for i in range(len(H_labels)):
        ax = axs[i,j]
        ax.set_ylim((0, 2.2))
        ax.legend(loc = 'lower right')
        if i == 0:
            ax.set_title(f'${ini_label_bloch}$')
        if j == 0:
            ax.set_ylabel(fr'$\xi^2$ ({H_labels[i]})')
        if i == rows-1:
            ax.set_xlabel('$t\ [s]$')
        ax.text(.05, .95, f'$({tags[cols*i+j]})$', transform=ax.transAxes, va='top')
        

filename = f'output/portraits/compare fillings M={M}, U={U}, J={J}, Omega={Omega}, phi ={phi/pi:.3f}pi, {bc}, tf={t_f}'
#fig.savefig(filename+'.png')
fig.savefig(filename+'.pdf')
plt.show()
