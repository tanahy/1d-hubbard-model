import sys

from matplotlib.gridspec import GridSpec

from hubbard.hubbard import *
from hubbard.utilities import * 

N = 4
M = N

beta = 1
alpha = beta/40
Omega = alpha

phi = 2*np.pi/N

xi = alpha**2/beta * 1 / (2*(N-1)*(1-np.cos(phi))) #According to eqs. (66) and (71) on Emilia's notes but a 4 is missing 

print('J_SE:', beta)
print('J_SOC:', alpha)
print('xi:', xi)

assert alpha/beta < 1

t_f = 1/(xi) 

psi = 0#np.pi/2
theta = np.pi/2
#bc = 'open'
#bc = 'periodic'
bc = 'ring'
frame = 'lab'

save = True

N_steps = 200
dt = t_f / N_steps
F = Hubbard(N, M, 'Fermi', bc = bc)
F.change_frame(frame, phi = phi)

ax_labels = [r'$\xi^2$']

H_1 = xi*(F.Sz**2)
H = [H_1,]
H_labels = ['$H_{OAT}$',]

F.change_frame(frame, phi = phi)
H_i = F.H_SE(beta) + F.H_SOC(alpha)
H += [H_i]
H_labels += [fr'$H_{{eff}}$']

fig, ax = plt.subplots(1, constrained_layout=True)
axs = [ax,]

lines = iter(['solid','dashed', 'dashdot']+12*['None'])
markers = iter(3*['None']+['+', '^', 's', 'o', '*', 'x','<','>','1','2','3','4'])

wf = F.initial_state_fermi()
wf = expm(-1j*F.Sy*theta).dot(wf)
wf = expm(-1j*F.Sz*psi).dot(wf)
assert calculate_ortho(wf, expm(-1j*F.Sy*np.pi).dot(wf))

for k in range(len(H)):
    wf2 = np.copy(wf)

    F.H = H[k]
    label = H_labels[k]
    print(label)
    ls = next(lines)
    marker = next(markers)
    
    U_dt = F.time_evolution(dt)
    t = 0
    t_list = []
    ssp_list = []
    si_list = [[] for i in range(9)]
    for i in range(N_steps):
        ssp = F.spin_squeezing(wf2, operator = 'S')
        ssp_list.append(ssp)
        t_list.append(t)
        t += dt
        wf2 = U_dt.dot(wf2)
   
    t_list = np.array(t_list)
    axs[-1].plot(t_list, ssp_list, ls = ls, marker = marker, label = label)

for i,ax in enumerate(axs):
    ax.legend()
    ax.set_xlabel('t')
    ax.set_ylabel(ax_labels[i])
axs[-1].set_ylim((0,1.5))
title = fr'$N={N}, M={M}, \alpha = {alpha}, \beta = {beta}, \phi={phi/np.pi:.2f}\pi$'

fig.suptitle(title+fr', $\|\Psi_0\rangle = \|\theta={theta/np.pi:.2f}\pi,\phi={psi/np.pi:.2f}\pi \rangle  $')
if save:
    title = title.replace("\\","").replace('$',"")
    main_title = 'Spin Squeezing' 
    title = (f'output/{main_title}, {title}, {frame} {bc},'
             + f'init_state=({theta/np.pi:.2f}pi,{psi/np.pi:.2f}pi)')
    plt.savefig(title+'.pdf')
plt.show()
