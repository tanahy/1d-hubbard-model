#!/usr/bin/env python
# coding: utf-8
import sys

from hubbard.hubbard import *
from hubbard.utilities import * 

N = M = 16
frac = 10#, 5
filling = .99
label = 0

if len(sys.argv) > 1:
    N = M   = int(sys.argv[1])
    frac    = float(sys.argv[2])
    filling = float(sys.argv[3])
    label   = sys.argv[4]

J = 1
U = 24.4*J
U = U, U, U#*.99
delta = 28*J
J_SE = 4*J**2/U[-1]
q0 = M-1
E_gap = 1-np.cos(np.pi/M*q0)
Omega = J_SE * E_gap / frac

psi = 0
theta = np.pi/2
bc = 'open'

N_steps = 100

scheme = 'parallel'

filename = f'output/evs_N={N}_frac={frac}_filling={filling}_label={label}'

j = np.arange(N)+1

q = np.arange(1, N)
beta = Omega * np.cos(np.pi/N*(j-.5)*q0)

Eq = J_SE*(np.cos(np.pi*q/N) - 1)
x, y = np.meshgrid(j,q)
p = np.cos(np.pi/N*(x-.5)*y)

cq = np.sqrt(2)/N*np.sum(p*beta, axis=1)
chi = 1/(N-1)*sum(abs(cq)**2/Eq)
t_f = np.pi/abs(chi)

b = Bose2cHubbard(M, M, bc = bc, basis = 'single')
total_A = []
total_A += b.A
for n in range(M-1, -1, -1):
    b.N = n
    b.basis()
    total_A += b.A
b.N = N
b.A = total_A
b.D = len(b.A)
b.T = {v: tag for tag, v in enumerate(b.A)}

b.set_scheme(scheme)
b.S_j_calc()
b.S_calc()
b.theta = theta
b.phi = psi

b.H = b.H_XXZ_inhomo(J, U, beta)
realization = evolve_sampling(b, filling, t_f, N_steps)
while True:
    N_real, t, evs = next(realization)
    np.savez(filename, N_realizations = N_real, t = t, evs = evs)
