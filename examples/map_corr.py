import sys
import os
import glob

import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
from hubbard.hubbard import *
from hubbard.utilities import *
from hubbard.plot import *

###############################
plot_by_omega = False
plot_limit = 7
limit_plotting = False
calc_full_evo = False
plot_domains = False
fourier_analysis = True

delta_1 = (1e-1,2e-1)
a, b = 3, 1
delta_2 = 2e-1

accepted_squeezing = 1#.8

N = 3
U = 1 
t_f = 20
if len(sys.argv) >= 3:
    print('Parameters introduced by command line')
    N = int(sys.argv[1])
    t_f = float(sys.argv[2])
    if len(sys.argv) >= 4:
        U = sys.argv[3]
###############################

#           1       2       3       4       5       6
colors = ['black','brown','red','orange','blue','green']
type_labels = ['I','II','III','IV','V','VI']

title = f'Map, N={N}, U={U}, tf={t_f}'
filename_pattern = 'output/'+title+'*.npz'

for filename in glob.iglob(filename_pattern):
    data = np.load(filename, allow_pickle=True)
    time = data['time']
    minima = data['ssp_min']
    t_min = data['t_min']
    parameters = data['parameters']
    evolution = data['evolution']
    Omega = data['Omega']
    Phi = data['phi']

    ssp_best = np.empty((len(Omega), len(Phi)))
    ssp_best[:] = np.nan
    t_best = np.empty((len(Omega), len(Phi)))
    t_best[:] = np.nan
    ranking = np.empty((len(Omega), len(Phi)))
    ranking[:] = np.nan

    Z = np.empty((len(evolution), len(time)))
    Z[:] = np.nan

    for i in range(len(minima)):
        if minima[i] < accepted_squeezing:
            print(i, parameters[i], t_min[i], minima[i])
            Z[i,:len(evolution[i])] = np.array(evolution[i])
            j,omega,u,phi = parameters[i]
            k = list(Omega).index(omega)
            p = list(Phi).index(phi)
            ssp_best[k, p] = minima[i] 
            t_best[k, p] = t_min[i]
            ranking[k, p] = i
    
    #rows =  if plot_domains else 4
    rows = 5
    cols = 4
    fig, axs = plt.subplots(rows,cols, sharex=True, sharey=True, figsize=(15,10))
    
    ticks           = [a*pi for a in [0,1/2, 1, 7/6,3/2, 2]]
    tick_labels     = ['0','$1/2\pi$','$\pi$',r'$\frac{7}{6}\pi$','$3/2\pi$','$2\pi$']
    
    for ax in axs:
        ax.set_yscale("log")
        ax.set_ylabel('$\Omega$')
        ax.set_ylim(Omega[0], Omega[-1])
        ax.axvline(pi,color='red')
        ax.axvline(7/6*pi,color='purple')
        ax.set_xticks(ticks)
        ax.set_xticklabels(tick_labels)

    fig.suptitle(f'U={U}')
    axs[0].set_title(r'$\xi_{best}$')
    X,Y = np.meshgrid(Phi,Omega)
    im = axs[0].pcolormesh(X,Y,ssp_best, vmin=0., vmax=1, alpha=1., shading='auto')
    #cont = axs[0].contour(X,Y,ssp_best, vmin=0., vmax=1)
    fig.colorbar(im, ax=axs[0])
    
    if len(Phi) < 30:
        texts = annotate_heatmap(Phi, Omega, im, ranking, valfmt="{x:.0f}")
    '''
    phi_splitted_by_omega = [[Phi_min[i] for i in range(len(Phi_min)) if Omega_min[i] == omega] for omega in set(Omega_min)]
    phi_min_gen = [find_critical_phi(phi,delta=2*(Phi[1]-Phi[0])) for phi in phi_splitted_by_omega]

    phi_min = []
    phi_labels = []
    i = 0
    for gen in phi_min_gen:
        for phi_i in gen:
            phi_min.append(phi_i)
            phi_labels.append(f'$\phi_{i}={phi_i/pi:.2f}\pi$')
            print(f'phi_{i}', phi_i)
            i += 1
    '''
    '''
    w, h = 50,40

    annotation_box(Phi, Omega, (.6, 1), (1.8,0.5), ranking, evolution, axs[1], width=w, height=h)
    annotation_box(Phi, Omega, (.4, 10), (1.8,30), ranking, evolution, axs[1], width=w, height=h)
    annotation_box(Phi, Omega, (3.66, 10), (4,1), ranking, evolution, ax1, width=w, height=h)
    annotation_box(Phi, Omega, (3.66, 100), (5.5,10), ranking, evolution, ax1, width=w, height=h)

    annotation_box(Phi, Omega, (2.6, 10), (2,1), ranking, evolution, ax2, width=w, height=h)
    annotation_box(Phi, Omega, (.6, 100), (1.7,50), ranking, evolution, ax2, width=w, height=h)
    annotation_box(Phi, Omega, (6, 80), (5,20), ranking, evolution, ax2, width=w, height=h)
    '''

    axs[1].set_xlabel('$\phi$')
    axs[1].set_title('$t_{best}$')
    im = axs[1].pcolormesh(X,Y,t_best,alpha=1., shading='auto')
    #cont = axs[1].contour(X,Y,t_best)
    if len(Phi) < 30:
        texts = annotate_heatmap(Phi, Omega, im, ranking, valfmt="{x:.0f}")
    fig.colorbar(im, ax=axs[1])

    if plot_domains:

        Omega_min   = parameters[:,1] 
        Phi_min     = parameters[:,3] 
        #phi_min_gen = find_critical_phi(Phi_min,delta=abs(Phi[1]-Phi[0]))
        #phi_min_gen = find_critical_phi_by_domains(Phi_min,Omega_min,delta=abs(Phi[1]-Phi[0]))
        phi_min_gen = find_domains_1(Phi,Omega,ssp_best,t_best/np.nanmax(t_best),ranking, delta = delta_1)
        #phi_min     = [i for i,ranks in phi_min_gen]
        phi_min, domains = [], []
        for i,(phi_i,ranks) in enumerate(phi_min_gen):
            if i >= 20:
                break
            phi_min.append(phi_i)
            domains.append(ranks)

        phi_labels  = [f'$\phi_{i}={phi_min[i]/pi:.2f}\pi$' for i in range(len(phi_min))]

        for ax in axs:
            if len(phi_min) < 5:
                for phi_i in phi_min:
                    ax.axvline(phi_i,color='orange')
                secax = ax.secondary_xaxis('top')
                secax.set_xticks(phi_min)
                secax.set_xticklabels(phi_labels)

        #phi_min_gen_2 = find_domains_2(Phi,Omega,ssp_best/t_best/np.nanmin(t_best),delta=5e-1)
        composite = a*ssp_best+b*t_best/np.nanmax(t_best)
        composite /= np.nanmax(composite)
        #composite = -abs(composite-.5)+.5
        #composite /= np.nanmax(composite)
        phi_min_gen_2 = find_domains_2(Phi,Omega,composite,delta=delta_2)
        domain_centers = []
        domains_2 = np.empty(t_best.shape)
        domains_2[:] = np.nan
        for j, (coords, mask) in enumerate(phi_min_gen_2):
            if j >= 30:
                break
            domain_centers.append(coords)
            domains_2[mask] = j

        colored_domains = np.empty(ranking.shape)
        colored_domains[:] = np.nan
        for i, domain in enumerate(domains):
            for val in domain:
                mask = ranking == val 
                colored_domains[mask] = i
        im = discrete_pcolormesh(X,Y,colored_domains, axs[cols], cmap='copper', alpha = 1.,shading='auto')
        texts = annotate_heatmap(Phi, Omega, im, colored_domains, valfmt="{x:.0f}", singular_values=True)
        
        centers = np.array(domain_centers)
        im = axs[cols+1].pcolormesh(X,Y,composite, cmap='copper',alpha = .5,shading='auto')
        cont = axs[cols+1].contour(X,Y,composite, cmap='copper',alpha = 1.)
        fig.colorbar(im, ax=axs[cols+1])
        im = discrete_pcolormesh(X,Y,domains_2, axs[cols+2], cmap='copper',alpha = 1.,shading='auto')
        axs[cols+2].scatter(centers[:,0],centers[:,1],c='red')
        for i,center in enumerate(domain_centers):
            axs[cols+2].text(center[0],center[1], i,
                            horizontalalignment="center", verticalalignment="center")

    if plot_by_omega:
        fig2, axs = plt.subplots(len(Omega),sharex=True, figsize=(10,10*len(Omega)))
        for i in range(len(minima)):
            omega = parameters[i][1]
            k = list(Omega).index(omega)
            if not limit_plotting or len(axs[k].lines) < plot_limit:
                if calc_full_evo:
                    print(f'Calculating evolution for #{i}')
                    x, y = evo_calc(N, parameters[i], time[len(evolution[i])-1], t_f=float(t_f))
                else:
                    x, y = time[:len(evolution[i])], evolution[i]
                axs[k].plot(x, y, label=f'i={i}')
        i = 0
        for ax in axs:
            omega = Omega[i]
            i += 1
            ax.set_ylabel(rf'$\xi(t, \Omega={omega:.1f})$')
            if len(ax.lines) > 0: 
                ax.set_ylim((0,min(1.5, max([max(line.get_ydata()) for line in ax.lines]))))
                if len(ax.lines) <= plot_limit:
                    ax.legend()
                else:
                    empty_line = ax.plot([],[],color='white', label='...')
                    lines = ax.lines[:plot_limit]+empty_line
                    labels = [line.get_label() for line in lines]
                    ax.legend(lines, labels)
        axs[-1].set_xlabel('time')
    
    if plot_domains:
        N_domains = len(domain_centers)
        #N_domains = len(domains)
        rows, columns = 2, N_domains // 2 + 1
        for j in range(2,10):
            if N_domains % j == 0:
                columns = N_domains // j
                rows = j
        fig4, axs = plt.subplots(rows,columns,sharex=True)
        axs = axs.flatten()
        #for i, domain in enumerate(domains):
        for i in range(N_domains):
            #domain = domains[i]
            domain = ranking[domains_2 == i]
            ax = axs[i]
            for rank in domain.flatten():
                rank = int(rank)
                if not limit_plotting or len(ax.lines) < plot_limit:
                    if calc_full_evo:
                        print(f'Calculating evolution for #{rank}')
                        x, y = evo_calc(N, parameters[rank], time[len(evolution[rank])-1], t_f=float(t_f))
                    else:
                        x, y = time[:len(evolution[rank])], evolution[rank]
                    ax.plot(x, y, label=f'i={rank}')
            ax.set_ylabel(f'Domain {i}')
            if len(ax.lines) > 0: 
                ax.set_ylim((0,min(1.5, max([max(line.get_ydata()) for line in ax.lines]))))
                if len(ax.lines) <= plot_limit:
                    ax.legend()
                else:
                    empty_line = ax.plot([],[],color='white', label='...')
                    lines = ax.lines[:plot_limit]+empty_line
                    labels = [line.get_label() for line in lines]
                    ax.legend(lines, labels)
        axs[-1].set_xlabel('time')
   
    '''
    filename = f'output/maps/Map, N={N}, U={U}, tf={t_f}'
    filename += f', logOmega={np.log10(Omega[0])} to {np.log10(Omega[-1])}'
    filename = filename+', full_evo' if calc_full_evo else filename
    filename += '.pdf'

    fig.savefig(filename)
    if plot_by_omega:
        fig2.savefig(filename.replace('Map','Map_examples'))
    fig3.savefig(filename.replace('Map','Map_classification'))
    '''
plt.show()
