from setuptools import setup
setup(name = 'hubbard',
      version = '0.1',
      description = 'Module for exact calculations of the 1D Hubbard models.',
      url = 'https://gitlab.com/tanahy/1d-hubbard-model',
      author = 'Tanausu Hernandez Yanes',
      author_email = 'hdez@ifpan.edu.pl',
      packages = ['hubbard'],
      zip_safe = False,
      )

